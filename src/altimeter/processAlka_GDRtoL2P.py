'processAlka_GDRtoL2P.py'

'process SARAL AltiKa Altimetry IGDR data to GlobWave L2P'
'requires: variablesL2P.py version 1.7'
'Ellis Ash, SatOC, e.ash@satoc.eu'
'version 1.0, 22/10/13'


import re
import fnmatch
import os
import time
import numpy
import netCDF4
import variablesL2P as vaL


version = '1.0'
calibs = {'slope_swh': 1.0, \
          'offs_swh': -0.0}   
errors = {'slope': 0.0, \
          'offs': 0.0}        


def setattribs(varobj, attnames, invals, nval):
    'loop to set variable attributes'
    count = 0
    while (count < nval):   # loop through attributes
        if invals[count]:   # invals is 0 if no attribute required for variable
            if attnames[count] == 'valid_min':  # needed to set 'valid_min' attribute to 0 (assigned -1)
                if invals[count] < 0:
                    invals[count] = numpy.array(0).astype(numpy.int8)
            setattr(varobj,attnames[count],invals[count])
        count += 1


def getflags(n, size=8, idlist=[0, 1, 2, 3, 4, 5, 6, 7]):
    'gives bit values of bytes'
    bStr = ''
    for bit in idlist:
        nshft = n >> (size - bit - 1)
        bStr = bStr + str(nshft % 2)
    return bStr


def setflagqual(ndata, datain):
    'set rejections flag and a flag quality variable'
    
    noflag = numpy.zeros(ndata).astype(int)
    base2 = 2**numpy.arange(32)

    hardware_0 = noflag
    attitude_1 = noflag
    alt_land_4 = (datain.variables['surface_type'][0:ndata] > 1).astype(int)
    altocean_5 = (datain.variables['surface_type'][0:ndata] > 0).astype(int)
    rad_land_6 = (datain.variables['rad_surf_type'][0:ndata] > 0).astype(int)
    alt_rice_7 = (datain.variables['ice_flag'][0:ndata] > 0).astype(int)
    rad_rain_8 = noflag
    qual_ssh_11 = (datain.variables['qual_alt_1hz_range'][0:ndata] > 0).astype(int)
    qual_swh_12 = (datain.variables['qual_alt_1hz_swh'][0:ndata] > 0).astype(int)
    qual_sg0_13 = (datain.variables['qual_alt_1hz_sig0'][0:ndata] > 0).astype(int)
    qual_orb_15 = noflag
    qualswh2_16 = noflag
    qualsg02_17 = noflag
    qualonwf_18 = (datain.variables['qual_alt_1hz_off_nadir_angle_wf'][0:ndata] > 0).astype(int)
    qualonpf_19 = noflag
    iceextdb_20 = noflag
    
    flgqual = alt_land_4 | rad_land_6 | qual_ssh_11 | qual_swh_12 | qual_sg0_13
    rainice = alt_rice_7

    flgvar = (base2[27]*alt_land_4) + \
             (base2[25]*rad_land_6) + \
             (base2[24]*alt_rice_7) + \
             (base2[23]*rad_rain_8) + \
             (base2[20]*qual_ssh_11) + \
             (base2[19]*qual_swh_12) + \
             (base2[18]*qual_sg0_13) + \
             (base2[13]*qualonwf_18) + \
             (base2[0]*flgqual)

    return flgvar, flgqual, rainice


def setswhqual(ndata, datain, flgqual, rainice):

    swh_numval_ku = datain.variables['swh_numval_ku'][0:ndata]
    swh_ku = datain.variables['swh_ku'][0:ndata]
    swh_rms_ku = datain.variables['swh_rms_ku'][0:ndata]
    sig0_rms_ku = datain.variables['sig0_rms_ku'][0:ndata]
    wind_speed_alt = datain.variables['wind_speed_alt'][0:ndata]
    off_nadir_angle_wf_ku = datain.variables['off_nadir_angle_wf_ku'][0:ndata]
    
    datqual = ((swh_numval_ku > 18) & (swh_numval_ku < 127)) & \
              (swh_ku < 32767) & \
              ((swh_rms_ku > 0) & (swh_rms_ku < 32767)) & \
              (sig0_rms_ku <= 100) & \
              (wind_speed_alt < 32767) & \
              ((off_nadir_angle_wf_ku >= -200) & (off_nadir_angle_wf_ku <= 2500))

    swhqual = 2 * ((datqual == 0) | (flgqual == 1)) # quality is 2 for bad (datqual gives True for good)

    swhgood = numpy.where(swhqual == 0)
    swhqual[swhgood] = rainice[swhgood]    # quality is 0 for good, 1 for good but rain probable

    return swhqual


def processalkagdr(fname, outdir):

    try:
        datain = netCDF4.Dataset(fname,'r')  # causes a runtime error if not available?
    except IOError, e:
        print 'file open error:', e
    else:
        print 'data file opened ok'

    # get size of time dimension
    for dimname, dimobj in datain.dimensions.iteritems():	# iterate through dictionary key/value pairs
        print dimname, len(dimobj)
            
    ndata = len(datain.dimensions['time'])
    print 'number data points:', ndata, '\n'

    # get global attributes
    for name in datain.ncattrs():
        print name, getattr(datain,name)
        if re.match('^xref', name):
            break

    # open nc file for output
    prefix = 'GW_L2P_ALT_ALKA_GDR_'
    outfile = outdir + prefix + fname[-39:-8] + '_' + fname[-48:-45] + '_' + fname[-43:-40] + '.nc'
    ncfile = netCDF4.Dataset(outfile, 'w', format='NETCDF3_CLASSIC')

    # create dimensions
    #ndata = 5   # overide full length for development
    ncfile.createDimension(vaL.dim1, ndata)

    # create and initialise variables and variable attributes
    varobjdict = {}
    varsset = vaL.varsL2P[0:7] + vaL.varsL2P[10:13] + vaL.varsL2P[16:18] + vaL.varsL2P[19:23] + vaL.varsL2P[24:25] + \
              vaL.varsL2P[26:27] + vaL.varsL2P[28:29] + vaL.varsL2P[32:33] + vaL.varsL2P[34:35] + \
              vaL.varsL2P[36:40] + vaL.varsL2P[41:42]
    for varname in varsset:
        fval = vaL.fvals[vaL.fvalis[varname]]
        varobj = ncfile.createVariable(varname,vaL.varsizes[varname],(vaL.dim1,), fill_value = fval)
        invals = [vaL.lnames1[varname], vaL.snames[varname], vaL.units[varname], vaL.sources[varname], vaL.insts[varname], \
                  vaL.calforms[varname], vaL.calrefs[varname], vaL.valrefs[varname], vaL.qflags[varname], vaL.vmins[varname], \
                  vaL.vmaxs[varname], vaL.flvals[varname], vaL.flmasks[varname], vaL.flmeans[varname], vaL.scalefs[varname], vaL.coords[varname], vaL.commJAS2[varname]]
        setattribs(varobj, vaL.attnames_nc4, invals, len(vaL.attnames_nc4))

        if varname == 'time':
            varobj.calendar = 'gregorian'    # not set from variablesL2P
        if varname == 'swh_calibrated':
            varobj.calibration_formula = str(calibs['slope_swh']) + '*swh + ' + str(calibs['offs_swh'])
        if varname == 'bathymetry':
            varobj.add_offset = 0    # not set from variablesL2P
            varobj.coordinates = 'lon lat'  # set here to retain order
        if varname == 'surface_air_pressure':
            varobj.add_offset = 100000    # not set from variablesL2P
            varobj.coordinates = 'lon lat'  # set here to retain order

        varobj[:] = fval
        varobjdict[varname] = varobj

    # create global attributes
    globattd = vaL.globattdict
    fnbits = re.split('/', fname)
    sourcefile = fnbits[-1]

    for ga in vaL.globatts:
        setattr(ncfile, ga, globattd[ga])

    ncfile.title = 'GlobWave L2P derived from AltiKa IGDR (SRL_IPN) Product'
    ncfile.history = time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime()) + ' UTC : Creation'
    ncfile.software_version = 'SatOC GlobWave AltiKa GDR to L2P Processor ' + version
    ncfile.source_provider = 'CNES'
    ncfile.source_name = sourcefile
    #ncfile.source_software = srcsoft
    ncfile.mission_name = getattr(datain,'mission_name')
    ncfile.altimeter_sensor_name = getattr(datain,'altimeter_sensor_name')
    ncfile.radiometer_sensor_name = getattr(datain,'radiometer_sensor_name')
    ncfile.acq_station_name = getattr(datain,'processing_center')
    ncfile.cycle_number = getattr(datain,'cycle_number')
    ncfile.pass_number = getattr(datain,'pass_number')
    ncfile.equator_crossing_time = re.sub(' ', 'T', getattr(datain,'equator_time')) + ' UTC'
    ncfile.equator_crossing_longitude = getattr(datain,'equator_longitude')
    ncfile.start_date = re.sub(' ', 'T', getattr(datain,'first_meas_time')) + ' UTC'
    ncfile.stop_date = re.sub(' ', 'T', getattr(datain,'last_meas_time')) + ' UTC'


    # transcribe or generate data

    for varname in vaL.varsL2P:
        alname = vaL.alnames[varname]
        if (alname):    # all the straight transcription of data
            varobj = varobjdict[varname]
            varobj[:] = datain.variables[alname][0:ndata]
            
        elif (varname == 'time'):
            alname = varname
            timediff = 5478*24*60*60    # AltiKa time is relative to 2000-01-01
            varobj = varobjdict[varname]
            varobj[:] = datain.variables[alname][0:ndata] + timediff
            
        elif (varname == 'lat'):
            alname = varname
            varobj = varobjdict[varname]
            varobj[:] = datain.variables[alname][0:ndata]/1.e6
            
        elif (varname == 'lon'):
            alname = varname
            varobj = varobjdict[varname]
            varobj[:] = datain.variables[alname][0:ndata]/1.e6
            
        elif (varname == 'rejection_flags'):
            varobj = varobjdict[varname]
            flgvar, flgqual, rainice = setflagqual(ndata, datain)
            varobj[:] = flgvar

    # close nc file
    ncfile.close()
