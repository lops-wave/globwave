'processGfo__GDRtoL2P.py'

'process GFO Altimetry GDR data to GlobWave L2P'
'requires: variablesL2P.py version 1.4'
'Ellis Ash, SatOC, e.ash@satoc.eu'
'version 1.0, 30/11/09'
'version 1.1, 23/12/09, removal redundant variables, add_offset attribute for bathymetry, surface_temperature -> surfce_air_temperature, change scaling logic, sigma0 quality as for swh'
'version 1.2, 18/01/10, update to regular expression for decimal equator longitude'
'version 1.3, 18/01/10, change of datarr data type to long to read large uint32 values'
'version 1.4, 18/01/10, update to regular expression when no data points, cycle and pass to 3 sig figs in filename'
'version 1.5, 25/01/10, update to only create output file if there are data points'
'version 1.6, 08/03/10, corrections to default values and range_rms scaling'
'version 1.7, 16/03/10, include add_offset for surface_air_pressure, update to variablesL2P version'
'version 1.8, 19/10/10, correction to calculation of swh_calibrated'


import re
import fnmatch
import os
import time
import struct
import numpy
import netCDF3
import variablesL2P as vaL


version = '1.8'
epochdiff = 5479*24*60*60  # system epoch is 1970-01-01, GFO epoch 1985-01-01
calibs = {'slope_swh': 1.0625, \
          'offs_swh': 0.0754}


def setattribs(varobj, attnames, invals, nval):
    'loop to set variable attributes'
    count = 0
    while (count < nval):   # loop through attributes
        if invals[count]:   # invals is 0 if no attribute required for variable
            if attnames[count] == 'valid_min':  # needed to set 'valid_min' attribute to 0 (assigned -1)
                if invals[count] < 0:
                    invals[count] = numpy.array(0).astype(numpy.int8)
            setattr(varobj,attnames[count],invals[count])
        count += 1
        

def getflags(n, size=8, idlist=[0, 1, 2, 3, 4, 5, 6, 7]):
    'gives bit values of bytes'
    bStr = ''
    for bit in idlist:
        nshft = n >> (size - bit - 1)
        bStr = bStr + str(nshft % 2)
    return bStr


def setflagqual(ndata, datarr):
    'set rejections flag and a flag quality variable'
    
    flgvar = numpy.zeros((int(ndata)), int) - 1   # array of -1 to take flags
    flgqual = numpy.zeros((int(ndata)), int) - 1   # array of -1 to take flag related quality
    rain = numpy.zeros((int(ndata)), int) - 1   # array of -1 to take rain flag
    spare1 = '0'
    spare10 = '0000000000'
    
    count = 0
    while (count < ndata):   # loop for all data points
        
        hardware_0 = '0'
        attitude_1 = getflags(datarr[count, 14], 32, [13,])
        alt_land_4 = str(int(getflags(datarr[count, 12], 16, [0, 1]) == '11'))
        altocean_5 = str(int(getflags(datarr[count, 12], 16, [0, 1]) > '00'))
        rad_land_6 = getflags(datarr[count, 15], 32, [20,])
        alt_rice_7 = '0'
        rad_rain_8 = getflags(datarr[count, 15], 32, [19,])
        qual_ssh_11 = getflags(datarr[count, 14], 32, [10,])
        qual_swh_12 = str(int(getflags(datarr[count, 14], 32, [12,])) | int(getflags(datarr[count, 14], 32, [21,])))
        qual_sg0_13 = str(int(getflags(datarr[count, 14], 32, [27,])) | int(getflags(datarr[count, 14], 32, [20,])) | int(getflags(datarr[count, 14], 32, [11,])))
        qual_orb_15 = getflags(datarr[count, 14], 32, [25,])
        qualswh2_16 = '0'
        qualsg02_17 = '0'
        qualonwf_18 = '0'
        qualonpf_19 = '0'
        iceextdb_20 = '0'
        
        flgstr = hardware_0 + attitude_1 + spare1 + spare1 + alt_land_4 + altocean_5 + rad_land_6 + alt_rice_7 + \
                 rad_rain_8 + spare1 + spare1 + qual_ssh_11 + qual_swh_12 + qual_sg0_13 + spare1 + qual_orb_15 + \
                 qualswh2_16 + qualsg02_17 + qualonwf_18 + qualonpf_19 + iceextdb_20 + spare10

        flgsnok = int(qual_ssh_11) | int(qual_swh_12) | int(qual_sg0_13) | int(qual_orb_15)
        flgqual[count] = flgsnok
        flgstr = flgstr + str(flgsnok)   # set bit 31 if rejected by other flags

        flgint = int(flgstr, 2)
        if int(flgstr, 2) >= 2**31:   # integer is signed
            flgint = int(int(flgstr, 2) - 2**32)
        flgvar[count] = flgint

        rain[count] = rad_rain_8 

        count += 1

    return flgvar, flgqual, rain


def setswhqual(ndata, datarr, flgqual, rain):

    nvals_swh = datarr[0:ndata, 13]
    sshu_std = datarr[0:ndata, 8]
    swh_std = datarr[0:ndata, 9]
    swh = datarr[0:ndata, 4]
    sigma0 = datarr[0:ndata, 5]
    agc_std = datarr[0:ndata, 10]
    
    datqual = (nvals_swh > 8) & \
              ((sshu_std > 0) & (sshu_std < 110)) & (swh_std > 0) & ((swh_std < 10) | (swh_std < (0.1* swh))) & \
              ((sigma0 > 0) & (sigma0 < 3000)) & \
              ((agc_std > 0) & (agc_std < 30))

    swhqual = 2 * ((datqual == 0) | (flgqual == 1)) # quality is 2 for bad (datqual gives True for good)

    swhgood = numpy.where(swhqual == 0)
    swhqual[swhgood] = rain[swhgood]    # quality is 0 for good, 1 for good but rain probable

    return swhqual


def processgfo_gdr(fname, outdir):
    
    try:
        datain = open(fname,'r')
    except IOError, e:
        print 'file open error:', e
    else:
        print 'data file opened ok'

    # read header and extract information
    lnum = 0
    header = ''
    while (lnum < 20):  # GDR_HEADER 20 lines
       header = header + datain.readline()
       lnum += 1

    atts = ('CYCLE_NUMBER', 'PASS_NUMBER', 'NUMBER_GDR_RECORDS')
    for label in atts:
        patn = '(' + label + ' = +)(\d+)(;|\.\d+)'
        m = re.search(patn, header)
        if label == 'CYCLE_NUMBER':
            cycle = m.group(2)
            print 'CYCLE_NUMBER: ' + cycle
            cycle = "%03d" % int(cycle)
        if label == 'PASS_NUMBER':
            passn = m.group(2)
            print 'PASS_NUMBER: ' + passn
            passn = "%03d" % int(passn)
        if label == 'NUMBER_GDR_RECORDS':
            ndata = int(m.group(2))
            
    atts = ('PASS_BEGIN_TIME', 'SOFTWARE_VERSION', 'PASS_END_TIME')
    for label in atts:
        patn = '(' + label + ' = +)(\d*\.\d+)'
        m = re.search(patn, header)
        if label == 'PASS_BEGIN_TIME':
            tfp = m.group(2)
            tfp = float(tfp) + epochdiff
            tfp = time.gmtime(tfp)
            ftime = time.strftime("%Y%m%d_%H%M%S", tfp)
            fmt = time.strftime("%Y-%m-%dT%H:%M:%S", tfp)
        if label == 'SOFTWARE_VERSION':
            srcsoft = m.group(2)        
        if label == 'PASS_END_TIME':
            tlp = m.group(2) 
            tlp = float(tlp) + epochdiff
            tlp = time.gmtime(tlp)
            ltime = time.strftime("%Y%m%d_%H%M%S", tlp)
            lmt = time.strftime("%Y-%m-%dT%H:%M:%S", tlp)

    atts = ('EQ_CROSSING_TIME_LON',)
    for label in atts:
        patn = '(' + label + ' = )(\d*\.\d+)( +)(\d*\.\d+)'
        m = re.search(patn, header)
        if label == 'EQ_CROSSING_TIME_LON':
            eqt = m.group(2)
            eqt = float(eqt) + epochdiff
            eqt = time.gmtime(eqt)
            eqtime = time.strftime("%Y-%m-%dT%H:%M:%S", eqt)
            eqlon = m.group(4)

    if (ndata):   # there are data points

        # read input data records
        count = 0
        datarr = numpy.zeros((ndata,16), long) - 1   # array of -1 to take data
        while True:
            record = datain.read(184)  # GDR_DATA_RECORD length
            if record:
                sec, musec, lat, lon, swh, sig0, wind_sp, bathy, range_std, swh_std, agc_std, \
                     sq_off_nad, flags, swh_numval, qual1, qual2 \
                      = struct.unpack('>IIii 16x HHH 20x h 12x HHH 10x hh 3x b 72x II 8x', record)
                datarr[count,:] = [sec, musec, lat, lon, swh, sig0, wind_sp, bathy, range_std, swh_std, agc_std, \
                                   sq_off_nad, flags, swh_numval, qual1, qual2]
                count += 1
            else:
                break

        datain.close()

        # open nc file for output
        prefix = 'GW_L2P_ALT_GFO__GDR_'
        outfile = outdir + prefix + ftime + '_' + ltime + '_' + cycle + '_' + passn + '.nc'
        ncfile = netCDF3.Dataset(outfile, 'w', format='NETCDF3_CLASSIC')

        # create dimensions
        ncfile.createDimension(vaL.dim1, ndata)

        # create and initialise variables and variable attributes
        varobjdict = {}
        varsset = vaL.varsL2P[0:7] + vaL.varsL2P[10:13] + vaL.varsL2P[16:18] + vaL.varsL2P[19:23] + \
                  vaL.varsL2P[24:25] + vaL.varsL2P[26:27] + vaL.varsL2P[32:33] + vaL.varsL2P[34:35] + \
                  vaL.varsL2P[36:42]
        for varname in varsset:
            varobj = ncfile.createVariable(varname,vaL.varsizes[varname],(vaL.dim1,))
            fval = vaL.fvals[vaL.fvalis[varname]]
            invals = [fval, vaL.lnames1[varname], vaL.snames[varname], vaL.units[varname], vaL.sources[varname], vaL.insts[varname], \
                      vaL.calforms[varname], vaL.calrefs[varname], vaL.valrefs[varname], vaL.qflags[varname], vaL.vmins[varname], \
                      vaL.vmaxs[varname], vaL.flvals[varname], vaL.flmasks[varname], vaL.flmeans[varname], vaL.scalefs[varname], vaL.coords[varname], vaL.commGFO[varname]]
            setattribs(varobj, vaL.attnames, invals, len(vaL.attnames))

            if varname == 'time':
                varobj.calendar = 'gregorian'    # not set from variablesL2P
            if varname == 'swh_calibrated':
                varobj.calibration_formula = str(calibs['slope_swh']) + '*swh + ' + str(calibs['offs_swh'])
            if varname == 'bathymetry':
                varobj.add_offset = 0    # not set from variablesL2P
                varobj.coordinates = 'lon lat'  # set here to retain order
            if varname == 'surface_air_pressure':
                varobj.add_offset = 100000    # not set from variablesL2P
                varobj.coordinates = 'lon lat'  # set here to retain order    

            varobj[:] = fval
            varobjdict[varname] = varobj

        # create global attributes

        globattd = vaL.globattdict
        fnbits = re.split('/', fname)
        sourcefile = fnbits[-1]

        for ga in vaL.globatts:
            setattr(ncfile, ga, globattd[ga])

        ncfile.title = 'GlobWave L2P derived from GFO GDR Product'
        ncfile.history = time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime()) + ' UTC : Creation'
        ncfile.software_version = 'SatOC GlobWave GFO GDR to L2P Processor ' + version
        ncfile.source_provider = 'NOAA'
        ncfile.source_name = sourcefile
        ncfile.source_software = srcsoft
        ncfile.mission_name = 'Geosat Follow-On (GFO)'
        ncfile.cycle_number = cycle
        ncfile.pass_number = passn
        ncfile.equator_crossing_time = eqtime + ' UTC'
        ncfile.equator_crossing_longitude = eqlon
        ncfile.start_date = fmt + ' UTC'
        ncfile.stop_date = lmt + ' UTC'


        # transcribe or generate data

        sel = numpy.where(datarr == 65535)  # change unsigned short default to signed
        datarr[sel] = 2**15 - 1

        # time:
        varobj = varobjdict['time']
        varobj[:] = (datarr[0:ndata,0]) + (datarr[0:ndata,1]/1000000.)

        # lat:
        varobj = varobjdict['lat']
        varobj[:] = datarr[0:ndata,2]/1.e6

        # lon:
        varobj = varobjdict['lon']
        varobj[:] = datarr[0:ndata,3]/1.e6

        # swh:
        varobj = varobjdict['swh']
        swharr = datarr[0:ndata,4].copy()
        sel = numpy.where(swharr < 32767)
        swharr[sel] = swharr[sel] * 10   # Scale factor adjusted for L2P, 1e-2 to 1e-3
        varobj[sel] = swharr[sel]

        # swh_calibrated:
        varobj = varobjdict['swh_calibrated']
        varobj[sel] = swharr[sel]*calibs['slope_swh'] + calibs['offs_swh']/vaL.scalefs['swh']
        swhcal = varobj[sel]
        selovf = numpy.where(swhcal > 32767)
        selneg = numpy.where(swhcal < 0)
        swhcal[selovf] = 32767
        swhcal[selneg] = 0
        varobj[sel] = swhcal

        # swh_quality:
        varobj = varobjdict['swh_quality']
        flgvar, flgqual, rain = setflagqual(ndata, datarr)
        swhqualarr = setswhqual(ndata, datarr, flgqual, rain)
        varobj[:] = swhqualarr

        # swh_standard_error:
        varobj = varobjdict['swh_standard_error']

        # swh_2nd:
        #varobj = varobjdict['swh_2nd']

        # swh_2nd_calibrated:
        #varobj = varobjdict['swh_2nd_calibrated']

        # swh_2nd_quality:
        #varobj = varobjdict['swh_2nd_quality']

        # sigma0:
        varobj = varobjdict['sigma0']
        varobj[:] = datarr[0:ndata,5]

        # sigma0_calibrated:
        varobj = varobjdict['sigma0_calibrated']

        # sigma0_quality:
        varobj = varobjdict['sigma0_quality']
        varobj[:] = swhqualarr

        # sigma0_2nd:
        #varobj = varobjdict['sigma0_2nd']

        # sigma0_2nd_calibrated:
        #varobj = varobjdict['sigma0_2nd_calibrated']

        # sigma0_2nd_quality:
        #varobj = varobjdict['sigma0_2nd_quality']

        # wind_speed_alt:
        varobj = varobjdict['wind_speed_alt']
        varobj[:] = datarr[0:ndata,6]

        # wind_speed_alt_calibrated:
        varobj = varobjdict['wind_speed_alt_calibrated']

        # wind_speed_rad:
        #varobj = varobjdict['wind_speed_rad']

        # wind_speed_model_u:
        varobj = varobjdict['wind_speed_model_u']
        #varobj[:] = ancillaryL2P.get_model_u(time, lat, lon)

        # wind_speed_model_v:
        varobj = varobjdict['wind_speed_model_v']

        # rejection_flags:
        varobj = varobjdict['rejection_flags']
        varobj[:] = flgvar

        # swh_rms:
        varobj = varobjdict['swh_rms']
        swhrmsarr = datarr[0:ndata,9]
        sel = numpy.where(swhrmsarr < 32767)
        varobj[sel] = swhrmsarr[sel] * 10   # Scale factor adjusted for L2P, 1e-2 to 1e-3

        # swh_rms_2nd:
        #varobj = varobjdict['swh_rms_2nd']

        # swh_num_valid:
        varobj = varobjdict['swh_num_valid']
        varobj[:] = datarr[0:ndata,12]

        # swh_num_valid_2nd:
        #varobj = varobjdict['swh_num_valid_2nd']

        # sigma0_rms:
        varobj = varobjdict['sigma0_rms']
        varobj[:] = datarr[0:ndata,10]  # AGC_Std for GFO

        # sigma0_rms_2nd:
        #varobj = varobjdict['sigma0_rms_2nd']

        # sigma0_num_valid:
        #varobj = varobjdict['sigma0_num_valid']

        # sigma0_num_valid_2nd:
        #varobj = varobjdict['sigma0_num_valid_2nd']

        # peakiness:
        #varobj = varobjdict['peakiness']

        # peakiness_2nd:
        #varobj = varobjdict['peakiness_2nd']

        # off_nadir_angle_wf:
        varobj = varobjdict['off_nadir_angle_wf']
        varobj[:] = datarr[0:ndata,11]

        # off_nadir_angle_pf:
        #varobj = varobjdict['off_nadir_angle_pf']

        # range_rms:
        varobj = varobjdict['range_rms']
        rangerms = datarr[0:ndata,8]
        sel = numpy.where(rangerms < 32767)
        varobj[sel] = rangerms[sel] * 10    # Scale factor adjusted for L2P, 1e-3 to 1e-4

        # range_rms_2nd:
        #varobj = varobjdict['range_rms_2nd']

        # bathymetry:
        varobj = varobjdict['bathymetry']
        varobj[:] = datarr[0:ndata,7]

        # distance_to_coast:
        varobj = varobjdict['distance_to_coast']

        # sea_surface_temperature
        varobj = varobjdict['sea_surface_temperature']

        # surface_air_temperature
        varobj = varobjdict['surface_air_temperature']

        # surface_air_humidity
        varobj = varobjdict['surface_air_humidity']

        # surface_air_pressure
        varobj = varobjdict['surface_air_pressure']

        # close nc file
        ncfile.close()

    else:

        datain.close()
