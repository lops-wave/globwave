'processCryo_GDRtoL2P.py'

'process CryoSat-2 Altimetry IGDR data to GlobWave L2P'
'requires: variablesL2P.py version 1.9'
'Ellis Ash, SatOC, e.ash@satoc.eu'
'version 1.0, 08/03/12'
'version 1.1, 12/03/14, correction to sigma0 and wind scaling, addition of calibration and quality values'
'version 1.2, 11/04/14, corrections for use of netCDF4 module'
'version 1.3, 28/04/14, adjustment to quality logic for negative swh'


import re
import fnmatch
import os
import time
import numpy
import netCDF4
import variablesL2P as vaL


version = '1.3'
calibs = {'limit_swh': 2.45, \
          'slope_swh': 1.0058, \
          'offs_swh': -0.1057, \
          'a0_swh': 0.4889, 'a1_swh': 0.4712, 'a2_swh': 0.1546, 'a3_swh': -0.0145}
errors = {'slope': 0.0, \
          'offs': 0.0}        


def setattribs(varobj, attnames, invals, nval):
    'loop to set variable attributes'
    count = 0
    while (count < nval):   # loop through attributes
        if invals[count]:   # invals is 0 if no attribute required for variable
            if attnames[count] == 'valid_min':  # needed to set 'valid_min' attribute to 0 (assigned -1)
                if invals[count] < 0:
                    invals[count] = numpy.array(0).astype(numpy.int8)
            if attnames[count] != '_FillValue':
                setattr(varobj,attnames[count],invals[count])
        count += 1


def getflags(n, size=16, idlist=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]):
    'gives bit values of bytes'
    bStr = ''
    for bit in idlist:
        nshft = n >> (size - bit - 1)
        bStr = bStr + str(nshft % 2)
    return bStr


def setflagqual(ndata, flgarr):
    'set rejections flag and a flag quality variable'
    
    flgvar = numpy.zeros((int(ndata)), int) - 1   # array of -1 to take flags
    flgqual = numpy.zeros((int(ndata)), int) - 1   # array of -1 to take flag related quality
    rainice = numpy.zeros((int(ndata)), int) - 1   # array of -1 to take rain or ice flag
    noflag = '0'
    spare1 = '0'
    spare10 = '0000000000'
    
    count = 0
    while (count < ndata):   # loop for all data points
        
        hardware_0 = noflag
        attitude_1 = noflag
        alt_land_4 = getflags(flgarr[count], 16, [11,])
        altocean_5 = getflags(flgarr[count], 16, [10,])
        rad_land_6 = noflag
        alt_rice_7 = getflags(flgarr[count], 16, [13,])
        rad_rain_8 = noflag
        qual_ssh_11 = getflags(flgarr[count], 16, [4,])
        qual_swh_12 = getflags(flgarr[count], 16, [3,])
        qual_sg0_13 = getflags(flgarr[count], 16, [2,])
        qual_orb_15 = noflag
        qualswh2_16 = noflag
        qualsg02_17 = noflag
        qualonwf_18 = noflag
        qualonpf_19 = noflag
        iceextdb_20 = noflag
       
        flgstr = hardware_0 + attitude_1 + spare1 + spare1 + alt_land_4 + altocean_5 + rad_land_6 + alt_rice_7 + \
                 rad_rain_8 + spare1 + spare1 + qual_ssh_11 + qual_swh_12 + qual_sg0_13 + spare1 + qual_orb_15 + \
                 qualswh2_16 + qualsg02_17 + qualonwf_18 + qualonpf_19 + iceextdb_20 + spare10

        flgsnok = int(altocean_5) or int(rad_land_6) or int(qual_ssh_11) or int(qual_swh_12) or int(qual_sg0_13)
        flgqual[count] = flgsnok
        flgstr = flgstr + str(flgsnok)   # set bit 31 if rejected by other flags

        flgint = int(flgstr, 2)
        if int(flgstr, 2) >= 2**31:   # integer is signed
            flgint = int(int(flgstr, 2) - 2**32)
        flgvar[count] = flgint

        rainice[count] = int(alt_rice_7) or int(rad_rain_8)

        count += 1

    return flgvar, flgqual, rainice


def setswhqual(ndata, datain, flgqual, rainice):

    swh_ku = datain.variables['swh_ku'][0:ndata]
    swh_rms_ku = datain.variables['swh_rms_ku'][0:ndata]
    swhrmslim = {-0.9: 0.2040, -0.8: 0.4266, -0.7: 0.6196, -0.6: 0.6968, -0.5: 0.7830, \
        -0.4: 0.8537, -0.3: 0.9201, -0.2: 0.9831, -0.1: 1.0384, 0: 1.0888, 0.1: 1.1408, \
        0.2: 1.1822, 0.3: 1.2095, 0.4: 1.2304, 0.5: 1.2361, 0.6: 1.2293, 0.7: 1.2071, \
        0.8: 1.1867, 0.9: 1.1458, 1.0: 1.1038, 1.1: 1.0538, 1.2: 0.9887, 1.3: 0.9264, \
        1.4: 0.8730, 1.5: 0.8294, 1.6: 0.7961, 1.7: 0.7744, 1.8: 0.7603, 1.9: 0.7518, \
        2.0: 0.7500, 2.1: 0.7540, 2.2: 0.7591, 2.3: 0.7712, 2.4: 0.7826, 2.5: 0.7881, \
        2.6: 0.8025, 2.7: 0.8140, 2.8: 0.8300, 2.9: 0.8407, 3.0: 0.8526, 3.1: 0.8652, \
        3.2: 0.8790, 3.3: 0.8944, 3.4: 0.9065, 3.5: 0.9151, 3.6: 0.9320, 3.7: 0.9397, \
        3.8: 0.9530, 3.9: 0.9634, 4.0: 0.9744, 4.1: 0.9936, 4.2: 1.0161, 4.3: 1.0245, \
        4.4: 1.0529, 4.5: 1.0689, 4.6: 1.0936, 4.7: 1.1096, 4.8: 1.1090, 4.9: 1.1346, \
        5.0: 1.1627, 5.1: 1.1838, 5.2: 1.2056, 5.3: 1.2281, 5.4: 1.2513, 5.5: 1.2753, \
        5.6: 1.3001, 5.7: 1.3257, 5.8: 1.3520, 5.9: 1.3793, 6.0: 1.4074, 6.1: 1.4364, \
        6.2: 1.4663, 6.3: 1.4971, 6.4: 1.5289, 6.5: 1.5617, 6.6: 1.5954, 6.7: 1.6302, \
        6.8: 1.6660, 6.9: 1.7029, 7.0: 1.7409, 7.1: 1.7800, 7.2: 1.8203, 7.3: 1.8616, \
        7.4: 1.9042, 7.5: 1.9480, 7.6: 1.9930, 7.7: 2.0392, 7.8: 2.0867, 7.9: 2.1355}

    datqual = numpy.ones(ndata)

    count = 0
    while (count < ndata):   # loop for all data points
        if (swh_ku[count] < 8.):
            swhkey = int(swh_ku[count]*10)/10.
            if (swhkey < -0.9): swhkey = -0.9
            if (swh_rms_ku[count] > swhrmslim[swhkey]):
                datqual[count] = 0
        count += 1
    sel8p = numpy.where(swh_ku > 8.)
    datqual[sel8p] = (swh_rms_ku[sel8p] < 2.160)

    swhqual = 2 * ((datqual == 0) | (flgqual == 1)) # quality is 2 for bad (datqual gives True for good)

    swhgood = numpy.where(swhqual == 0)
    swhqual[swhgood] = rainice[swhgood]    # quality is 0 for good, 1 for good but rain probable

    return swhqual


def processcryogdr(fname, outdir):
    
    try:
        datain = netCDF4.Dataset(fname,'r')  # causes a runtime error if not available?
    except IOError, e:
        print 'file open error:', e
    else:
        print 'data file opened ok'

    # get size of time dimension
    #for dimname, dimobj in datain.dimensions.iteritems():	# iterate through dictionary key/value pairs
    #    print dimname, len(dimobj)
            
    ndata = len(datain.dimensions['time'])
    print 'number data points:', ndata, '\n'

    # get global attributes
    for name in datain.ncattrs():
        #print name, getattr(datain,name)
        if re.match('^log', name):
            break
        
    cycle = getattr(datain,'cycle_number')
    cycle = "%03d" % int(cycle)
    passn = getattr(datain,'pass_number')
    passn = "%03d" % int(passn)
    sfmtime = getattr(datain,'first_meas_time')
    tfp = time.strptime(sfmtime[0:19], "%Y-%m-%d %H:%M:%S")
    ftime = time.strftime("%Y%m%d_%H%M%S", tfp)
    slmtime = getattr(datain,'last_meas_time')
    tlp = time.strptime(slmtime[0:19], "%Y-%m-%d %H:%M:%S")
    ltime = time.strftime("%Y%m%d_%H%M%S", tlp)

    # open nc file for output
    prefix = 'GW_L2P_ALT_CRYO_GDR_'
    outfile = outdir +'/' + prefix + ftime + '_' + ltime + '_' + cycle + '_' + passn + '.nc'
    ncfile = netCDF4.Dataset(outfile, 'w', format='NETCDF3_CLASSIC')
    print "outfile :",outfile

    # create dimensions
    ncfile.createDimension(vaL.dim1, ndata)

    # create and initialise variables and variable attributes
    varobjdict = {}
    varsset = vaL.varsL2P[0:7] + vaL.varsL2P[10:13] + vaL.varsL2P[16:18] + vaL.varsL2P[19:23] + vaL.varsL2P[24:25] + \
              vaL.varsL2P[26:27] + vaL.varsL2P[33:35] + vaL.varsL2P[36:40] + vaL.varsL2P[41:42]
    for varname in varsset:
        fval = vaL.fvals[vaL.fvalis[varname]]
        varobj = ncfile.createVariable(varname,vaL.varsizes[varname],(vaL.dim1,), fill_value=fval)
        invals = [vaL.lnames1[varname], vaL.snames[varname], vaL.units[varname], vaL.sources[varname], vaL.insts[varname], \
                  vaL.calforms[varname], vaL.calrefs[varname], vaL.valrefs[varname], vaL.qflags[varname], vaL.vmins[varname], \
                  vaL.vmaxs[varname], vaL.flvals[varname], vaL.flmasks[varname], vaL.flmeans[varname], vaL.scalefs[varname], vaL.coords[varname], vaL.commJAS2[varname]]
        setattribs(varobj, vaL.attnames_nc4, invals, len(vaL.attnames_nc4))

        if varname == 'time':
            varobj.calendar = 'gregorian'    # not set from variablesL2P
        if varname == 'swh_calibrated':
            varobj.calibration_formula = str(calibs['slope_swh']) + '*swh + ' + str(calibs['offs_swh'])
        if varname == 'off_nadir_angle_pf':
            varobj.add_offset = 0.0161    # not set from variablesL2P
            varobj.coordinates = 'lon lat'  # set here to retain order
        if varname == 'bathymetry':
            varobj.add_offset = 0    # not set from variablesL2P
            varobj.coordinates = 'lon lat'  # set here to retain order
        if varname == 'surface_air_pressure':
            varobj.add_offset = 100000    # not set from variablesL2P
            varobj.coordinates = 'lon lat'  # set here to retain order

        varobj[:] = fval
        varobjdict[varname] = varobj

    # create global attributes
    globattd = vaL.globattdict
    fnbits = re.split('/', fname)
    sourcefile = fnbits[-1]

    for ga in vaL.globatts:
        setattr(ncfile, ga, globattd[ga])

    ncfile.title = 'GlobWave L2P derived from CRYOSAT IGDR Product'
    ncfile.history = time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime()) + ' UTC : Creation'
    ncfile.software_version = 'SatOC GlobWave Cryosat IGDR to L2P Processor ' + version
    ncfile.source_provider = 'NOAA'
    ncfile.source_name = sourcefile
    #ncfile.source_software = srcsoft
    ncfile.mission_name = getattr(datain,'mission_name')
    #ncfile.altimeter_sensor_name = getattr(datain,'altimeter_sensor_name')
    #ncfile.radiometer_sensor_name = getattr(datain,'radiometer_sensor_name')
    #ncfile.acq_station_name = getattr(datain,'acq_station_name')
    ncfile.cycle_number = cycle
    ncfile.pass_number = passn
    ncfile.equator_crossing_time = re.sub(' ', 'T', getattr(datain,'equator_time')) + ' UTC'
    ncfile.equator_crossing_longitude = getattr(datain,'equator_longitude')
    ncfile.start_date = re.sub(' ', 'T', getattr(datain,'first_meas_time')) + ' UTC'
    ncfile.stop_date = re.sub(' ', 'T', getattr(datain,'last_meas_time')) + ' UTC'


    # transcribe or generate data
    for varname in vaL.varsL2P:
        c2name = vaL.c2names[varname]
        if (c2name):    # all the straight transcription of data
            varobj = varobjdict[varname]
            if c2name in datain.variables.keys():
                varobj[:] = datain.variables[c2name][0:ndata]
                
        elif (varname == 'lat'):
            c2name = varname
            # set auto conversion to False 20121130 CPR correct bug in value manip. 
            datain.variables[varname].set_auto_maskandscale(False)
            varobj = varobjdict[varname]
            varobj[:] = datain.variables[c2name][0:ndata]/1.e7
            
        elif (varname == 'lon'):
            c2name = varname
            # set auto conversion to False 20121130 CPR correct bug in value manip.
            datain.variables[varname].set_auto_maskandscale(False)
            varobj = varobjdict[varname]
            varobj[:] = datain.variables[c2name][0:ndata]/1.e7
            
        elif (varname == 'swh_calibrated'):
            c2name = 'swh_ku'
            varobj = varobjdict[varname]
            swharr = datain.variables[c2name][0:ndata]
            sel = swharr.nonzero()
            varobj[sel] = swharr[sel]*calibs['slope_swh'] + calibs['offs_swh']
            sel = numpy.where(swharr <= calibs['limit_swh'])  # cubic calibration part
            if len(sel[0]) > 0:
                varobj[sel] = ( (swharr[sel])**3*calibs['a3_swh'] + \
                              (swharr[sel])**2*calibs['a2_swh'] + \
                              (swharr[sel])*calibs['a1_swh'] + calibs['a0_swh'] )
            swhcal = varobj[0:ndata]
            selovf = numpy.where(swhcal > 32.767)
            selneg = numpy.where(swhcal < 0)
            if len(selovf[0]) > 0: swhcal[selovf] = 32.767
            if len(selneg[0]) > 0: swhcal[selneg] = 0
            varobj[0:ndata] = swhcal

        elif (varname == 'swh_quality'):
            varobj = varobjdict[varname]
            c2name = 'flags'
            flgarr = datain.variables[c2name][0:ndata]
            flgvar, flgqual, rainice = setflagqual(ndata, flgarr)
            swhqualarr = setswhqual(ndata, datain, flgqual, rainice)
            varobj[:] = swhqualarr
            
        elif (varname == 'sigma0_quality'):
            varobj = varobjdict[varname]
            varobj[:] = swhqualarr
            
        elif (varname == 'rejection_flags'):
            varobj = varobjdict[varname]
            varobj[:] = flgvar

        elif (varname == 'off_nadir_angle_pf'):
            varobj = varobjdict[varname]
            pitch = datain.variables['attitude_pitch'][0:ndata]
            roll = datain.variables['attitude_roll'][0:ndata]
            selp = numpy.where(pitch < 32.767)
            selr = numpy.where(roll < 32.767)
            sel = selp and selr
            onapf = (pitch[sel]**2 + roll[sel]**2)
            varobj[sel] = onapf

    # close nc file
    ncfile.close()
    datain.close()
