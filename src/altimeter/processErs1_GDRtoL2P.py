'processErs1_GDRtoL2P.py'

'process ERS-1 Altimetry GDR data to GlobWave L2P'
'requires: variablesL2P.py version 1.4'
'Ellis Ash, SatOC, e.ash@satoc.eu'
'version 1.0, 22/12/09'
'version 1.1, 10/01/10, correction to cycle and pass calculation'
'version 1.2, 04/03/10, corrections to range_rms scaling and default values'
'version 1.3, 11/03/09, include add_offset for surface_air_pressure, update to variablesL2P version'


import re
import fnmatch
import os
import time
import struct
import numpy
import netCDF3
import variablesL2P as vaL


version = '1.3'
epochdiff = 7305*24*60*60  # system epoch is 1970-01-01, ERS1 epoch 1990-01-01
calibs = {'slope_swh': 1.1259, \
          'offs_swh': 0.1854}


def setattribs(varobj, attnames, invals, nval):
    'loop to set variable attributes'
    count = 0
    while (count < nval):   # loop through attributes
        if invals[count]:   # invals is 0 if no attribute required for variable
            if attnames[count] == 'valid_min':  # needed to set 'valid_min' attribute to 0 (assigned -1)
                if invals[count] < 0:
                    invals[count] = numpy.array(0).astype(numpy.int8)
            setattr(varobj,attnames[count],invals[count])
        count += 1


def getflags(n, size=8, idlist=[0, 1, 2, 3, 4, 5, 6, 7]):
    'gives bit values of bytes'
    bStr = ''
    for bit in idlist:
        nshft = n >> (size - bit - 1)
        bStr = bStr + str(nshft % 2)
    return bStr


def setflagqual(ndata, datarr):
    'set rejections flag and a flag quality variable'
    
    flgvar = numpy.zeros((int(ndata)), int) - 1   # array of -1 to take flags
    flgqual = numpy.zeros((int(ndata)), int) - 1   # array of -1 to take flag related quality
    spare1 = '0'
    spare10 = '0000000000'
    
    count = 0
    while (count < ndata):   # loop for all data points
        
        hardware_0 = '0'
        attitude_1 = '0'
        alt_land_4 = str(int(getflags(datarr[count, 0], 32, [1, 2, 3]) == '010'))
        altocean_5 = getflags(datarr[count, 0], 32, [0,])
        rad_land_6 = getflags(datarr[count, 0], 32, [20,])
        alt_rice_7 = '0'
        rad_rain_8 = '0'
        qual_ssh_11 = getflags(datarr[count, 0], 32, [4,])
        qual_swh_12 = getflags(datarr[count, 0], 32, [7,])
        qual_sg0_13 = getflags(datarr[count, 0], 32, [8,])
        qual_orb_15 = getflags(datarr[count, 0], 32, [23,])
        qualswh2_16 = '0'
        qualsg02_17 = '0'
        qualonwf_18 = '0'
        qualonpf_19 = '0'
        iceextdb_20 = '0'
        
        flgstr = hardware_0 + attitude_1 + spare1 + spare1 + alt_land_4 + altocean_5 + rad_land_6 + alt_rice_7 + \
                 rad_rain_8 + spare1 + spare1 + qual_ssh_11 + qual_swh_12 + qual_sg0_13 + spare1 + qual_orb_15 + \
                 qualswh2_16 + qualsg02_17 + qualonwf_18 + qualonpf_19 + iceextdb_20 + spare10

        flgsnok = int(altocean_5) or int(qual_swh_12) or int(qual_sg0_13)
        flgqual[count] = flgsnok
        flgstr = flgstr + str(flgsnok)   # set bit 31 if rejected by other flags

        flgint = int(flgstr, 2)
        if int(flgstr, 2) >= 2**31:   # integer is signed
            flgint = int(int(flgstr, 2) - 2**32)
        flgvar[count] = flgint

        count += 1

    return flgvar, flgqual  # no rain flag in ERS1


def setswhqual(ndata, datarr, flgqual):

    nval = datarr[0:ndata, 5]
    std_h_alt = datarr[0:ndata, 6]
    swh = datarr[0:ndata, 8]
    std_swh = datarr[0:ndata, 7]
    std_sigma0 = datarr[0:ndata, 9]
    
    datqual = (nval > 16) & \
              (std_h_alt < 400) & \
              ((swh > 0) & (swh < 3000)) & \
              (std_swh < 200) & \
              ((std_sigma0 > 0) & (std_sigma0 < 30))

    swhqual = 2 * ((datqual == 0) | (flgqual == 1)) # quality is 2 for bad (datqual gives True for good)

    return swhqual


def processers1gdr(fname, outdir):
        
    try:
        datain = open(fname,'r')
    except IOError, e:
        print 'file open error:', e
    else:
        print 'data file opened ok'

    # read header and extract information
    header = datain.read(3960) # PASS_FILE_HEADER size 3960
#    print header
    atts = ('Pass_Nbmes', )
    for label in atts:
        patn = '(' + label + ' = )(\d+)(;|\.\d+)'
        m = re.search(patn, header)
        if label == 'Pass_Nbmes':
            ndata = int(m.group(2))
            

    # read input data records
    count = 0
    datarr = numpy.zeros((int(ndata),13), int) - 1   # array of -1 to take data
    while True:
        record = datain.read(180)  # OPR_pass_file_data_record length
        if record:
            flags, sec, musec, lat, lon, nval, range_std, swh_std, swh, sig0_std, sig0, wind_sp, sq_off_nad \
                  = struct.unpack('>4x iiiiii 4x i 94x hh 4x hh 6x h 18x i 8x', record)
            datarr[count,:] = [flags, sec, musec, lat, lon, nval, range_std, swh_std, swh, \
                               sig0_std, sig0, wind_sp, sq_off_nad]
            count += 1
            if count == 1:
                tfp = sec
        else:
            tlp = sec
            break

    datain.close()

    # open nc file for output
    prefix = 'GW_L2P_ALT_ERS1_GDR_'
    #outfile = 'test.nc'

    tfp = tfp + epochdiff
    tfp = time.gmtime(tfp)
    ftime = time.strftime("%Y%m%d_%H%M%S", tfp)
    fmt = time.strftime("%Y-%m-%dT%H:%M:%S", tfp)
    tlp = tlp + epochdiff
    tlp = time.gmtime(tlp)
    ltime = time.strftime("%Y%m%d_%H%M%S", tlp)
    lmt = time.strftime("%Y-%m-%dT%H:%M:%S", tlp)

    patn = '(1A)(\d+)(A|D)'   # extract abs orbit from filename
    m = re.search(patn, fname)
    orbit = int(m.group(2))
    ifasc = int(m.group(3) == 'A')
    if (orbit < 3753):      # phases A and B
        cycle = (orbit - 141)/43
        passn = 2 * ((orbit - 141)%43 + 1) - ifasc
    elif (orbit < 4154):
        cycle = 83
        passn = 2 * ((orbit + 355)%501 + 1) - ifasc
    elif (orbit < 12671):   # phase C
        cycle = (orbit + 37930)/501
        passn = 2 * ((orbit + 37930)%501 + 1) - ifasc
    elif (orbit < 12755):   # phase C
        cycle = 101
        passn = 2 * ((orbit - 71)%84 + 1) - ifasc
    elif (orbit < 14302):   # phase D
        cycle = (orbit - 8342)/43
        passn = 2 * ((orbit - 8342)%43 + 1) - ifasc
    elif (orbit < 14922):   # phase E
        cycle = 139
        passn = 2 * ((orbit - 456)%2411 + 1) - ifasc
    elif (orbit < 16747):   # phase E
        cycle = 140
        passn = 2 * ((orbit - 322)%1825 + 1) - ifasc
    elif (orbit < 19213):   # phase F
        cycle = (orbit + 325560)/2411
        passn = 2 * ((orbit + 325560)%2411 + 1) - ifasc
    elif (orbit < 19248):   # phase F
        cycle = 143
        passn = 2 * ((orbit - 33)%35 + 1) - ifasc
    else:                   # phase G
        cycle = (orbit + 53117)/501
        passn = 2 * ((orbit + 53117)%501 + 1) - ifasc

    cycle = "%03d" % cycle
    passn = "%04d" % passn

    outfile = outdir + prefix + ftime + '_' + ltime + '_' + cycle + '_' + passn + '.nc'
    ncfile = netCDF3.Dataset(outfile, 'w', format='NETCDF3_CLASSIC')

    # create dimensions
    ncfile.createDimension(vaL.dim1, ndata)

    # create and initialise variables and variable attributes
    varobjdict = {}
    varsset = vaL.varsL2P[0:7] + vaL.varsL2P[10:13] + vaL.varsL2P[16:18] + vaL.varsL2P[19:23] + \
              vaL.varsL2P[24:25] + vaL.varsL2P[26:27] + vaL.varsL2P[32:33] + vaL.varsL2P[34:35] + \
              vaL.varsL2P[36:42]
    for varname in varsset:
        varobj = ncfile.createVariable(varname,vaL.varsizes[varname],(vaL.dim1,))
        fval = vaL.fvals[vaL.fvalis[varname]]
        invals = [fval, vaL.lnames1[varname], vaL.snames[varname], vaL.units[varname], vaL.sources[varname], vaL.insts[varname], \
                  vaL.calforms[varname], vaL.calrefs[varname], vaL.valrefs[varname], vaL.qflags[varname], vaL.vmins[varname], \
                  vaL.vmaxs[varname], vaL.flvals[varname], vaL.flmasks[varname], vaL.flmeans[varname], vaL.scalefs[varname], vaL.coords[varname], vaL.commERS2[varname]]
        setattribs(varobj, vaL.attnames, invals, len(vaL.attnames))

        if varname == 'time':
            varobj.calendar = 'gregorian'    # not set from variablesL2P
        if varname == 'swh_calibrated':
            varobj.calibration_formula = str(calibs['slope_swh']) + '*swh + ' + str(calibs['offs_swh'])
        if varname == 'bathymetry':
            varobj.add_offset = 0    # not set from variablesL2P
            varobj.coordinates = 'lon lat'  # set here to retain order
        if varname == 'surface_air_pressure':
            varobj.add_offset = 100000    # not set from variablesL2P
            varobj.coordinates = 'lon lat'  # set here to retain order

        varobj[:] = fval
        varobjdict[varname] = varobj

    # create global attributes

    globattd = vaL.globattdict
    fnbits = re.split('/', fname)
    sourcefile = fnbits[-1]

    for ga in vaL.globatts:
        setattr(ncfile, ga, globattd[ga])
        
    ncfile.title = 'GlobWave L2P derived from ERS-1 GDR Product'
    ncfile.history = time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime()) + ' UTC : Creation'
    ncfile.software_version = 'SatOC GlobWave ERS-1 GDR to L2P Processor ' + version
    ncfile.source_provider = 'European Space Agency'
    ncfile.source_name = sourcefile
    ncfile.mission_name = 'ERS-1'
    ncfile.altimeter_sensor_name = 'RA'
    ncfile.cycle_number = cycle
    ncfile.pass_number = passn
    ncfile.start_date = fmt + ' UTC'
    ncfile.stop_date = lmt + ' UTC'


    # transcribe or generate data

    # time:
    timediff = 1826*24*60*60    # ERS2 time is relative to 1990-01-01
    varobj = varobjdict['time']
    varobj[:] = (datarr[0:ndata,1]) + (datarr[0:ndata,2]/1000000.) + timediff

    # lat:
    varobj = varobjdict['lat']
    varobj[:] = datarr[0:ndata,3]/1.e6

    # lon:
    varobj = varobjdict['lon']
    varobj[:] = datarr[0:ndata,4]/1.e6

    # swh:
    varobj = varobjdict['swh']
    swharr = datarr[0:ndata,8].copy()
    sel = numpy.where(swharr < 32767)
    swharr[sel] = swharr[sel] * 10  # Scale factor adjusted for L2P, 1e-2 to 1e-3
    varobj[sel] = swharr[sel]

    # swh_calibrated:
    varobj = varobjdict['swh_calibrated']
    varobj[sel] = swharr[sel]*calibs['slope_swh'] + calibs['offs_swh']/vaL.scalefs['swh']

    # swh_quality:
    varobj = varobjdict['swh_quality']
    flgvar, flgqual = setflagqual(ndata, datarr)
    swhqualarr = setswhqual(ndata, datarr, flgqual)
    varobj[:] = swhqualarr

    # swh_standard_error:
    varobj = varobjdict['swh_standard_error']

    # swh_2nd:
    #varobj = varobjdict['swh_2nd']

    # swh_2nd_calibrated:
    #varobj = varobjdict['swh_2nd_calibrated']

    # swh_2nd_quality:
    #varobj = varobjdict['swh_2nd_quality']

    # sigma0:
    varobj = varobjdict['sigma0']
    varobj[:] = datarr[0:ndata,10]

    # sigma0_calibrated:
    varobj = varobjdict['sigma0_calibrated']

    # sigma0_quality:
    varobj = varobjdict['sigma0_quality']
    varobj[:] = swhqualarr

    # sigma0_2nd:
    #varobj = varobjdict['sigma0_2nd']

    # sigma0_2nd_calibrated:
    #varobj = varobjdict['sigma0_2nd_calibrated']

    # sigma0_2nd_quality:
    #varobj = varobjdict['sigma0_2nd_quality']

    # wind_speed_alt:
    varobj = varobjdict['wind_speed_alt']
    varobj[:] = datarr[0:ndata,11]

    # wind_speed_alt_calibrated:
    varobj = varobjdict['wind_speed_alt_calibrated']

    # wind_speed_rad:
    #varobj = varobjdict['wind_speed_rad']

    # wind_speed_model_u:
    varobj = varobjdict['wind_speed_model_u']

    # wind_speed_model_v:
    varobj = varobjdict['wind_speed_model_v']

    # rejection_flags:
    varobj = varobjdict['rejection_flags']
    varobj[:] = flgvar

    # swh_rms:
    varobj = varobjdict['swh_rms']
    swhrmsarr = datarr[0:ndata,7]
    sel = numpy.where(swhrmsarr < 32767)
    varobj[sel] = swhrmsarr[sel] * 10  # Scale factor adjusted for L2P, 1e-2 to 1e-3

    # swh_rms_2nd:
    #varobj = varobjdict['swh_rms_2nd']

    # swh_num_valid:
    varobj = varobjdict['swh_num_valid']
    swhnumval = datarr[0:ndata,5]
    sel = numpy.where(swhnumval < 127)
    varobj[sel] = swhnumval[sel]

    # swh_num_valid_2nd:
    #varobj = varobjdict['swh_num_valid_2nd']

    # sigma0_rms:
    varobj = varobjdict['sigma0_rms']
    varobj[:] = datarr[0:ndata,9]

    # sigma0_rms_2nd:
    #varobj = varobjdict['sigma0_rms_2nd']

    # sigma0_num_valid:
    #varobj = varobjdict['sigma0_num_valid']

    # sigma0_num_valid_2nd:
    #varobj = varobjdict['sigma0_num_valid_2nd']

    # peakiness:
    #varobj = varobjdict['peakiness']

    # peakiness_2nd:
    #varobj = varobjdict['peakiness_2nd']

    # off_nadir_angle_wf:
    varobj = varobjdict['off_nadir_angle_wf']
    offnadwf = datarr[0:ndata,12]
    sel = numpy.where(offnadwf < 32767)
    varobj[sel] = offnadwf[sel] / 100    # Scale factor adjusted for L2P, 1e-6 to 1e-4

    # off_nadir_angle_pf:
    #varobj = varobjdict['off_nadir_angle_pf']

    # range_rms:
    varobj = varobjdict['range_rms']
    rangerms = datarr[0:ndata,6]
    sel = numpy.where(rangerms < 32767)
    varobj[sel] = rangerms[sel] * 10    # Scale factor adjusted for L2P, 1e-3 to 1e-4

    # range_rms_2nd:
    #varobj = varobjdict['range_rms_2nd']

    # bathymetry:
    varobj = varobjdict['bathymetry']

    # distance_to_coast:
    varobj = varobjdict['distance_to_coast']

    # sea_surface_temperature
    varobj = varobjdict['sea_surface_temperature']

    # surface_air_temperature
    varobj = varobjdict['surface_air_temperature']

    # surface_air_humidity
    varobj = varobjdict['surface_air_humidity']

    # surface_air_pressure
    varobj = varobjdict['surface_air_pressure']

    # close nc file
    ncfile.close()
