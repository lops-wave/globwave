'processEnvi_NRTtoL2P.py'

'process Envisat Altimetry GDR data to GlobWave L2P'
'requires: variablesL2P.py version 1.4'
'Ellis Ash, SatOC, e.ash@satoc.eu'
'version 0.1, 02/12/09, no quality information'
'version 0.2, 02/12/09, update to calibration values and reference'
'version 1.0, 08/02/10, added quality calculation, variables update in line with GDR version'
'version 1.1, 17/08/10, corrections to default values, included add_offset for bathymetry and surface_air_pressure'
'version 1.2, 21/10/10, update to calibration logic to set overflow values to default and -ve values to zero'


import re
import fnmatch
import os
import time
import struct
import numpy
import netCDF3
import variablesL2P as vaL


version = '1.2'
calibs = {'slope_swh': 1.093, \
          'offs_swh': -0.233}


def setattribs(varobj, attnames, invals, nval, varname):
    'loop to set variable attributes'
    count = 0
    while (count < nval):   # loop through attributes
        if invals[count]:   # invals is 0 if no attribute required for variable
            if attnames[count] == 'valid_min':  # needed to set 'valid_min' attribute to 0 (assigned -1)
                if invals[count] < 0:
                    invals[count] = numpy.array(0).astype(numpy.int8)
            if attnames[count] == 'flag_meanings': # needed for C->S band substitution
                invals[count] = re.sub('C', 'S', str(vaL.flmeans[varname]))
            setattr(varobj,attnames[count],invals[count])
        count += 1


def getflags(n, size=8, idlist=[0, 1, 2, 3, 4, 5, 6, 7]):
    'gives bit values of bytes'
    bStr = ''
    for bit in idlist:
        nshft = n >> (size - bit - 1)
        bStr = bStr + str(nshft % 2)
    return bStr


def setflagqual(ndata, flgarr):
    'set rejections flag and a flag quality variable'
    
    flgvar = numpy.zeros((int(ndata)), int) - 1   # array of -1 to take flags
    flgqual = numpy.zeros((int(ndata)), int) - 1   # array of -1 to take flag related quality
    rain = numpy.zeros((int(ndata)), int) - 1   # array of -1 to take alt_surface_type = 1
    spare1 = '0'
    spare10 = '0000000000'
    
    count = 0
    while (count < ndata):   # loop for all data points
        
        hardware_0 = getflags(flgarr[count, 1], 32, [28,])
        attitude_1 = '0'
        alt_land_4 = str(int(flgarr[count, 5] == 3))
        altocean_5 = str(int(flgarr[count, 5] > 0))
        rad_land_6 = str(int(flgarr[count, 6] == 1))
        alt_rice_7 = str(int(flgarr[count, 5] == 2))
        rad_rain_8 = str(int(flgarr[count, 7] != 0))
        qual_ssh_11 = getflags(flgarr[count, 1], 32, [15,])
        qual_swh_12 = '0'
        qual_sg0_13 = getflags(flgarr[count, 1], 32, [27,])
        qual_orb_15 = getflags(flgarr[count, 1], 32, [4,])
        qualswh2_16 = '0'
        qualsg02_17 = '0'
        qualonwf_18 = '0'
        qualonpf_19 = '0'
        iceextdb_20 = '0'
        
        flgstr = hardware_0 + attitude_1 + spare1 + spare1 + alt_land_4 + altocean_5 + rad_land_6 + alt_rice_7 + \
                 rad_rain_8 + spare1 + spare1 + qual_ssh_11 + qual_swh_12 + qual_sg0_13 + spare1 + qual_orb_15 + \
                 qualswh2_16 + qualsg02_17 + qualonwf_18 + qualonpf_19 + iceextdb_20 + spare10

        flgsnok = int(alt_land_4) or int(alt_rice_7) or int(qual_ssh_11)
        flgqual[count] = flgsnok
        flgstr = flgstr + str(flgsnok)   # set bit 31 if rejected by other flags

        flgint = int(flgstr, 2)
        if int(flgstr, 2) >= 2**31:   # integer is signed
            flgint = int(int(flgstr, 2) - 2**32)
        flgvar[count] = flgint

        rain[count] = int(rad_rain_8)
        
        count += 1

    return flgvar, flgqual, rain


def setswhqual(ndata, datarr, flgqual, rain):

    range_rms_ku = datarr[0:ndata, 5]
    swh_numval_ku = datarr[0:ndata, 11]
    swh_ku = datarr[0:ndata, 7]
    swh_rms_ku = datarr[0:ndata, 9]
    sig0_ku = datarr[0:ndata, 13]
    sig0_rms_ku = datarr[0:ndata, 15]
    wind_speed_alt = datarr[0:ndata, 22]
    ku_peak = datarr[0:ndata, 25]
    off_nad_angle_wvf = datarr[0:ndata, 20]
    
    datqual = (range_rms_ku < 200) & \
              ((swh_numval_ku > 18) & (swh_numval_ku < 255)) & \
              ((swh_ku > 0) & (swh_ku < 32767)) & \
              ((swh_rms_ku > 0) & (swh_rms_ku < 32767)) & \
              (swh_rms_ku < (845.7 - (0.050 * swh_ku) + (0.0000384 * swh_ku**2))) & \
              ((sig0_ku > 0) & (sig0_ku < 32767)) & \
              ((sig0_rms_ku > 0) & (sig0_rms_ku < 300)) & \
              (wind_speed_alt < 32767) & \
              ((ku_peak > 1500) & (ku_peak < 1800)) & \
              (abs(off_nad_angle_wvf) < 1000)

    swhqual = 2 * ((datqual == 0) | (flgqual == 1)) # quality is 2 for bad (datqual gives True for good)

    swhgood = numpy.where(swhqual == 0)
    swhqual[swhgood] = rain[swhgood]    # quality is 0 for good, 1 for good but rain True

    return swhqual


def processenvinrt(fname, outdir):
    
    try:
        datain = open(fname,'r')
    except IOError, e:
        print 'file open error:', e
    else:
        print 'data file opened ok'

    # read header and extract information
    mpheader = datain.read(1247) # PASS_FILE_HEADER size 3960
    #print mpheader
    spheader = datain.read(2618)
    #print spheader
    dsdra2 = datain.read(280)
    #print dsdra2
    dsdmwr = datain.read(280)
    dsdrest = datain.read(14000)

    atts = ('ACQUISITION_STATION', 'SOFTWARE_VER')
    for label in atts:
        patn = '(' + label + '=\")(.*?)( *\")'
        m = re.search(patn, mpheader)
        if label == 'ACQUISITION_STATION':
            acqst = m.group(2)
        if label == 'SOFTWARE_VER':
            srcsoft = m.group(2)

    atts = ('CYCLE', 'REL_ORBIT')
    for label in atts:
        patn = '(' + label + '=\+)(\d+)'
        m = re.search(patn, mpheader)
        if label == 'CYCLE':
            cycle = m.group(2)
            print 'CYCLE:' + cycle
        if label == 'REL_ORBIT':
            orbit = m.group(2)
            orbit = "%03d" % int(orbit)
            print 'ORBIT:' + orbit
            
    atts = ('RA2_FIRST_RECORD_TIME', 'RA2_LAST_RECORD_TIME')
    for label in atts:
        patn = '(' + label + '=\")(\d+-\w+-\d+)( )(\d+:\d+:\d+)(\.\d+)'
        m = re.search(patn, spheader)
        if label == 'RA2_FIRST_RECORD_TIME':
            strtime = m.group(2) + ' ' + m.group(4)
            tfp = time.strptime(strtime, "%d-%b-%Y %H:%M:%S")
            ftime = time.strftime("%Y%m%d_%H%M%S", tfp)
            fmt = time.strftime("%Y-%m-%dT%H:%M:%S", tfp)
        if label == 'RA2_LAST_RECORD_TIME':
            strtime = m.group(2) + ' ' + m.group(4)
            tlp = time.strptime(strtime, "%d-%b-%Y %H:%M:%S")
            ltime = time.strftime("%Y%m%d_%H%M%S", tlp)
            lmt = time.strftime("%Y-%m-%dT%H:%M:%S", tlp)

    atts = ('NUM_DSR', )
    for label in atts:
        patn = '(' + label + '=\+)(\d+)'
        m = re.search(patn, dsdra2)
        if label == 'NUM_DSR':
            ndata = int(m.group(2))

    # read input data records
    count = 0
    datarr = numpy.zeros((int(ndata),27), int) - 1   # array of -1 to take data
    flgarr = numpy.zeros((int(ndata),8), long) - 1   # array of -1 to take flags
    while (count < int(ndata)):
        record = datain.read(356)  # RA2_WWV record length
        if record:
            days, secs, musec, lat, lon, range_std, range_std_s, swh, swh_s, swh_std, swh_std_s, \
                  swh_numval, swh_numval_s, sig0, sig0_s, sig0_std, sig0_std_s, sig0_numval, sig0_numval_s, \
                  off_nadir_pf, off_nadir_wf, bathy, windsp_alt, windsp_mod_u, windsp_mod_v, peakiness, peakiness_s \
                  = struct.unpack('>iII 4x ii 32x HH 56x hhhhHH hhhhHH 52x hh 8x i 20x hhh 86x HH 32x', record)
    # b, h, i decode 1, 2, 4 byte integers respectively, capitals are UNSIGNED, nx ignores n bytes
            datarr[count,:] = [days, secs, musec, lat, lon, range_std, range_std_s, swh, swh_s, swh_std, swh_std_s, \
                               swh_numval, swh_numval_s, sig0, sig0_s, sig0_std, sig0_std_s, sig0_numval, sig0_numval_s, \
                               off_nadir_pf, off_nadir_wf, bathy, windsp_alt, windsp_mod_u, windsp_mod_v, peakiness, peakiness_s]
            qual, mcd, instr, fault, mwr_instr, alt_land, rad_land, rain \
                  = struct.unpack('>12x b 19x I 232x I 3x h 3x 32x H 30x HH 2x H 4x', record)
            flgarr[count,:] = [qual, mcd, instr, fault, mwr_instr, alt_land, rad_land, rain]
            count += 1

    datain.close()

    # open nc file for output
    prefix = 'GW_L2P_ALT_ENVI_NRT_'
    #outfile = 'test.nc'

    #outfile = outdir + prefix + ftime + '_' + ltime + '_' + cycle + '_' + orbit + '.nc'
    #modif FKO
    outFileName = prefix + ftime + '_' + ltime + '_' + cycle + '_' + orbit + '.nc'
    outfile = os.path.join( outdir, outFileName )
    ncfile = netCDF3.Dataset(outfile, 'w', format='NETCDF3_CLASSIC')

    # create dimensions
    ncfile.createDimension(vaL.dim1, ndata)

    # create and initialise variables and variable attributes
    varobjdict = {}
    varsset = vaL.varsL2P[0:18] + vaL.varsL2P[19:42]
    for varname in varsset:
        varobj = ncfile.createVariable(varname,vaL.varsizes[varname],(vaL.dim1,))
        fval = vaL.fvals[vaL.fvalis[varname]]
        invals = [fval, re.sub('C', 'S', vaL.lnames1[varname]), vaL.snames[varname], vaL.units[varname], vaL.sources[varname], vaL.insts[varname], \
                  vaL.calforms[varname], vaL.calrefs[varname], vaL.valrefs[varname], vaL.qflags[varname], vaL.vmins[varname], \
                  vaL.vmaxs[varname], vaL.flvals[varname], vaL.flmasks[varname], vaL.flmeans[varname], vaL.scalefs[varname], vaL.coords[varname], vaL.commENVI[varname]]
        setattribs(varobj, vaL.attnames, invals, len(vaL.attnames), varname)

        if varname == 'time':
            varobj.calendar = 'gregorian'    # not set from variablesL2P
        if varname == 'swh_calibrated':
            varobj.calibration_formula = str(calibs['slope_swh']) + '*swh + ' + str(calibs['offs_swh'])
	    varobj.calibration_reference = 'Durrant T H, Greenslade D J M & Simmonds I, 2009, Validation of Jason-1 and Envisat remotely sensed wave heights, J. Atmos. Oce. Tech. 26, 123-134'
        if varname == 'bathymetry':
            varobj.add_offset = 0    # not set from variablesL2P
            varobj.coordinates = 'lon lat'  # set here to retain order
        if varname == 'surface_air_pressure':
            varobj.add_offset = 100000    # not set from variablesL2P
            varobj.coordinates = 'lon lat'  # set here to retain order

        varobj[:] = fval
        varobjdict[varname] = varobj

    # create global attributes

    globattd = vaL.globattdict
    fnbits = re.split('/', fname)
    sourcefile = fnbits[-1]

    for ga in vaL.globatts:
        setattr(ncfile, ga, globattd[ga])

    ncfile.title = 'GlobWave L2P derived from Envisat NRT (WWV) Product'
    ncfile.history = time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime()) + ' UTC : Creation'
    ncfile.software_version = 'SatOC GlobWave Envisat NRT to L2P Processor ' + version
    ncfile.source_provider = 'European Space Agency'
    ncfile.source_name = sourcefile
    ncfile.source_software = srcsoft
    ncfile.mission_name = 'Envisat'
    ncfile.altimeter_sensor_name = 'RA2'
    ncfile.acq_station_name = acqst
    ncfile.cycle_number = cycle
    ncfile.pass_number = orbit   
    ncfile.start_date = fmt + ' UTC'
    ncfile.stop_date = lmt + ' UTC'


    # transcribe or generate data

    # time:
    timediff = 5478*24*60*60    # Envisat time is relative to 2000-01-01
    varobj = varobjdict['time']
    varobj[:] = (datarr[0:ndata,0]*24*60*60) + (datarr[0:ndata,1]) + (datarr[0:ndata,2]/1000000.) + timediff

    # lat:
    varobj = varobjdict['lat']
    varobj[:] = datarr[0:ndata,3]/1.e6

    # lon:
    varobj = varobjdict['lon']
    lonarr = datarr[0:ndata,4]/1.e6
    lonarr[numpy.where(lonarr < 0)] += 360
    varobj[:] = lonarr

    # swh:
    varobj = varobjdict['swh']
    swharr = datarr[0:ndata,7]
    varobj[:] = swharr

    # swh_calibrated:
    varobj = varobjdict['swh_calibrated']
    sel = numpy.where(swharr < 32767)
    if len(sel[0]) > 0:
        swhcal = swharr[sel]*calibs['slope_swh'] + calibs['offs_swh']/vaL.scalefs['swh']
        selovf = numpy.where(swhcal > 32767)
        selneg = numpy.where(swhcal < 0)
        swhcal[selovf] = 32767
        swhcal[selneg] = 0
        varobj[sel] = swhcal

    # swh_quality:
    varobj = varobjdict['swh_quality']
    flgvar, flgqual, rain = setflagqual(ndata, flgarr)
    swhqualarr = setswhqual(ndata, datarr, flgqual, rain)
    varobj[:] = swhqualarr

    # swh_standard_error:
    varobj = varobjdict['swh_standard_error']

    # swh_2nd:
    varobj = varobjdict['swh_2nd']
    varobj[:] = datarr[0:ndata,8]

    # swh_2nd_calibrated:
    varobj = varobjdict['swh_2nd_calibrated']

    # swh_2nd_quality:
    varobj = varobjdict['swh_2nd_quality']

    # sigma0:
    varobj = varobjdict['sigma0']
    varobj[:] = datarr[0:ndata,13]

    # sigma0_calibrated:
    varobj = varobjdict['sigma0_calibrated']

    # sigma0_quality:
    varobj = varobjdict['sigma0_quality']
    varobj[:] = swhqualarr

    # sigma0_2nd:
    varobj = varobjdict['sigma0_2nd']
    varobj[:] = datarr[0:ndata,14]

    # sigma0_2nd_calibrated:
    varobj = varobjdict['sigma0_2nd_calibrated']

    # sigma0_2nd_quality:
    varobj = varobjdict['sigma0_2nd_quality']

    # wind_speed_alt:
    varobj = varobjdict['wind_speed_alt']
    u10arr = datarr[0:ndata,22]
    sel = numpy.where(u10arr < 32767)
    varobj[sel] = u10arr[sel] / 10 # Scale factor adjusted for L2P, 1e-3 to 1e-2

    # wind_speed_alt_calibrated:
    varobj = varobjdict['wind_speed_alt_calibrated']

    # wind_speed_rad:
    #varobj = varobjdict['wind_speed_rad']

    # wind_speed_model_u:
    varobj = varobjdict['wind_speed_model_u']
    windspmu = datarr[0:ndata,23]
    sel = numpy.where(windspmu < 32767)
    varobj[sel] = windspmu[sel] / 10 # Scale factor adjusted for L2P, 1e-3 to 1e-2

    # wind_speed_model_v:
    varobj = varobjdict['wind_speed_model_v']
    windspmv = datarr[0:ndata,24]
    sel = numpy.where(windspmv < 32767)
    varobj[sel] = windspmv[sel] / 10 # Scale factor adjusted for L2P, 1e-3 to 1e-2

    # rejection_flags:
    varobj = varobjdict['rejection_flags']
    varobj[:] = flgvar

    # swh_rms:
    varobj = varobjdict['swh_rms']
    varobj[:] = datarr[0:ndata,9]

    # swh_rms_2nd:
    varobj = varobjdict['swh_rms_2nd']
    varobj[:] = datarr[0:ndata,10]

    # swh_num_valid:
    varobj = varobjdict['swh_num_valid']
    swhnumval = datarr[0:ndata,11]
    sel = numpy.where(swhnumval < 127)
    varobj[sel] = swhnumval[sel]

    # swh_num_valid_2nd:
    varobj = varobjdict['swh_num_valid_2nd']
    swhnumval2 = datarr[0:ndata,12]
    sel = numpy.where(swhnumval2 < 127)
    varobj[sel] = swhnumval2[sel]

    # sigma0_rms:
    varobj = varobjdict['sigma0_rms']
    varobj[:] = datarr[0:ndata,15]

    # sigma0_rms_2nd:
    varobj = varobjdict['sigma0_rms_2nd']
    varobj[:] = datarr[0:ndata,16]

    # sigma0_num_valid:
    varobj = varobjdict['sigma0_num_valid']
    sig0numval = datarr[0:ndata,17]
    sel = numpy.where(sig0numval < 127)
    varobj[sel] = sig0numval[sel]

    # sigma0_num_valid_2nd:
    varobj = varobjdict['sigma0_num_valid_2nd']
    sig0numval2 = datarr[0:ndata,18]
    sel = numpy.where(sig0numval2 < 127)
    varobj[sel] = sig0numval2[sel]

    # peakiness:
    varobj = varobjdict['peakiness']
    varobj[:] = datarr[0:ndata,25]

    # peakiness_2nd:
    varobj = varobjdict['peakiness_2nd']
    varobj[:] = datarr[0:ndata,26]

    # off_nadir_angle_wf:
    varobj = varobjdict['off_nadir_angle_wf']
    varobj[:] = datarr[0:ndata,20]

    # off_nadir_angle_pf:
    varobj = varobjdict['off_nadir_angle_pf']
    varobj[:] = datarr[0:ndata,19]

    # range_rms:
    varobj = varobjdict['range_rms']
    rangerms = datarr[0:ndata,5]
    sel = numpy.where(rangerms < 32767)
    varobj[sel] = rangerms[sel] * 10    # Scale factor adjusted for L2P, 1e-3 to 1e-4

    # range_rms_2nd:
    varobj = varobjdict['range_rms_2nd']
    rangerms2 = datarr[0:ndata,6]
    sel = numpy.where(rangerms2 < 32767)
    varobj[sel] = rangerms2[sel] * 10    # Scale factor adjusted for L2P, 1e-3 to 1e-4

    # bathymetry:
    varobj = varobjdict['bathymetry']
    varobj[:] = datarr[0:ndata,21]

    # distance_to_coast:
    varobj = varobjdict['distance_to_coast']

    # sea_surface_temperature
    varobj = varobjdict['sea_surface_temperature']

    # surface_air_temperature
    varobj = varobjdict['surface_air_temperature']

    # surface_air_humidity
    varobj = varobjdict['surface_air_humidity']

    # surface_air_pressure
    varobj = varobjdict['surface_air_pressure']

    # close nc file
    ncfile.close()

    return outfile
