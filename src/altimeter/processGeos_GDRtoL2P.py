'processGeos_GDRtoL2P.py'

'process Geosat Altimetry GDR data (JGM-3) to GlobWave L2P'
'requires: variablesL2P.py version 1.4'
'Ellis Ash, SatOC, e.ash@satoc.eu'
'version 1.0, 04/01/10'
'version 1.1, 04/03/10, corrections to default values'
'version 1.2, 16/03/09, include add_offset for surface_air_pressure, update to variablesL2P version'
'version 1.3, 15/08/12, correction to land flag calculation'


import re
import fnmatch
import os
import time
import struct
import numpy
import netCDF4
import variablesL2P as vaL


version = '1.3'
epochdiff = 5479*24*60*60  # system epoch is 1970-01-01, Geosat epoch 1985-01-01
calibs = {'slope_swh': 1., \
          'offs_swh': 0.}


def setattribs(varobj, attnames, invals, nval):
    'loop to set variable attributes'
    count = 0
    while (count < nval):   # loop through attributes
        if invals[count]:   # invals is 0 if no attribute required for variable
            if attnames[count] == 'valid_min':  # needed to set 'valid_min' attribute to 0 (assigned -1)
                if invals[count] < 0:
                    invals[count] = numpy.array(0).astype(numpy.int8)
            if attnames[count] != '_FillValue':
                setattr(varobj,attnames[count],invals[count])
        count += 1
        

def getflags(n, size=8, idlist=[0, 1, 2, 3, 4, 5, 6, 7]):
    'gives bit values of bytes'
    bStr = ''
    for bit in idlist:
        nshft = n >> (size - bit - 1)
        bStr = bStr + str(nshft % 2)
    return bStr


def setflagqual(ndata, datarr):
    'set rejections flag and a flag quality variable'
    
    flgvar = numpy.zeros((int(ndata)), int) - 1   # array of -1 to take flags
    flgqual = numpy.zeros((int(ndata)), int) - 1   # array of -1 to take flag related quality
    spare1 = '0'
    spare10 = '0000000000'
    
    count = 0
    while (count < ndata):   # loop for all data points
        
        hardware_0 = '0'
        attitude_1 = '0'
        alt_land_4 = str(int(getflags(datarr[count, 8], 16, [15,]) == '0'))
        altocean_5 = '0'
        rad_land_6 = '0'
        alt_rice_7 = '0'
        rad_rain_8 = '0'
        qual_ssh_11 = getflags(datarr[count, 8], 16, [12,])
        qual_swh_12 = getflags(datarr[count, 8], 16, [13,])
        qual_sg0_13 = getflags(datarr[count, 8], 16, [8,])
        qual_orb_15 = '0'
        qualswh2_16 = '0'
        qualsg02_17 = '0'
        qualonwf_18 = getflags(datarr[count, 8], 16, [9,])
        qualonpf_19 = '0'
        iceextdb_20 = '0'
        
        flgstr = hardware_0 + attitude_1 + spare1 + spare1 + alt_land_4 + altocean_5 + rad_land_6 + alt_rice_7 + \
                 rad_rain_8 + spare1 + spare1 + qual_ssh_11 + qual_swh_12 + qual_sg0_13 + spare1 + qual_orb_15 + \
                 qualswh2_16 + qualsg02_17 + qualonwf_18 + qualonpf_19 + iceextdb_20 + spare10

        flgsnok = int(alt_land_4) or int(qual_ssh_11) or int(qualonwf_18)
        flgqual[count] = flgsnok
        flgstr = flgstr + str(flgsnok)   # set bit 31 if rejected by other flags

        flgint = int(flgstr, 2)
        if int(flgstr, 2) >= 2**31:   # integer is signed
            flgint = int(int(flgstr, 2) - 2**32)
        flgvar[count] = flgint

        count += 1

    return flgvar, flgqual


def setswhqual(ndata, datarr, flgqual):

    swh = datarr[0:ndata, 5]
    sig_0 = datarr[0:ndata, 7]
    sig_h = datarr[0:ndata, 4]
    att = datarr[0:ndata, 9]
    
    datqual = ((swh > 0) & (swh < 2500)) & \
              ((sig_0 > 500) & (sig_0 < 2000)) & \
              ((sig_h > 0) & (sig_h < 10)) & \
              ((att > 0) & (att < 70))

    swhqual = 2 * ((datqual == 0) | (flgqual == 1)) # quality is 2 for bad (datqual gives True for good)

    swhgood = numpy.where(swhqual == 0)
    if datarr[0, 0] >= 115166716 :
        swhqual[swhgood] = 1    # quality is 0 for good, 1 for good but cycle >= 40 (degraded)

    return swhqual


def processgeosgdr(fname, outdir):

    try:
        datain = open(fname,'r')
    except IOError, e:
        print 'file open error:', e
    else:
        print 'data file opened ok'

    # read header and extract information
    # no header in geosat

    # count input data records
    count = 0
    while True:
        record = datain.read(78)  # GDR_DATA_RECORD length
        if record:
            count += 1
        else:
            break
    ndata = count
    datain.close()

    try:
        datain = open(fname,'r')
    except IOError, e:
        print 'file open error:', e
    else:
        print 'data file opened ok'

    # read input data records
    count = 0
    datarr = numpy.zeros((int(ndata),10), int) - 1   # array of -1 to take data
    while True:
        record = datain.read(78)  # GDR_DATA_RECORD length
        if record:
            sec, musec, lat, lon, sig_h, swh, wind_sp, sig0, flags, att \
                  = struct.unpack('>iiii 6x h 22x hhh 4x h 18x h', record)
            datarr[count, :] = [sec, musec, lat, lon, sig_h, swh, wind_sp, sig0, flags, att]
            count += 1
            if count == 1:
                tfp = sec

        else:
            tlp = sec
            break

    datain.close()

    # open nc file for output
    tfp = tfp + epochdiff
    tfp = time.gmtime(tfp)
    ftime = time.strftime("%Y%m%d_%H%M%S", tfp)
    fmt = time.strftime("%Y-%m-%dT%H:%M:%S", tfp)
    tlp = tlp + epochdiff
    tlp = time.gmtime(tlp)
    ltime = time.strftime("%Y%m%d_%H%M%S", tlp)
    lmt = time.strftime("%Y-%m-%dT%H:%M:%S", tlp)

    cycle = '000'
    passn = '000'

    prefix = 'GW_L2P_ALT_GEOS_GDR_'
    #outfile = 'test.nc'
    outfile = outdir + prefix + ftime + '_' + ltime + '_' + cycle + '_' + passn + '.nc'
    ncfile = netCDF4.Dataset(outfile, 'w', format='NETCDF3_CLASSIC')

    # create dimensions
    ncfile.createDimension(vaL.dim1, ndata)

    # create and initialise variables and variable attributes
    varobjdict = {}
    varsset = vaL.varsL2P[0:7] + vaL.varsL2P[10:13] + vaL.varsL2P[16:18] + vaL.varsL2P[19:22] + vaL.varsL2P[32:33] + vaL.varsL2P[34:35] + vaL.varsL2P[36:42]
    #for varname in vaL.varsL2P:
    for varname in varsset:
        fval = vaL.fvals[vaL.fvalis[varname]]
        varobj = ncfile.createVariable(varname,vaL.varsizes[varname],(vaL.dim1,), fill_value=fval)
        fval = vaL.fvals[vaL.fvalis[varname]]
        invals = [fval, vaL.lnames1[varname], vaL.snames[varname], vaL.units[varname], vaL.sources[varname], vaL.insts[varname], \
                  vaL.calforms[varname], vaL.calrefs[varname], vaL.valrefs[varname], vaL.qflags[varname], vaL.vmins[varname], \
                  vaL.vmaxs[varname], vaL.flvals[varname], vaL.flmasks[varname], vaL.flmeans[varname], vaL.scalefs[varname], vaL.coords[varname], vaL.commGFO[varname]]
        setattribs(varobj, vaL.attnames, invals, len(vaL.attnames))

        if varname == 'time':
            varobj.calendar = 'gregorian'    # not set from variablesL2P
        if varname == 'swh_calibrated':
            varobj.calibration_formula = str(calibs['slope_swh']) + '*swh + ' + str(calibs['offs_swh'])
        if varname == 'bathymetry':
            varobj.add_offset = 0    # not set from variablesL2P
            varobj.coordinates = 'lon lat'  # set here to retain order
        if varname == 'surface_air_pressure':
            varobj.add_offset = 100000    # not set from variablesL2P
            varobj.coordinates = 'lon lat'  # set here to retain order

        varobj[:] = fval
        varobjdict[varname] = varobj

    # create global attributes

    globattd = vaL.globattdict
    fnbits = re.split('/', fname)
    sourcefile = fnbits[-1]

    for ga in vaL.globatts:
        setattr(ncfile, ga, globattd[ga])

    ncfile.title = 'GlobWave L2P derived from Geosat GDR Product'
    ncfile.history = time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime()) + ' UTC : Creation'
    ncfile.software_version = 'SatOC GlobWave Geosat GDR to L2P Processor ' + version
    ncfile.source_provider = 'NOAA'
    ncfile.mission_name = 'Geosat'
    ncfile.start_date = fmt + ' UTC'
    ncfile.stop_date = lmt + ' UTC'


    # transcribe or generate data

    # time:
    varobj = varobjdict['time']
    varobj[:] = (datarr[0:ndata,0]) + (datarr[0:ndata,1]/1000000.)

    # lat:
    varobj = varobjdict['lat']
    varobj[:] = datarr[0:ndata,2]/1.e6

    # lon:
    varobj = varobjdict['lon']
    varobj[:] = datarr[0:ndata,3]/1.e6

    # swh:
    varobj = varobjdict['swh']
    swharr = datarr[0:ndata,5].copy()
    sel = numpy.where(swharr < 32767)
    swharr[sel] = swharr[sel] * 10  # Scale factor adjusted for L2P, 1e-2 to 1e-3
    varobj[sel] = swharr[sel]

    # swh_calibrated:
    varobj = varobjdict['swh_calibrated']
    varobj[sel] = swharr[sel]*calibs['slope_swh'] + calibs['offs_swh']/vaL.scalefs['swh']

    # swh_quality:
    varobj = varobjdict['swh_quality']
    flgvar, flgqual = setflagqual(ndata, datarr)
    swhqualarr = setswhqual(ndata, datarr, flgqual)
    varobj[:] = swhqualarr

    # swh_standard_error:
    varobj = varobjdict['swh_standard_error']

    # swh_2nd:
    #varobj = varobjdict['swh_2nd']

    # swh_2nd_calibrated:
    #varobj = varobjdict['swh_2nd_calibrated']

    # swh_2nd_quality:
    #varobj = varobjdict['swh_2nd_quality']

    # sigma0:
    varobj = varobjdict['sigma0']
    varobj[:] = datarr[0:ndata,7]

    # sigma0_calibrated:
    varobj = varobjdict['sigma0_calibrated']

    # sigma0_quality:
    varobj = varobjdict['sigma0_quality']
    varobj[:] = swhqualarr

    # sigma0_2nd:
    #varobj = varobjdict['sigma0_2nd']

    # sigma0_2nd_calibrated:
    #varobj = varobjdict['sigma0_2nd_calibrated']

    # sigma0_2nd_quality:
    #varobj = varobjdict['sigma0_2nd_quality']

    # wind_speed_alt:
    varobj = varobjdict['wind_speed_alt']
    varobj[:] = datarr[0:ndata,6]

    # wind_speed_alt_calibrated:
    varobj = varobjdict['wind_speed_alt_calibrated']

    # wind_speed_rad:
    #varobj = varobjdict['wind_speed_rad']

    # wind_speed_model_u:
    varobj = varobjdict['wind_speed_model_u']
    #varobj[:] = ancillaryL2P.get_model_u(time, lat, lon)

    # wind_speed_model_v:
    varobj = varobjdict['wind_speed_model_v']

    # rejection_flags:
    varobj = varobjdict['rejection_flags']
    varobj[:] = flgvar

    # swh_rms:
    #varobj = varobjdict['swh_rms']

    # swh_rms_2nd:
    #varobj = varobjdict['swh_rms_2nd']

    # swh_num_valid:
    #varobj = varobjdict['swh_num_valid']

    # swh_num_valid_2nd:
    #varobj = varobjdict['swh_num_valid_2nd']

    # sigma0_rms:
    #varobj = varobjdict['sigma0_rms']

    # sigma0_rms_2nd:
    #varobj = varobjdict['sigma0_rms_2nd']

    # sigma0_num_valid:
    #varobj = varobjdict['sigma0_num_valid']

    # sigma0_num_valid_2nd:
    #varobj = varobjdict['sigma0_num_valid_2nd']

    # peakiness:
    #varobj = varobjdict['peakiness']

    # peakiness_2nd:
    #varobj = varobjdict['peakiness_2nd']

    # off_nadir_angle_wf:
    varobj = varobjdict['off_nadir_angle_wf']
    offnadwf = datarr[0:ndata,9]
    sel = numpy.where(offnadwf < 32767)
    varobj[sel] = offnadwf[sel] * offnadwf[sel]   # Square to get degree2 and scale factor 1e-4

    # off_nadir_angle_pf:
    #varobj = varobjdict['off_nadir_angle_pf']

    # range_rms:
    varobj = varobjdict['range_rms']
    rangerms = datarr[0:ndata,4]
    sel = numpy.where(rangerms < 32767)
    varobj[sel] = rangerms[sel] * 100  # Scale factor adjusted for L2P, 1e-2 to 1e-4

    # range_rms_2nd:
    #varobj = varobjdict['range_rms_2nd']

    # bathymetry:
    varobj = varobjdict['bathymetry']

    # distance_to_coast:
    varobj = varobjdict['distance_to_coast']

    # sea_surface_temperature
    varobj = varobjdict['sea_surface_temperature']

    # surface_air_temperature
    varobj = varobjdict['surface_air_temperature']

    # surface_air_humidity
    #varobj = varobjdict['surface_air_humidity']

    # surface_air_pressure
    varobj = varobjdict['surface_air_pressure']

    # close nc file
    ncfile.close()
