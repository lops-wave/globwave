'processTopx_GDRtoL2P.py'

'process TOPEX/POSEIDON Altimetry GDR data to GlobWave L2P'
'requires: variablesL2P.py version 1.4'
'Ellis Ash, SatOC, e.ash@satoc.eu'
'version 1.0, 3/12/09'
'version 1.1, 23/12/09, removal redundant variables, add_offset attribute for bathymetry, surface_temperature -> surfce_air_temperature, change scaling logic, sigma0 quality as for swh'
'version 1.2, 18/01/10, corrections for files with topex/poseidon mixed data' 
'version 1.3, 18/01/10, correction to regular expression for equator longitude'
'version 1.4, 25/01/10, minor change to flags logic'
'version 1.5, 17/03/10, corrections to default values, validation logic for poseidon, calibration logic for mixed sensors, attribute updates'
'version 1.6, 02/11/10, correction to calibration offset, TOPEX A side'


import re
import fnmatch
import os
import time
import struct
import numpy
import netCDF3
import variablesL2P as vaL


version = '1.6'


def setattribs(varobj, attnames, invals, nval):
    'loop to set variable attributes'
    count = 0
    while (count < nval):   # loop through attributes
        if invals[count]:   # invals is 0 if no attribute required for variable
            if attnames[count] == 'valid_min':  # needed to set 'valid_min' attribute to 0 (assigned -1)
                if invals[count] < 0:
                    invals[count] = numpy.array(0).astype(numpy.int8)
            setattr(varobj,attnames[count],invals[count])
        count += 1


def getflags(n, size=8, idlist=[0, 1, 2, 3, 4, 5, 6, 7]):
    'gives bit values of bytes as a string'
    bStr = ''
    for bit in idlist:
        nshft = n >> (size - bit - 1)
        bStr = bStr + str(nshft % 2)
    return bStr


def setflagqual(ndata, flgarr):
    'set rejections flag and a flag quality variable'
    
    flgvar = numpy.zeros((int(ndata)), int) - 1   # array of -1 to take flags
    flgqual = numpy.zeros((int(ndata)), int) - 1   # array of -1 to take flag related quality
    rain = numpy.zeros((int(ndata)), int) - 1   # array of -1 to take rain flag
    topex = numpy.zeros((int(ndata)), int) - 1   # array of -1 to indicate topex not poseidon
    topx = 0
    psdn = 0
    selt = 0
    selp = 0
    spare1 = '0'
    spare10 = '0000000000'
    
    count = 0
    while (count < ndata):   # loop for all data points
        
        hardware_0 = str(flgarr[count, 0])
        attitude_1 = str(int(flgarr[count, 3]>0 or flgarr[count, 4]>0))
        alt_land_4 = getflags(flgarr[count, 8], 8, [6,])
        altocean_5 = getflags(flgarr[count, 8], 8, [7,])
        rad_land_6 = getflags(flgarr[count, 8], 8, [5,])
        alt_rice_7 = getflags(flgarr[count, 8], 8, [4,])
        rad_rain_8 = getflags(flgarr[count, 9], 8, [7,])
        if int(hardware_0):   # TOPEX
            qual_ssh_11 = getflags(flgarr[count, 6], 8, [0,])
            qual_swh_12 = getflags(flgarr[count, 7], 8, [3,])
            qual_sg0_13 = getflags(flgarr[count, 7], 8, [1,])
        else:       # Poseidon
            qual_ssh_11 = str(int(getflags(flgarr[count, 6], 8, [6, 7]) > '00'))
            qual_swh_12 = str(int(getflags(flgarr[count, 6], 8, [4, 5]) > '00'))
            qual_sg0_13 = str(int(getflags(flgarr[count, 6], 8, [2, 3]) > '00'))
        qual_orb_15 = '0'
        qualswh2_16 = '0'
        qualsg02_17 = '0'
        qualonwf_18 = '0'
        qualonpf_19 = '0'
        iceextdb_20 = '0'
        
        flgstr = hardware_0 + attitude_1 + spare1 + spare1 + alt_land_4 + altocean_5 + rad_land_6 + alt_rice_7 + \
                 rad_rain_8 + spare1 + spare1 + qual_ssh_11 + qual_swh_12 + qual_sg0_13 + spare1 + qual_orb_15 + \
                 qualswh2_16 + qualsg02_17 + qualonwf_18 + qualonpf_19 + iceextdb_20 + spare10

        if int(hardware_0):   # TOPEX
            flgsnok = int(alt_land_4) or int(alt_rice_7) or int(qual_sg0_13)
        else:       # Poseidon
            flgsnok = int(alt_land_4) or int(alt_rice_7) or int(qual_swh_12) or int(qual_sg0_13)
        flgqual[count] = flgsnok
        flgstr = flgstr + str(flgsnok)   # set bit 31 if rejected by other flags

#        if count > 300 and count < 350:
#        print flgstr

        flgint = int(flgstr, 2)
        if int(flgstr, 2) >= 2**31:   # integer is signed
            flgint = int(int(flgstr, 2) - 2**32)
        flgvar[count] = flgint

        rain[count] = rad_rain_8 

        topex[count] = hardware_0

        count += 1

    if (sum(topex) == ndata):   # all data are Topex
        topx = 1
    elif (sum(topex) == 0):    # all data are Poseidon
        psdn = 1
    else:
        selt = numpy.where(topex == 1)   # select where topex
        selp = numpy.where(topex == 0)   # select where poseidon

    return flgvar, flgqual, rain, topx, psdn, selt, selp


def setswhqual(ndata, datarr, flgqual, rain, topx, psdn, selt, selp):

    swh_k = datarr[0:ndata, 7]
    swh_pts_avg = datarr[0:ndata, 11]
    swh_rms_k = datarr[0:ndata, 9]
    sigma0_k = datarr[0:ndata, 12]
    agc_pts_avg = datarr[0:ndata, 15]
    agc_rms_k = datarr[0:ndata, 14]
    rms_h_alt = datarr[0:ndata, 6]
    att_wvf = datarr[0:ndata, 4]

    if (topx):
        datqual = ((swh_k > 0) & (swh_k < 65534)) & \
                  (swh_pts_avg > 7) & \
                  ((swh_rms_k > 0) & ((swh_rms_k < 100) | (swh_rms_k < 0.1*swh_k))) & \
                  (sigma0_k < 32767) & \
                  (agc_pts_avg > 15) & \
                  (agc_rms_k < 20) & \
                  (rms_h_alt < 80) & \
                  (att_wvf < 20)

    elif (psdn):
        datqual = ((swh_k > 0) & (swh_k < 65534)) & \
                  ((sigma0_k > 700) & (sigma0_k < 2500)) & \
                  (rms_h_alt < 200) & \
                  ((att_wvf > 0) & (att_wvf < 30))

    else:
        print 'mixed topex and poseidon'
        datqual = numpy.zeros(ndata)
        tqual = ((swh_k[selt] > 0) & (swh_k[selt] < 65534)) & \
                  (swh_pts_avg[selt] > 7) & \
                  ((swh_rms_k[selt] > 0) & ((swh_rms_k[selt] < 100) | (swh_rms_k[selt] < 0.1*swh_k[selt]))) & \
                  (sigma0_k[selt] < 32767) & \
                  (agc_pts_avg[selt] > 15) & \
                  (agc_rms_k[selt] < 20) & \
                  (rms_h_alt[selt] < 80) & \
                  (att_wvf[selt] < 20)
        pqual = ((swh_k[selp] > 0) & (swh_k[selp] < 65534)) & \
                  ((sigma0_k[selp] > 700) & (sigma0_k[selp] < 2500)) & \
                  (rms_h_alt[selp] < 200) & \
                  ((att_wvf[selp] > 0) & (att_wvf[selp] < 30))
        datqual[selt] = tqual
        datqual[selp] = pqual

    swhqual = 2 * ((datqual == 0) | (flgqual == 1)) # quality is 2 for bad (datqual gives True for good)

    swhgood = numpy.where(swhqual == 0)
    swhqual[swhgood] = rain[swhgood]    # quality is 0 for good, 1 for good but rain probable

    return swhqual


def setcalibs(cycle):
    'set calibrations that vary with cycle / altimeter'

    icycle = int(cycle)
    if (icycle <= 235): # Side A
        
        if (icycle < 98):
            dh = 0
        else:
            a0 = 0.0864
            a1 = -6.0426e-4
            a2 = -7.7894e-6
            a3 = 6.9624e-8
            p98 = a0 + a1*(98) + a2*(98**2) + a3*(98**3)
            dh = p98 - (a0 + a1*(icycle) + a2*(icycle**2) + a3*(icycle**3))
        
        slope_swh = 1.0539
        offs_swh = -0.0766 + dh

    else:   # Side B
        
        slope_swh = 1.0237
        offs_swh = -0.0476

    caltopx = {'slope_swh': slope_swh, \
              'offs_swh': offs_swh}

    calpsdn = {'slope_swh': 0.9914, \
              'offs_swh': -0.0103}

    return caltopx, calpsdn


def processtopxgdr(fname, outdir):
    
    try:
        datain = open(fname,'r')
    except IOError, e:
        print 'file open error:', e
    else:
        print 'data file opened ok'

    # read header and extract information
    header = datain.read(7524) # PASS_FILE_HEADER size 7524
    #print header

    atts = ('Generating_Software_Name', 'Build_Id')
    for label in atts:
        patn = '(' + label + ' = )(.*?)(;)'
        m = re.search(patn, header)
        if label == 'Generating_Software_Name':
            srcver = m.group(2)
        if label == 'Build_Id':
            srcsoft = m.group(2)

    atts = ('Cycle_Number', 'Pass_Number', 'Pass_Data_Count', 'Equator_Longitude')
    for label in atts:
        patn = '(' + label + ' = +)(\d+)(;|\.\d+)'
        m = re.search(patn, header)
        if label == 'Cycle_Number':
            cycle = m.group(2)
            print 'cycle:' + cycle
        if label == 'Pass_Number':
            passn = m.group(2)
            print 'pass:' + passn
        if label == 'Pass_Data_Count':
            ndata = m.group(2)
            ndata = int(ndata)
        if label == 'Equator_Longitude':
            eqlon = m.group(2) + m.group(3)
            
    atts = ('Equator_Time', 'Time_First_Pt', 'Time_Last_Pt')
    for label in atts:
        patn = '(' + label + ' = )(\d+)(-)(\d+)(T)(\d+:\d+:\d+)(\.\d+)'
        m = re.search(patn, header)
        if label == 'Equator_Time':
            strtime = m.group(2) + ' ' + m.group(4) + ' ' + m.group(6)
            eqt = time.strptime(strtime, "%Y %j %H:%M:%S")
            eqtime = time.strftime("%Y-%m-%dT%H:%M:%S", eqt)
        if label == 'Time_First_Pt':
            strtime = m.group(2) + ' ' + m.group(4) + ' ' + m.group(6)
            tfp = time.strptime(strtime, "%Y %j %H:%M:%S")
            ftime = time.strftime("%Y%m%d_%H%M%S", tfp)
            fmt = time.strftime("%Y-%m-%dT%H:%M:%S", tfp)
        if label == 'Time_Last_Pt':
            strtime = m.group(2) + ' ' + m.group(4) + ' ' + m.group(6)
            tlp = time.strptime(strtime, "%Y %j %H:%M:%S")
            ltime = time.strftime("%Y%m%d_%H%M%S", tlp)
            lmt = time.strftime("%Y-%m-%dT%H:%M:%S", tlp)

    # open nc file for output
    prefix = 'GW_L2P_ALT_TOPX_GDR_'
    #outfile = 'test.nc'
    outfile = outdir + prefix + ftime + '_' + ltime + '_' + cycle + '_' + passn + '.nc'
    ncfile = netCDF3.Dataset(outfile, 'w', format='NETCDF3_CLASSIC')

    # read input data records
    count = 0
    datarr = numpy.zeros((int(ndata),18), int) - 1   # array of -1 to take data
    flgarr = numpy.zeros((int(ndata),10), int) - 1   # array of -1 to take flags
    while True:
        record = datain.read(228)  # DATA_RECORD length
        if record:
            days, msec, lat, lon, off_nad_wf, off_nad_pf, range_rms, swh, swh_c, swh_rms, swh_rms_c, \
                  swh_numval, sig0, sig0_c, agc_rms, agc_numval, u10, bathy \
                  = struct.unpack('<hi 14x ii 48x BB 25x h 31x HHBBB 10x HH 4x H 8x b 17x B h 36x', record)
            datarr[count,:] = [days, msec, lat, lon, off_nad_wf, off_nad_pf, range_rms, swh, swh_c, swh_rms, swh_rms_c, \
                               swh_numval, sig0, sig0_c, agc_rms, agc_numval, u10, bathy]
            alton, instr_tp, imanv, lat_err, lon_err, att_pf, alt_bad1, alt_bad2, geo_bad1, geo_bad2 \
                   = struct.unpack('<198x bB 2x bbbb 6x BB 9x BB 3x', record)
            flgarr[count,:] = [alton, instr_tp, imanv, lat_err, lon_err, att_pf, alt_bad1, alt_bad2, geo_bad1, geo_bad2]
            count += 1
        else:
            break

    datain.close()

    # create dimensions
    ncfile.createDimension(vaL.dim1, ndata)

    # create and initialise variables and variable attributes
    varobjdict = {}
    varsset = vaL.varsL2P[0:18] + vaL.varsL2P[19:25] + vaL.varsL2P[26:27] + vaL.varsL2P[28:29] + vaL.varsL2P[32:35] + vaL.varsL2P[36:42]
    for varname in varsset:
        varobj = ncfile.createVariable(varname,vaL.varsizes[varname],(vaL.dim1,))
        fval = vaL.fvals[vaL.fvalis[varname]]
        invals = [fval, vaL.lnames1[varname], vaL.snames[varname], vaL.units[varname], vaL.sources[varname], vaL.insts[varname], \
                  vaL.calforms[varname], vaL.calrefs[varname], vaL.valrefs[varname], vaL.qflags[varname], vaL.vmins[varname], \
                  vaL.vmaxs[varname], vaL.flvals[varname], vaL.flmasks[varname], vaL.flmeans[varname], vaL.scalefs[varname], vaL.coords[varname], vaL.commTOPX[varname]]
        setattribs(varobj, vaL.attnames, invals, len(vaL.attnames))

        if varname == 'time':
            varobj.calendar = 'gregorian'    # not set from variablesL2P
        if varname == 'swh_calibrated':
            varobj.calibration_formula = 'refer to L2P Product User Guide'
        if varname == 'sigma0_rms':
            varobj.comment = 'Corresponds to agc_rms_k from the L2 data'
        if varname == 'sigma0_num_valid':
            varobj.comment = 'Corresponds to agc_pts_avg from the L2 data'
        if varname == 'bathymetry':
            varobj.add_offset = 0    # not set from variablesL2P
            varobj.coordinates = 'lon lat'  # set here to retain order
        if varname == 'surface_air_pressure':
            varobj.add_offset = 100000    # not set from variablesL2P
            varobj.coordinates = 'lon lat'  # set here to retain order

        varobj[:] = fval    # fill with default values
        varobjdict[varname] = varobj

    # create global attributes
    globattd = vaL.globattdict
    fnbits = re.split('/', fname)
    sourcefile = fnbits[-1]

    for ga in vaL.globatts:
        setattr(ncfile, ga, globattd[ga])

    ncfile.title = 'GlobWave L2P derived from TOPEX/POSEIDON GDR Product'
    ncfile.history = time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime()) + ' UTC : Creation'
    ncfile.software_version = 'SatOC GlobWave TOPEX/POSEIDON GDR to L2P Processor ' + version
    ncfile.source_provider = 'CNES'
    ncfile.source_name = sourcefile
    ncfile.source_version = srcver
    ncfile.source_software = srcsoft
    ncfile.mission_name = 'TOPEX/POSEIDON'
    ncfile.altimeter_sensor_name = 'Altimeters-T/P'
    ncfile.cycle_number = cycle
    ncfile.pass_number = passn
    ncfile.equator_crossing_time = eqtime + ' UTC'
    ncfile.equator_crossing_longitude = eqlon
    ncfile.start_date = fmt + ' UTC'
    ncfile.stop_date = lmt + ' UTC'


# transcribe or generate data

    sel = numpy.where(datarr == 65535)  # change unsigned short default to signed
    datarr[sel] = 2**15 - 1

    # time:
    timediff = -9862*24*60*60    # TOPEX time is relative to 1958-01-01
    varobj = varobjdict['time']
    varobj[:] = (datarr[0:ndata,0]*24*60*60) + (datarr[0:ndata,1]/1000.) + timediff

    # lat:
    varobj = varobjdict['lat']
    varobj[:] = datarr[0:ndata,2]/1.e6

    # lon:
    varobj = varobjdict['lon']
    varobj[:] = datarr[0:ndata,3]/1.e6

    # swh:
    varobj = varobjdict['swh']
    swharr = datarr[0:ndata,7]
    sel = numpy.where(swharr < 32767)
    swharr[sel] = swharr[sel] * 10  # Scale factor adjusted for L2P, 1e-2 to 1e-3
    varobj[sel] = swharr[sel]

    # swh_calibrated:
    varobj = varobjdict['swh_calibrated']
    flgvar, flgqual, rain, topx, psdn, selt, selp = setflagqual(ndata, flgarr)
    caltopx, calpsdn = setcalibs(cycle)
    if (topx):
        calibs = caltopx
        varobj[sel] = swharr[sel]*calibs['slope_swh'] + calibs['offs_swh']/vaL.scalefs['swh']
    elif (psdn):
        calibs = calpsdn
        varobj[sel] = swharr[sel]*calibs['slope_swh'] + calibs['offs_swh']/vaL.scalefs['swh']
    else:
        calibs = caltopx
        varobj[selt] = swharr[selt]*calibs['slope_swh'] + calibs['offs_swh']/vaL.scalefs['swh']
        calibs = calpsdn
        varobj[selp] = swharr[selp]*calibs['slope_swh'] + calibs['offs_swh']/vaL.scalefs['swh']   
    swhcal = varobj[sel]
    selovf = numpy.where(swhcal > 32767)
    selneg = numpy.where(swhcal < 0)
    swhcal[selovf] = 32767
    swhcal[selneg] = 0
    varobj[sel] = swhcal

    # swh_quality:
    varobj = varobjdict['swh_quality']
    # flgvar, flgqual, rain, topx, psdn = setflagqual(ndata, flgarr)    # called above to set topx and psdn
    swhqualarr = setswhqual(ndata, datarr, flgqual, rain, topx, psdn, selt, selp)
    varobj[:] = swhqualarr

    # swh_standard_error:
    varobj = varobjdict['swh_standard_error']

    # swh_2nd:
    varobj = varobjdict['swh_2nd']
    swh2arr = datarr[0:ndata,8]
    sel = numpy.where(swh2arr < 32767)
    if len(sel[0]) > 0:
        varobj[sel] = swh2arr[sel] * 10  # Scale factor adjusted for L2P, 1e-2 to 1e-3

    # swh_2nd_calibrated:
    varobj = varobjdict['swh_2nd_calibrated']

    # swh_2nd_quality:
    varobj = varobjdict['swh_2nd_quality']

    # sigma0:
    varobj = varobjdict['sigma0']
    varobj[:] = datarr[0:ndata,12]

    # sigma0_calibrated:
    varobj = varobjdict['sigma0_calibrated']

    # sigma0_quality:
    varobj = varobjdict['sigma0_quality']
    varobj[:] = swhqualarr

    # sigma0_2nd:
    varobj = varobjdict['sigma0_2nd']
    varobj[:] = datarr[0:ndata,13]

    # sigma0_2nd_calibrated:
    varobj = varobjdict['sigma0_2nd_calibrated']

    # sigma0_2nd_quality:
    varobj = varobjdict['sigma0_2nd_quality']

    # wind_speed_alt:
    varobj = varobjdict['wind_speed_alt']
    windspalt = datarr[0:ndata,16]
    sel = numpy.where(windspalt < 255)
    varobj[sel] = windspalt[sel] * 10  # Scale factor adjusted for L2P, 1e-1 to 1e-2

    # wind_speed_alt_calibrated:
    varobj = varobjdict['wind_speed_alt_calibrated']

    # wind_speed_rad:
    #varobj = varobjdict['wind_speed_rad']

    # wind_speed_model_u:
    varobj = varobjdict['wind_speed_model_u']

    # wind_speed_model_v:
    varobj = varobjdict['wind_speed_model_v']

    # rejection_flags:
    varobj = varobjdict['rejection_flags']
    varobj[:] = flgvar

    # swh_rms:
    varobj = varobjdict['swh_rms']
    swhrmsarr = datarr[0:ndata,9]
    sel = numpy.where(swhrmsarr < 255)
    if len(sel[0]) > 0:
        varobj[sel] = swhrmsarr[sel] * 10  # Scale factor adjusted for L2P, 1e-2 to 1e-3

    # swh_rms_2nd:
    varobj = varobjdict['swh_rms_2nd']
    swhrms2arr = datarr[0:ndata,10]
    sel = numpy.where(swhrms2arr < 255)
    if len(sel[0]) > 0:
        varobj[sel] = swhrms2arr[sel] * 10  # Scale factor adjusted for L2P, 1e-2 to 1e-3

    # swh_num_valid:
    varobj = varobjdict['swh_num_valid']
    varobj[:] = datarr[0:ndata,11]

    # swh_num_valid_2nd:
    #varobj = varobjdict['swh_num_valid_2nd']

    # sigma0_rms:
    varobj = varobjdict['sigma0_rms']
    varobj[:] = datarr[0:ndata,14]

    # sigma0_rms_2nd:
    #varobj = varobjdict['sigma0_rms_2nd']

    # sigma0_num_valid:
    varobj = varobjdict['sigma0_num_valid']
    varobj[:] = datarr[0:ndata,15]

    # sigma0_num_valid_2nd:
    #varobj = varobjdict['sigma0_num_valid_2nd']

    # peakiness:
    #varobj = varobjdict['peakiness']

    # peakiness_2nd:
    #varobj = varobjdict['peakiness_2nd']

    # off_nadir_angle_wf:
    varobj = varobjdict['off_nadir_angle_wf']
    ona_wf = datarr[0:ndata,4].copy()
    sel = numpy.where(ona_wf < 255)
    varobj[sel] = ona_wf[sel]**2  # square to make consistent with other altimeters

    # off_nadir_angle_pf:
    varobj = varobjdict['off_nadir_angle_pf']
    ona_pf = datarr[0:ndata,5].copy()
    sel = numpy.where(ona_pf < 255)
    varobj[sel] = ona_pf[sel]**2  # square to make consistent with other altimeters

    # range_rms:
    varobj = varobjdict['range_rms']
    rangerms = datarr[0:ndata,6]
    sel = numpy.where(rangerms < 32767)
    varobj[sel] = rangerms[sel] * 10    # Scale factor adjusted for L2P, 1e-3 to 1e-4

    # range_rms_2nd:
    #varobj = varobjdict['range_rms_2nd']

    # bathymetry:
    varobj = varobjdict['bathymetry']
    varobj[:] = datarr[0:ndata,17]

    # distance_to_coast:
    varobj = varobjdict['distance_to_coast']

    # sea_surface_temperature
    varobj = varobjdict['sea_surface_temperature']

    # surface_air_temperature
    varobj = varobjdict['surface_air_temperature']

    # surface_air_humidity
    varobj = varobjdict['surface_air_humidity']

    # surface_air_pressure
    varobj = varobjdict['surface_air_pressure']

    # close nc file
    ncfile.close()
