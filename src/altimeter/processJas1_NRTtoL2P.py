'processJas1_NRTtoL2P.py'

'process Jason-1 Altimetry NRT (OSDR) data to GlobWave L2P'
'requires: variablesL2P.py version 1.4'
'Ellis Ash, SatOC, e.ash@satoc.eu'
'version 1.0, 10/02/10'
'version 1.1, 17/08/10, correction to default values, include add_offset for surface_air_pressure, update to variablesL2P version'
'version 1.2, 21/10/10, update to calibration logic to set overflow values to default and -ve values to zero, quality rejection of zero swh'
'version 1.3, 16/11/10, correction to off_swh : set to negative value'
'version 1.4, 06/12/11, correction to the time point 65535 seconds into the day'


import re
import fnmatch
import os
import time
import struct
import numpy
import netCDF3
import variablesL2P as vaL


version = '1.3'
calibs = {'slope_swh': 1.041, \
          'offs_swh': -0.076}   # Durrant et al (2009)


def setattribs(varobj, attnames, invals, nval):
    'loop to set variable attributes'
    count = 0
    while (count < nval):   # loop through attributes
        if invals[count]:   # invals is 0 if no attribute required for variable
            if attnames[count] == 'valid_min':  # needed to set 'valid_min' attribute to 0 (assigned -1)
                if invals[count] < 0:
                    invals[count] = numpy.array(0).astype(numpy.int8)
            setattr(varobj,attnames[count],invals[count])
        count += 1


def getflags(n, size=8, idlist=[0, 1, 2, 3, 4, 5, 6, 7]):
    'gives bit values of bytes'
    bStr = ''
    for bit in idlist:
        nshft = n >> (size - bit - 1)
        bStr = bStr + str(nshft % 2)
    return bStr


def setflagqual(ndata, flgarr):
    'set rejections flag and a flag quality variable'
    
    flgvar = numpy.zeros((int(ndata)), int) - 1   # array of -1 to take flags
    flgqual = numpy.zeros((int(ndata)), int) - 1   # array of -1 to take flag related quality
    rainice = numpy.zeros((int(ndata)), int) - 1   # array of -1 to take rain or ice flag
    spare1 = '0'
    spare10 = '0000000000'
    
    count = 0
    while (count < ndata):   # loop for all data points
        
        hardware_0 = getflags(flgarr[count, 5], 8, [6,])
        attitude_1 = '0'
        alt_land_4 = str(int(getflags(flgarr[count, 0], 8, [6, 7]) > '00'))
        altocean_5 = str(int(flgarr[count, 1] == 1))
        rad_land_6 = str(int(getflags(flgarr[count, 0], 8, [6, 7]) == '01'))
        alt_rice_7 = '0'
        rad_rain_8 = '0'
        qual_ssh_11 = getflags(flgarr[count, 3], 8, [7,])
        qual_swh_12 = getflags(flgarr[count, 3], 8, [5,])
        qual_sg0_13 = getflags(flgarr[count, 3], 8, [3,])
        qual_orb_15 = '0'
        qualswh2_16 = getflags(flgarr[count, 3], 8, [4,])
        qualsg02_17 = getflags(flgarr[count, 3], 8, [2,])
        qualonwf_18 = getflags(flgarr[count, 3], 8, [1,])
        qualonpf_19 = getflags(flgarr[count, 3], 8, [0,])
        iceextdb_20 = '0'
        
        flgstr = hardware_0 + attitude_1 + spare1 + spare1 + alt_land_4 + altocean_5 + rad_land_6 + alt_rice_7 + \
                 rad_rain_8 + spare1 + spare1 + qual_ssh_11 + qual_swh_12 + qual_sg0_13 + spare1 + qual_orb_15 + \
                 qualswh2_16 + qualsg02_17 + qualonwf_18 + qualonpf_19 + iceextdb_20 + spare10

        flgsnok = int(altocean_5) or int(rad_land_6) or int(qual_ssh_11) or int(qual_swh_12) or int(qual_sg0_13)
        flgqual[count] = flgsnok
        flgstr = flgstr + str(flgsnok)   # set bit 31 if rejected by other flags

#        print flgstr, flgarr[count, 3]
        flgint = int(flgstr, 2)
        if int(flgstr, 2) >= 2**31:   # integer is signed
            flgint = int(int(flgstr, 2) - 2**32)
        flgvar[count] = flgint

        count += 1

    return flgvar, flgqual


def setswhqual(ndata, datarr, flgqual):

    range_rms_ku = datarr[0:ndata,12]
    swh_numval_ku = datarr[0:ndata,7]
    swh_ku = datarr[0:ndata,5]
    swh_rms_ku = datarr[0:ndata,6]
    sig0_rms_ku = datarr[0:ndata,10]
    wind_speed_alt = datarr[0:ndata,15]
    off_nadir_angle_ku_wvf = datarr[0:ndata,14]
    
    datqual = (range_rms_ku <= 1500) & \
              ((swh_numval_ku > 18) & (swh_numval_ku < 255)) & \
              ((swh_ku > 0) & (swh_ku < 65535)) & \
              ((swh_rms_ku > 0) & (swh_rms_ku < 65535)) & \
              (swh_rms_ku < (996.1 - (0.0398 * swh_ku) + (0.0000132 * (swh_ku**2)))) & \
              (sig0_rms_ku <= 100) & \
              (wind_speed_alt < 65535) & \
              ((off_nadir_angle_ku_wvf >= -200) & (off_nadir_angle_ku_wvf <= 2500))

    swhqual = 2 * ((datqual == 0) | (flgqual == 1)) # quality is 2 for bad (datqual gives True for good)

    return swhqual


def processjas1nrt(fname, outdir):
    
    try:
        datain = open(fname,'r')
    except IOError, e:
        print 'file open error:', e
    else:
        print 'data file opened ok'

    # read header and extract information
    header = datain.read(2320) # PASS_FILE_HEADER size 2320

    atts = ('Reference_Software', 'Altimeter_Sensor_Name', 'Radiometer_Sensor_Name', \
            'Acquisition_Station_Name', 'Producer_Agency_Name')
    for label in atts:
        patn = '(' + label + ' = +)(.*?)( *;)'
        m = re.search(patn, header)
        if label == 'Reference_Software':
            srcsoft = m.group(2)
        if label == 'Altimeter_Sensor_Name':
            altsens = m.group(2)
        if label == 'Radiometer_Sensor_Name':
            radsens = m.group(2)
        if label == 'Acquisition_Station_Name':
            acqst = m.group(2)
        if label == 'Producer_Agency_Name':
            agency = m.group(2)        

    atts = ('Cycle_Number', 'Absolute_Revolution_Number', 'Pass_Data_Count')
    for label in atts:
        patn = '(' + label + ' = +)(\d+)(;|\.\d+)'
        m = re.search(patn, header)
        if label == 'Cycle_Number':
            cycle = m.group(2)
            cycle = "%03d" % int(cycle)
        if label == 'Absolute_Revolution_Number':
            absrev = int(m.group(2))
            passn = absrev % 254
            passn = "%03d" % passn
        if label == 'Pass_Data_Count':
            ndata = int(m.group(2))

    atts = ('Equator_Longitude', )
    for label in atts:
        patn = '(' + label + ' = \+)(\d+)(\.\d+)'
        m = re.search(patn, header)
        if label == 'Equator_Longitude':
            eqlon = m.group(2) + m.group(3)

            
    atts = ('Equator_Time', 'First_Measurement_Time', 'Last_Measurement_Time')
    for label in atts:
        patn = '(' + label + ' = )(\d+-\d+-\d+)(T)(\d+:\d+:\d+)(\.\d+)'
        m = re.search(patn, header)
        if label == 'Equator_Time':
            eqtime = m.group(2) + 'T' + m.group(4)
        if label == 'First_Measurement_Time':
            strtime = m.group(2) + ' ' + m.group(4)
            tfp = time.strptime(strtime, "%Y-%m-%d %H:%M:%S")
            ftime = time.strftime("%Y%m%d_%H%M%S", tfp)
            fmt = time.strftime("%Y-%m-%dT%H:%M:%S", tfp) # same as strtime
        if label == 'Last_Measurement_Time':
            strtime = m.group(2) + ' ' + m.group(4)
            tlp = time.strptime(strtime, "%Y-%m-%d %H:%M:%S")
            ltime = time.strftime("%Y%m%d_%H%M%S", tlp)
            lmt = time.strftime("%Y-%m-%dT%H:%M:%S", tlp)  # same as strtime

    # open nc file for output
    prefix = 'GW_L2P_ALT_JAS1_NRT_'
    #outfile = 'test.nc'

    #modif FKO
    outFileName = prefix + ftime + '_' + ltime + '_' + cycle + '_' + passn + '.nc'
    outfile = os.path.join( outdir, outFileName )
    ncfile = netCDF3.Dataset(outfile, 'w', format='NETCDF3_CLASSIC')

    # read input data records
    count = 0
    datarr = numpy.zeros((int(ndata),17), int) - 1   # array of -1 to take data
    flgarr = numpy.zeros((int(ndata),6), int) - 1   # array of -1 to take flags
    while True:
        record = datain.read(80)  # DATA_RECORD length
        if record:
            days, secs, msec, lat, lon, swh, swh_rms, swh_numval, sig0, sig0_c, sig0_rms, sig0_rms_c,  \
                  range_rms, range_rms_c, off_nadir_wf, windsp_alt, windsp_rad \
                  = struct.unpack('>III iI 8x HHB 3x HHHH 12x HH 2x h 6x H 2x H 4x', record)
            datarr[count,:] = [days, secs, msec, lat, lon, swh, swh_rms, swh_numval, \
                               sig0, sig0_c, sig0_rms, sig0_rms_c, range_rms, range_rms_c, \
                               off_nadir_wf, windsp_alt, windsp_rad]
            surface, alt_echo, rad_surf, qual_alt, qual_rad, alt_st \
                   = struct.unpack('>20x BBBBBB 54x', record)
            flgarr[count,:] = [surface, alt_echo, rad_surf, qual_alt, qual_rad, alt_st]
            count += 1

        else:
            break

    datain.close()

    # create dimensions
    #ndata = 5   # overide full length for development
    ncfile.createDimension(vaL.dim1, ndata)

    # create and initialise variables and variable attributes
    varobjdict = {}
    varsset = vaL.varsL2P[0:30] + vaL.varsL2P[32:42]
    for varname in varsset:
        varobj = ncfile.createVariable(varname,vaL.varsizes[varname],(vaL.dim1,))
        fval = vaL.fvals[vaL.fvalis[varname]]
        invals = [fval, vaL.lnames1[varname], vaL.snames[varname], vaL.units[varname], vaL.sources[varname], vaL.insts[varname], \
                  vaL.calforms[varname], vaL.calrefs[varname], vaL.valrefs[varname], vaL.qflags[varname], vaL.vmins[varname], \
                  vaL.vmaxs[varname], vaL.flvals[varname], vaL.flmasks[varname], vaL.flmeans[varname], vaL.scalefs[varname], vaL.coords[varname], vaL.commJAS1[varname]]
        setattribs(varobj, vaL.attnames, invals, len(vaL.attnames))

        if varname == 'time':
            varobj.calendar = 'gregorian'    # not set from variablesL2P
        if varname == 'swh_calibrated':
            varobj.calibration_formula = str(calibs['slope_swh']) + '*swh + ' + str(calibs['offs_swh'])
	    varobj.calibration_reference = 'Durrant T H, Greenslade D J M & Simmonds I, 2009, Validation of Jason-1 and Envisat remotely sensed wave heights, J. Atmos. Oce. Tech. 26, 123-134'
        if varname == 'bathymetry':
            varobj.add_offset = 0    # not set from variablesL2P
            varobj.coordinates = 'lon lat'  # set here to retain order
        if varname == 'surface_air_pressure':
            varobj.add_offset = 100000    # not set from variablesL2P
            varobj.coordinates = 'lon lat'  # set here to retain order

        varobj[:] = fval
        varobjdict[varname] = varobj

    # create global attributes

    globattd = vaL.globattdict
    fnbits = re.split('/', fname)
    sourcefile = fnbits[-1]

    for ga in vaL.globatts:
        setattr(ncfile, ga, globattd[ga])

    ncfile.title = 'GlobWave L2P derived from Jason-1 NRT (OSDR) Product'
    ncfile.history = time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime()) + ' UTC : Creation'
    ncfile.software_version = 'SatOC GlobWave Jason-1 NRT to L2P Processor ' + version
    ncfile.source_provider = agency
    ncfile.source_name = sourcefile
    ncfile.source_software = srcsoft
    ncfile.mission_name = 'Jason-1'
    ncfile.altimeter_sensor_name = altsens
    ncfile.radiometer_sensor_name = radsens
    ncfile.acq_station_name = acqst
    ncfile.cycle_number = cycle
    ncfile.pass_number = passn
    ncfile.equator_crossing_time = eqtime + ' UTC'
    ncfile.equator_crossing_longitude = eqlon
    ncfile.start_date = fmt + ' UTC'
    ncfile.stop_date = lmt + ' UTC'


    # transcribe or generate data

    sel = numpy.where(datarr[:,3:] == 65535)  # change unsigned short default to signed
    datarr[:,3:][sel] = 2**15 - 1

    # time:
    timediff = -9862*24*60*60    # Jason-1 time is relative to 1958-01-01
    varobj = varobjdict['time']
    varobj[:] = (datarr[0:ndata,0]*24*60*60) + (datarr[0:ndata,1]) + (datarr[0:ndata,2]/1000000.) + timediff

    # lat:
    varobj = varobjdict['lat']
    varobj[:] = datarr[0:ndata,3]/1.e6

    # lon:
    varobj = varobjdict['lon']
    varobj[:] = datarr[0:ndata,4]/1.e6

    # swh:
    varobj = varobjdict['swh']
    swharr = datarr[0:ndata,5].copy()
    varobj[:] = swharr

    # swh_calibrated:
    varobj = varobjdict['swh_calibrated']
    sel = numpy.where(swharr < 32767)
    if len(sel[0]) > 0:
        swhcal = swharr[sel]*calibs['slope_swh'] + calibs['offs_swh']/vaL.scalefs['swh']
        selovf = numpy.where(swhcal > 32767)
        selneg = numpy.where(swhcal < 0)
        swhcal[selovf] = 32767
        swhcal[selneg] = 0
        varobj[sel] = swhcal

    # swh_quality:
    varobj = varobjdict['swh_quality']
    flgvar, flgqual = setflagqual(ndata, flgarr)
    swhqualarr = setswhqual(ndata, datarr, flgqual)
    varobj[:] = swhqualarr

    # swh_standard_error:
    varobj = varobjdict['swh_standard_error']

    # swh_2nd:
    varobj = varobjdict['swh_2nd']

    # swh_2nd_calibrated:
    varobj = varobjdict['swh_2nd_calibrated']

    # swh_2nd_quality:
    varobj = varobjdict['swh_2nd_quality']

    # sigma0:
    varobj = varobjdict['sigma0']
    varobj[:] = datarr[0:ndata,8]

    # sigma0_calibrated:
    varobj = varobjdict['sigma0_calibrated']

    # sigma0_quality:
    varobj = varobjdict['sigma0_quality']
    varobj[:] = swhqualarr

    # sigma0_2nd:
    varobj = varobjdict['sigma0_2nd']
    varobj[:] = datarr[0:ndata,9]

    # sigma0_2nd_calibrated:
    varobj = varobjdict['sigma0_2nd_calibrated']

    # sigma0_2nd_quality:
    varobj = varobjdict['sigma0_2nd_quality']

    # wind_speed_alt:
    varobj = varobjdict['wind_speed_alt']
    varobj[:] = datarr[0:ndata,15]

    # wind_speed_alt_calibrated:
    varobj = varobjdict['wind_speed_alt_calibrated']

    # wind_speed_rad:
    varobj = varobjdict['wind_speed_rad']
    varobj[:] = datarr[0:ndata,16]

    # wind_speed_model_u:
    varobj = varobjdict['wind_speed_model_u']

    # wind_speed_model_v:
    varobj = varobjdict['wind_speed_model_v']

    # rejection_flags:
    varobj = varobjdict['rejection_flags']
    varobj[:] = flgvar

    # swh_rms:
    varobj = varobjdict['swh_rms']
    varobj[:] = datarr[0:ndata,6]

    # swh_rms_2nd:
    varobj = varobjdict['swh_rms_2nd']

    # swh_num_valid:
    varobj = varobjdict['swh_num_valid']
    swhnum = datarr[0:ndata,7]
    sel = numpy.where(swhnum < 127)
    varobj[sel] = swhnum[sel]

    # swh_num_valid_2nd:
    varobj = varobjdict['swh_num_valid_2nd']

    # sigma0_rms:
    varobj = varobjdict['sigma0_rms']
    varobj[:] = datarr[0:ndata,10]

    # sigma0_rms_2nd:
    varobj = varobjdict['sigma0_rms_2nd']
    varobj[:] = datarr[0:ndata,11]

    # sigma0_num_valid:
    varobj = varobjdict['sigma0_num_valid']
    varobj[sel] = swhnum[sel]

    # sigma0_num_valid_2nd:
    varobj = varobjdict['sigma0_num_valid_2nd']

    # peakiness:
    #varobj = varobjdict['peakiness']

    # peakiness_2nd:
    #varobj = varobjdict['peakiness_2nd']

    # off_nadir_angle_wf:
    varobj = varobjdict['off_nadir_angle_wf']
    varobj[:] = datarr[0:ndata,14]

    # off_nadir_angle_pf:
    varobj = varobjdict['off_nadir_angle_pf']

    # range_rms:
    varobj = varobjdict['range_rms']
    varobj[:] = datarr[0:ndata,12]

    # range_rms_2nd:
    varobj = varobjdict['range_rms_2nd']
    varobj[:] = datarr[0:ndata,13]

    # bathymetry:
    varobj = varobjdict['bathymetry']

    # distance_to_coast:
    varobj = varobjdict['distance_to_coast']

    # sea_surface_temperature
    varobj = varobjdict['sea_surface_temperature']

    # surface_air_temperature
    varobj = varobjdict['surface_air_temperature']

    # surface_air_humidity
    #varobj = varobjdict['surface_air_humidity']

    # surface_air_pressure
    varobj = varobjdict['surface_air_pressure']

    # close nc file
    ncfile.close()

    return outfile
