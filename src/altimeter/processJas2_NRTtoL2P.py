'processJas2_NRTtoL2P.py'

'process Jason-2 Altimetry NRT (OPN) data to GlobWave L2P'
'requires: variablesL2P.py version 1.6'
'Ellis Ash, SatOC, e.ash@satoc.eu'
'version 1.0, 10/02/10'
'version 1.1, 22/07/10, update to calibration logic and values'
'version 1.2, 17/08/10, include add_offset for surface_air_pressure, update to variablesL2P version, correction to cycle and pass in file name'
'version 1.3, 21/10/10, update to calibration logic to set overflow values to default and -ve values to zero, quality rejection of zero swh'
'version 1.4, 14/08/12, removal of off_nadir_angle_pf flag due to level 2 data update (version D)'


import re
import fnmatch
import os
import time
import numpy
import netCDF3
import variablesL2P as vaL


version = '1.4'
calibs = {'slope_swh': 1.041, \
          'offs_swh': -0.042}   # From GlobWave error analysis 


def setattribs(varobj, attnames, invals, nval):
    'loop to set variable attributes'
    count = 0
    while (count < nval):   # loop through attributes
        if invals[count]:   # invals is 0 if no attribute required for variable
            if attnames[count] == 'valid_min':  # needed to set 'valid_min' attribute to 0 (assigned -1)
                if invals[count] < 0:
                    invals[count] = numpy.array(0).astype(numpy.int8)
            setattr(varobj,attnames[count],invals[count])
        count += 1


def getflags(n, size=8, idlist=[0, 1, 2, 3, 4, 5, 6, 7]):
    'gives bit values of bytes'
    bStr = ''
    for bit in idlist:
        nshft = n >> (size - bit - 1)
        bStr = bStr + str(nshft % 2)
    return bStr


def setflagqual(ndata, datain):
    'set rejections flag and a flag quality variable'
    
    noflag = numpy.zeros(ndata).astype(int)
    base2 = 2**numpy.arange(32)

    hardware_0 = (datain.variables['alt_state_flag_oper'][0:ndata] > 0).astype(int)
    attitude_1 = noflag
    alt_land_4 = (datain.variables['surface_type'][0:ndata] > 0).astype(int)
    altocean_5 = (datain.variables['alt_echo_type'][0:ndata] > 0).astype(int)
    rad_land_6 = (datain.variables['surface_type'][0:ndata] > 1).astype(int)
    alt_rice_7 = (datain.variables['ice_flag'][0:ndata] > 0).astype(int)
    rad_rain_8 = (datain.variables['rain_flag'][0:ndata] > 0).astype(int)
    qual_ssh_11 = (datain.variables['qual_alt_1hz_range_ku'][0:ndata] > 0).astype(int)
    qual_swh_12 = (datain.variables['qual_alt_1hz_swh_ku'][0:ndata] > 0).astype(int)
    qual_sg0_13 = (datain.variables['qual_alt_1hz_sig0_ku'][0:ndata] > 0).astype(int)
    qual_orb_15 = noflag
    qualswh2_16 = (datain.variables['qual_alt_1hz_swh_c'][0:ndata] > 0).astype(int)
    qualsg02_17 = (datain.variables['qual_alt_1hz_sig0_c'][0:ndata] > 0).astype(int)
    qualonwf_18 = (datain.variables['qual_alt_1hz_off_nadir_angle_wf_ku'][0:ndata] > 0).astype(int)
    qualonpf_19 = noflag
    iceextdb_20 = noflag
    
    flgqual = altocean_5 | rad_land_6 | qual_ssh_11 | qual_swh_12 | qual_sg0_13
    rainice = alt_rice_7 | rad_rain_8

    flgvar = (base2[31]*hardware_0) + \
             (base2[27]*alt_land_4) + \
             (base2[26]*altocean_5) + \
             (base2[25]*rad_land_6) + \
             (base2[24]*alt_rice_7) + \
             (base2[23]*rad_rain_8) + \
             (base2[20]*qual_ssh_11) + \
             (base2[19]*qual_swh_12) + \
             (base2[18]*qual_sg0_13) + \
             (base2[15]*qualswh2_16) + \
             (base2[14]*qualsg02_17) + \
             (base2[13]*qualonwf_18) + \
             (base2[12]*qualonpf_19) + \
             (base2[0]*flgqual)

    return flgvar, flgqual, rainice


def setswhqual(ndata, datain, flgqual, rainice):

    swh_numval_ku = datain.variables['swh_numval_ku'][0:ndata]
    swh_ku = datain.variables['swh_ku'][0:ndata]
    swh_rms_ku = datain.variables['swh_rms_ku'][0:ndata]
    sig0_rms_ku = datain.variables['sig0_rms_ku'][0:ndata]
    wind_speed_alt = datain.variables['wind_speed_alt'][0:ndata]
    off_nadir_angle_wf_ku = datain.variables['off_nadir_angle_wf_ku'][0:ndata]
    
    datqual = ((swh_numval_ku > 18) & (swh_numval_ku < 127)) & \
              ((swh_ku > 0) & (swh_ku < 32767)) & \
              ((swh_rms_ku > 0) & (swh_rms_ku < 32767)) & \
              (sig0_rms_ku <= 100) & \
              (wind_speed_alt < 32767) & \
              ((off_nadir_angle_wf_ku >= -200) & (off_nadir_angle_wf_ku <= 2500))

    dq_arr=numpy.ma.getdata(datqual) # FKO
    swhqual = 2 * (( dq_arr == 0) | (flgqual == 1)) # quality is 2 for bad (datqual gives True for good)

    swhgood = numpy.where(swhqual == 0)
    swhqual[swhgood] = rainice[swhgood]    # quality is 0 for good, 1 for good but rain probable

    return swhqual


def processjas2nrt(fname, outdir):
    
    try:
        datain = netCDF3.Dataset(fname,'r')  # causes a runtime error if not available?
    except IOError, e:
        print 'file open error:', e
    else:
        print 'data file opened ok'

    # get size of time dimension
    ndata = len(datain.dimensions['time'])

    # open nc file for output
    prefix = 'GW_L2P_ALT_JAS2_NRT_'
    fbits = re.split('_', fname)
    #outfile = 'test.nc'

    #modif JFP (new nomenclature : JA2_OPN_2PdS150_189_20120804_203326_20120804_223116.nc )
    outFileName = prefix + fname[-34:-3] + '_' + fname[-42:-39] + '_' + fname[-38:-35] + '.nc'
    outfile = os.path.join( outdir, outFileName )
    ncfile = netCDF3.Dataset(outfile, 'w', format='NETCDF3_CLASSIC')

    # create dimensions
    ncfile.createDimension(vaL.dim1, ndata)

    # create and initialise variables and variable attributes
    varobjdict = {}
    varsset = vaL.varsL2P[0:30] + vaL.varsL2P[32:42]
    for varname in varsset:
        varobj = ncfile.createVariable(varname,vaL.varsizes[varname],(vaL.dim1,))
        fval = vaL.fvals[vaL.fvalis[varname]]
        invals = [fval, vaL.lnames1[varname], vaL.snames[varname], vaL.units[varname], vaL.sources[varname], vaL.insts[varname], \
                  vaL.calforms[varname], vaL.calrefs[varname], vaL.valrefs[varname], vaL.qflags[varname], vaL.vmins[varname], \
                  vaL.vmaxs[varname], vaL.flvals[varname], vaL.flmasks[varname], vaL.flmeans[varname], vaL.scalefs[varname], vaL.coords[varname], vaL.commJAS2[varname]]
        setattribs(varobj, vaL.attnames, invals, len(vaL.attnames))

        if varname == 'time':
            varobj.calendar = 'gregorian'    # not set from variablesL2P
        if varname == 'swh_calibrated':
            varobj.calibration_formula = str(calibs['slope_swh']) + '*swh + ' + str(calibs['offs_swh'])
            varobj.calibration_reference = 'Ash E R & Carter D J T, September 2010, Satellite wave data quality report, GlobWave Deliverable D.16'
        if varname == 'bathymetry':
            varobj.add_offset = 0    # not set from variablesL2P
            varobj.coordinates = 'lon lat'  # set here to retain order
        if varname == 'surface_air_pressure':
            varobj.add_offset = 100000    # not set from variablesL2P
            varobj.coordinates = 'lon lat'  # set here to retain order

        varobj[:] = fval
        varobjdict[varname] = varobj

    # create global attributes
    globattd = vaL.globattdict
    fnbits = re.split('/', fname)
    sourcefile = fnbits[-1]

    for ga in vaL.globatts:
        setattr(ncfile, ga, globattd[ga])

    ncfile.title = 'GlobWave L2P derived from Jason-2 NRT (OPN) Product'
    ncfile.history = time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime()) + ' UTC : Creation'
    ncfile.software_version = 'SatOC GlobWave Jason-2 NRT to L2P Processor ' + version
    ncfile.source_provider = 'NOAA'
    ncfile.source_name = sourcefile
    #ncfile.source_software = srcsoft
    ncfile.mission_name = getattr(datain,'mission_name')
    ncfile.altimeter_sensor_name = getattr(datain,'altimeter_sensor_name')
    ncfile.radiometer_sensor_name = getattr(datain,'radiometer_sensor_name')
    ncfile.acq_station_name = getattr(datain,'acq_station_name')
    ncfile.cycle_number = getattr(datain,'cycle_number')
    ncfile.pass_number = getattr(datain,'pass_number')
    ncfile.equator_crossing_time = re.sub(' ', 'T', getattr(datain,'equator_time')) + ' UTC'
    ncfile.equator_crossing_longitude = getattr(datain,'equator_longitude')
    ncfile.start_date = re.sub(' ', 'T', getattr(datain,'first_meas_time')) + ' UTC'
    ncfile.stop_date = re.sub(' ', 'T', getattr(datain,'last_meas_time')) + ' UTC'


    # transcribe or generate data

    for varname in vaL.varsL2P:
        j2name = vaL.j2names[varname]
        if (j2name):    # all the straight transcription of data
            varobj = varobjdict[varname]
            varobj[:] = datain.variables[j2name][0:ndata]
        elif (varname == 'time'):
            j2name = varname
            timediff = 5478*24*60*60    # Jason-2 time is relative to 2000-01-01
            varobj = varobjdict[varname]
            varobj[:] = datain.variables[j2name][0:ndata] + timediff
        elif (varname == 'lat'):
            j2name = varname
            varobj = varobjdict[varname]
            varobj[:] = datain.variables[j2name][0:ndata]/1.e6
        elif (varname == 'lon'):
            j2name = varname
            varobj = varobjdict[varname]
            varobj[:] = datain.variables[j2name][0:ndata]/1.e6
        elif (varname == 'swh_calibrated'):
            j2name = 'swh_ku'
            varobj = varobjdict[varname]
            swharr = datain.variables[j2name][0:ndata]
            sel = numpy.ma.where(swharr < 32767)
            if len(sel[0]) > 0:
                swhcal = swharr[sel]*calibs['slope_swh'] + calibs['offs_swh']/vaL.scalefs['swh']
                selovf = numpy.where(swhcal > 32767)
                selneg = numpy.where(swhcal < 0)
                swhcal[selovf] = 32767
                swhcal[selneg] = 0
                varobj[sel] = swhcal
        elif (varname == 'swh_quality'): 
            varobj = varobjdict[varname]
            flgvar, flgqual, rainice = setflagqual(ndata, datain)
            swhqualarr = setswhqual(ndata, datain, flgqual, rainice)
            varobj[:] = swhqualarr
        elif (varname == 'sigma0_quality'):
            varobj = varobjdict[varname]
            varobj[:] = swhqualarr
        elif (varname == 'rejection_flags'):
            varobj = varobjdict[varname]
            varobj[:] = flgvar

    # close nc file
    ncfile.close()

    return outfile
