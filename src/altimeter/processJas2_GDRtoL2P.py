'processJas2_GDRtoL2P.py'

'process Jason-2 Altimetry GDR data to GlobWave L2P'
'requires: variablesL2P.py version 1.9'
'Ellis Ash, SatOC, e.ash@satoc.eu'
'version 1.0, 23/12/09'
'version 1.1, 18/01/10, minor update to flag logic after processing error'
'version 1.2, 18/01/10, reworked setflagqual function for faster array processing'
'version 1.3, 05/03/10, adjusted logic for and commented out calibration pending Jason-2 values'
'version 1.4, 16/03/09, include add_offset for surface_air_pressure, update to variablesL2P version'
'version 1.5, 30/03/09, correction to cycle and pass numbers in file name'
'version 1.6, 03/11/10, update to calibration'
'version 1.7, 02/06/11, included swh error calculation and set negative calibrated values to zero'
'version 1.8, 26/01/12, added missing errors dictionary'
'version 1.9, 04/09/13, adaptation to Jason2 version D'
'version 1.10, 11/04/14, scaling corrections for use of netCDF4 module'
'version 1.11, 01/05/14, correction to fill value assignment, change of calibration reference'
'version 1.12, 04/11/14, adaptation to netCDF4 format. CPR.'

import re
import fnmatch
import os
import time
import datetime
import numpy
import netCDF4
import variablesL2P as vaL

version = '1.12'
calibs = {'slope_swh': 1.0149, \
          'offs_swh': 0.0277}   # Jason-2 calibration from Globwave errors analysis
errors = {'slope': 0.052, \
          'offs': 0.058}        


def setattribs(varobj, attnames, invals, nval):
    'loop to set variable attributes'
    count = 0
    while (count < nval):   # loop through attributes
        if invals[count]:   # invals is 0 if no attribute required for variable
            if attnames[count] == 'valid_min':  # needed to set 'valid_min' attribute to 0 (assigned -1)
                if invals[count] < 0:
                    invals[count] = numpy.array(0).astype(numpy.int8)
            if attnames[count] != '_FillValue':
                setattr(varobj,attnames[count],invals[count])
        count += 1


def getflags(n, size=8, idlist=[0, 1, 2, 3, 4, 5, 6, 7]):
    'gives bit values of bytes'
    bStr = ''
    for bit in idlist:
        nshft = n >> (size - bit - 1)
        bStr = bStr + str(nshft % 2)
    return bStr


def setflagqual(ndata, datain):
    'set rejections flag and a flag quality variable'
    
    noflag = numpy.zeros(ndata).astype(int)
    base2 = 2**numpy.arange(32)

    hardware_0 = (datain.variables['alt_state_flag_oper'][0:ndata] > 0).astype(int)
    attitude_1 = noflag
    alt_land_4 = (datain.variables['surface_type'][0:ndata] > 0).astype(int)
    altocean_5 = (datain.variables['alt_echo_type'][0:ndata] > 0).astype(int)
    rad_land_6 = (datain.variables['surface_type'][0:ndata] > 1).astype(int)
    alt_rice_7 = (datain.variables['ice_flag'][0:ndata] > 0).astype(int)
    rad_rain_8 = (datain.variables['rain_flag'][0:ndata] > 0).astype(int)
    qual_ssh_11 = (datain.variables['qual_alt_1hz_range_ku'][0:ndata] > 0).astype(int)
    qual_swh_12 = (datain.variables['qual_alt_1hz_swh_ku'][0:ndata] > 0).astype(int)
    qual_sg0_13 = (datain.variables['qual_alt_1hz_sig0_ku'][0:ndata] > 0).astype(int)
    qual_orb_15 = noflag
    qualswh2_16 = (datain.variables['qual_alt_1hz_swh_c'][0:ndata] > 0).astype(int)
    qualsg02_17 = (datain.variables['qual_alt_1hz_sig0_c'][0:ndata] > 0).astype(int)
    qualonwf_18 = (datain.variables['qual_alt_1hz_off_nadir_angle_wf_ku'][0:ndata] > 0).astype(int)
    qualonpf_19 = noflag
    iceextdb_20 = noflag
    
    flgqual = altocean_5 | rad_land_6 | qual_ssh_11 | qual_swh_12 | qual_sg0_13
    rainice = alt_rice_7 | rad_rain_8

    flgvar = (base2[31]*hardware_0) + \
             (base2[27]*alt_land_4) + \
             (base2[26]*altocean_5) + \
             (base2[25]*rad_land_6) + \
             (base2[24]*alt_rice_7) + \
             (base2[23]*rad_rain_8) + \
             (base2[20]*qual_ssh_11) + \
             (base2[19]*qual_swh_12) + \
             (base2[18]*qual_sg0_13) + \
             (base2[15]*qualswh2_16) + \
             (base2[14]*qualsg02_17) + \
             (base2[13]*qualonwf_18) + \
             (base2[0]*flgqual)

    return flgvar, flgqual, rainice


def setswhqual(ndata, datain, flgqual, rainice):

    swh_numval_ku = datain.variables['swh_numval_ku'][0:ndata]
    swh_ku = datain.variables['swh_ku'][0:ndata]
    swh_rms_ku = datain.variables['swh_rms_ku'][0:ndata]
    sig0_rms_ku = datain.variables['sig0_rms_ku'][0:ndata]
    wind_speed_alt = datain.variables['wind_speed_alt'][0:ndata]
    off_nadir_angle_wf_ku = datain.variables['off_nadir_angle_wf_ku'][0:ndata]
    
    datqual = ((swh_numval_ku > 18) & (swh_numval_ku < 127)) & \
              (swh_ku < 32767) & \
              ((swh_rms_ku > 0) & (swh_rms_ku < 32767)) & \
              (sig0_rms_ku <= 100) & \
              (wind_speed_alt < 32767) & \
              ((off_nadir_angle_wf_ku >= -200) & (off_nadir_angle_wf_ku <= 2500))

    swhqual = 2 * ((datqual == 0) | (flgqual == 1)) # quality is 2 for bad (datqual gives True for good)

    swhgood = numpy.where(swhqual == 0)
    swhqual[swhgood] = rainice[swhgood]    # quality is 0 for good, 1 for good but rain probable

    return swhqual


def processjas2gdr(fname, outdir):
    
    try:
        datain = netCDF4.Dataset(fname,'r')  # causes a runtime error if not available?
    except IOError, e:
        print 'file open error:', e
    else:
        print 'data file opened ok'

    # get size of time dimension
    ndata = len(datain.dimensions['time'])

    # get global attributes
    for name in datain.ncattrs():
        getattr(datain,name)
        if re.match('^xref', name):
            break

    # open nc file for output
    prefix = 'GW_L2P_ALT_JAS2_GDR_'
	#JA2_GPN_2PdP131_111_20120126_091203_20120126_100815.nc.bz2
    cycle = os.path.basename(fname)[12:15]
    orbit = fname.split('_')[-5]
    dates = '_'.join(fname.split('.')[0].split('_')[-4:])
    date = datetime.datetime.strptime(dates[0:15], '%Y%m%d_%H%M%S')
    datedir = os.path.join(outdir, date.strftime('%Y/%j'))
    if not os.path.exists(datedir):
        os.makedirs(datedir)
    outfile = os.path.join(datedir, prefix + dates + '_' + cycle + '_' + orbit + '.nc')
    print outfile
    #outfile = outdir + prefix + fname[-31:] + '_' + fname[-39:-36] + '_' + fname[-35:-32] + '.nc'
    ncfile = netCDF4.Dataset(outfile, 'w', format='NETCDF4_CLASSIC')

    # create dimensions
    ncfile.createDimension(vaL.dim1, ndata)

    # create and initialise variables and variable attributes
    varobjdict = {}
    # select variables from the shared list in variablesL2P
    varsset = vaL.varsL2P[0:8] + vaL.varsL2P[10:11] + vaL.varsL2P[13:14] +  vaL.varsL2P[16:17] +  vaL.varsL2P[18:30] + vaL.varsL2P[32:33] + vaL.varsL2P[34:37]
    
    for varname in varsset:
        fval = vaL.fvals[vaL.fvalis[varname]]
        varobj = ncfile.createVariable(varname,vaL.varsizes[varname],(vaL.dim1,), fill_value=fval)
        invals = [vaL.lnames1[varname], vaL.snames[varname], vaL.units[varname], vaL.sources[varname], vaL.insts[varname], \
                  vaL.calforms[varname], vaL.calrefs[varname], vaL.valrefs[varname], vaL.qflags[varname], vaL.vmins[varname], \
                  vaL.vmaxs[varname], vaL.flvals[varname], vaL.flmasks[varname], vaL.flmeans[varname], vaL.coords[varname], vaL.commJAS2[varname]]
        setattribs(varobj, vaL.attnames_nc4, invals, len(vaL.attnames_nc4))

        if varname == 'time':
            varobj.calendar = 'gregorian'    # not set from variablesL2P
        if varname == 'swh_calibrated':
            varobj.calibration_formula = str(calibs['slope_swh']) + '*swh + ' + str(calibs['offs_swh'])
            varobj.calibration_reference = 'Ash E R & Carter D J T, September 2010, Satellite wave data quality report, GlobWave Deliverable D.16'
        if varname == 'bathymetry':
            #varobj.add_offset = 0    # not set from variablesL2P -->
            varobj.coordinates = 'lon lat'  # set here to retain order
        if varname == 'surface_air_pressure':
            #varobj.add_offset = 100000    # not set from variablesL2P -->
            varobj.coordinates = 'lon lat'  # set here to retain order

        varobj[:] = fval
        varobjdict[varname] = varobj

    # create global attributes
    globattd = vaL.globattdict
    fnbits = re.split('/', fname)
    sourcefile = fnbits[-1]

    for ga in vaL.globatts:
        setattr(ncfile, ga, globattd[ga])

    ncfile.title = 'GlobWave L2P derived from Jason-2 GDR (GPN) Product'
    ncfile.history = time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime()) + ' UTC : Creation'
    ncfile.software_version = 'SatOC GlobWave Jason-2 GDR to L2P Processor ' + version
    ncfile.source_provider = 'CNES'
    ncfile.source_name = sourcefile
    #ncfile.source_software = srcsoft
    ncfile.mission_name = getattr(datain,'mission_name')
    ncfile.altimeter_sensor_name = getattr(datain,'altimeter_sensor_name')
    ncfile.radiometer_sensor_name = getattr(datain,'radiometer_sensor_name')
    ncfile.acq_station_name = getattr(datain,'acq_station_name')
    ncfile.cycle_number = getattr(datain,'cycle_number')
    ncfile.pass_number = getattr(datain,'pass_number')
    ncfile.equator_crossing_time = re.sub(' ', 'T', getattr(datain,'equator_time')) + ' UTC'
    ncfile.equator_crossing_longitude = getattr(datain,'equator_longitude')
    ncfile.start_date = re.sub(' ', 'T', getattr(datain,'first_meas_time')) + ' UTC'
    ncfile.stop_date = re.sub(' ', 'T', getattr(datain,'last_meas_time')) + ' UTC'


    # transcribe or generate data (j2name from origin file, varname in GW file)
#    for varname in vaL.varsL2P:
    for varname in varsset :
        j2name = vaL.j2names[varname]
        if (varname in ['swh_num_valid', 'swh_num_valid_2nd', 'sigma0_num_valid', 'sigma0_num_valid_2nd', 'bathymetry']):    # all the straight transcription of data
            varobj = varobjdict[varname]
            varobj[:] = datain.variables[j2name][0:ndata]
            
        elif (varname in ['sigma0', 'sigma0_calibrated', 'sigma0_2nd', 'sigma0_2nd_calibrated', 'wind_speed_alt', 'wind_speed_alt_calibrated', 'wind_speed_rad', \
                          'wind_speed_model_u', 'wind_speed_model_v', 'sigma0_rms', 'sigma0_rms_2nd']):
            # scale factor 0.01
            varobj = varobjdict[varname]
            datain.variables[j2name].set_auto_maskandscale(True)
            #varobj[:] = datain.variables[j2name][0:ndata] * 0.01 # ok if maskandscale at False
            varobj[:] = datain.variables[j2name][0:ndata]
        
        elif (varname in ['swh_2nd', 'swh_2nd_calibrated', 'swh', 'swh_rms', 'swh_rms_2nd']):
            # scale factor 0.001
            varobj = varobjdict[varname]
            datain.variables[j2name].set_auto_maskandscale(True)
            #varobj[:] = datain.variables[j2name][0:ndata] * 0.001 # ok if maskandscale at False
            varobj[:] = datain.variables[j2name][0:ndata]
        
        elif (varname in ['off_nadir_angle_wf', 'range_rms', 'range_rms_2nd']):
            # scale factor 0.0001
            varobj = varobjdict[varname]
            datain.variables[j2name].set_auto_maskandscale(True)
            #varobj[:] = datain.variables[j2name][0:ndata] * 0.0001 # ok if maskandscale at False
            varobj[:] = datain.variables[j2name][0:ndata]
            
        elif (varname in ['lat', 'lon']):
            # scale factor 1.e-6
            varobj = varobjdict[varname]
            datain.variables[j2name].set_auto_maskandscale(True)
            #varobj[:] = datain.variables[j2name][0:ndata] * 1.e-6  # ok if maskandscale at False
            varobj[:] = datain.variables[j2name][0:ndata]
            
        elif (varname == 'time'):
            j2name = varname
            timediff = 5478*24*60*60    # Jason-2 time is relative to 2000-01-01
            varobj = varobjdict[varname]
            varobj[:] = datain.variables[j2name][0:ndata] + timediff
                        
        elif (varname == 'swh_calibrated'):
            j2name = 'swh_ku'
            varobj = varobjdict[varname]
            datain.variables[j2name].set_auto_maskandscale(True)
            swharr = datain.variables[j2name][0:ndata] #* 0.001
            sel = swharr.nonzero()
            swhsel = swharr[sel]
            swhcal = swhsel*calibs['slope_swh'] + calibs['offs_swh']
            selovf = numpy.where(swhcal > 32.767)
            selneg = numpy.where(swhcal < 0)
            if len(selovf[0]) > 0: swhcal[selovf] = 32.767
            if len(selneg[0]) > 0: swhcal[selneg] = 0
            varobj[sel] = swhcal
               
        elif (varname == 'swh_standard_error'):
            selgt1 = numpy.where((swharr > 1.000) & (swharr < 32.767))
            selle1 = numpy.where(swharr <= 1.000)
            varobj = varobjdict[varname]
            if len(selgt1[0]) > 0:
                varobj[selgt1] = swharr[selgt1]*errors['slope'] + errors['offs']
            if len(selle1[0]) > 0:
                varobj[selle1] = errors['slope'] + errors['offs']
            
        elif (varname == 'swh_quality'):
            varobj = varobjdict[varname]
            flgvar, flgqual, rainice = setflagqual(ndata, datain)
            swhqualarr = setswhqual(ndata, datain, flgqual, rainice)
            varobj[:] = swhqualarr
            
#        elif (varname == 'sigma0_quality'):  # swh_quality copy
#            varobj = varobjdict[varname]
#            varobj[:] = swhqualarr
            
        elif (varname == 'rejection_flags'):
            varobj = varobjdict[varname]
            varobj[:] = flgvar
        
                
        
        # should added by another script : distance_to_coast
        

    # close nc file
    ncfile.close()
    datain.close()


if __name__=='__main__':
    
    import sys
    
    infile = sys.argv[1]
    outdir = sys.argv[2]
    
    processjas2gdr (infile, outdir)