'processJas1_GDRtoL2P.py'

'process Jason-1 Altimetry GDR data to GlobWave L2P'
'requires: variablesL2P.py version 1.4'
'Ellis Ash, SatOC, e.ash@satoc.eu'
'version 1.0, 24/11/09'
'version 1.1, 26/11/09, updates to global attributes, corrections to variable attributes, addition of flag_rejection flag, change of calibration logic' 
'version 1.2, 23/12/09, removal redundant variables, add_offset attribute for bathymetry, surface_temperature -> surfce_air_temperature, change scaling logic, sigma0 quality as for swh'
'version 1.3, 05/02/09, fix to flags logic, conversion of default values from unsigned to signed'
'version 1.4, 05/03/09, minor corrections to default values'
'version 1.5, 11/03/09, include add_offset for surface_air_pressure, update to variablesL2P version'
'version 1.6, 02/06/11, included swh error calculation and set negative calibrated values to zero'
'version 1.7, 06/12/11, correction to the time point 65535 seconds into the day'
'version 1.8, 31/07/14, adaptation to netCDF4 format'
'version 1.9, 14/11/14, homogenize format to other GW products. CPR'



import re
import fnmatch
import os
import time
import struct
import numpy
import netCDF4
import variablesL2P as vaL
import datetime

version = '1.9'

# GDR version b
calibs = {'slope_swh': 1.0250,
          'offs_swh': 0.0588}
errors = {'slope': 0.052,
          'offs': 0.055}
# GDR version c
calibs = {'slope_swh': 1.0211,
          'offs_swh': 0.0139}


def setattribs(varobj, attnames, invals, nval):
    'loop to set variable attributes'
    count = 0
    while (count < nval):   # loop through attributes
        if invals[count]:   # invals is 0 if no attribute required for variable
            if attnames[count] == 'valid_min':  # needed to set 'valid_min' attribute to 0 (assigned -1)
                if invals[count] < 0:
                    invals[count] = numpy.array(0).astype(numpy.int8)
            if attnames[count] != '_FillValue':
                setattr(varobj, attnames[count], invals[count])
        count += 1


def getflags(n, size=8, idlist=[0, 1, 2, 3, 4, 5, 6, 7]):
    'gives bit values of bytes'
    bStr = ''
    for bit in idlist:
        nshft = n >> (size - bit - 1)
        bStr = bStr + str(nshft % 2)
    return bStr


def setflagqual(ndata, flgarr):
    'set rejections flag and a flag quality variable'
    # array of -1 to take flags
    flgvar = numpy.zeros((int(ndata)), int) - 1
    # array of -1 to take flag related quality
    flgqual = numpy.zeros((int(ndata)), int) - 1
    # array of -1 to take rain or ice flag
    rainice = numpy.zeros((int(ndata)), int) - 1
    spare1 = '0'
    spare10 = '0000000000'

    count = 0
    while (count < ndata):   # loop for all data points
        hardware_0 = getflags(flgarr[count, 5], 8, [6, ])
        attitude_1 = '0'
        alt_land_4 = str(int(getflags(flgarr[count, 0], 8, [6, 7]) > '00'))
        altocean_5 = str(int(flgarr[count, 1] == 1))
        rad_land_6 = str(int(getflags(flgarr[count, 0], 8, [6, 7]) == '01'))
        alt_rice_7 = str(int(flgarr[count, 7] == 1))
        rad_rain_8 = str(int(flgarr[count, 6] == 1))
        qual_ssh_11 = getflags(flgarr[count, 3], 8, [7,])
        qual_swh_12 = getflags(flgarr[count, 3], 8, [5,])
        qual_sg0_13 = getflags(flgarr[count, 3], 8, [3,])
        qual_orb_15 = '0'
        qualswh2_16 = getflags(flgarr[count, 3], 8, [4,])
        qualsg02_17 = getflags(flgarr[count, 3], 8, [2,])
        qualonwf_18 = getflags(flgarr[count, 3], 8, [1,])
        qualonpf_19 = getflags(flgarr[count, 3], 8, [0,])
        iceextdb_20 = '0'

        flgstr = hardware_0 + attitude_1 + spare1 + spare1 + alt_land_4 + altocean_5 + rad_land_6 + alt_rice_7 + \
                 rad_rain_8 + spare1 + spare1 + qual_ssh_11 + qual_swh_12 + qual_sg0_13 + spare1 + qual_orb_15 + \
                 qualswh2_16 + qualsg02_17 + qualonwf_18 + qualonpf_19 + iceextdb_20 + spare10

        flgsnok = int(altocean_5) or int(rad_land_6) or int(qual_ssh_11) or int(qual_swh_12) or int(qual_sg0_13)
        flgqual[count] = flgsnok
        flgstr = flgstr + str(flgsnok)   # set bit 31 if rejected by other flags

        flgint = int(flgstr, 2)
        if int(flgstr, 2) >= 2**31:   # integer is signed
            flgint = int(int(flgstr, 2) - 2**32)
        flgvar[count] = flgint

        rainice[count] = int(alt_rice_7) or int(rad_rain_8)

        count += 1

    return flgvar, flgqual, rainice


def setswhqual(ndata, datarr, flgqual, rainice):

    range_rms_ku = datarr[0:ndata, 5]
    swh_numval_ku = datarr[0:ndata, 11]
    swh_ku = datarr[0:ndata, 7]
    swh_rms_ku = datarr[0:ndata, 9]
    sig0_rms_ku = datarr[0:ndata, 15]
    wind_speed_alt = datarr[0:ndata, 24]
    off_nadir_angle_ku_wvf = datarr[0:ndata, 19]

    datqual = (range_rms_ku <= 1500) & \
              ((swh_numval_ku > 18) & (swh_numval_ku < 255)) & \
              (swh_ku < 65535) & \
              ((swh_rms_ku > 0) & (swh_rms_ku < 65535)) & \
              (swh_rms_ku < (996.1 - (0.0398 * swh_ku) + (0.0000132 * (swh_ku**2)))) & \
              (sig0_rms_ku <= 100) & \
              (wind_speed_alt < 65535) & \
              ((off_nadir_angle_ku_wvf >= -200) & (off_nadir_angle_ku_wvf <= 2500))

    swhqual = 2 * ((datqual == 0) | (flgqual == 1)) # quality is 2 for bad (datqual gives True for good)

    swhgood = numpy.where(swhqual == 0)
    swhqual[swhgood] = rainice[swhgood]    # quality is 0 for good, 1 for good but rain/ice probable

    return swhqual


def process(fname, outdir):
    """Process a Jason-1 GDR file to GlobWave specs"""
    try:
        datain = open(fname, 'r')
    except IOError, e:
        print 'file open error:', e
    else:
        print 'data file opened ok'

    # read header and extract information
    header = datain.read(3520)  # PASS_FILE_HEADER size 3520
    # print header

    atts = ('Reference_Software',
            'Altimeter_Sensor_Name',
            'Radiometer_Sensor_Name',
            'Acquisition_Station_Name',
            'Producer_Agency_Name')
    for label in atts:
        patn = '(' + label + ' = +)(.*?)( *;)'
        m = re.search(patn, header)
        if label == 'Reference_Software':
            srcsoft = m.group(2)
        if label == 'Altimeter_Sensor_Name':
            altsens = m.group(2)
        if label == 'Radiometer_Sensor_Name':
            radsens = m.group(2)
        if label == 'Acquisition_Station_Name':
            acqst = m.group(2)
        if label == 'Producer_Agency_Name':
            agency = m.group(2)

    atts = ('Cycle_Number', 'Pass_Number', 'Pass_Data_Count')
    for label in atts:
        patn = '(' + label + ' = +)(\d+)(;|\.\d+)'
        m = re.search(patn, header)
        print "\n m = ",m
        if label == 'Cycle_Number':
            cycle = m.group(2)
            cycle = "%03d" % int(cycle)
            print 'cycle:' + cycle
        if label == 'Pass_Number':
            passn = m.group(2)
            passn = "%03d" % int(passn)
            print 'pass:' + passn
        if label == 'Pass_Data_Count':
            ndata = m.group(2)
            ndata = int(ndata)

    atts = ('Equator_Longitude', )
    for label in atts:
        patn = '(' + label + ' = \+)(\d+)(\.\d+)'
        m = re.search(patn, header)
        if label == 'Equator_Longitude':
            eqlon = m.group(2) + m.group(3)

    atts = ('Equator_Time', 'First_Measurement_Time', 'Last_Measurement_Time')
    for label in atts:
        patn = '(' + label + ' = )(\d+-\d+-\d+)(T)(\d+:\d+:\d+)(\.\d+)'
        m = re.search(patn, header)
        if label == 'Equator_Time':
            eqtime = m.group(2) + 'T' + m.group(4)
        if label == 'First_Measurement_Time':
            strtime = m.group(2) + ' ' + m.group(4)
            tfp = time.strptime(strtime, "%Y-%m-%d %H:%M:%S")
            ftime = time.strftime("%Y%m%d_%H%M%S", tfp)
            fmt = time.strftime("%Y-%m-%dT%H:%M:%S", tfp) # same as strtime
        if label == 'Last_Measurement_Time':
            strtime = m.group(2) + ' ' + m.group(4)
            tlp = time.strptime(strtime, "%Y-%m-%d %H:%M:%S")
            ltime = time.strftime("%Y%m%d_%H%M%S", tlp)
            lmt = time.strftime("%Y-%m-%dT%H:%M:%S", tlp)  # same as strtime

    # open nc file for output
    prefix = 'GW_L2P_ALT_JAS1_GDR_'  
    date = datetime.datetime.strptime(ltime, '%Y%m%d_%H%M%S')
    datedir = os.path.join(outdir, date.strftime('%Y/%j'))
    print datedir  ##
    if not os.path.exists(datedir):
        os.makedirs(datedir)
    outfile = os.path.join(datedir,prefix + ftime + '_' + ltime + '_' + cycle + '_' + passn + '.nc')
    ncfile = netCDF4.Dataset(outfile, 'w', format='NETCDF4_CLASSIC')

    # read input data records
    count = 0
    datarr = numpy.zeros((int(ndata),26), int) - 1   # array of -1 to take data
    flgarr = numpy.zeros((int(ndata),8), int) - 1   # array of -1 to take flags
    while True:
        record = datain.read(440)  # DATA_RECORD length
        if record:
            days, secs, msec, lat, lon, range_rms, range_rms_c, swh, swh_c, swh_rms, swh_rms_c, \
                  swh_numval, swh_numval_c, sig0, sig0_c, sig0_rms, sig0_rms_c, sig0_numval, sig0_numval_c, \
                  off_nadir_wf, off_nadir_pf, bathy, windsp_mod_u, windsp_mod_v, windsp_alt, windsp_rad \
                  = struct.unpack('>III iI 268x HH 36x HHHH BB 4x HHHH BB 18x hh 18x h 26x hhHH 12x', record)
            datarr[count,:] = [days, secs, msec, lat, lon, range_rms, range_rms_c, swh, swh_c, swh_rms, swh_rms_c, \
                               swh_numval, swh_numval_c, sig0, sig0_c, sig0_rms, sig0_rms_c, sig0_numval, sig0_numval_c, \
                               off_nadir_wf, off_nadir_pf, bathy, windsp_mod_u, windsp_mod_v, windsp_alt, windsp_rad]
            surface, alt_echo, rad_surf, qual_alt, qual_rad, alt_st, rain, ice \
                   = struct.unpack('>20x BBBB x BB 407x BB 4x', record)
            flgarr[count,:] = [surface, alt_echo, rad_surf, qual_alt, qual_rad, alt_st, rain, ice]
            count += 1
        else:
            break

    datain.close()

    # create dimensions
    ncfile.createDimension(vaL.dim1, ndata)

    # create and initialise variables and variable attributes
    varobjdict = {}
    varsset = vaL.varsL2P[0:8] + vaL.varsL2P[10:11] + vaL.varsL2P[13:14] +  vaL.varsL2P[16:17] +  vaL.varsL2P[18:30] + vaL.varsL2P[32:37]
    
    for varname in varsset:
        fval = vaL.fvals[vaL.fvalis[varname]]
        varobj = ncfile.createVariable(varname,vaL.varsizes[varname],(vaL.dim1,), fill_value=fval)
        invals = [fval, vaL.lnames1[varname], vaL.snames[varname], vaL.units[varname], vaL.sources[varname], vaL.insts[varname], \
                  vaL.calforms[varname], vaL.calrefs[varname], vaL.valrefs[varname], vaL.qflags[varname], vaL.vmins[varname], \
                  vaL.vmaxs[varname], vaL.flvals[varname], vaL.flmasks[varname], vaL.flmeans[varname], vaL.coords[varname], vaL.commJAS1[varname]]
        setattribs(varobj, vaL.attnames, invals, len(vaL.attnames))

        if varname == 'time':
            varobj.calendar = 'gregorian'    # not set from variablesL2P
        if varname == 'swh_calibrated':
            varobj.calibration_formula = str(calibs['slope_swh']) + '*swh + ' + str(calibs['offs_swh'])
        if varname == 'bathymetry':
            varobj.coordinates = 'lon lat'  # set here to retain order
#        if varname == 'surface_air_pressure':
#            varobj.coordinates = 'lon lat'  # set here to retain order

        varobj[:] = fval
        varobjdict[varname] = varobj

    # create global attributes
    globattd = vaL.globattdict
    fnbits = re.split('/', fname)
    sourcefile = fnbits[-1]

    for ga in vaL.globatts:
        setattr(ncfile, ga, globattd[ga])

    ncfile.title = 'GlobWave L2P derived from Jason-1 GDR Product'
    ncfile.history = time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime()) + ' UTC : Creation'
    ncfile.software_version = 'SatOC GlobWave Jason-1 GDR to L2P Processor ' + version
    ncfile.source_provider = agency
    ncfile.source_name = sourcefile
    ncfile.source_software = srcsoft
    ncfile.mission_name = 'Jason-1'
    ncfile.altimeter_sensor_name = altsens
    ncfile.radiometer_sensor_name = radsens
    ncfile.acq_station_name = acqst
    ncfile.cycle_number = cycle
    ncfile.pass_number = passn
    ncfile.equator_crossing_time = eqtime + ' UTC'
    ncfile.equator_crossing_longitude = eqlon
    ncfile.start_date = fmt + ' UTC'
    ncfile.stop_date = lmt + ' UTC'

    # transcribe or generate data

    sel = numpy.where(datarr[:,3:] == 65535)  # change unsigned short default to signed
    datarr[:,3:][sel] = 2**15 - 1

    # time:
    timediff = -9862*24*60*60    # Jason-1 time is relative to 1958-01-01
    varobj = varobjdict['time']
    varobj[:] = (datarr[0:ndata,0]*24*60*60) + (datarr[0:ndata,1]) + (datarr[0:ndata,2]/1000000.) + timediff

    # lat:
    varobj = varobjdict['lat']
    varobj[:] = datarr[0:ndata, 3] / 1.e6

    # lon:
    varobj = varobjdict['lon']
    lon = datarr[0:ndata, 4] / 1.e6
    lon[lon > 180.] -= 360.
    varobj[:] = lon

    # swh:
    varobj = varobjdict['swh']
    swharr = datarr[0:ndata, 7].copy()
    swharr = numpy.ma.masked_equal(swharr, 32767) * 0.001
    varobj[:] = swharr

    # swh_calibrated:
    varobj = varobjdict['swh_calibrated']
    varobj[:] = swharr*calibs['slope_swh'] + calibs['offs_swh']

    # swh_quality:
    varobj = varobjdict['swh_quality']
    flgvar, flgqual, rainice = setflagqual(ndata, flgarr)
    swhqualarr = setswhqual(ndata, datarr, flgqual, rainice)
    varobj[:] = swhqualarr

    # swh_standard_error:
    selle1 = numpy.ma.where(swharr <= 1)
    varobj = varobjdict['swh_standard_error']
    swherr = swharr*errors['slope'] + errors['offs']
    if len(selle1[0]) > 0:
        swherr[selle1] = errors['slope'] + errors['offs']
    varobj[:] = swherr

    # swh_2nd:
    varobj = varobjdict['swh_2nd']
    varobj[:] = numpy.ma.masked_equal(datarr[0:ndata, 8], 32767) * 0.001

    # swh_2nd_calibrated:
    #varobj = varobjdict['swh_2nd_calibrated']

    # swh_2nd_quality:
#    varobj = varobjdict['swh_2nd_quality']

    # sigma0:
    varobj = varobjdict['sigma0']
    varobj[:] = numpy.ma.masked_equal(datarr[0:ndata, 13], 32767) * 0.01

    # sigma0_calibrated:
#    varobj = varobjdict['sigma0_calibrated']

    # sigma0_quality:  equal to swh_quality
#    varobj = varobjdict['sigma0_quality']
#    varobj[:] = swhqualarr

    # sigma0_2nd:
    varobj = varobjdict['sigma0_2nd']
    varobj[:] = numpy.ma.masked_equal(datarr[0:ndata, 14], 32767) * 0.01

    # sigma0_2nd_calibrated:
#    varobj = varobjdict['sigma0_2nd_calibrated']

    # sigma0_2nd_quality:
#    varobj = varobjdict['sigma0_2nd_quality']

    # wind_speed_alt:
    varobj = varobjdict['wind_speed_alt']
    varobj[:] = numpy.ma.masked_equal(datarr[0:ndata, 24], 32767) * 0.01

    # wind_speed_alt_calibrated:
#    varobj = varobjdict['wind_speed_alt_calibrated']

    # wind_speed_rad:
    varobj = varobjdict['wind_speed_rad']
    varobj[:] = numpy.ma.masked_equal(datarr[0:ndata, 25], 32767) * 0.01

    # wind_speed_model_u:
    varobj = varobjdict['wind_speed_model_u']
    varobj[:] = numpy.ma.masked_equal(datarr[0:ndata, 22], 32767) * 0.01

    # wind_speed_model_v:
    varobj = varobjdict['wind_speed_model_v']
    varobj[:] = numpy.ma.masked_equal(datarr[0:ndata, 23], 32767) * 0.01

    # rejection_flags:
    varobj = varobjdict['rejection_flags']
    varobj[:] = flgvar

    # swh_rms:
    varobj = varobjdict['swh_rms']
    varobj[:] = numpy.ma.masked_equal(datarr[0:ndata, 9], 32767) * 0.001

    # swh_rms_2nd:
    varobj = varobjdict['swh_rms_2nd']
    varobj[:] = numpy.ma.masked_equal(datarr[0:ndata, 10], 32767) * 0.001

    # swh_num_valid:
    varobj = varobjdict['swh_num_valid']
    swhnumval = datarr[0:ndata, 11]
    varobj[:] = numpy.ma.masked_equal(swhnumval, 127)

    # swh_num_valid_2nd:
    varobj = varobjdict['swh_num_valid_2nd']
    swhnumval2 = datarr[0:ndata, 12]
    varobj[:] = numpy.ma.masked_equal(swhnumval2, 127)

    # sigma0_rms:
    varobj = varobjdict['sigma0_rms']
    varobj[:] = numpy.ma.masked_equal(datarr[0:ndata, 15], 32767) * 0.01

    # sigma0_rms_2nd:
    varobj = varobjdict['sigma0_rms_2nd']
    varobj[:] = numpy.ma.masked_equal(datarr[0:ndata, 16], 32767) * 0.01

    # sigma0_num_valid:
    varobj = varobjdict['sigma0_num_valid']
    sig0numval = datarr[0:ndata, 17]
    varobj[:] = numpy.ma.masked_equal(sig0numval, 127)

    # sigma0_num_valid_2nd:
    varobj = varobjdict['sigma0_num_valid_2nd']
    sig0numval2 = datarr[0:ndata, 18]
    varobj[:] = numpy.ma.masked_equal(sig0numval2, 127)

    # peakiness:
#    varobj = varobjdict['peakiness']

    # peakiness_2nd:
#    varobj = varobjdict['peakiness_2nd']

    # off_nadir_angle_wf:
    varobj = varobjdict['off_nadir_angle_wf']
    varobj[:] = numpy.ma.masked_equal(datarr[0:ndata, 19], 32767) * 0.0001

    # off_nadir_angle_pf:
    varobj = varobjdict['off_nadir_angle_pf']
    varobj[:] = numpy.ma.masked_equal(datarr[0:ndata, 20], 32767) * 0.0001

    # range_rms:
    varobj = varobjdict['range_rms']
    varobj[:] = numpy.ma.masked_equal(datarr[0:ndata, 5], 32767) * 0.0001

    # range_rms_2nd:
    varobj = varobjdict['range_rms_2nd']
    varobj[:] = numpy.ma.masked_equal(datarr[0:ndata, 6], 32767) * 0.0001

    # bathymetry:
    varobj = varobjdict['bathymetry']
    varobj[:] = datarr[0:ndata, 21]

    # distance_to_coast:
#    varobj = varobjdict['distance_to_coast']

    # sea_surface_temperature
#    varobj = varobjdict['sea_surface_temperature']

    # surface_air_temperature
#    varobj = varobjdict['surface_air_temperature']

    # surface_air_humidity
#    varobj = varobjdict['surface_air_humidity']

    # surface_air_pressure
#    varobj = varobjdict['surface_air_pressure']

    # close nc file
    ncfile.close()


if __name__ == '__main__':

    import sys

    infile = sys.argv[1]
    outdir = sys.argv[2]

    process(infile, outdir)

