#!# -*- coding: latin-1 -*-
# -*- coding: utf-8 -*-
"""
author: Antoine Grouazel:
date: 26/02/2016
purpose: convert SPT (raw files from cerema) to globwave
files are 30min coverage
cetmefmooredbuoy.py is obolset (maccensi)
and cetmefglobwaveconvertor.py is useless (agrouaze) since the source at ISI/CDOCO is now closed
:env: 
    export PYTHONPATH=/home/agrouaze/git/cerbere_gitlab/cerbere:/home/agrouaze/git/cerform/:/home/agrouaze/git:/home/agrouaze/git/globwave:/home/agrouaze/git/globwave/src/insitu
"""
import os
import logging
import glob
import numpy
import datetime
import collections
import pdb
import xlrd
import traceback
import re
from cerform import cfconvention
from cerform.wave import from_kurtskew_2_a1b1a2b2,moments2dirspread
from cerbere.datamodel.pointtimeseries import PointTimeSeries
from cerbere.datamodel.variable import Variable
from cerbere.mapper.ncfile import NCFile
from cerbere.mapper.abstractmapper import WRITE_NEW
from cerbere.datamodel.field import Field, QCLevel, QCDetail
from globwave.src.insitu.shared_convert_methods import GetFinalNameOfTheBuoy,treshold_to_create_new_file,Define_output_filename,time_units
from globwave.src.insitu.variables_conventions import VarConventions,freqDim,freqVarName,freqRangeName
#         import sys
#         sys.path.append('/opt/python_virtualenv/local_agrouaze_hibou/lib/python2.7/site-packages/LatLon')
#         from lat_lon import string2latlon
from lat_lon_copied_modified import string2latlon #modified from https://pypi.org/project/LatLon/
# from lxml.html.builder import PARAM
SOURCE_DIR = '/home/cercache/project/globwave/data/provider/cetmef/SPT_all/'

#source_name (in my mapped structure),official_name_cersat,freq_dependant
#the source name are coming from the format description .pdf
PARAMS_CEREMA = {'frequency':("CENTRALFREQ",True),
                 "density":("EF",True),
                 "direction":("VMDR",True),
                 "etalement":("WSDS",True),
                 "skewness":("SKSK",True),
                 "variance_vert":("VAVA",True),
                 "kurtosis":("KUKU",True),
                 "real_variance":(None,True),
                 "img_variance":(None,True),
                 "part_e_reelle_normalise_second_harmonique":(None,True),
                 "part_e_imag_normalise_second_harmonique":(None,True),
                 "TxNbr":(None,False),
                 "Hm0":("VHM0",False),
                 "Tz":("VGTA",False),
                 "Sm":(None,False),
                 "tref":(None,False),
                 "Tw":(None,False),
                 "Battery":(None,False),
                 "Voff":(None,False),
                 "Xoff":(None,False),
                 "Yoff":(None,False),
                 "Compas":(None,False),
                 "Incl":(None,False),
                 "a1":("A1",True),
                 "b1":("B1",True),
                 "a2":("A2",True),
                 "b2":("B2",True),
                 "theta1":("THETA1",True),
                 "stheta1":("STHETA1",True),
                 "stheta2":("STHETA2",True),
                 "theta2":("THETA2",True),
                 }

def DefineDimensionOfVariable(nbtimes,nb_freq,variable):
    '''define the dimensions of the new file'''
    spectrumtype = 'nondirectional'
    dims = collections.OrderedDict() ## ajout selon JFP
    dims['time'] = nbtimes #defaut dimensions
    if variable in PARAMS_CEREMA:
        if PARAMS_CEREMA[variable][1]:
    #     if variable in ['frequency','density','direction','etalement','skewness','kurtosis','variance_vert','real_variance','img_variance','part_e_reelle_normalise_second_harmonique','part_e_imag_normalise_second_harmonique']:
            dims[ freqDim[spectrumtype] ] = nb_freq
    logging.debug('dimensions of the variable are %s',dims)
    return dims

def get_buoy_position(buoy_id):
    lon_str="24d37'55.25\"W"
    lat_str ="73d42'10.75\"S"
    found = False
    if '_' in buoy_id:
        buoy_id = buoy_id.split('_')[0]
    pos_file = '/home/cercache/project/globwave/prog/GLOBWAVE/src/insitu/buoynetworks/cetmef/Candhis_campagnes_coordonnees_fev-16.xls'
    # Open the workbook
    xl_workbook = xlrd.open_workbook(pos_file)
    # List sheet names, and pull a sheet by name
    sheet_names = xl_workbook.sheet_names()

    xl_sheet = xl_workbook.sheet_by_name(sheet_names[0])
    #
    num_cols = xl_sheet.ncols   # Number of columns
    for row_idx in range(0, xl_sheet.nrows):    # Iterate through rows

        val = xl_sheet.cell(row_idx, 0).value.replace(' ','')
        if val==buoy_id:
            found = True
            if False:
                raw_lon = xl_sheet.cell(row_idx, 3).value.strip('0')
                raw_lat = xl_sheet.cell(row_idx, 2).value.strip('0')
            else:
                raw_lon = xl_sheet.cell(row_idx, 3).value
                raw_lat = xl_sheet.cell(row_idx, 2).value
#             lon_str = raw_lon.replace('\\xb0','d').replace("'",'.00"')
#             lat_str = raw_lat.replace('\\xb0','d').replace("'",'.00"')
            lon_str = raw_lon.replace('\\xb0','d').replace("'",'')
            lat_str = raw_lat.replace('\\xb0','d').replace("'",'')
            if False:
                lat_str = re.sub(r'\xb0', 'd',lat_str).replace(' ','').replace(',0',"'")
                lon_str = re.sub(r'\xb0', 'd',lon_str).replace(' ','').replace(',0',"'")
            else:#fixed the 14 may 2019
                lat_str = re.sub(r'\xb0', 'd',lat_str).replace(' ','').replace(',',"'")
                lon_str = re.sub(r'\xb0', 'd',lon_str).replace(' ','').replace(',',"'")
    if found is False:
        logging.warn("position is a default value!! cant find the id = %s in the file %s",buoy_id,pos_file)

    tmp = "( "+lon_str+","+lat_str+" )"
    logging.info('tmp: %s',tmp)

    lon,lat = parse_lonlat(tmp)
    return lon,lat


def parse_lonlat(coord):
    """ 
    Pass in string in degrees like "( 24d37'55.25\"W, 73d42'10.75\"S)"
    Returns decimal tuple (lon, lat)
    """
    #patch to replace comma by a single quote
    valcomma = coord.count(',')
    if valcomma>1:
#         print 'patch',valcomma
        j = coord.find(",")
       
        k = coord.rfind(",")
#         print j,k
#         coord[k:k+1] = "\'"
        coord = coord[0:k]+"\'"+coord[k+1:]
        if valcomma==3:
            coord = coord[0:j]+"\'"+coord[j+1:]
#         print 'after',coord
    #\(\s*(\d+)d(\d+)\\'([\d.]+)"([WE]),\s*(\d+)d(\d+)\\'([\d.]+)"([NS])\s*\)
    try:
#         latlon_regex = r"\(\s*(\d+)d(\d+)\'([\d.]+)\"([WE]),\s*(\d+)d(\d+)\'([\d.]+)\"([NS])\s*\)"
        latlon_regex = r"\(\s*(\d+)d(\d+)\'([\d]+)\"([WE]),\s*(\d+)d(\d+)\'([\d]+)\"([NS])\s*\)"
        latlon_regex = r"\(\s*(\d+)d(\d+)\'([\d]+)([WE]),\s*(\d+)d(\d+)\'([\d]+)([NS])\s*\)"
        m = re.match(latlon_regex, coord)
#         print('first try')
        parts = m.groups()
#         print('parts',parts)
        #add test

        #LatLon.string2latlon('048 17.420 N', '004 58.100 W', 'd% %M% %H')
#         lat,lon = string2latlon(parts[0]+' '+parts[1]+'.'+parts[2]+' '+parts[3], parts[4]+' '+parts[5]+'.'+parts[6]+' '+parts[7], 'd% %M% %H')
        reslatlon = string2latlon(parts[4]+' '+parts[5]+'.'+parts[6]+' '+parts[7], parts[0]+' '+parts[1]+'.'+parts[2]+' '+parts[3], 'd% %M% %H')
#         logging.debug('tested lat lon ',reslatlon,type(reslatlon),dir(reslatlon),type(reslatlon.lat.decimal_degree),dir(reslatlon.lat))
        lat = reslatlon.lat.decimal_degree
        lon = reslatlon.lon.decimal_degree
        if False:
            #add end
    #         print('parts',parts)
            lon = int(parts[0]) + float(parts[1]) / 60 + float(parts[2]) / 3600
            lon = int(parts[0]) + float(parts[1]) / 60 + float(parts[2]) / 600 #idea agrouaze that the minutes+decimales and not seconds
            if parts[3] == 'W':
                lon *= -1
            #lat = int(parts[4]) + float(parts[5]) / 60 + float(parts[6]) / 3600
            lat = int(parts[4]) + float(parts[5]) / 60 + float(parts[6]) / 600
            if parts[7] == 'S':
                lat *= -1
    except:
        print('in the execept',traceback.format_exc())
#         latlon_regex = r"\(\s*(\d+)d(\d+),([\d.]+)\"([WE]),\s*(\d+)d(\d+),([\d.]+)\"([NS])\s*\)"
#         m = re.match(latlon_regex, coord)
#         parts = m.groups()
#         lon = int(parts[0]) + float(parts[1]) / 60 + float(parts[2]) / 3600
#         if parts[3] == 'W':
#             lon *= -1
#         lat = int(parts[4]) + float(parts[5]) / 60 + float(parts[6]) / 3600
#         if parts[7] == 'S':
#             lat *= -1
    return (lon, lat)


def find_files_month(year,month,buoy_id):
    """
    Args:
        year (str): YYYY
        month (str): MM
    Return:
        listing (list):
    """
    listing = sorted(glob.glob(os.path.join(SOURCE_DIR,buoy_id,year,year+month+'*.spt')))
    if listing==[]:
        pat_2 = os.path.join(SOURCE_DIR,buoy_id,year,year+'-'+month+'*.spt')
        logging.info('second pattern of reseach : %s',pat_2)
        listing = sorted(glob.glob(pat_2))
    logging.debug('Nber of file in the Month : %s',len(listing))
#     listing = listing[10] #to speed up debug
    return listing

def concat_values_in_time(year,month,buoy_id):
    """
    aggregate all the data on a given month
    """
    cpt = collections.defaultdict(int)
    data_cat = {}
    listing0 = find_files_month(year,month,buoy_id)
    #listing1 = listing0[::-1] #to have first older dates tentative to fix issue wrong order dates
    listing1 = listing0
    logging.info('Nber of files in the month: %s',len(listing1))
    for it,fe in enumerate(listing1): #loop on files
        cpt['files_concatenated'] +=1 
        logging.debug('%s/%s %s',it+1,len(listing1),fe)
        data_single = read_raw_data(fe)
        for vv in data_single.keys():
            logging.debug('variable: %s',vv)
            if vv not in ['station']:
                if vv not in data_cat:
                    data_cat[vv] = data_single[vv]
                else:
#                     logging.debug('%s %s',data_cat[vv],data_single[vv])
#                     logging.debug('%s %s',data_cat[vv].shape,data_single[vv])
                    if vv in PARAMS_CEREMA:
                        if PARAMS_CEREMA[vv][1]:
                            data_cat[vv] = numpy.ma.vstack([data_cat[vv],data_single[vv]])
                        else:
                            data_cat[vv] = numpy.ma.concatenate([data_cat[vv],data_single[vv]])
                    else:
                        data_cat[vv] = numpy.ma.concatenate([data_cat[vv],data_single[vv]])
#                     data_cat[vv] = numpy.ma.vstack(data_cat[vv],data_single[vv])
    logging.info('%s',cpt)
    return data_cat

def get_geolocation_buoy(buoy_id):
    lon,lat = get_buoy_position(buoy_id)
    depth = 0
#     lon,lat,depth = geoloc[buoy_id]
    return lon,lat,depth


def create_fields(rawdata):
    '''
    get the values, qc, and store them into Field and Variable cerbere classes
    '''
    cpt = collections.defaultdict(int)
    ofields = collections.OrderedDict()
    ovars = {}
    logging.debug('rwadata: %s',rawdata.keys())
    nbtimes = len(rawdata['Tz'])
    nb_freq = rawdata['density'].shape[1]
    for var in rawdata.keys():
        logging.debug('treated variable: %s',var)
        dims = DefineDimensionOfVariable(nbtimes,nb_freq,var)
        if var not in ['dt']:
            if PARAMS_CEREMA[var][0] is not None: 
                conv_info = VarConventions(PARAMS_CEREMA[var][0])
                if conv_info is not None:
                    stdname,longname,description,sensor,stdunits = conv_info
                    if sensor == 'wave_sensor':
                        logging.debug('translate %s -> %s',var,stdname)
                        varobj = Variable(shortname=PARAMS_CEREMA[var][0],
                                                description= description,
                                                authority = cfconvention.get_convention(),
                                                standardname = stdname )
                        ovars[var] = varobj
                        var_nb_dimension = len(dims)
                        values = rawdata[var]
#                         if var=='Hm0':
#                             values = values/100. #cm->m
                        logging.debug('shape of values for variable %s is %s ',var,values.shape)
                        if values.count() != 0 and (values.mask==False).any():
                            fieldobj = Field(ovars[var],
                                                             dims,
                                                             values = values,
                                                             units = stdunits,
                                                             qc_levels=None,
                                                             qc_details=None)
                            ofields[var] = fieldobj
                            cpt['field_created'] +=1 
                        else:
                            logging.error('variable %s has not been added to the dict because there is no useable values',var)
                            logging.debug('type of values :%s   values.count:%s',type(values),values.count())
                            cpt['no_usable_values'] +=1 
                    else:
                        logging.info('this param %s is related to a sensor different from wave sensor',var)
                        cpt['other_sensor'] +=1 
                else:
                    logging.info('variable %s is not found in the official LOPS dico',PARAMS_CEREMA[var][0])
                    cpt['var_not_defined_LOPS'] +=1 
        else:
            logging.info('%s is not defined in he CEREMA dico',var)
            cpt['var_not_defined_CEREMA'] +=1 
    logging.info('%s',cpt)
    return ofields,ovars

def CreateFinalTimeSerie(year,month,buoy_id,outputdir):
    '''
     Create Time Series and define final name of the file netCDF
    '''
    ts = None
    values = concat_values_in_time(year,month,buoy_id)
    if values != {}:
#         logging.debug('values = %s',values)
        lon,lat,depth = get_geolocation_buoy(buoy_id)
        logging.info('lon : %s  lat : %s depth: %s',lon,lat,depth)
        ofields,ovars = create_fields(values)
    #     vartime = Variable(shortname='time',description='time',standardname='time')
    #     time_dim = collections.OrderedDict([('time', 1)])
    #     srctime = Field(variable=vartime,dimensions=time_dim,units=time_units,values=values['dt'])
        if ofields != {}:
            ts = PointTimeSeries( fields = ofields,
                                    longitude = lon,
                                    latitude = lat,
                                    depth = depth,
                                    times = values['dt'] )
        start_dt = values['dt'][0]
        end_dt = values['dt'][-1]
        globalmetadata = {}
        globalmetadata['wmo_id'] = ''
        globalmetadata['platform_code'] = buoy_id
        ofname = Define_output_filename('wave_sensor',lon,lat,start_dt,end_dt,globalmetadata,spectral='nondirectional',outputdir=outputdir)
        if os.path.exists(ofname):
            logging.info('removing the existing version of %s',ofname)
            os.remove( ofname )
        tsncf = NCFile(url=ofname, mode=WRITE_NEW, ncformat='NETCDF4')
        ts.save( tsncf, attrs=globalmetadata )
        tsncf.close()
        logging.info('file written %s',ofname)
    else:
        logging.info('No data for month %s and buoy :%s',year+month,buoy_id)
        
    return ts

def read_raw_data(fiename):
    """
    read raw data and return a dictionary with the values -> aggregation on frequencies (all data on the same column)
    note: data are spectral but not directional 
    (meaning the data are not direction dependent even there is direction information that can be retrieved associating different variables)
    """
    raw_data = {}
    logging.info('read SPT file: %s',fiename)
    fid = open(fiename,'r')
    lines = fid.readlines()
    if lines != []:
        fid.close()
        raw_data['TxNbr'] = numpy.array([float(lines[0].strip('\r\n'))])
        raw_data['Hm0'] =numpy.array([ float(lines[1].strip('\r\n'))/100.]) #centimeter -> meter
        raw_data['Tz'] = numpy.array([float(lines[2].strip('\r\n'))]) #second
        raw_data['Sm'] = numpy.array([float(lines[3].strip('\r\n'))]) #m2/Hz facteur de normalisation de l energie
        raw_data['tref'] = numpy.array([float(lines[4].strip('\r\n'))]) #celsius
        raw_data['Tw'] = numpy.array([float(lines[5].strip('\r\n'))]) #celsius
        raw_data['Battery'] = numpy.array([float(lines[6].strip('\r\n'))]) #
        raw_data['Voff'] = numpy.array([float(lines[7].strip('\r\n'))]) #m/sec2
        raw_data['Xoff'] = numpy.array([float(lines[8].strip('\r\n'))]) #m/sec2
        raw_data['Yoff'] = numpy.array([float(lines[9].strip('\r\n'))]) #m/sec2
        raw_data['Compas'] = numpy.array([float(lines[10].strip('\r\n'))]) #degree
        raw_data['Incl'] = numpy.array([float(lines[11].strip('\r\n'))]) #degree
        raw_data['frequency'] = numpy.array([])
        raw_data['density'] = numpy.array([])
        raw_data['direction'] = numpy.array([])
        raw_data['etalement'] = numpy.array([])
        raw_data['skewness'] = numpy.array([])
        raw_data['kurtosis'] = numpy.array([])
        raw_data['variance_vert'] = numpy.array([])
        raw_data['real_variance'] = numpy.array([])
        raw_data['img_variance'] = numpy.array([])
        raw_data['part_e_reelle_normalise_second_harmonique'] = numpy.array([])
        raw_data['part_e_imag_normalise_second_harmonique'] = numpy.array([])
        for ll in range(12,len(lines)-1):
    #         logging.debug('line : %s %s',ll,lines[ll])
            line_cont = lines[ll]
            raw_data['frequency'] = numpy.append(raw_data['frequency'],float(line_cont.split(',')[0]))
            raw_data['density'] = numpy.append(raw_data['density'],float(line_cont.split(',')[1]))
            raw_data['direction'] = numpy.append(raw_data['direction'],float(line_cont.split(',')[2]))
            raw_data['etalement'] = numpy.append(raw_data['etalement'],float(line_cont.split(',')[3]))
            raw_data['skewness'] = numpy.append(raw_data['skewness'],float(line_cont.split(',')[4]))
            raw_data['kurtosis'] = numpy.append(raw_data['kurtosis'],float(line_cont.split(',')[5]))
            raw_data['variance_vert'] = numpy.append(raw_data['variance_vert'],float(line_cont.split(',')[6]))
            raw_data['real_variance'] = numpy.append(raw_data['real_variance'],float(line_cont.split(',')[7]))
            raw_data['img_variance'] = numpy.append(raw_data['img_variance'],float(line_cont.split(',')[8]))
            raw_data['part_e_reelle_normalise_second_harmonique'] = numpy.append(raw_data['part_e_reelle_normalise_second_harmonique'],float(line_cont.split(',')[9]))
            raw_data['part_e_imag_normalise_second_harmonique'] = numpy.append(raw_data['part_e_imag_normalise_second_harmonique'],float(line_cont.split(',')[10]))
    #     raw_data['date'] = lines[-1].split(',')[0]
    #     raw_data['heure'] = lines[-1].split(',')[1]
        raw_data['dt'] = numpy.array([datetime.datetime.strptime(lines[-1].split(',')[0]+lines[-1].split(',')[1],'%d/%m/%Y%H:%M')])
        if len(lines[-1].split(','))>=3:
            logging.debug('last line = %s',lines[-1])
            raw_data['station'] = lines[-1].split(',')[2]
        else:
            pass
        #ajout agrouaze juin 2018
        a1,b1,a2,b2 = from_kurtskew_2_a1b1a2b2(th1=raw_data['direction'],sth1=raw_data['etalement'],kurt=raw_data['kurtosis'],skew=raw_data['skewness'],trueNorth=0)
        raw_data['a1'] = a1
        raw_data['b1'] = b1
        raw_data['a2'] = a2
        raw_data['b2'] = b2
        theta1, theta2, stheta1, stheta2 = moments2dirspread(a1, b1, a2, b2)
        raw_data['theta1'] = (270.-theta1)%360.#to fit F ardhuin values (june 2018)
        raw_data['theta2'] = (90.-theta2)%360. #to fit F ardhuin values (june 2018)
        raw_data['stheta1'] = stheta1
        raw_data['stheta2'] = stheta2
        
        raw_data['density'] = raw_data['Sm']*raw_data['density']
    else:
        logging.info('empty file: %s',fiename)
    return raw_data

if __name__ =='__main__':
    root = logging.getLogger()
    if root.handlers:
        for handler in root.handlers:
            root.removeHandler(handler) 
    import argparse
    parser = argparse.ArgumentParser(description='CEREMA conversion SPT-> .nc')
    parser.add_argument('-y','--year', type=str, help='year YYYY')
    parser.add_argument('-m','--month', type=str, help='month MM')
    parser.add_argument('--verbose', action='store_true',default=False)
    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    logging.info('start processing SPT file')
#     onefile = '/home/cercache/project/globwave/data/provider/cetmef/SPT_2013_2015_direct_provision/97501/2015/201504220031.spt'
#     list_spt = glob.glob('/home/cercache/project/globwave/data/provider/cetmef/SPT_2013_2015_direct_provision/97501/2015/20150312*.spt')
#     print len(list_spt)
#     for it,tt in enumerate(list_spt):
#         logging.debug('%s/%s %s',it+1,len(list_spt),tt)
#         test = read_raw_data(tt)
#     print test
#     year = '2015'
#     month = '03'
#     buoy_id = '97501'
#     buoy_id = '08504' #do not have the same number of line in header
    buoy_id = '02911_Pierres-Noires'
    buoy_id = '02920_Ile_de_Sein'
    logging.info('buoy to treat: %s',buoy_id)
#     find_files_month(year,month,buoy_id)
    lon,lat = get_buoy_position(buoy_id)
    outdir = '/home/cercache/users/agrouaze/temporaire/globwave/cetmef/'
    CreateFinalTimeSerie(args.year,args.month,buoy_id,outputdir=outdir)
