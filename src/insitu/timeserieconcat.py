#!/bin/python
# functions to find contigous timeseries and concatenate 2 timeseries
# those functions are meant to manage files when you have to deal with previous and new files
# author: agrouaze
# history: may2014
import os
import datetime
import logging
import numpy
import glob
import pdb
# import re
from cerbere.mapper.abstractmapper import WRITE_NEW
from cerbere.mapper.ncfile import NCFile
from collections import OrderedDict
from cerbere.datamodel.pointtimeseries import PointTimeSeries
from cerbere.datamodel.pointcollection import PointCollection
from cerbere.datamodel.variable import Variable
import netCDF4

def findWidderPlatformFile(filename):
    # return the name of the file having anterior starting date and posterior ending date otherwise return None
    # ultimately this function will prevent conversion of files that already exist or have already a longer duration
    logging.debug('enter findWidderPlatformFile method')
    logging.debug('file analysed is %s', filename)
    babase = os.path.basename(filename)
    platform = babase.split('_')[0]
    thedir = os.path.dirname(filename)
#         indicescor=[]
#         [indicescor.append(m.start()) for m in re.finditer('_2', babase)]
    start_date_input_file = babase.split('_')[1]
    endingdateCurrentFile = babase.split('_')[2]
#         start_date_input_file=babase[indicescor[0]+1:indicescor[1]]
    start_date_input_file = datetime.datetime.strptime(start_date_input_file, '%Y%m%dT%H%M')
    start_date_input_file = netCDF4.date2num(start_date_input_file,"days since 1950-01-01T00:00:00Z")
    endingdateCurrentFile = datetime.datetime.strptime(endingdateCurrentFile, '%Y%m%dT%H%M')
    endingdateCurrentFile = netCDF4.date2num(endingdateCurrentFile,"days since 1950-01-01T00:00:00Z")
    flagfound = False
    wider = []
    if platform != ' ':
        patero = thedir + '/' + platform + '_*.nc'
        logging.debug('pattern of relatives %s', patero)
        relatives = glob.glob(patero)
        for rere in relatives:
            if rere != filename:
                basere = os.path.basename(rere)
                startingdate = basere.split('_')[1]
                endingdate = basere.split('_')[2]
#                     endingdate=basere[indicescor[1]+1:indicescor[1]+1+13]
                startingdate = datetime.datetime.strptime(startingdate, '%Y%m%dT%H%M')
                startingdate = netCDF4.date2num(startingdate,"days since 1950-01-01T00:00:00Z")
                endingdate = datetime.datetime.strptime(endingdate, '%Y%m%dT%H%M')
                endingdate = netCDF4.date2num(endingdate,"days since 1950-01-01T00:00:00Z")
                logging.debug('ending date found: %s endinf date analysed %s', endingdate,endingdateCurrentFile)
                logging.debug('starting date foudn %s and staring date of the file: %s',startingdate, start_date_input_file)
#                 logging.debug('time difference between latest ending date found and the file starting date: %s', start_date_input_file - latestdate)
#                 logging.debug('thresholdsup: %s', thresholdsup)
#                 if start_date_input_file-latestdate<=thresholdsup and start_date_input_file-latestdate>datetime.timedelta(0,0,0):
                if start_date_input_file >= startingdate and endingdate >= endingdateCurrentFile:
                    
#                     return latestfile
                    flagfound = True
                    wider = rere
                    logging.debug('one encapsuling file for file %s has been found : %s', filename,wider)
        if flagfound == True:
            return wider
        else:
            return None
        
def sortContigousPlatformFiles(list_filenames):
    '''from a given set of filenames, create lists of contigous filenames '''
    list_of_list = []
    copy_list = list_filenames
    
    while copy_list != []:
        list_found = findContigousPlatformFiles(copy_list[0])
        logging.debug('list found %s',list_found)
        for xx in list_found:
#             print 'xx',xx
            if xx in copy_list:
                ind = copy_list.index(xx)
                logging.debug('%s is treated indice %s',copy_list[ind],ind)
                del copy_list[ind]
#                 print copy_list
                
            else:
                logging.debug('one of the file has already been grouped thus it means that there are intersecting files')
#                 logging.error('%s is done',copy_list[0])
#                 if len(copy_list)>0:
#                     print 'ahaha ',copy_list
#                     del copy_list[0] #make as if this file intersector was treated
                break #stop the loop since the first element has been processed
        list_of_list.append(list_found)
    return list_of_list
        
def findContigousPlatformFiles(filename,network):
    ''' return the previous filename from the same 
    platform when it exist and return None otherwise
    network: name of the network of the file (string) ex: GLOBWAVE,CETMEF,NDBC 
    '''
    final_list_of_contigous_files = [filename]
#     thresholdsup = datetime.timedelta(0, 86400, 0)
    thresholdsup = 1
    logging.debug('enter findContigousPlatformFiles method')
    logging.debug('file analysed is %s', filename)
    babase = os.path.basename(filename)
    platform = babase.split('_')[0]
    thedir = os.path.dirname(filename)
#         indicescor=[]
#         [indicescor.append(m.start()) for m in re.finditer('_2', babase)]
    start_date_input_file = babase.split('_')[1]
    end_date_input_file = babase.split('_')[2]
#         start_date_input_file=babase[indicescor[0]+1:indicescor[1]]
    start_date_input_file = datetime.datetime.strptime(start_date_input_file, '%Y%m%dT%H%M')
    start_date_input_file = netCDF4.date2num(start_date_input_file,"days since 1950-01-01T00:00:00Z")
    end_date_input_file = datetime.datetime.strptime(end_date_input_file,'%Y%m%dT%H%M')
    end_date_input_file = netCDF4.date2num(end_date_input_file,"days since 1950-01-01T00:00:00Z")
#     print 'end',end_date_input_file
#     pdb.set_trace()
#         end_dates = []
#         start_dates = []
    if platform != ' ':
        patero = thedir + '/' + platform + '_*.nc'
        logging.debug('pattern of relatives %s', patero)
        relatives = glob.glob(patero)
        for rere in relatives:
            if rere != filename:
                basere = os.path.basename(rere)
                startingdate = basere.split('_')[1]
                endingdate = basere.split('_')[2]
#                     endingdate=basere[indicescor[1]+1:indicescor[1]+1+13]
                startingdate = datetime.datetime.strptime(startingdate, '%Y%m%dT%H%M')
                startingdate = netCDF4.date2num(startingdate,"days since 1950-01-01T00:00:00Z")
                endingdate = datetime.datetime.strptime(endingdate, '%Y%m%dT%H%M')
                endingdate = netCDF4.date2num(endingdate,"days since 1950-01-01T00:00:00Z")
                #test intersection
#                 print start_date_input_file - endingdate
                if (start_date_input_file <= startingdate  and  end_date_input_file >= startingdate) or (end_date_input_file >= endingdate and start_date_input_file<= endingdate):
                    logging.warning('file %s is intersecting file %s',rere,filename)
                #jointure debut
                if start_date_input_file - endingdate <= thresholdsup and start_date_input_file - endingdate > 0:
                    logging.debug('insert %s',rere)
                    final_list_of_contigous_files.insert(0,rere)
                    start_date_input_file = startingdate
                #jointure fin
#                 print end_date_input_file - startingdate
                if startingdate - end_date_input_file  <= thresholdsup and end_date_input_file - startingdate < 0:
                    final_list_of_contigous_files.append(rere)
                    logging.debug('append %s',rere)
                    end_date_input_file = endingdate
#                     end_dates.append(endingdate)
#                     start_dates.append(startingdate)
#         if end_dates != []:
#             maxind = numpy.argmax(end_dates)
#             latestdate = end_dates[maxind]
#             latestfile = relatives[maxind]
#             logging.debug('ending date found: %s', latestdate)
#             logging.debug('starting date of the file: %s', start_date_input_file)
#             logging.debug('time difference between latest ending date found and the file starting date: %s', start_date_input_file - latestdate)
#             logging.debug('thresholdsup: %s', thresholdsup)
#             if start_date_input_file - latestdate <= thresholdsup and start_date_input_file - latestdate > datetime.timedelta(0, 0, 0):
#                 logging.debug('one contigous file for this platform has been found with less than %s: %s', thresholdsup, latestfile)
#                 return latestfile
#             else:
#                 logging.debug('no contigous file found')
#                 return None
#         else:
#             logging.debug('no contigous file found')
#             return None
    return final_list_of_contigous_files

def GetDifferentPlatformName(directory):
    '''browse a directory and return the list of the different platform name present'''
    files = glob.glob(directory+"*.nc")
    list_platform = []
    for ff in files:
        name_platform = ff.split('_')[0]
        if (name_platform in list_platform) == False:
            list_platform.append(name_platform)
    return list_platform

def ConcatenateFiles(file1, file2):
    '''concatenate files when contigous'''
    #file1 is the most recent
    logging.debug('enter ConcatenateFiles method')
#     indicescor=[]
    base1 = os.path.basename(file1)
    base2 = os.path.basename(file2)
#     [indicescor.append(m.start()) for m in re.finditer('_2', base1)]
    cuts1 = base1.split('_')
    cuts2 = base2.split('_')
    dirout = os.path.dirname(file1)
#     plat=base1[0:indicescor[0]]
    plat = cuts1[0]
    start1 = cuts1[1]
    start2 = cuts2[1]
#     start1=base1[indicescor[0]+1:indicescor[1]]
    start1 = datetime.datetime.strptime(start1, '%Y%m%dT%H%M')
#     start2=base2[indicescor[0]+1:indicescor[1]]
    start2 = datetime.datetime.strptime(start2, '%Y%m%dT%H%M')
    end1 = datetime.datetime.strptime(cuts1[2], '%Y%m%dT%H%M')
    end2 = datetime.datetime.strptime(cuts2[2], '%Y%m%dT%H%M')
    if end1 > end2:
        endn = end1
    else:
        endn = end2
    endnchar = datetime.datetime.strftime(endn, '%Y%m%dT%H%M')
    if start1 > start2:
        startn = start2
    else:
        startn = start1
    startnchar = datetime.datetime.strftime(startn, '%Y%m%dT%H%M')
    newfilename = dirout + '/' + plat + '_' + startnchar + '_' + endnchar + '_' + cuts1[3] + '_' + cuts1[4] + '_' + cuts1[5] + '_' + cuts1[6]
#         newfilename=os.path.join(dirout,plat,'_',startnchar,'_',endnchar,'_',cuts1[3],'_',cuts1[4],'_',cuts1[5],'_',cuts1[6],'.nc')
    logging.info('new file concatenated will be %s', newfilename)
    ofields = {}
    ncA = NCFile(file1)
    allfields = ncA.get_fieldnames()
    ncB = NCFile(file2)
    lon = ncB.read_values('lon')[0]
    lat = ncB.read_values('lat')[0]
    timesA = ncA.read_values('time')
    timesB = ncB.read_values('time')
    time_units = ncA.read_field('time').units
    depth = ncA.read_values('depth')[0]
    globalmetadata = ncA.read_global_attributes()
    newglobattrib = {}
    for y, toto in enumerate(globalmetadata):
        valatt = ncA.read_global_attribute(toto)
        if toto == 'time_coverage_start':
            newglobattrib[toto] = startnchar
        elif toto == 'time_coverage_stop':
            newglobattrib[toto] = endnchar
        elif toto == 'time_coverage_end':
            pass
        else:   
            newglobattrib[toto] = valatt
#         newglobattrib['starting_date']=startnchar
#         newglobattrib['ending_date']=endnchar
    newtimes = numpy.concatenate((timesB, timesA))
    for cf in allfields:
        if cf not in ['depth', 'lat', 'lon', 'time']:
            fiA = ncA.read_field(cf)
            fiB = ncB.read_field(cf)
            if len(fiA.dimensions)==1:
                nbtimeA = ncA.get_dimsize('time')
                nbtimeB = ncB.get_dimsize('time')
                newfield = fiA
                newfield.dims = OrderedDict([('time', nbtimeA + nbtimeB)])
                valA = ncA.read_values(cf)
                valB = ncB.read_values(cf)
                newvals = numpy.append(valB, valA)
            elif len(fiA.dimensions)==2:
                for dim in ncA.get_dimnames():
                    if dim != 'time':
                        seconddim = dim
                lenseconddim = len(ncA.dimensions[seconddim])
                newfield = fiA
                nbtimeA = ncA.get_dimsize('time')
                nbtimeB = ncB.get_dimsize('time')
                newfield.dims = OrderedDict([('time', nbtimeA + nbtimeB),(seconddim,lenseconddim)])
                valA = ncA.read_values(cf)
                valB = ncB.read_values(cf)
                newvals = numpy.append(valB, valA)
            newvals = numpy.ma.array(newvals)
            newfield.set_values(newvals)
    #             ofields.append(newfield)
            ofields[cf] = newfield
    timeobj = (newtimes, time_units)
    newtimeserie = PointTimeSeries(fields=ofields, longitude=lon, latitude=lat, depth=depth, times=timeobj)
    return newtimeserie

# def WriteTheConcatenationInNetCDFAnDDeletePreviousFiles
'''this function is obsolet since when there
 are files to concatenate they are written then
 with another existing function and input
  files are provider files so no need to destroy them'''
#     logging.info(newfilename)
#     if os.path.exists(newfilename):
#         os.remove(newfilename)
#     tsncf = NCFile(url=newfilename, mode=WRITE_NEW, ncformat='NETCDF4')
#     newtimeserie.save(tsncf, attrs=newglobattrib)
#     # suppression of orginal contigous files
#     logging.warning('concatenation: deleting % and %s to build %s',file1,file2,newfilename)
#     os.remove(file1)
#     os.remove(file2)
    
if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, filemode='w', format='%(asctime)s %(levelname)s %(message)s')
#     f1 = '/home/cercache/users/agrouaze/temporaire/myocean_globwave_test_concat/WMOVaderoarna_20140301T0000_20140301T2300_Lat_58.48N_Lon_10.93E_1.nc'
#     f2 = '/home/cercache/users/agrouaze/temporaire/myocean_globwave_test_concat/WMOVaderoarna_20140301T2301_20140302T0900_Lat_58.48N_Lon_10.93E_2.nc'
#     f1 = '/tmp/test_concat/WMOVaderoarna_20140301T0000_20140301T2300_Lat_58.48N_Lon_10.93E_1.nc'
#     relative = findContigousPlatformFiles(f1)
#     print relative
    listfiles = glob.glob('/tmp/test_concat/WMOVaderoarna*')
    res = sortContigousPlatformFiles(listfiles)
    for toto in res:
        print toto
#     if relative is not None:
#         pass
#     #     ConcatenateFiles(f2,relative)
#     else:
#         print 'no concat'
#     e1 = '/home/cercache/project/globwave/data/globwave/oceansites/ocean_temperature_sensor/2014/05/WMONADR-S1_20140508T2330_20140508T2330_Lat_44.74N_Lon_12.46E.nc'
#     print findWidderPlatformFile(e1)