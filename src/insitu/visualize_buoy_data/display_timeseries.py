#agrouaze
#display timeseries of buoys
# import matplotlib
# matplotlib.use('Agg')
from matplotlib import pyplot as plt
import numpy
import os
import time
import logging
import glob
import resource
from optparse import OptionParser
import fnmatch
import datetime
import numpy as np
import pdb
import pandas as pd
import matplotlib.patheffects as pe
import netCDF4
from matplotlib import rc, font_manager
import matplotlib.dates as mdates
import sys
from scipy.ndimage import gaussian_filter1d
sys.path.append('/home/losafe/users/agrouaze/git')
from globwave.src.insitu.globwave_shared_infos import active_networks
sys.path.append('/home/losafe/users/agrouaze/git/oceanheatflux')
from oceanheatflux.bin.compute_flux_from_pmel_buoy import PMEL_DIR_FORMATED,buoy_mapper
# sys.path.append('/home/losafe/users/agrouaze/git/globwave/src/insitu')

# from variables_conventions import Ranges,VarConventions
from globwave.src.insitu.variables_conventions import Ranges,VarConventions
from globwave.src.insitu.find_buoy_file.globwave_paths import list_paths_glob


#TODO: add a show_everything_about_a_buoy (all aparam on whole period)

def read_single_file(file_path,parameter,one_dimension_only=True):
    """
    could be replace by a generic mapper?
    """
    
    nc = netCDF4.Dataset(file_path)
    if parameter in nc.variables.keys():
        param_val = nc.variables[parameter][:]
        if len(param_val.shape)>1:
            if one_dimension_only:
                param_val = numpy.ma.mean(param_val,axis=1)
#             else:
#                 param_val = param_val
        units = nc.variables[parameter].units
        if 'time' in nc.variables.keys():
            timename = "time"
        else:
            timename = 'TIME'
        times = nc.variables[timename][:]
        
        time_unit = nc.variables[timename].units
        times_dt = netCDF4.num2date(times, units=time_unit, calendar='standard')
#         for dodo in times_dt:
#             if not isinstance(dodo,datetime.datetime):
#                 print 'dodo,',dodo
#                 sldjfsl
        try:
            name = nc.getncattr('station_name')
        except:
            name = nc.getncattr('platform_name')
    
    else:
        param_val = None
        times_dt = None
        units = ''
        name = ''
    nc.close()
    
    return param_val,times_dt,units,name

def BrowseAndCount(parameter,buoy_id,rep,endyear,start_year):
    """
    method to find all the files of a given buoy and return the values of a parameter and the times steps
    obsolet? since we have the globwave_path.py and checlnumberofbuoypermonth.py
    """
    vector_time = []
    hit_vector = numpy.zeros((endyear+1-start_year)*12)
    cpt = 0
    for year in range(start_year,endyear+1):
        for month in range(1,13):
            cpt+=1
            month_date = datetime.datetime(year,month,15)
            month_date_num = netCDF4.date2num(month_date,'seconds since 1970-01-01 00:00:00')
#             print type(month_date_num)
            vector_time.append(month_date)
            dirmonth = os.path.join(rep,str(year),str(month).zfill(2)+'/')
            if os.path.exists(dirmonth):
                listfiles = glob.glob(dirmonth+buoy_id+'*.nc')
                for file in listfiles:
                    nc = netCDF4.Dataset(file,'r')
                    nbobs = len(nc.dimensions['time'])
                    nc.close()
                    hit_vector[cpt] = hit_vector[cpt] + nbobs
                    logging.debug('nb osb %s in %s/%s',nbobs,month,year)
#     pattern = buoy_id+'*'
#     for root,dirs,files in os.walk(rep):
#         for file in fnmacth(pattern):
    return hit_vector,vector_time

def plot_point_timeseries(vals,times,buoy_id,name_buouy,param,units,axis,quality_flags=None):
    """
    plot timeseries -
    Return fig handler
    systematically hide masked value and values marked as bad
    """
#     colors_flag = ['r','b','g','y','b','m','c','orange','grey','brown']
    fontProperties = {'family':'sans-serif',
    'weight' : 'normal', 'size' : 9}
    ticks_font = font_manager.FontProperties(family='Bitstream Vera Sans', style='normal',
    size=7, weight='normal', stretch='normal')
#     fig_handler.add_subplot(total_subplot,1,subplot_nb)
    plt.sca(axis)
    plt.grid()
#     plt.title(buoy_id+': '+name_buouy)
#     plt.plot_date(times,vals,'-.',label=param)
#     vals 
#     vals_unmasked= vals
#     vals_unmasked[vals_unmasked.mask==True] = 0
#     vals_unmasked.mask = False
#     if quality_flags is not None:
#         vals = numpy.ma.masked_where(quality_flags!=4, vals, copy=False)
#         vals.mask[quality_flags!=4] = True
#         for flag in range(0,10):
#             tmpflag = vals_unmasked[quality_flags==flag]
#             tmptime = times[quality_flags==flag]
#             plt.plot_date(tmptime,tmpflag,'o',label='quality_flags %s'%flag,color=colors_flag[flag],alpha=.1)
    plt.plot_date(times,vals,'o',color='b',alpha=.1)
    #gaussian filter
    sig = 70
    argsorder = np.argsort(times)
    vals = vals[argsorder]
    times = times[argsorder]
    yynonan = vals[(np.isfinite(vals)) & (vals<1000) & (vals.mask)==False]
    xxnonan = times[np.isfinite(vals) & (vals<1000) & (vals.mask==False)]
    filtered = gaussian_filter1d(yynonan,sigma=sig)

    plt.plot_date(xxnonan,filtered,'m-',lw=2.7,label='(gaussian filter sigma=%s)'%(sig),path_effects=[pe.Stroke(linewidth=5, foreground='k'), pe.Normal()],alpha=0.7)
#     ax = plt.gca()
#     ax.xaxis_date()
    axis.set_xlabel('Date [YYYY/MM]',fontsize=8)
    
#     label.set_fontproperties(ticks_font)
    axis.set_xticklabels(axis.get_xticks(), fontProperties)
    for label in axis.get_xticklabels():
        label.set_rotation(30)
        label.set_horizontalalignment('right')
        label.set_fontproperties(ticks_font)
    axis.xaxis.set_major_formatter(mdates.DateFormatter('%Y/%m/%d %H'))
    axis.set_ylabel(param+' '+units,fontsize=12)
   
#     plt.legend()
    return

def PlotBarSerie(hit_vector,vector_time,buoy_id,figid,cbuoy,total):
    """
    Args:
        vector_time (datetime obj)
    """
    fontProperties = {'family':'sans-serif','sans-serif':['Helvetica'],
    'weight' : 'normal', 'size' : 9}
    ticks_font = font_manager.FontProperties(family='Bitstream Vera Sans', style='normal',
    size=7, weight='normal', stretch='normal')
    figid.add_subplot(total,1,cbuoy)
    plt.grid()
    plt.title(buoy_id)
    plt.bar(vector_time,hit_vector, width=30)
    ax = plt.gca()
#     ax.xaxis_date()
    ax.set_xlabel('Date [YYYY/MM]',fontsize=8)
    
#     label.set_fontproperties(ticks_font)
    ax.set_xticklabels(ax.get_xticks(), fontProperties)
    for label in ax.get_xticklabels():
        label.set_rotation(30)
        label.set_horizontalalignment('right')
        label.set_fontproperties(ticks_font)
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y/%m'))
    ax.set_ylabel('#')
#     filout = '/home/cercache/users/agrouaze/resultats/globwave_monitore/'+buoy_id+'_full_timserie.png'
#     plt.savefig(filout)
#     logging.info('output %s',filout)
    return figid

def saveFig(figid,filout,dpi=200):
#     filout = '/home/cercache/users/agrouaze/resultats/globwave_monitore/cetmef_full_timserie.png'
    figid.savefig(filout,dpi=dpi)
    logging.info('output %s',filout)
    return

def FindCETMEF_Buoys(rep):
    """
    obsolet since we have globwave_path.py ??
    """
    buoylist = []
    names = []
    for root,dirnames,fielnames in os.walk(rep):
        for file in fielnames:
            logging.debug('file %s',file)
            nc =  netCDF4.Dataset(os.path.join(root,file),'r')
            inst = nc.getncattr('institution')
            try:
                name = nc.getncattr('platform_name')
            except:
                name = nc.getncattr('station_name')
            nc.close()
            if 'CETMEF' in inst:
                id = file.split('_')[0]
                if not (id in buoylist):
                    buoylist.append(id)
                    names.append(name)
    logging.debug('nb file cetmef found %s',len(buoylist))
    logging.debug('list %s',buoylist)
    return buoylist,names

def GatherSubplot(subfig1,subfig2):
    fig = plt.figure()
    ax3 = fig.add_subplot(2,1,1)
    ax4 = fig.add_subplot(2,1,2)
    return fig
    
def test_pmel():
    #     parameter = 'dominant_wave_height'
#     parameter = 'significant_swell_wave_height'
#     buoy_id = 'WMO61187'
#     buoy_id = 'WMO62163'
#     buoylist = ['WMO61187','WMO61190']
#     rep = '/home/cercache/users/agrouaze/data/test_globe_wave_convert/wave_sensor/'
#     rep = '/home/cercache/project/globwave/data/globwave/oceansites/wave_sensor/'
#     rep_dated = rep + '2014/05/'
#     buoylist,names = FindCETMEF_Buoys(rep_dated)
#     file_path = '/home/cercache/project/globwave/data/globwave/oceansites/wave_sensor/2015/05/WMO62163_20150501T0000_20150531T2300_Lat_47.50N_Lon_8.40W.nc'
#     print buoylist
#     print names
#     start_date = 1990
#     end_date = 2014
#     total = len(buoylist)
    total = 1
    figid = plt.figure(figsize=(18,12))
    buoy_id = 'WMO32011'
    dd_month = '200210'
    name = 'toto'
    plt.title(buoy_id+': '+name+' '+dd_month)
#     file_path = buoylist[0]
    cbuoy = 1
#     for cbuoy,buoy_id in enumerate(buoylist):
#     hit_vector,vector_time = BrowseAndCount(parameter,buoy_id,rep,end_date,start_date)
#     hit_vector,vector_time,units,name = read_single_file(file_path,parameter)
#     figid = PlotBarSerie(hit_vector,vector_time,buoy_id,figid,cbuoy,total)
#     fifid = plot_point_timeseries(hit_vector,vector_time,buoy_id,name,parameter,units,figid,1,2)
#     file_path = '/home/cercache/project/globwave/data/globwave/oceansites/anemometer/2015/05/WMO62163_20150501T0000_20150530T2300_Lat_47.50N_Lon_8.40W.nc'
#     parameter = 'wind_speed'
    
    parameter = 'airtemp'
    file_path ='rien'
#     filename = '/home/cercache/project/globwave/data/globwave/pmel/anemometer/2002/10/WMO32011_20021001T0000_20021031T0000_Lat_3.50N_Lon_95.00W.nc'
    ins = buoy_mapper(PMEL_DIR_FORMATED,buoy_wmo=buoy_id,date=dd_month)
    units = 'degreeC'
    hit_vector,vector_time,quality = ins.get_param('t')
    hit_vector = numpy.ma.squeeze(hit_vector)
    logging.debug('shapes %s %s %s',hit_vector.shape,vector_time.shape,quality.shape)
#     ins.read_pmel_data(filename)
#     hit_vector = ins['u']
#     vector_time = ins['times']
#     hit_vector,vector_time,units,name = read_single_file(file_path,parameter)
    param = ['u','p','us','rain','t']
    figid, axs = plt.subplots(len(param),1,figsize=(20,18))
    plt.sca(axs[0])
    logging.debug('hit_vector mean : %s',hit_vector.mean())
    for ee,papa in enumerate(param):
        logging.debug('param: %s',papa)
        hit_vector,vector_time,quality = ins.get_param(papa)
        print quality
        if hit_vector is not None and vector_time is not None and quality is not None:
            logging.debug('val: %s time: %s quality :%s',hit_vector.shape,vector_time.shape,quality.shape)
            hit_vector = numpy.ma.squeeze(hit_vector)
            quality = quality.squeeze()
            plot_point_timeseries(hit_vector,vector_time,buoy_id,name,papa,'unknown',axs[ee],quality_flags=quality)
#     plot_point_timeseries(hit_vector,vector_time,buoy_id,name,parameter,units,axs[1],quality_flags=quality)
    
    saveFig(figid,filout='/tmp/'+buoy_id+os.path.basename(file_path)+'.png')
    
def read_values_from_listfpath(list_paths,papa):
    param_concat = numpy.ma.array([])
    time_param = numpy.ma.array([])
    logging.debug('param: %s',papa)
    #read the files
    logging.debug('Nber of files found: %s',len(list_paths))
    if len(list_paths)>0:
        logging.info('First file buoy: %s',list_paths[0])
        logging.info('Last file buoy: %s',list_paths[-1])
    cpt = 0
    name_station = 'unknown'
    for ff in list_paths:
        logging.debug('file %s',ff)
#         print ff
        if os.path.exists(ff):
            param_val,times_dt,units,name_station = read_single_file(ff,papa,one_dimension_only=False)
#             pdb.set_trace()
            if param_val is not None:
                #concat the values
                logging.debug('param length: %s',param_val.shape)
#                 print param_val.shape
                
                if cpt==0:
                    param_concat = param_val
                    time_param = times_dt
                else:
                    param_concat = numpy.ma.concatenate([param_concat,param_val])
                    time_param = numpy.ma.concatenate([time_param,times_dt])
                cpt += 1
#                 print 'big',param_concat.shape
            else:
                logging.debug('param %s is None',papa)
        else:
            logging.debug('%s doesnt exist',ff)
        
    return param_concat,time_param,name_station

def read_values_from_listfpath_v2(list_paths,papalist):
    """
    improvement: one single IO for many parameters
    """
#     param_concat = numpy.ma.array([])
#     time_param = numpy.ma.array([])
    resu = {}
    logging.debug('param: %s',papalist)
    #read the files
    logging.debug('Nber of files found: %s',len(list_paths))
    logging.info('First file buoy: %s',list_paths[0])
    logging.info('Last file buoy: %s',list_paths[-1])
    cpt = 0
    for ff in list_paths:
        logging.debug('file %s',ff)
#         print ff
        if os.path.exists(ff):
            for qq,papa in enumerate(papalist):
#                 print papa
                param_val,times_dt,units,name_station = read_single_file(ff,papa,one_dimension_only=False)
#                 print 'times_dt',type(times_dt)
#             pdb.set_trace()
                if param_val is not None:
                    #concat the values
                    logging.debug('param length: %s',param_val.shape)
    #                 print param_val.shape
                    
#                     if cpt==0:
                    if papa not in resu.keys():
#                         param_concat = param_val
                        if papa not in ['time']:
                            resu[papa] = param_val
                        #time is by default always retrieved
                        resu['time'] = times_dt
#                         time_param = times_dt
                    else:
                        if papa not in ['time']:
                            resu[papa] = numpy.ma.concatenate([resu[papa],param_val])
                        if qq==0:
                            logging.debug('qq=%s ff=%s',qq,ff)
                            resu['time'] = numpy.concatenate([resu['time'],times_dt])
#                         param_concat = numpy.ma.concatenate([param_concat,param_val])
#                         time_param = numpy.ma.concatenate([time_param,times_dt])

                    cpt += 1
    #                 print 'big',param_concat.shape
                else:
                    logging.debug('param %s is None',papa)
        else:
            logging.debug('%s doesnt exist',ff)
#     print 'type time',type(times_dt)
#     for tutu in resu['time']:
#         if not isinstance(tutu,datetime.datetime):
#             print 'tutu',tutu
#             sdjksdfl
    #sort on time
    df = pd.DataFrame(resu)
    df.index = df.time
    df = df.sort_index()
    logging.debug('Nber of element in the concatenate buoy dataframe: %s',len(df))
    return df
    
def test_globwave_official_format(buoy_id,start,stop,netw,param=None,output_path=None):
    """
    dev a tool to read globwave file from WMO,start,stop
    Args:
        param (list):
    """
    
    file_path ='_globwave_ts_display_test_'
    if param is None:
        param = ['wind_speed','wind_direction','sea_surface_significant_wave_height','significant_swell_wave_height']
#     units = ['']
    figid, axs = plt.subplots(len(param),1,figsize=(14,9))
    
#     plt.sca(axs[0])
    for ee,papa in enumerate(param):
        #find the sensor
        tmp = VarConventions(papa)
        if tmp is None:
            logging.warning('%s is not refrenced into the variable catalogue',papa)
            units_cv = ''
            sensor_cv = 'wave_sensor'
        else:
            GWname,longname_cv,description_cv,sensor_cv,units_cv = tmp
        #find the files
        _,list_paths = list_paths_glob(outputdir='/tmp',startperiod=start,stopperiod=stop,network=[netw],buoyid=buoy_id,sensors_wanted=[sensor_cv])
#         param_concat = numpy.ma.array([])
#         time_param = numpy.ma.array([])
#         logging.debug('param: %s',papa)
#         #read the files
#         logging.debug('Nber of files found: %s',len(list_paths))
#         for ff in list_paths:
#             logging.debug('file %s',ff)
#             param_val,times_dt,units,name_param = read_single_file(ff,papa)
#             if param_val is not None:
#                 #concat the values
#                 logging.debug('param length: %s',param_val.shape)
#                 param_concat = numpy.ma.concatenate([param_concat,param_val])
#                 time_param = numpy.ma.concatenate([time_param,times_dt])
        param_concat,time_param,name_buoy = read_values_from_listfpath(list_paths,papa)
        #plot
        if isinstance(axs,list):
            plot_point_timeseries(param_concat,time_param,buoy_id,name_buoy,papa,units_cv,axs[ee])
        else:
            plot_point_timeseries(param_concat,time_param,buoy_id,name_buoy,papa,units_cv,axs)
    plt.suptitle(name_buoy+'\n'+buoy_id+' '+netw)
    if output_path is None:
        output_path = '/tmp/'+buoy_id+os.path.basename(file_path)+'.png'
    saveFig(figid,filout=output_path)
    
if __name__ == '__main__':
    list_usages = ['TS','BAR','reader']
    root = logging.getLogger()
    if root.handlers:
        for handler in root.handlers:
            root.removeHandler(handler)
    parser = OptionParser()
    parser.add_option("-o","--outputdir",
                      action="store", type="string",
                      dest="outputdir", metavar="string",
                      help="directory where will be created the png ")
    parser.add_option("-q","--quiet",
                      action="store_true", default=False,
                      dest="quiet",
                      help="quiet mode")
    parser.add_option("-u",'--usage',
                      action="store",type='choice',choices=list_usages,
                      dest="usage",
                      help="what do you want: %s ?"%(' or '.join(list_usages)))
    parser.add_option("-n",'--network',
                      action="store",type='choice',choices=active_networks.keys(),
                      dest="network",
                      help="which network do you want: %s ?"%active_networks.keys())
    (options, args) = parser.parse_args()
    if options.quiet:
        logging.basicConfig(level=logging.INFO)
    else:
        logging.basicConfig(level=logging.DEBUG)
    if options.outputdir is None:
        raise Exception('you must give a -o args')
    logging.info('start display a timeserie')
#     test_pmel()
    if options.usage == 'TS':
        if options.outputdir is None:
            raise Exception('you must give a -n args')
#         buoy_id = 'WMO62052'#pierres noires
    #     buoy_id = 'WMO31007'#test
#         name_buoy = 'Stratus'
#         buoy_id = 'WMO32012'
        buoy_id = 'WMO46059'
        buoy_id = "WMO62067" #ile dyeu
    #     dd_month = '200210'
        start_dt = datetime.datetime(2017,5,1)
        stop_dt = datetime.datetime(2017,7,1)
        param = ['significant_wave_height']
        param = ['sea_surface_significant_wave_height']
#         param = ['significant_wave_height','dominant_wave_period']
        test_globwave_official_format(buoy_id,start_dt,stop_dt,options.network,param=param)
#         test_globwave_official_format()
    elif options.usage == 'reader':
        t0 = time.time()
#         list_files = ['/home/cercache/project/globwave/data/globwave/ndbc/nrt/wave_sensor/2016/05/WMO46059_20160501T0000_20160525T2220_Lat_38.05N_Lon_129.97W.nc','/home/cercache/project/globwave/data/globwave/ndbc/nrt/wave_sensor/2016/03/WMO46059_20160301T0000_20160331T2350_Lat_38.05N_Lon_129.97W.nc','/home/cercache/project/globwave/data/globwave/ndbc/nrt/wave_sensor/2016/02/WMO46059_20160201T0000_20160229T2350_Lat_38.05N_Lon_129.97W.nc','/home/cercache/project/globwave/data/globwave/ndbc/nrt/wave_sensor/2016/01/WMO46059_20160101T0000_20160131T2350_Lat_38.05N_Lon_129.97W.nc','/home/cercache/project/globwave/data/globwave/ndbc/nrt/wave_sensor/2016/04/WMO46059_20160401T0000_20160430T2350_Lat_38.05N_Lon_129.97W.nc']
        list_files = ['/home/cercache/project/globwave/data/globwave/ndbc/nrt/wave_sensor/2016/04/WMO52212_20160420T2200_20160428T1130_Lat_7.63N_Lon_134.67E.nc','/home/cercache/project/globwave/data/globwave/ndbc/nrt/wave_sensor/2016/04/WMO52212_20160406T1730_20160419T2030_Lat_7.63N_Lon_134.67E.nc']
#         list_param = ['significant_wave_height','dominant_wave_period']
        list_param = ['time']
        
        res = read_values_from_listfpath_v2(list_files,list_param)
        elapse = time.time()-t0
        print 'time to read %s files and %s params: %1.1f seconds'%(len(list_files),len(list_param),elapse)
        print res[list_param[0]][0],'->',res[list_param[0]][-1]
        print res['time']
        pdb.set_trace()
    print 'memory usage:',resource.getrusage(resource.RUSAGE_SELF).ru_maxrss,' bytes'
