'''
author: agrouaze
purpose: map postions of buoys from globwave repositories
creation: jan2014
01/09/2016: add methods to read TAO-NDBC data
'''
import glob
import datetime
import os,sys
import numpy as np
import netCDF4 as netcdf
import matplotlib
from dateutil import rrule
matplotlib.use('Agg')
import matplotlib.patches as patches
from matplotlib.patches import Polygon
from mpl_toolkits.basemap import Basemap
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset
import matplotlib.pyplot as plt
import traceback
from globwave.src.insitu.read_buoys_data.reader_tao_ndbc_provider import get_files_list_tao,get_lon_lat
import logging
import pdb
import pandas as pd
import netCDF4
FONTSIZE=20
dioc = {
        'CDIP':('b','/home/cercache/project/globwave/data/globwave/cdip/','o'),
#         'CORIOLIS-DM':('g','/home/cercache/project/globwave/data/globwave/oceansites/','s'),
        'CORIOLIS':('g','/home/datawork-cersat-public/cache/project/globwave/data/globwave/coriolis/','s'),#avril 2018 y a plus que coriolis et ndbc qui sont utilisable
        'NDBC':('r','/home/cercache/project/globwave/data/globwave/ndbc/nrt/','.'),
        'NDBC_TXT':('r','/home/cercache/project/globwave/data/provider/ndbc/spectra_txt/','.'),
        'NDBC_ARCH':('r','/home/cercache/project/globwave/data/globwave/ndbc/archive/','.'),
        'ECMWF':('m','/home/cercache/project/globwave/data/globwave/ecmwf/new_format/','*'),
        'CEREMA':('y','/home/cercache/project/globwave/data/globwave/cetmef/latest/','^'),
        'MEDS':('y','/home/cercache/project/globwave/data/globwave/meds/','<'),
        'PMEL':('c','/home/cercache/project/globwave/data/globwave/pmel/','o'),
        'TAO':('c','/home/cercache/project/globwave/data/provider/tao/tao_from_ndbc/','o'),
        }
areas = {#W-E-N-S
         'grandlacs':(-92.3,-80,49,41.0),
        'global':(-179,179.0,72.0,-65.0),
         'USWestCoast':(-130.,-115,51,29),
         'north_pacific':(-155,-119,68,30),
         'europe':(-25,50,65,33)
         }

areas_inset = {#W-E-N-S ; loc on the minibox,colorofthelines,bboxanchor(a,b)
#          'grandlacs':[(-92.3,-80,49,41.0),1,'c'],
#          'USWestCoast':[(-130.,-115,51,29),2,'g'],
         'north_pacific':[(-150,-119,60,30),2,'k',(1.01,0.65)],
         'hawai':[(-163,-151,27,15),3,'g',(-0.05,0.5)],
         'eastcoast':[(-90,-50,46,25),1,'c',(0.7,-0.05)],
         'caraibes':[(-70,-40,22,10),5,'r',(0,-0.05)],
         }
MONITORE_DIR = '/home/cercache/users/agrouaze/resultats/globwave_monitore/'
def build_stat_network_table(year,sensor,suffix='',parameters=None,spectrum_test=False,dirout=None):
    """
    create a table png gathering information about nber of buoys per network
    sensor (str) all,wind,wave,directional_wave
    year (str) YYYY
    """
    logging.info('start to build table for networks')
    if parameters is None:
        parameters_str = ''
    else:
        parameters_str = parameters

    fig = plt.figure(figsize=(6,3))
    ax0 = plt.gca()
    ax0.set_frame_on(False)
    ax0.xaxis.set_visible(False)
    ax0.yaxis.set_visible(False)
    plt.title('number of buoys with '+sensor+' in '+year)
    nb_line = len(dioc.keys())
    header = ['network']#,'January','February','March','April','May']
    for dd in rrule.rrule(rrule.MONTHLY,dtstart=datetime.datetime(1901,1,1),until=datetime.datetime(1901,12,31)):
        header.append(dd.strftime('%B'))
    logging.debug('header: %s',header)
    lines_name = ['%1d' % xx for xx in range(1,nb_line+1)]
#     nb_column = len(header)
    nb_column = len(dioc.keys())
    nb_month = datetime.datetime.now().month
    data = np.zeros((nb_month,nb_column))
    for mm,month in enumerate(range(1,nb_month+1)):
        for nn,net in enumerate(dioc.keys()):
            month_str = str(month).zfill(2)
            aa,bb,cc,dd,ee,ff,gg = GetThePositions(net,year,month_str,sensor,suffix=suffix,area='global',parameters=parameters)
            data[mm,nn] = len(aa)
    cell_text = []
    for ll,lili in enumerate(dioc.keys()):#loop over networks
#         line_cont = []
#         for coco in nb_column:#loop over categories
#         cell_text.append(['%1.1f' % xx for xx in data[:,coco]])
        monthly_res = ['%1.1f' % month for month in data[:,ll]]
        line_cont = [lili]+monthly_res
        cell_text.append(line_cont)
    the_table = plt.table(cellText=cell_text,
                      rowLabels=lines_name,
#                       rowColours=colors,
                      colLabels=header,
                      loc='center',
#                       cellColours=cmap(norm(pourcentvals)),
#                             cellColours=plt.cm.rainbow(pourcentvals),
                      )
    
    plt.show()
    if False:# i dont want to save the figure anymore since I have notebooks
        
        if spectrum_test:
            sensor_str = sensor+'_spectrum'
        else:
            sensor_str = sensor
        if dirout is None:
            filout = os.path.join(MONITORE_DIR,'table_buoys_globwave_network_'+sensor_str+'.png')
        else:
            filout = os.path.join(dirout,'table_buoys_globwave_network_'+sensor_str+'.png')
        
        
        
        if os.path.exists(filout):
            os.remove(filout)
        plt.savefig(filout,dpi=350)
        logging.info('filout %s',filout)
    return


def CheckParametersAvailability(parameters,wmo,network,year,month):
    '''return True if all the parameters specified are found in archive dirs'''
    dira = dioc[network][1]
    buoy_files = glob.glob(dira +'*/'+year+'/'+month+'/'+wmo+'*.nc')
    bool_res = np.array([],dtype='bool')
    for jj,param in enumerate(parameters):
#         bool_res.append(Fadlse)
        bool_res = np.concatenate([bool_res,[False]])
        for file_buoy in buoy_files:
            nc = netcdf.Dataset(file_buoy,'r')
            keys = nc.variables.keys()
            nc.close()
            if param in keys:
                bool_res[jj] = True
                logging.debug('CheckParametersAvailability | %s found for %s',param,wmo)
                break
    if (bool_res==True).all():
        res = True
    else:
        res = False
#     files = GetListFiles(reseau,suffix='',year,month,capteur)
    return res

def is_with_spectrum(network,file_path,filehandler):
    """
    patch to spot frequencial content for networks that do not have spectrum keyword in filename
    """
    res = False
    directional = False
    if network in ['NDBC','CDIP','CORIOLIS-NRT']:
#         pdb.set_trace()
        if 'spectrum' in file_path:
            res = True
            directional = test_is_directional(filehandler)
    else:
#         pdb.set_trace()
#         nc = netCDF4.Dataset(file_path,'r')
        if 'frequency' in filehandler.dimensions.keys():
            res = True
            directional = test_is_directional(filehandler)
#         nc.close()
    return res,directional

def test_is_directional(filehandler):
    if 'stheta2' in filehandler.variables.keys():
        res = True
    else:
        res  = False
    return res

def GetListFiles(reseau,year,month,capteur,suffix=''):
    """
    year (str) YYYY
    month (str) MM
    capteur (str) 
    """
    dira = dioc[reseau][1]
    directo = dira + capteur+'/'+year+'/'+month+'/*'+suffix+'.nc'
    logging.debug('pattern of research: %s',directo)
    files = glob.glob(directo)
    return files

def GetThePositions(reseau,year,month,capteur,suffix='',area='global',parameters=None):
    """
    method to get buoys geoloc and metadata (designed for globwave format)
    :args:
        capteur (str): wave_sensr for instance
    """
    wmo=[]
    if reseau != 'TAO':
        files = GetListFiles(reseau,year,month,capteur,suffix='')
    else:
        files = get_files_list_tao(reseau,year,month,'wind',suffix='')
#         flagdone=False
    logging.info('%s %s %s %s %s %s => number of files found %s',reseau,year,month,capteur,suffix,area, len(files))
    LON = []
    LAT = []
    FILES = []
    DIST2SHORE = []
    spectra_flag = []
    names = []
    directional_spectra = []
    for fifi in files:
        logging.debug('file %s',fifi)
        
        tmp=os.path.basename(fifi)
        res = tmp.split('_')
        if reseau != 'TAO':
            platname = res[0]
            try:
                nc = netcdf.Dataset(fifi,'r')
            except:
                print fifi
                print traceback.format_exc()
            lat = nc.variables['lat'][:]
            lon = nc.variables['lon'][:]
            station_name = nc.getncattr('station_name')
            spectrumpresent,directional = is_with_spectrum(reseau,fifi, nc)
    #         dist2shore = nc.getncattr('distance_to_shore')
            if 'distance_to_shore' in nc.variables.keys():
                dist2shore = nc.variables['distance_to_shore'][0]
            else:
                dist2shore = 99999.0
            nc.close()
        else:
            lon,lat = get_lon_lat(fifi)
            platname = os.path.basename(fifi).split('_')[1]
            dist2shore = 99999.0
            
        
        #print platname,reseau
        if lon[0]<areas[area][1] and lon[0]>areas[area][0] and lat[0]>areas[area][3] and lat[0]<areas[area][2]:
            if not platname in wmo: #to avoid multiple entries for a same buoy
                if parameters is None:
                    buoyok = True
                else:
                    if reseau != 'TAO':
                        buoyok = CheckParametersAvailability(parameters,platname,reseau,year,month)
                    else:
                        buoyok = True #pour tao on met toujours ok pour le moment
                if buoyok == True:
#                     spectrumpresent = is_with_spectrum(reseau, fifi)
#                     if buoyok == True:
                    wmo.append(platname)
                    DIST2SHORE.append(dist2shore)
                    LON.append(lon[0])
                    LAT.append(lat[0])
                    FILES.append(fifi)
                    spectra_flag.append(spectrumpresent)
                    names.append(station_name)
                    directional_spectra.append(directional)
    data = {}
    data['lon'] = LON
    data['lat'] = LAT
    data['wmo'] = wmo
    data['dist'] = DIST2SHORE
    data['With_spectra'] = spectra_flag
    data['name'] = names
    data['directional'] = directional_spectra
    df = pd.DataFrame(data=data,columns=data.keys())
    return df

def PrintPositions2File(lon,lat,wmo,dist,files,filout):
    fid = open(filout,'w')
    fid.write('#lon;lat;buoyid;distance2shore;file \n')
    for cc,lolo in enumerate(lon):
        sent = str(lon[cc])+';'+str(lat[cc])+';'+wmo[cc]+';'+str(dist[cc])+';'+files[cc]+'\n'
        logging.debug('%s',sent)
        fid.write(sent)
    fid.close()
    logging.info('%s',filout)
    return

def add_inset_zoom(rectgeo,axisfigure,loc,colorline,data_strucutre,name_labels,reseau,anchor):
    west,east,north,south = rectgeo
    axins = zoomed_inset_axes(axisfigure, 5,bbox_to_anchor=anchor,bbox_transform=axisfigure.figure.transFigure)
#     axins = zoomed_inset_axes(axisfigure, 2, loc=loc,bbox_to_anchor=anchor)
    #definition du petit carre
    #axins.set_xlim(-30, 0)
    #axins.set_ylim(3, 18)
    #define the petite box coordinates geogrpahic
#     axins.set_xlim(-130,-120)
#     axins.set_ylim(25, 50)
    axins.set_xlim(west,east)
    axins.set_ylim(south, north)
    
    
    plt.xticks(visible=False)
    plt.yticks(visible=False)
    
    mark_inset(axisfigure, axins, loc1=2, loc2=4, fc="none", ec=colorline,zorder=-1,lw=2)
    map2 = Basemap(llcrnrlon=west,llcrnrlat=south,urcrnrlon=east,urcrnrlat=north, ax=axins,resolution='i')
#     map2 = Basemap(llcrnrlon=-125,llcrnrlat=25,urcrnrlon=-115,urcrnrlat=50, ax=axins)
#     map2.drawmapboundary(fill_color='#7777ff')
    map2.drawmapboundary(fill_color='#85A6D9')
    map2.fillcontinents(color='#ddaa66', lake_color='#7777ff', zorder=-10)
#     m.fillcontinents(color='grey',lake_color='#85A6D9',zorder=-10)
    map2.drawcoastlines(color='#6D5F47', linewidth=.4)
    map2.drawcountries()
    #partie payload de l inset
    core_map_draw(data_strucutre,name_labels,reseau,map2,add_name=True)
    
    

    
#     map2.scatter(x, y, s=cases/5., c='r', alpha=0.5)
    
    
    
def add_name_of_some_buoys(df,name_labels,projection):
    if name_labels is not None:
        for nami in name_labels:
            condi = (df['name']==nami)
            if np.sum(condi)>0:
                Lon = df['lon'][condi].values[0]
                Lat = df['lat'][condi].values[0]
                print 'jai trouver',nami,Lon,Lat
                x,y = projection(Lon,Lat)
    #             ax = plt.gca()
                plt.text(x,y,nami,fontsize=FONTSIZE)
            #ax.annotate(nami,xy=(x,y), xycoords='data', xytext=(x, y), textcoords='data')
    


def core_map_draw(df,name_labels,reseau,projection,rectarea,add_name=False,legend=False):
    alpha=0.5
    if len(df)>0:
#         print df
        markersize = 9
        if add_name:
            add_name_of_some_buoys(df,name_labels,projection)
        #je trouve que cest idiot de ne pas tout montrer
    #             if withspectra==False:
    #                 condi = (data_strucutre[reseau]['With_spectra']==False)
    #                 print "reduce to non directional buoys minus %s buoys"%(np.sum(condi)),
    #                 data_strucutre[reseau] = data_strucutre[reseau][condi]
    #             elif withspectra == 'only':
    #                 condi = (data_strucutre[reseau]['With_spectra']==True)
    #                 print "reduce to directional buoys minus %s buoys"%(np.sum(condi)),
    #                 data_strucutre[reseau] = data_strucutre[reseau][condi]
    #             else:
    #                 print 'we take all buoys with or without spectra'
        #seperate the dataset
        typebuoys = {'spectral_not_directional':((df['With_spectra']==True) & (df['directional']==False),'*',' spectral not directional','red'),
                     'classic':((df['With_spectra']==False),'.','','blue'),
                     'spectraldirectional':((df['directional']==True),'o',' spectral and directional','m')}
        for wispec  in typebuoys.keys(): 
            condi = typebuoys[wispec][0]
            #reduce dataset to the area chosen
            west,east,north,south=rectarea
            conditions_geo = (df['lon']<east) & (df['lon']>west) & (df['lat']<north) & (df['lat']>south)
            sub_dataset = df[condi & conditions_geo]
            if len(sub_dataset)>0:
                LON = sub_dataset['lon'].values
                if len(LON)>1:
                    plotok = True
                LAT = sub_dataset['lat'].values
                wmo = sub_dataset['wmo']
                label_sent = reseau+' network '+str(len(wmo))+typebuoys[wispec][2]+' buoys'
                x,y=projection(LON,LAT)
                marker = typebuoys[wispec][1]
                projection.plot(x,y,dioc[reseau][0]+dioc[reseau][2],alpha=alpha,markersize=markersize,markeredgecolor='black',markeredgewidth=0.1,marker=marker,color=typebuoys[wispec][3])
                plpl = projection.plot(x[0],y[0],dioc[reseau][0]+dioc[reseau][2],alpha=alpha,label=label_sent,markersize=markersize,markeredgecolor='black',markeredgewidth=0.1,marker=marker,color=typebuoys[wispec][3])
    if legend:
        plt.legend(fontsize=FONTSIZE)
    return plpl
        
        

def PlotMapBuoys(data_strucutre,sensor,month,year,network=None,area='global',suffix=None,output=None,withspectra=True,name_labels=None,insetzoom=True):
    '''
    map parameters
    :arg:
        withspectra (bool/str): True/False/'only'
    '''
    
    logging.info('start mapping data')
#     if sensor is None:
#         sensor_str = ''
#     else:
#         sensor_str = sensor
#     if suffix is None:
#         suffix_str = ''
#     else:
#         suffix_str =suffix
#     logging.info('sensor sought: %s',sensor)
    plotok = False
    fig = plt.figure(figsize=(20,12),facecolor='white')
    ax = plt.subplot(111)
    plt.title('insitu moored platforms available at IFREMER on '+month+'/'+year)
    print areas.keys()
    mW = areas[area][0]
    mE = areas[area][1]
    mN = areas[area][2]
    mS = areas[area][3]
#     if area=='global':
#         mW=-179
#         mE=179
#         mN=70
#         mS=-60
#     elif area=='UWWestCoast':
#     #US West Coast
#         mW=-130.
#         mE=-115
#         mN=51
#         mS=29.0
#     x_offset_annotation = -500000.0
    x_offset_annotation = 10.0
    plt.title('buoy networks available in GlobWave format '+year+'/'+month ,fontsize=FONTSIZE)
    if True:
        m = Basemap(projection='cyl', 
              lat_0=0, lon_0=-0,llcrnrlat=mS,urcrnrlat=mN,llcrnrlon=mW, urcrnrlon=mE,resolution='i')
    else:
        m = Basemap(projection='merc',llcrnrlat=mS,urcrnrlat=mN,llcrnrlon=mW, urcrnrlon=mE,resolution='c')
    m.drawmapboundary(fill_color='#85A6D9')
    m.drawcoastlines(color='#6D5F47', linewidth=.4)
#     m.fillcontinents(color='grey',lake_color='#85A6D9',zorder=-10)
    m.fillcontinents(color='#ddaa66', lake_color='#7777ff', zorder=-10)
#     m.drawcountries(color='r')
    #m.shadedrelief()
    meridians = np.arange(mW,mE,np.around((mE-mW)/10.0))
    m.drawmeridians(meridians,labels=[0,0,0,1],fontsize=FONTSIZE,fmt='%i')
    parallels = np.arange(mS,mN,np.around((mN-mS)/10.0))
    m.drawparallels(parallels,labels=[1,0,0,0],fontsize=FONTSIZE,fmt='%i')
    

    
    #add buoys name
    if network == None:
        for reseau in data_strucutre:
            df = data_strucutre[reseau]
            plpl = core_map_draw(df,name_labels,reseau,projection=m,add_name=False,legend=True,rectarea=areas[area])
                    
            #inset zoom
            if insetzoom:
                for zone in areas_inset:
#                 for zone in ['north_pacific']:
                    print "zone",zone
                    rectgeo = areas_inset[zone][0]
                    west,east,north,south = rectgeo
                    conditions = (data_strucutre[reseau]['lon']<east) & (data_strucutre[reseau]['lon']>west) & (data_strucutre[reseau]['lat']<north) & (data_strucutre[reseau]['lat']>south)
                    subset_datastruct = data_strucutre[reseau][conditions]
                    add_inset_zoom(rectgeo,ax,areas_inset[zone][1],areas_inset[zone][2],subset_datastruct,name_labels,reseau,areas_inset[zone][3])
#     else: #deprecated
#         reseau = network
#         #reduce dataset to the area chosen
#         conditions = (data_strucutre[reseau]['lon']<east) & (data_strucutre[reseau]['lon']>west) & (data_strucutre[reseau]['lat']<north) & (data_strucutre[reseau]['lat']>south)
#         subset_datastruct = data_strucutre[reseau][conditions]
#         LON = subset_datastruct['lon']
#         LAT = subset_datastruct['lat']
#         wmo = subset_datastruct['wmo']
#         label_sent = reseau+' network '+str(len(wmo))+' buoys'
#         if len(LON)>1:
#             plotok = True
#             x,y=m(LON,LAT)
# #             pdb.set_trace()
#             m.plot(x,y,dioc[reseau][0]+'.',alpha=0.9,markersize=markersize,markeredgecolor='black',markeredgewidth=0.01)
#             m.plot(x[0],y[0],dioc[reseau][0]+'.',alpha=0.9,label=label_sent,markersize=markersize,markeredgecolor='black',markeredgewidth=0.01)
#             for cc,ll in enumerate(LON):
#                 plt.annotate(wmo[cc], xy=(x[cc],y[cc]), xytext=(x[cc]+x_offset_annotation,y[cc] ),fontsize=6,arrowprops=dict(facecolor='black', shrink=0.05,width=0.4,headwidth=0.4),)
    if plotok == True:
        for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
             ax.get_xticklabels() + ax.get_yticklabels()):
            item.set_fontsize(20)
        ax.legend(fontsize=10,bbox_to_anchor=[1.1, 0.2])
        plt.draw()
        for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
             ax.get_xticklabels() + ax.get_yticklabels()):
            item.set_fontsize(20)
        plt.show()
        if output is None:
            if network == None:
                output = '/home/cercache/users/agrouaze/resultats/globwave_monitore/map_globwave_networks_'+year+'_'+month+'_'+suffix_str+'_'+sensor_str+'.png'
            else:
                output = '/home/cercache/users/agrouaze/resultats/globwave_monitore/map_globwave_network_'+reseau+'_'+year+'_'+month+'_'+suffix_str+'_'+sensor_str+'.png'
        plt.savefig(output,dpi=300)
        logging.info('file %s',output)
    else:
        logging.info('no plot because no data')
    return
# 
# def prepare_data_buoys(reseau,year,month,capteur,suffix,chosen_area,parameters):
#     """*put all buoys data in a dataframe pandas
#     """
#     
#     LON,LAT,wmo,dist2shore,FILES,flagspectra,names = GetThePositions(reseau,year,month,capteur,suffix,area=chosen_area,parameters=parameters)
#     data = {}
#     data['lon'] = LON
#     data['lat'] = LAT
#     data['wmo'] = wmo
#     data['dist'] = dist2shore
#     data['With_spectra'] = flagspectra
#     data['name'] = names
#     df = pd.DataFrame(data=data,columns=data.keys())
#     return df

def wrapper_to_map_networks_together(databuoys,networks_list,year,month,zone=None,output=None,withspectrum=True,name_labels=None,insetzoom=True):
    """
    wrapper that allow to map many buoy networks on the same plot
    :Args:
        databuoys (dict): dictionnary containing the dataframe of each buoys netwroks
        name_labels (list): list of str of the name buoys to annotate
    """
    logging.info('start map buoys for netowrks: %s',networks_list)
    #     parameters = ['relative_humidity','wind_speed','sea_surface_temperature','air_temperature']
#     parameters = ['dew_point_temperature','wind_speed','sea_surface_temperature','air_temperature']
    parameters = None
    if zone is None:
        chosen_area = 'global'
    else:
        chosen_area = zone
#     month = '10'
    
    suffix = ''
    suffix ='spectrum'
    data = {}
    for reseau in networks_list:
        logging.info('reseau : %s',reseau)
#     for reseau in ['CETMEF']:
        if reseau in ['PMEL','CDIP','ECMWF']:
            capteur = 'anemometer'
        else:
            capteur = 'wave_sensor'
        filout = os.path.join(MONITORE_DIR,'positions_buoys_%s_%s_%s_%s_%s.txt' % (chosen_area,capteur,year,month,reseau))
        data[reseau] = {}
        logging.info('%s', reseau)
        #dfbuoys = prepare_data_buoys(reseau,year,motnh,capteur,suffix,chosen_area,parameters)
        #data[reseau] = dfbuoys
#         print len(LON),len(dist2shore),len(LAT),len(wmo)
#         PrintPositions2File(LON,LAT,wmo,dist2shore,FILES,filout)
#     PlotMapBuoys(data,capteur,network='NDBC',area=chosen_area)
#     PlotMapBuoys(data,capteur,network='CDIP')
#         PlotMapBuoys(data,capteur,network=reseau)
    PlotMapBuoys(databuoys,capteur,month,year,area=chosen_area,output=output,withspectra=withspectrum,name_labels=name_labels,insetzoom=insetzoom)
    return data

if __name__ == '__main__':
    from optparse import OptionParser
    list_choice_usage = ['map','table']
    parser = OptionParser()
    parser.add_option("-v","--verbose",
                      action='store_true',default=False,
                      help="verbose mode [default is quiet]")
    parser.add_option("-n","--network",
                      action='store',default=dioc.keys(),dest='network',
                      help="which network you want to analyze [optional default is all: %s]"%dioc.keys())
    parser.add_option("-y","--year",
                      action='store',default=str(datetime.datetime.now().year),dest='year',
                      help="which year (YYYY) you want to analyze [optional default is current]")
    parser.add_option("-m","--month",
                      action='store',default=str(datetime.datetime.now().month),dest='month',
                      help="which month (MM) you want to analyze [optional default is current]")
    parser.add_option("-o","--output",
                      action='store',dest='output',
                      help="path where to store the png to be created [optional]")
    parser.add_option("-a","--area",
                      action='store',dest='area',
                      help="which area you want to analyze in %s [optional default is global]"%areas)
    parser.add_option("-u","--usage",
                      action='store',type='choice',choices=list_choice_usage,
                      dest='usage',
                      help="choose what kind of information you want to get in %s [mandatory]"%list_choice_usage)
    (options, args) = parser.parse_args()
    if options.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO,filemode='w', format='%(asctime)s %(lineno)d %(levelname)s %(message)s')
    year = '2017'
    if options.usage is None:
        raise Exception('must give -u option')
#     capteur = 'wave_sensor'
#     capteur = 'anemometer'
#     suffix = ''
#     parameters = 'central_frequency'
#     parameters = None
    if isinstance(options.network,str):
        networkz = [options.network]
    else:
        networkz = options.network
    if options.usage == 'map':
        wrapper_to_map_networks_together(networkz,options.year,options.month,zone=options.area)
    elif options.usage == 'table':
#         build_stat_network_table(year,'anemometer',suffix='',parameters=None)
#         build_stat_network_table(year,'wave_sensor',suffix='',parameters=None)
        build_stat_network_table(year,'wave_sensor',suffix='',spectrum_test=True)
#     
