"""
@author: Antoine GROUAZEL
@purpose: creation of kml file containing position in time of the different buoys of a network
@context: I need an easy tool to check coloc possibilties (calypso is too complicated presently)
"""
#TODO add variables in description window
#add all dates
import numpy
import netCDF4
import glob
import logging
import time
import os
from optparse import OptionParser
import pdb
import simplekml
import datetime
dir_globwave = '/home/cercache/project/globwave/data/globwave/'
googl_url = 'http://maps.google.com/mapfiles/kml/paddle/'

YEARS = ['2014','2015']
# dir_network = {
#                'cetmef':(dir_globwave+'cetmef/latest/wave_sensor/2015/04/',simplekml.Color.yellow,googl_url+'ylw-circle.png',googl_url+'ylw-stars.png'),
#                'coriolis':(dir_globwave+'oceansites/anemometer/2013/04/',simplekml.Color.blue,googl_url+'pink-circle.png',googl_url+'pink-stars.png'),
# #                'ndbc':(dir_globwave+'/ndbc/nrt/wave_sensor/2015/04/',simplekml.Color.yellow,'http://maps.google.com/mapfiles/kml/pushpin/pink-pushpin.png'),
#                'ndbc':(dir_globwave+'ndbc/nrt/anemometer/2013/04/',simplekml.Color.green,googl_url+'grn-circle.png',googl_url+'grn-stars.png'),
#                'cdip':(dir_globwave+'cdip/wave_sensor/2015/03/',simplekml.Color.red,googl_url+'red-circle.png',googl_url+'red-stars.png'),
#                'pmel':(dir_globwave+'pmel/anemometer/2015/05/',simplekml.Color.red,googl_url+'orange-circle.png',googl_url+'orange-stars.png'),
#                'meds':(dir_globwave+'meds/latest/wave_sensor/2014/05/',simplekml.Color.purple,googl_url+'purple-circle.png',googl_url+'purple-stars.png'),
#                }
dir_network = {
#                'cetmef':(dir_globwave+'cetmef/latest/*/%s/*/',simplekml.Color.yellow,googl_url+'ylw-circle.png',googl_url+'ylw-stars.png'),
               'coriolis':(dir_globwave+'oceansites/*/%s/*/',simplekml.Color.blue,googl_url+'pink-circle.png',googl_url+'pink-stars.png'),
#                'ndbc':(dir_globwave+'/ndbc/nrt/wave_sensor/2015/04/',simplekml.Color.yellow,'http://maps.google.com/mapfiles/kml/pushpin/pink-pushpin.png'),
               'ndbc':(dir_globwave+'ndbc/nrt/*/%s/*/',simplekml.Color.green,googl_url+'grn-circle.png',googl_url+'grn-stars.png'),
               'cdip':(dir_globwave+'cdip/*/%s/*/',simplekml.Color.red,googl_url+'red-circle.png',googl_url+'red-stars.png'),
               'pmel':(dir_globwave+'pmel/*/%s/*/',simplekml.Color.red,googl_url+'orange-circle.png',googl_url+'orange-stars.png'),
               'meds':(dir_globwave+'meds/*/%s/*/',simplekml.Color.purple,googl_url+'purple-circle.png',googl_url+'purple-stars.png'),
               }
def init_buoy_info():
    geo_loc = {}
    geo_loc['lon'] = numpy.array([])
    geo_loc['lat'] = numpy.array([])
    geo_loc['time_start'] = numpy.array([])
    geo_loc['time_stop'] = numpy.array([])
    geo_loc['directional'] = False
    geo_loc['path'] = numpy.array([])
    geo_loc['sensor'] = []
#     geo_loc['buoyid'] = numpy.array([])
    return geo_loc

def update_buoy_info(geo_loc,lon,lat,start,stop,directional,path,sensor):
    """
    update information for a given buoy id
    the update take place each time there is a new netcdf (by default it is nbparam time per month)
    workaround: update only for wave => 1 new geoloc/month
    """
#     if geo_loc['lon'] == numpy.array([]) or (geo_loc['lon'] != numpy.array([]) and (geo_loc['lon'][-1] != lon or geo_loc['lat'][-1] != lat)): #update only if there is a difference
        
    if 'anemometer' in path: #
        geo_loc['path'] = numpy.concatenate([geo_loc['path'],[path]])
        geo_loc['time_start'] = numpy.concatenate([geo_loc['time_start'],[start]])
        geo_loc['time_stop'] = numpy.concatenate([geo_loc['time_stop'],[stop]])
        geo_loc['lon'] = numpy.concatenate([geo_loc['lon'],lon])
        geo_loc['lat'] = numpy.concatenate([geo_loc['lat'],lat])
    if 'wave_sensor' in path and '_spectrum' in path:
        geo_loc['path'] = numpy.concatenate([geo_loc['path'],[path]])
        geo_loc['time_start'] = numpy.concatenate([geo_loc['time_start'],[start]])
        geo_loc['time_stop'] = numpy.concatenate([geo_loc['time_stop'],[stop]])
        geo_loc['lon'] = numpy.concatenate([geo_loc['lon'],lon])
        geo_loc['lat'] = numpy.concatenate([geo_loc['lat'],lat])
        geo_loc['directional'] = True
    if sensor not in geo_loc['sensor']:
        geo_loc['sensor'].append(sensor)
#         geoloc['time'] = numpy.concatenate([geoloc['time'],times])
#     geo_loc['buoyid'] = numpy.concatenate([geo_loc['buoyid'],[wmo]])
    return geo_loc
    
def read_netcdf(nc_file):
    sensor = os.path.basename(os.path.abspath(os.path.join(nc_file,os.pardir,os.pardir,os.pardir)))
    logging.debug('sensor: %s',sensor)
    logging.debug('open %s',nc_file)
    nc = netCDF4.Dataset(nc_file,'r')
    lon = nc.variables['lon'][:]
    lat = nc.variables['lat'][:]
    times = nc.variables['time'][:]
    start = netCDF4.num2date(times[0],nc.variables['time'].units)
    stop = netCDF4.num2date(times[-1],nc.variables['time'].units)
#     if 'frequency' in nc.dimensions.keys():
    if 'stheta1' in nc.variables.keys():
        directional = True
    else:
        directional = False
#         print start,stop
    aatribs = nc.ncattrs()
    wmo = ''
    base_name = os.path.basename(nc_file)
    if 'WMO' in base_name:
        wmo = base_name.split('_')[0]
    if 'station_name' in aatribs:
        wmo  = nc.getncattr('station_name')
    if wmo == '' and 'platform_code' in aatribs:
        wmo = nc.getncattr('platform_code')
    if wmo == '' and 'platform_name' in aatribs:
        wmo = nc.getncattr('platform_name')
    if wmo == '':
        logging.error('no wmo filled')
        raise
    
#         if len(lon)==1:
#             lon = numpy.repeat(lon,len(times))
#             lat = numpy.repeat(lat,len(times))
    nc.close()
    return wmo,lat,lon,start,stop,directional,sensor

def read_filename(nc_file):
    """
    Returns:
        wmo (str):
        lat (float):
        lon (float):
        start (datetime):
        stop (datetime):
        directional (bool):
        sensor (str):
    """
    sensor = os.path.basename(os.path.abspath(os.path.join(nc_file,os.pardir,os.pardir,os.pardir)))
    logging.debug('sensor: %s',sensor)
    base_name = os.path.basename(nc_file)
    splitos = base_name.split('_')
    wmo = splitos[0]
    if splitos[3].lower() == 'lat':
        if splitos[4][-1]=='S':
            factorLA = -1.
        else:
            factorLA = 1.
        if splitos[6].strip('.nc')[-1]=='W':
            factorLO = -1.
        else:
            factorLO = 1.
        lat = factorLA*numpy.array([float(splitos[4][0:-1])])
        lon = factorLO*numpy.array([float(splitos[6].strip('.nc')[0:-1])])
#     elif splitos[3].lower() == 'lon':
#         lon = numpy.array([float(splitos[4][0:-1])])
#         lat = numpy.array([float(splitos[6].strip('.nc')[0:-1])])
    if 'spectrum' in base_name:
        directional = True
    else:
        directional = False
    start = datetime.datetime.strptime(splitos[1],'%Y%m%dT%H%M')
    stop = datetime.datetime.strptime(splitos[2],'%Y%m%dT%H%M')
    return wmo,lat,lon,start,stop,directional,sensor
    

def get_buoys_data(dir_data,read_netcdf_flag=False):
    '''
    browse a directory and return a dictionary of geolocations
    Args:
        dir_data (str): directory where are stored the buoys (e.g. dir_globwave+'cdip/*/*/*/ )
        read_netcdf_flag (bool): [optional]
    '''
    buoy_data = {}
    list_nc = glob.glob(dir_data+'*.nc')
    logging.info('%s %s netcdf found',dir_data,len(list_nc))
    for nc_file in list_nc:
        if read_netcdf_flag == True:
            wmo,lat,lon,start,stop,directional,sensor = read_netcdf(nc_file)
        else:
            wmo,lat,lon,start,stop,directional,sensor = read_filename(nc_file)
        if wmo not in buoy_data.keys():
            buoy_data[wmo] = init_buoy_info()
        buoy_data[wmo] = update_buoy_info(buoy_data[wmo],lon,lat,start,stop,directional,nc_file,sensor)
    return buoy_data

def write_kml_file(outputdir):
    name_out = 'ifremer_globwave_insitu_buoys.kml'
    doc = simplekml.Kml(open=1,name=name_out)
    for network in dir_network.keys():
        start_a_network = time.time()
        logging.info('network %s',network)
        dir_data = dir_network[network][0]
        subfold = doc.newfolder(name=network)
        for year in YEARS:
            buoy_data = get_buoys_data(dir_data%year)
            
    #         for bb,buoys in enumerate(buoy_data['buoyid']):
            for bb,buoys in enumerate(buoy_data.keys()):
                for ff,files in enumerate(buoy_data[buoys]['lon']):#loop over the files???
                    single_point = subfold.newpoint(name=buoys, coords=[(buoy_data[buoys]['lon'][ff],buoy_data[buoys]['lat'][ff])])
                    start_date_str = datetime.datetime.strftime(buoy_data[buoys]['time_start'][ff],'%Y-%m-%d')
                    stop_date_str = datetime.datetime.strftime(buoy_data[buoys]['time_stop'][ff],'%Y-%m-%d')
                    single_point.timespan.begin = start_date_str
                    single_point.timespan.end = stop_date_str
                    single_point.style.color = dir_network[network][1]
                    description = buoy_data[buoys]['path'][ff] + '\n'
                    for sese in buoy_data[buoys]['sensor']:
                        description += sese+'\n'
                    single_point.style.balloonstyle.text = description
                    if buoy_data[buoys]['directional']:
                        single_point.style.iconstyle.icon.href = dir_network[network][3]
                    else:
                        single_point.style.iconstyle.icon.href = dir_network[network][2]
        elapsed_a_netqwork = time.time()-start_a_network
        logging.info('it tooks %1.0f seconds to treat %s network',elapsed_a_netqwork,network)
        logging.info('Nber of buoys found: %s',len(buoy_data.keys()))
    ouptut_path = os.path.join(outputdir,name_out)
    doc.save(ouptut_path)
    logging.info('output %s',ouptut_path)
    return



if __name__ == '__main__':
    root = logging.getLogger()
    if root.handlers:
        for handler in root.handlers:
            root.removeHandler(handler)
    parser = OptionParser()
    parser.add_option("-o","--outputdir",
                      action="store", type="string",
                      dest="outputdir", metavar="string",
                      help="directory where will be created the .kml ")
    parser.add_option("-q","--quiet",
                      action="store_true", default=False,
                      dest="quiet",
                      help="quiet mode")
    (options, args) = parser.parse_args()
    if options.quiet:
        logging.basicConfig(level=logging.INFO)
    else:
        logging.basicConfig(level=logging.DEBUG)
    logging.info('start creation of globwave KML file')
    if options.outputdir is None:
        outputdir = '/home/cercache/project/globwave/data/globwave/'
    else:
        outputdir = options.outputdir
    write_kml_file(outputdir)
    logging.info('end of script')