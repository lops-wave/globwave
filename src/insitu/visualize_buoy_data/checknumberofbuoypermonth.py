#pprog to monitor the number of buoys per month per provider in globwave format
#agrouaze
#2014
import glob,os
import logging
import numpy
import logging
import time
import datetime
import pylab
import netCDF4
from matplotlib import rc, font_manager
import matplotlib.dates as mdates
import pdb
import pygal
import pandas as pd
from matplotlib import pyplot as plt
import sys
from dateutil import rrule,relativedelta
sys.path.append('/home/losafe/users/agrouaze/git/globwave/src/insitu')
from globwave_shared_infos import active_networks
from globwave.src.insitu.read_buoys_data.buoy_reader import read_buoy
from map_buoy_networks import wrapper_to_map_networks_together,build_stat_network_table,GetThePositions
paths = {'ndbcnrt':'/home/cercache/project/globwave/data/globwave/ndbc/nrt/wave_sensor/',
         'ndbcarchive':'/home/cercache/project/globwave/data/globwave/ndbc/archive/wave_sensor/',
         'oceansites':'/home/cercache/project/globwave/data/globwave/oceansites/wave_sensor/',
         'test':'/home/cercache/users/agrouaze/data/test_globe_wave_convert/wave_sensor/'
         }
active_networks
def CountNumberOfDistinctBuoy(yearstr,monthint,network,dir_data,extension='*.nc'):
    month = str(monthint).zfill(2)
    if dir_data is not None:
        dirtmp = dir_data+yearstr+'/'+month+'/*'+extension
    else:
#         dirtmp = paths[network]+yearstr+'/'+month+'/*'+extension
        dirtmp = os.path.join(active_networks[network.upper()],'wave_sensor',yearstr,month,'*'+extension)
    logging.debug('dir %s',dirtmp)
    files = glob.glob(dirtmp)
    platlist = []
    for ff in files:
        base = os.path.basename(ff)
        platname = base.split('_')[0]
#         logging.debug('%s',platname)
        if platname not in platlist:
            platlist.append(platname)
#     print len(platlist)
    logging.debug('nuber of buoys: %s for month %s %s' ,len(platlist),month,yearstr)
    return len(platlist),platlist

def CountNumberOfFilesPerMonth(yearstr,monthint,network,dir_data,extension='.nc'):
    '''additional functinnality: gives the latest modification date of the files'''
    month = str(monthint).zfill(2)
    if dir_data is not None:
        files = glob.glob(dir_data+yearstr+'/'+month+'/*'+extension)
    else:
#         files = glob.glob(paths[network]+yearstr+'/'+month+'/*'+extension)
        files = glob.glob(os.path.join(active_networks[network.upper()],'wave_sensor',yearstr,month,'*'+extension))
    latestdate = datetime.datetime(1900,12,1)
    
    version_count = {}
    for ff in files:
        logging.debug('file %s',ff)
#         nc = netCDF4.Dataset(ff,'r')
#         version = nc.getncattr('processor_version')
#         nc.close()
#         if version not in version_count:
#             version_count[version] = 1
#         else:
#             version_count[version] = version_count[version] +1
#          dateofmodif = time.ctime(os.path.getmtime(ff))
        dateofmodif = time.strftime('%m/%d/%Y', time.gmtime(os.path.getmtime(ff)))
        dateofmodif_dt = datetime.datetime.strptime(dateofmodif,'%m/%d/%Y')
#         logging.debug('modif date %s %s',dateofmodif,type(dateofmodif))
        if dateofmodif_dt>latestdate:
            latestdate = dateofmodif_dt
#     ld = netCDF4.date2num(latestdate,units='days since 1900-02-01')
    logging.debug('latestdate %s',latestdate)
    return len(files),latestdate,version_count

def CompareNumberOfPlatform(platnb1,platnb2,timex,outputdir,network1,network2):
    fontProperties = {'family':'sans-serif','sans-serif':['Helvetica'],
    'weight' : 'normal', 'size' : 9}
    ticks_font = font_manager.FontProperties(family='Helvetica', style='normal',
    size=7, weight='normal', stretch='normal')
    fig = plt.figure(facecolor='white')
    plt.grid()
    plt.title('monthly statistics for GlobWave %s vs %s' %(network1,network2),fontsize=10)
    plt.bar(timex,platnb1,width=30,color='blue',label=network1)
    bars = plt.bar(timex,platnb2,color='r',width=30,label=network2)
    lega = plt.legend(bbox_to_anchor=(0.65,0.95),prop={'size':7})
    ax = plt.gca()
    ax.set_xlabel('Date [YYYY/MM]',fontsize=8)
#     ax.set_xticklabels(ax.get_xticks(), fontProperties)
    ax.set_xticklabels(numpy.arange(start, end+30, 720), fontProperties)
    for label in ax.get_xticklabels():
        label.set_rotation(30)
        label.set_horizontalalignment('right')
        label.set_fontproperties(ticks_font)
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y/%m'))
    ax.tick_params(axis = 'both', which = 'major', labelsize = 12)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 6)
    ax.set_ylabel('#')
    start, end = ax.get_xlim()
#     ax.xaxis.set_ticks(numpy.arange(start, end+30, 720))
    ax.set_xlim([-60+start,end+60])
    plt.show()
    filout = outputdir+network1+'_vs_'+network2+'_numberOfplatperMonth.png'
    plt.savefig(filout)
    logging.debug('figure output %s',filout )
    return

def PlotTotalOverYears(platnb,filenb,timex,start_year,end_year,modifdate,outputdir,network,totalversion=False):
#     ff = figure
#     fontProperties = {'family':'sans-serif','sans-serif':['Helvetica'],
#     'weight' : 'normal', 'size' : 9}
    ticks_font = font_manager.FontProperties(family='Helvetica', style='normal',
    size=7, weight='normal', stretch='normal')
    fig = plt.figure(facecolor='white',figsize=(9,8))
    nono = datetime.datetime.now()
    plt.grid()
    plt.title('monthly statistics for GlobWave %s \n last update %s' %(network,nono),fontsize=10)
    print len(timex),len(filenb)
    if totalversion != False:
        versions = {'0.4':('green'),'0.3':('blue'),'0.5':('yellow')}
        layer = {}
        for vv in versions:
            layer[vv] = []
            for titi in totalversion:
                print 'titi',titi
                if vv in titi:
                    print 'add',titi[vv]
                    layer[vv].append(titi[vv])
                else:
                    layer[vv].append(0)
        bot = numpy.zeros(len(timex))
        print 'layer,',layer['0.4'],len(layer['0.4'])
        for vv in versions:
            print vv
            if len(layer[vv])>0:
                plt.bar(timex,layer[vv],width=30,bottom=bot,color=versions[vv][0],label='number of files per version '+vv)
                bot = bot + layer[vv]
    else:
        plt.bar(timex,filenb,width=29,color='blue',label='number of files having wave parameters')
    timex = numpy.asanyarray(timex)
    platnb = numpy.asanyarray(platnb)
    logging.debug('shape of timex %s',timex.shape)
    plt.bar(timex,platnb,width=30,color='red',label='number of buoys having wave parameters',edgecolor = "none",alpha=0.7)
    lega = plt.legend(bbox_to_anchor=(0.65,0.95),prop={'size':7})
    ax = plt.gca()
    ax.set_xlabel('Date [YYYY/MM]',fontsize=8)
#     ax.set_xticklabels(ax.get_xticks(), fontProperties)
    for label in ax.get_xticklabels():
        label.set_rotation(30)
        label.set_horizontalalignment('right')
        label.set_fontproperties(ticks_font)
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y/%m/%d'))
    ax.tick_params(axis = 'both', which = 'major', labelsize = 12)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 6)
    ax.set_ylabel('#')
    start, end = ax.get_xlim()
#     ax.xaxis.set_ticks(numpy.arange(start, end+30, 720))
    ax.set_xlim([-60+start,end+60])
    plt.show()
    filout = outputdir+network+'_numberOfbuoyperMonth.png'
    plt.savefig(filout,dpi=250)
    logging.info('figure output %s',filout )
    #pygal
    bar_chart = pygal.Bar(margin=50)
    timex_str = []
    for tt in timex:
        timex_str.append(str(tt))
#     bar_chart.add('file number',[{'value': platnb, 'label': timex_str}])
    bar_chart.x_labels = timex_str
    bar_chart.add('file number', filenb)  # Add some values
    bar_chart.add('platform number',platnb)
    bar_chart.render_to_file(filout+'.svg')
    return

def CheckThatCalValS1AbuoysExist(yearstr,monthint,network):
    '''check that the list of calsite buoy from MPC sentinel1 is present in a network'''
    buoyslist=[
               '62103',
               '62107',
               '62142',
               '62144',
               '62145',
               '62146',
               '62164',
               '62165',
               '62301',
               '62303',
               '62304',
               '62305',
               '63112',
               '63113',
               '63110',
               '64046',
               '51002',
               '51003',
               '51004',
               '51100',
               '51101',
               '62029',
               '62081',
               '64045',
               '62105',
               '62095',
               '62163',
               '62001',
               '32012' #'stratus'
               ] #list defined by fabrice collard in updated kmz for cal sites  jira ticket : MPCS-619
    nb,names = CountNumberOfDistinctBuoy(yearstr,monthint,network)
    cpt=0
    for ii,buoy in enumerate(buoyslist):
        if 'WMO'+buoy in names:
            cpt+=1
            logging.info('buoy %s present',buoy)
        else:
            logging.info('buoy %s absent',buoy)
    logging.info('%s / %s present',cpt,len(buoyslist))
    return 
# def CompareNumberOfBuoysPerMonth(rep1,rep2):

def CountNumber_files_not_processed_PerMonth(date,network):
    """
    Args:
        date (datetime):
        network (str):
    """
    dic_conv_net = {'CORIOLIS':'oceansites'}
    if network in dic_conv_net.keys():
        net_mod =  dic_conv_net[network]
    else:
        net_mod = network
    if network in ['CORIOLIS']:
        dir_log = '/home/cercache/users/agrouaze/resultats/globwave_monitore/'

        pattern = os.path.join(dir_log,'list_'+net_mod+'_files_not_converted_'+date.strftime('%Y%m')+'*.txt')
        list_log = glob.glob(pattern)
        nb_not_process = 0
        for log in list_log:
            fid = open(log,'r')
            content = fid.readlines()
            nb_not_process += len(content)
            fid.close()
    else:
        dir_log = '/home/cercache/users/agrouaze/workspace/'

        pattern = os.path.join(dir_log,'globwave'+network,date.strftime('%Y%m')+'*','*','output','listing.err.*')
        list_log = glob.glob(pattern)
        nb_not_process = 0
        for log in list_log:
            fid = open(log,'r')
            content = fid.readlines()
            nb_not_process += len(content)
            fid.close()
    return nb_not_process

def plot_not_processed(totalfile,totaldatemonth,startyear,stopyear,outputdir,network):
    fontProperties = {'family':'sans-serif',
    'weight' : 'normal', 'size' : 9}
    ticks_font = font_manager.FontProperties(family='Helvetica', style='normal',
    size=7, weight='normal', stretch='normal')
    fig = plt.figure(facecolor='white',figsize=(9,8))
    nono = datetime.datetime.now()
    plt.grid()
    plt.title('monthly processing statistics for GlobWave %s \n last update %s' %(network,nono),fontsize=10)
    plt.bar(totaldatemonth,totalfile,width=29,color='blue',label='number of source files not processed')
    lega = plt.legend(bbox_to_anchor=(0.65,0.95),prop={'size':7})
    ax = plt.gca()
    ax.set_xlabel('Date [YYYY/MM]',fontsize=8)
    ax.set_xticklabels(ax.get_xticks(), fontProperties)
    for label in ax.get_xticklabels():
        label.set_rotation(30)
        label.set_horizontalalignment('right')
        label.set_fontproperties(ticks_font)
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y/%m/%d'))
    ax.tick_params(axis = 'both', which = 'major', labelsize = 12)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = 6)
    ax.set_ylabel('#')
    start, end = ax.get_xlim()
#     ax.xaxis.set_ticks(numpy.arange(start, end+30, 720))
    ax.set_xlim([-60+start,end+60])
    plt.show()
    filout = outputdir+'NumberOf_file_not_processed_perMonth_'+network+'.png'
    plt.savefig(filout,dpi=250)
    logging.info('figure output %s',filout )
    #pygal
    bar_chart = pygal.Bar(margin=50)
    timex_str = []
    for tt in totaldatemonth:
        timex_str.append(str(tt))
#     bar_chart.add('file number',[{'value': platnb, 'label': timex_str}])
    bar_chart.x_labels = timex_str
    bar_chart.add('nb of file not processed', totalfile)  # Add some values
    bar_chart.render_to_file(filout+'.svg')
    
    
def count_number_obs_ndbc_provider():
    """
    because of missing data for the 201107 in ndbc DM provider files
    i need a plot to explain
    """
#     id = '51000'
#     phi = numpy.arange(0,365,10)
#     time_wd = {'type':'t1t2','startDate': datetime(2007,10,1,14,0),'stopDate':datetime(2007,12,28,15,30)}    
#     lon,lat,time,f,k,sf,sp_list = read_buoy(id,time_wd,'ndbc_arc',phi = phi,con = 'to')
    year = '2011'
    for rep in ['meteo']:
        diro = os.path.join('/home/cercache/project/globwave/data/provider/ndbc/meteo/',year+'/')
        list_fiels = glob.glob(diro+'*.nc')
#         if 'meteo' in diro:
#             rep = 'meteo'
#         else:
#             rep = 'spectra'
        nb_obs = {}
        nb_files = {}
        monthes = []
        list_fiels = list_fiels[0:20]
        for fi,ff in enumerate(list_fiels):
            logging.debug(' file %s/%s %s',fi,len(list_fiels),ff)
            nc = netCDF4.Dataset(ff)
            times = nc.variables['time'][:]
            unti = nc.variables['time'].units
            timesdtd = netCDF4.num2date(times,unti)
            for tt in timesdtd:
                month = tt.strftime('%b')
                month_dt = datetime.datetime.strptime(month,'%b')
                if month not in nb_obs.keys():
                    nb_obs[month] = len(times)
                    monthes.append(month_dt)
                else:
                    nb_obs[month] += len(times)
                if month not in nb_files.keys():
                    nb_files[month] = [ff]
                else:
                    if ff not in nb_files[month]:
                        nb_files[month].append(ff)
    #         nb_obs += len()
            nc.close()
        df = pd.DataFrame.from_dict(nb_obs,orient='index')
        df.sort_index()
#         if rep =='meteo':
#             df = pd.DataFrame(list(nb_obs.iteritems()),
#                               columns=['date','nb_obs_'+rep])
#         else:
#             df['nb_obs_'+rep] = pd.Series(list(nb_obs.itervalues()), index=df.index)
#         nb_files_int = [len(nb_files[i]) for i in nb_files.keys()]
#         df['nb_files_'+rep] = pd.Series(nb_files_int, index=df.index)
    print df.keys(),df
#     plt.subplot(2,2,1)
#     df.nb_obs_spectra.plot(kind='bar',ax=plt.gca())
#     plt.title('Nber of observations in NDBC %s archive %s'%(rep,year))
#     plt.subplot(2,2,2)
    plt.title('Nber of observations in NDBC %s archive %s'%(rep,year))
#     df.nb_obs_meteo.plot(kind='bar',ax=plt.gca())
    df.plot(kind='bar')
    plt.gca().set_xlabel('Month of year')
#     plt.subplot(2,2,3)
#     plt.title('Nber of files in NDBC %s archive %s'%(rep,year))
#     df.nb_files_spectra.plot(kind='bar',ax=plt.gca())
#     plt.subplot(2,2,4)
#     df.nb_files_meteo.plot(kind='bar',ax=plt.gca())
#     plt.text(1,1,'Nber of files %s'%)
#     plt.bar(monthes,nb_files_int,label='Nber of files per month')
#     plt.show()
#     plt.title('Nber of files in NDBC %s archive %s'%(rep,year))
#     plt.grid()
#     plt.legend()
    filout = '/tmp/nb_obs_year_'+year+'.png'
    plt.savefig(filout)
#     print 'nb files',nb_files_int
    logging.info(' %s ',filout)

def Number_of_buoys_not_processed(network_list,start_date,stop_date):
    for network in network_list:
        logging.info('network: %s',network)
        totalfile = []
        totaldatemonth = []
        for dd in rrule.rrule(rrule.MONTHLY,dtstart=start,until=stop):
            month = dd.month
            year = str(dd.year)
            nbfiles = CountNumber_files_not_processed_PerMonth(dd,network)
            totalfile.append(nbfiles)
            totaldatemonth.append(dd)
        logging.info('total file %s',sum(totalfile))
        plot_not_processed(totalfile,totaldatemonth,start.year,stop.year,outputdir,network)

def perform_analysis_counting(network_list,start_date,stop_date):
    
#     dirdata = '/home/cercache/project/globwave/data/globwave/oceansites/reprocessing/'
    

    dirdata = None
#     syear=2010
#     eyear=2015

#     for yy in range(syear,eyear+1):
#         for mon in range(1,13):
#             monthdate = datetime.datetime(yy,mon,15)

    for network in network_list:
        logging.info('network: %s',network)
        totalfile = []
        totalplat=[]
        totaldate = []
        totaldatemonth = []
        for dd in rrule.rrule(rrule.MONTHLY,dtstart=start,until=stop):
            month = dd.month
            year = str(dd.year)
            nb,names = CountNumberOfDistinctBuoy(year,month,network,dirdata)
    #             nb2,names2 = CountNumberOfDistinctBuoy(str(yy),mon,network2)
            totalplat.append(nb)
    #             totalplat2.append(nb2)
            nbfiles,latestmodif,version_count = CountNumberOfFilesPerMonth(year,month,network,dirdata)
            totalfile.append(nbfiles)
            totaldate.append(latestmodif)
    #             totalversion.append(version_count)
            totaldatemonth.append(dd)
#     logging.debug('totaldate %s',totaldate)
        logging.info('total platform %s',sum(totalplat))
        logging.info('total file %s',sum(totalfile))
        PlotTotalOverYears(totalplat,totalfile,totaldatemonth,start.year,stop.year,totaldate,outputdir,network)
    
if __name__ == '__main__':
    from optparse import OptionParser
    parser = OptionParser()
    actions = ['classic','ndbcbug']
    parser.add_option('-q','--quiet',
                      action='store_true',default=False,
                      dest='quiet',
                      help='quiet mode [default verbose]')
    parser.add_option('-n','--network',
                      action='store',type='choice',choices=active_networks.keys(),
                      dest='network',
                      help='which network [optional default %s]'%active_networks.keys())
    parser.add_option('-a','--action',
                      action='store',type='choice',choices=actions,
                      dest='actions',
                      help='which actions in '%actions)
    (options, args) = parser.parse_args()
    if options.quiet==True:
        logging.basicConfig(level=logging.INFO,format='%(levelname)s - %(message)s')
    else:
        logging.basicConfig(level=logging.DEBUG,format='%(levelname)s - %(message)s')
    logging.info('starting %s',__name__)
    logging.basicConfig(level=logging.DEBUG,format='%(levelname)s - %(message)s')
#     year='2014'
#     outputdir = '/home/cercache/users/agrouaze/resultats/globwave_monitore/'
    outputdir = '/home/cersat5/www/public/cerweb/globwave/images/'
    if options.network is None:
        nets = active_networks.keys()
    else:
        nets = [options.network]
#     network = 'coriolis'
#     network2 = 'test' 
    if options.actions == 'classic':
        stop = datetime.datetime.now()
        start = stop - relativedelta.relativedelta(years=3)
        perform_analysis_counting(network_list=nets,start_date=start,stop_date=stop)
        Number_of_buoys_not_processed(network_list=nets,start_date=start,stop_date=stop)
        current_year =  str(datetime.datetime.now().year)
        all_data_4_map = {}
        for one_network in nets:
            all_data_4_map[one_network] = GetThePositions(one_network,current_year,datetime.datetime.now().strftime('%m'),capteur='wave_sensor',suffix='',area='global',parameters=None)
        wrapper_to_map_networks_together(all_data_4_map,networks_list=nets,year=current_year,month=datetime.datetime.now().strftime('%m'),output=os.path.join(outputdir,'map_networks_wave.png'))
        build_stat_network_table(current_year,'wave_sensor',suffix='',spectrum_test=True)
    elif options.actions == 'ndbcbug':
        count_number_obs_ndbc_provider()
#     CompareNumberOfPlatform(totalplat,totalplat2,totaldatemonth,outputdir,network,network2)
#     CheckThatCalValS1AbuoysExist('2014','07','ndbcnrt')
#     CheckThatCalValS1AbuoysExist('2014','07','myocean')