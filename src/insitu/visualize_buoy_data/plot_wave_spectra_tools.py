'''script from He Wang (2014 utils.py) 
purpose: give functions to convert variables to 2D spectrum(K,phi)
'''
import numpy as np
from numpy import sin,cos,pi
import sys
import os
from datetime import datetime
from cerform.wave import spfphi2kphi
import matplotlib
matplotlib.use('Agg')
from matplotlib.dates import DateFormatter
sys.path.insert(0,'/home/losafe/users/agrouaze/git/mpc-sentinel/mpc-sentinel/mpcsentinellibs/colocation/ww3spectra/')
from sp_plot import spec_plot1,spec_plot1_test
import netCDF4 as nc
from cerform.wave import period2wavelength
import math
from optparse import OptionParser
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.patches import Polygon
from globwave.src.insitu.read_buoys_data.buoy_nc_reader import read_buoy_nc
from globwave.src.insitu.read_buoys_data.data_path import path_globwave
import logging


def plot_spectra_direction_frequency(spectra,directions,frequency,filout):
    ax = plt.subplot(111, polar=True)
    ax.set_theta_offset(np.pi/2.0)
    ax.set_theta_direction(-1)
    my_cmap = plt.cm.get_cmap('rainbow')
    phi,ff = np.meshgrid(directions,frequency)
    CS3=ax.pcolormesh(phi,ff, spectra,cmap=my_cmap)
#     CS3=ax.pcolormesh(directions,frequency, spectra,cmap=my_cmap)
#     CS3=ax.pcolormesh(yedgesRAD,xedges, H,cmap=my_cmap)
    plt.colorbar(CS3)
    ax.grid(True)
    ax.set_title("A line plot on a polar axis", va='bottom')
    plt.savefig(filout)
    logging.info('filout %s',filout)
    return

def plot_hovmuller_spectra(spectra,frequency,time,filout,buoy_name='unknown'):
    ''' 
    time freqency diagram
    Args:
        spectra (numpy array): density energy
        frequency (numpy vector):
        time (numpy datenum vector):
        filout (str): path output file
        buoy_name (str): [optional]
    '''
    logging.info('start Hovmuller diagram')
    fig = plt.figure(figsize=(15,8))
    plt.title('station '+buoy_name)
    ax = plt.gca()
    ax.set_xlabel('time')
    ax.set_ylabel('frequency')
    time_m,f_m = np.meshgrid(frequency,time)
    if len(spectra.shape)==3:
        zz = np.mean(spectra,2)
    else:
        zz = spectra
    bins = np.arange(0.1,5,0.02)
    cont = plt.contourf(f_m,time_m,zz,bins)
    monthFormatter = DateFormatter('%Y%b%d')
    ax.xaxis.set_major_formatter(monthFormatter)
    ax.xaxis_date()
    ax.autoscale_view()
    plt.setp(plt.gca().get_xticklabels(), rotation=45, horizontalalignment='right')
    plt.colorbar(cont)
    plt.savefig(filout)
    logging.info('filout %s',filout)
    return

def read_globwave_spectra(file_path):
    """
    read spectral wave information from a classic globwave format
    """
    dirr = None
    
    fid = nc.Dataset(file_path,'r')
    if 'ndbc' in file_path:
        sf = fid.variables['spectral_wave_density'][:]
        freq = fid.variables['wave_directional_spectrum_central_frequency'][:]
    else:
#     try:
        sf = fid.variables['sea_surface_variance_spectral_density'][:]
#     except:
        
        freq = fid.variables['central_frequency'][:]
    if 'direction' in fid.variables.keys():
        dirr = fid.variables['direction'][:]
    timetmp = fid.variables['time'][:]
    tmetmp = nc.num2date(timetmp,fid.variables['time'].units)
    time = nc.date2num(tmetmp,'days since 0001-01-01 00:00:00')
    buoyname = fid.getncattr('station_name')
    fid.close()
    return buoyname,time,dirr,freq,sf


def test_cetmef_data_globwave_format():
    fn = '/home/cercache/project/globwave/data/globwave/ndbc/nrt/wave_sensor/2015/12/WMO32012_20151214T0800_20151214T2100_Lat_19.69S_Lon_85.57W_spectrum.nc'
#     fn = '/home/cercache/project/globwave/data/globwave/cetmef/tmp_processing/wave_sensor/2015/03/WMO97204_20150301T0000_20150326T1330_Lat_14.55N_Lon_-61.10W.nc'
#     fn = '/home/cercache/project/globwave/data/globwave/cetmef/tmp_processing/wave_sensor/2015/03/WMO01101_20150301T0000_20150326T1230_Lat_42.92N_Lon_3.12E.nc'
#     fn = '/home/cercache/project/globwave/data/globwave/cetmef/tmp_processing/wave_sensor/2015/03/WMO02911_20150301T0000_20150326T1130_Lat_48.29N_Lon_-4.97W.nc'
    buoyname,time,dirr,freq,sf = read_globwave_spectra(file_path=fn)
    k = period2wavelength(1/freq)
    spec,k = spfphi2kphi(np.squeeze(sf[0,:,:]),freq)
    filout0 = '/tmp/cetmef_globwave_spectra_wang.png'
    spec_plot1(spec,k,dir, cut_off=100.,file_out=filout0)
    logging.info('filout : %s',filout0)
    dirr = dirr*np.pi/180.0
    filout = '/tmp/cetmef_globwave_spectra.png'
    plot_spectra_direction_frequency(sf[0,:,:],dir,freq,filout)
    filout2 = '/tmp/cetmef_globwave_spectra_hovmuller.png'
    plot_hovmuller_spectra(sf,freq,time,filout2,buoyname)
    return

if __name__ == '__main__':
    list_usages = ['hovmuller','directional_spectra','cetmef_test']
    root = logging.getLogger()
    if root.handlers:
        for handler in root.handlers:
            root.removeHandler(handler)
    parser = OptionParser()
    parser.add_option("-o","--outputdir",
                      action="store", type="string",
                      dest="outputdir", metavar="string",
                      help="directory where will be created the png ")
    parser.add_option("-q","--quiet",
                      action="store_true", default=False,
                      dest="quiet",
                      help="quiet mode")
    parser.add_option("-u",'--usage',
                      action="store",type='choice',choices=list_usages,
                      dest="usage",
                      help="what do you want: %s ?"%(' or '.join(list_usages)))
    parser.add_option("-n",'--network',
                      action="store",type='choice',choices=path_globwave.keys(),
                      dest="network",
                      help="which network do you want: %s ?"%path_globwave.keys())
    (options, args) = parser.parse_args()
    if options.quiet:
        logging.basicConfig(level=logging.INFO)
    else:
        logging.basicConfig(level=logging.DEBUG)
    if options.outputdir is None:
        raise Exception('you must give a -o args')
#     phi = np.arange(0,365,5)

    #test for ndbc spectrum
#     fn = '/home/cercache/project/mpc-sentinel1/data/colocation/sentinel-1a/sar-mooredbuoy/ndbc_network/L1/WAVE/2014/327/s1a-s1-slc-hh-20141123t112213-20141123t112227-003408-003f9b-001.tiff_sentinel1A_ndbcnrt_matchups.nc'
# 

#     f = nc.Dataset(fn,'r')
# 
#     imatchup = 0
#     time = nc.num2date(f.variables['ndbcnrt_time'][:],f.variables['ndbcnrt_time'].units)[imatchup] 
# 
#     freq = f.variables['ndbcnrt_wave_directional_spectrum_central_frequency'][imatchup,:] 
# 
#     sf = f.variables['ndbcnrt_spectral_wave_density'][imatchup,:]  
#     theta1 = f.variables['ndbcnrt_theta1'][imatchup,:] 
#     theta2 = f.variables['ndbcnrt_theta2'][imatchup,:] 
#     stheta1 = f.variables['ndbcnrt_stheta1'][imatchup,:] 
#     stheta2 = f.variables['ndbcnrt_stheta2'][imatchup,:] 
# 
# 
#     a1,a2,b1,b2 = th2ab(theta1,theta2,stheta1,stheta2)
# 
# 
#     sp_fphi = buoy_spectrum2d(sf,a1,a2,b1,b2, dirs = phi)
#     spec, k= spfphi2kphi(sp_fphi,freq)
#     spec = spfrom2to(spec,phi)
# 
#     spec_plot1(spec,k,phi, cut_off=100.,file_out='test.png')
#     f.close()
    if options.usage == 'cetmef_test':
        test_cetmef_data_globwave_format()
    elif options.usage == 'directional_spectra':
        phi = np.arange(0,365,10)
        idd = '32012'
        time_wd = {'type':'t1t2','startDate': datetime(2015,12,11,23,40),'stopDate':datetime(2015,12,14,23,52)}
        lon,lat,time,f,k,sf,sp2d,c1 = read_buoy_nc(idd,time_wd,phi = phi,con = 'to', provider = options.network)
        
        for cpt in range(len(time)): #can be change because we dont need all the plot
#             datstr = str(time[cpt])
            datstr = time[cpt].strftime('%Y%m%d-%H%M%S')
            filotu = os.path.join(options.outputdir,idd+'_'+datstr+'.png')
            spec_plot1_test(sp2d[cpt],k,phi,title='buoy WMO'+idd,sub_title=datstr, cut_off=200.,file_out=filotu)
            print filotu
    elif options.usage == 'hovmuller':
        filout = os.path.join(options.outputdir,'hovmuller.png')
        fn = '/home/cercache/project/globwave/data/globwave/meds/latest/wave_sensor/2010/09/C44150_20100901T0020_20100930T2220_Lat_42.51N_Lon_64.02W_spectrum.nc'
        fn = '/home/cercache/project/globwave/data/globwave/ndbc/nrt/wave_sensor/2016/05/WMO45004_20160501T0000_20160511T1900_Lat_47.58N_Lon_86.58W_spectrum.nc'
#         fn = '/home/cercache/project/globwave/data/globwave/meds/latest/wave_sensor/2010/10/C44150_20101001T0020_20101031T2220_Lat_42.51N_Lon_64.02W_spectrum.nc'
        buoyname,time,dirr,frequency,spectra = read_globwave_spectra(file_path=fn)
        plot_hovmuller_spectra(spectra,frequency,time,filout,buoyname)
    logging.info('end of plotting')
        