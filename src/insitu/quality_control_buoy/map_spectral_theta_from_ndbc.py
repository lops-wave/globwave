"""
want to see what are the values of theta1 2 stheta1 2 in globwave format
"""
import os
import matplotlib 
import numpy
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import netCDF4
# ff = '/home/cercache/project/globwave/data/globwave/meds/wave_sensor/2015/10/C45138_20151001T0120_20151101T0020_Lat_49.54N_Lon_65.71W_spectrum.nc'
ff = '/home/cercache/project/globwave/data/globwave/ndbc/nrt/wave_sensor/2015/12/WMO52211_20151201T0000_20151201T2200_Lat_15.27N_Lon_145.66E_spectrum.nc'
nc = netCDF4.Dataset(ff)
theta1 = nc.variables['theta1'][:]
theta2 = nc.variables['theta2'][:]
stheta2 = nc.variables['stheta2'][:]
stheta1 = nc.variables['stheta1'][:]
fresq_cental = nc.variables['wave_directional_spectrum_central_frequency'][:]
times = nc.variables['time'][:]
thtas = {'theta1':(theta1,'mean wave direction'),'theta2':(theta2,'principal wave direction'),'stheta1':(stheta1,'directional spread of the entire sea state in degrees'),'stheta2':(stheta2,'directional spread of waves at the peak frequency in degrees')}
nc.close()
# fif = plt.figure()
f, axs = plt.subplots(4, 1,figsize=(18,18))
for ii,tt in enumerate(thtas.keys()):
    plt.sca(axs[ii])
    plt.grid()
    plt.gca().set_xlabel('Frequencies Hz')
    plt.gca().set_ylabel('Time')
    plt.title(tt+' = '+thtas[tt][1] )
    x,y = numpy.meshgrid(fresq_cental,times)
    pcm = plt.pcolormesh(x,y,thtas[tt][0],label=tt)
    plt.colorbar(pcm)
    plt.legend(fontsize=8,loc=2)
# plt.plot(lower,'b.',label='lower')
# plt.plot(bin_range_frequency,'g.',label='bin_range_frequency')
# plt.plot(central_frequency,'r.',label='central_frequency')
# plt.plot(lower+bin_range_frequency,'m.',label='upper (=lower+range)')
# 
# plt.gca().set_ylabel('Frequencies Hz')
# plt.sca(axs[1])
# plt.grid()
# plt.gca().set_xlabel('time')
# plt.plot(low_frequency_cutoff,'b.',label='low_frequency_cutoff')
# plt.legend()
# plt.tight_layout()
filout = '/tmp/globwave_theta.png'
plt.savefig(filout)
print filout