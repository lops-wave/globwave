import numpy
import glob
import os
import datetime
import fnmatch
import logging
import time
#critPOS=0.1 #in degree
critPOS=0 #in degree
critPOSintancity=0.1
critTIME=7*24 #in hours
#agrouaze
#march 2014
#give a list of platform id that are above the treshold in term of position moves
def getPlatformList(diro):
    files=glob.glob(diro+'*.nc')        
    listplat=[]
    for ff in files:
            basef=os.path.basename(ff)
            #print basef
            #logging.debug('file %s',basef)
            underscorpos=basef.split('_')
            platid=underscorpos[0]
            if platid not in listplat:
                listplat.append(platid)
    return listplat
def testPosition(lon,lat,prevlon,prevlat):
    #logging.debug('testposition %s %s ',lon,prevlon)
    #logging.debug('testposition %s %s ',lat,prevlat)
    if ( abs(lon-prevlon)>critPOS or abs(lat-prevlat)>critPOS):
        #logging.debug('diff lon %s',abs(lon-prevlon))
        #logging.debug('diff lat %s',abs(lat-prevlat))
        return True
    else:
        return False
        
def testIntancityOfPOsitionDiff(lon,lat,prevlon,prevlat):
    if ( abs(lon-prevlon)>critPOSintancity or abs(lat-prevlat)>critPOSintancity):
        #logging.debug('diff lon %s',abs(lon-prevlon))
        #logging.debug('diff lat %s',abs(lat-prevlat))
        return True
    else:
        return False
def getDateAndPOsition(filename):
    #logging.debug('filename %s',filename)
    underscorpos=filename.split('_')     
    #logging.debug('file %s %s',basef,cpt)
    if len(underscorpos)>7:
        lonstr=underscorpos[6]
    else:
        lonstr=underscorpos[6][0:-3]
    
    latstr=underscorpos[4]
    beg=underscorpos[1]
    fin=underscorpos[2]
    #logging.debug('lonstr %s',lonstr)
    if 'W' in lonstr:
            lonCUR=-float(lonstr[0:-1])
    else:
            lonCUR=float(lonstr[0:-1])
    if 'N' in latstr:
            latCUR=float(latstr[0:-1])
    else:
            latCUR=-float(latstr[0:-1])
    beg=datetime.datetime.strptime(beg,'%Y%m%dT%H%M%S')
    fin=datetime.datetime.strptime(fin,'%Y%m%dT%H%M%S')
    return lonCUR,latCUR,beg,fin
    
def CheckAmonth(year,month,rnetwork,network):
    
     #monthc='%2d' % month
    anyalert=False 
    monthc=str(month).zfill(2)
    diro=rnetwork+'/'+str(year)+'/'+monthc+'/'
    today=time.strftime("%d%m%Y")
    filelogalert='/home/cercache/users/agrouaze/resultats/alertbuoysGLOBWAVE/'+network+'_'+str(year)+monthc+'_dateOfanalyse_'+today+'.txt'
    if os.path.exists(filelogalert):
        #logging.debug('remove previous log file %s',filelogalert)
        os.remove(filelogalert)
    f=open(filelogalert,'w')
    #print diro
    #logging.debug('dir %s',diro)
    if os.path.exists(diro):
        #logging.debug('dir %s',diro)
        listplat=getPlatformList(diro)
                #logging.debug('platform %s',platid)
        for pla in listplat:
            sameid=glob.glob(diro+pla+'*.nc')
            if len(sameid)>1:
                cpt=0
                lonPREV=0
                latPREV=0
                nbmove=0
                lastmovedate=datetime.datetime(1,1,1,0,0,0)
                for foo in sameid:
                    basef=os.path.basename(foo)
                    cpt+=1
                    lonCUR,latCUR,beg,fin=getDateAndPOsition(basef)
                    if cpt>1:
                            #if ( abs(lonCUR-lonPREV)>critPOS or abs(latCUR-latPREV)>critPOS ) and (abs(dateCUR-lastmovedate)<critTIME and nbmove>1):
                            if testPosition(lonCUR,latCUR,lonPREV,latPREV):
                            #if ( abs(lonCUR-lonPREV)>critPOS or abs(latCUR-latPREV)>critPOS ):
                                nbmove=nbmove+1
                                logging.debug('one move spoted %s %s %s %s %s',pla,lonCUR,lonPREV,latCUR,latPREV)
                                res=testIntancityOfPOsitionDiff(lonCUR,latCUR,lonPREV,latPREV)
                                if nbmove>1 or res:
                                    #print 'strange moves spoted, platform id=',platid,beg,fin,lonCUR,latCUR
                                    anyalert=True
                                    sentence='strange move!%s %s %s %s %s<>%s %s<>%s' % (foo,pla, beg, fin, lonCUR, lonPREV, latCUR, latPREV)
                                    logging.info(sentence)
                                    f.write(sentence+'\n')
                                    
                    lonPREV=lonCUR
                    latPREV=latCUR
                    
    f.close()
    if anyalert:
        logging.debug('log alert written here %s',filelogalert)
    else:
        pass
        #logging.debug('no problem')
if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(levelname)-5s %(message)s',
                        datefmt='%d/%m/%Y %I:%M:%S')
    network='ndbc'
    #rnetwork='/home/cercache/project/globwave/data/globwave/ndbc/nrt/'
    rnetwork='/home/cercache/project/globwave/data/globwave/ndbc/archive/'
    for sensor in glob.glob(rnetwork+'*'):
        for year in range(1975,2015):
                for month in range(0,13): 
                    CheckAmonth(year,month,sensor,network)   
    print 'end of check '

    