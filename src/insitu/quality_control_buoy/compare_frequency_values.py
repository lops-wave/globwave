"""
want to check that the frequencies are ok in globwave format
"""
import os
import matplotlib 
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import netCDF4
ff = '/home/cercache/project/globwave/data/globwave/meds/wave_sensor/2015/10/C45138_20151001T0120_20151101T0020_Lat_49.54N_Lon_65.71W_spectrum.nc'

nc = netCDF4.Dataset(ff)
lower = nc.variables['bin_lower_frequency'][:]

low_frequency_cutoff =  nc.variables['low_frequency_cutoff'][:]
bin_range_frequency =  nc.variables['bin_range_frequency'][:]
central_frequency =  nc.variables['central_frequency'][:]
nc.close()
fif = plt.figure()
f, axs = plt.subplots(2, 1)
plt.sca(axs[0])
plt.grid()
plt.plot(lower,'b.',label='lower')
plt.plot(bin_range_frequency,'g.',label='bin_range_frequency')
plt.plot(central_frequency,'r.',label='central_frequency')
plt.plot(lower+bin_range_frequency,'m.',label='upper (=lower+range)')
plt.legend(fontsize=8,loc=2)
plt.gca().set_ylabel('Frequencies Hz')
plt.sca(axs[1])
plt.grid()
plt.gca().set_xlabel('time')
plt.plot(low_frequency_cutoff,'b.',label='low_frequency_cutoff')
plt.legend()
filout = '/tmp/globwave_frequency.png'
plt.savefig(filout)
print filout