"""
author: Antoine Grouazel
purpose: remove globwave files that only contains lon,lat,time,deph
context: usage of the buoy by iowaga to validate runs lead to crash with empty files
date: 06/01/2016
"""
import os
import sys
sys.path.append('/home/cercache/users/agrouaze/eclipse/sandbox/tests')
from test_problem_hdf_netcdf_written import test_number_2
import logging
import glob
import netCDF4
if __name__ == '__main__':
    cpt_empty = 0
    cpt_corrupted = 0
    logging.basicConfig(level=logging.DEBUG)
    dir_to_scan = '/home/cercache/project/globwave/data/globwave/ndbc/'
    list_nc = glob.glob(os.path.join(dir_to_scan,'*','*','*','*','*.nc'))
    #skip the begining
#     list_nc = list_nc[19850:]
    for ff,filo in enumerate(list_nc):
        logging.debug('%s/%s %s',ff+1,len(list_nc),filo)
        if test_number_2(filo)==False:
            logging.warn('%s is corrupted',filo)
            os.remove(filo)
            cpt_corrupted += 1
        else:
            nc = netCDF4.Dataset(filo)
            list_var = nc.variables.keys()
            nc.close()
            if len(list_var)<5:
                logging.warn('%s contains %s',filo,list_var)
                os.remove(filo)
                cpt_empty += 1
    logging.info('there was %s empty files',cpt_empty)
    logging.info('there was %s corrupted files',cpt_corrupted)