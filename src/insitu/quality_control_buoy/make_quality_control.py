"""
# Controle qualite des donnees insitu vague
# -----------------------------------------
#author Fabrice Collard translated in python by jfpiolle and edited by agrouaze
#last edit 26/05/2014
note: never tested, should be used at the buoy conversion into globwave format step
description of the flagging:
qc_details do not have to be the same for all the files. it must contain integer8 that can be
 translated into byte that indicate which test is positive (meaning problematic)
qc_level must be the same for all the files (all networks)
    Unknown : 0 (used by default when no quality control have been done by CERSAT)
    Unprocessed:1 (used when the quality control has failed)
    Bad: 2 (used when the data is obviously bad)
    Suspect:3 (used when the data is suspect)
    Good:4 (used when the quality control hasn't raised any alert)
quality control performed by provider can be used as a a stresser for suspicious data but do not direclty influence the quality_level of the data
24feb2016: agrouaze delete the modification in the file to have an easier process: spot wrong files, modify the processing script and redo the period concerned
"""
#TODO: update updatefile instances with the boolean array args
#TODO: test on a real case
#TODO: add the quality test to the differnt script of globwave conversion
#TODO
#test on moving buoys
import netCDF4 as netcdf
import numpy as np
from numpy import ma
import logging
import datetime
import glob
import sys
import os
sys.path.append('/home/losafe/users/agrouaze/PROGRAMMES/ROUTINE_PYTHON/my_tool_box')
from colored_logging_lib import ColoredLogger
from globwave.src.insitu.variables_conventions import Ranges
from globwave.src.insitu.timeserieconcat import findContigousPlatformFiles, findWidderPlatformFile
from math import floor
_bitTABLE = {
             'flagged_by_provider':1,
           'spare':2,
           'undefined':4,
           'invalid':8,
           'duplicated':16
           }



def factors(n):
    result = []
    for i in range(2,n+1): # test all integers between 2 and n
        s = 0;
        while n/i == floor(n/float(i)): # is n/i an integer?
            n = n/float(i)
            s += 1
        if s > 0:
            for k in range(s):
                result.append(i) # i is a pf s times
            if n == 1:
                return result


def test_pairwise_param(fid,filepath):
    res = False
    pair_param = {'wind_speed':'wind_direction',
#                   'sea_surface_significant_wave_height':'average_wave_period'
    }
    if 'anemometer' in filepath:
        if pair_param['wind_speed'] in fid.variables and 'wind_speed' not in fid.variables:
            res = True
    return res

def test_z_dim_in_wave_sensor_files(fid,filepath):
    res = False
    if 'wave_sensor' in filepath:
        dims = fid.dimensions
        if 'z' in dims:
            logger.warn('%s contain a Z dimension',filepath)
            res = True
    return res

def test_parameter_range(f,variable_name,values,flags):
    """
    flag out of ranges data
    """
    res = False
    #set to unprocessed (1) the flags inexistant
    flags[flags==0] = 1
   
    #get the ranges:
    if variable_name in Ranges.keys():
        
        mini,maxi = Ranges[variable_name]
        bad_data = (values<mini) or (values>maxi)
        if bad_data.size>0:
            res = True
    #change flags value for out of range
#     flags[bad_data] = 2 #bad
#     qc_details = ''
#     updatefile(f,flags,qchistory=None,'out_of_range',variable_name,)
    return res

def updatefile(f,qclevel,qchistory,strmeanings,variable,detail_bool_test_issue):
    """
    modify the qclelvel and qc_details for a given file
    Args:
        f (netCDF4 file handler):
        qclevel (array of int8):
        qchistory (array of str):
        strmeanings (dic): new test description
        variable (str): name of the variable to update
        detail_bool_test_issue (dic of array of boolean): contains 0 or 1 depending if the test is positive or not (1=problem detected,0=nominal)
    """
    f._redef()
    flag_meanings = f.variables[variable+'_qc_details'].flag_meanings
    test_names = flag_meanings.keys()
    last_test_byte = flag_meanings[test_names[-1]]
    qc_details_val = f.variables[variable+'_qc_details'][:]
    factor_byte = len(factors(last_test_byte))
    for test in strmeanings.keys():
        if test not in flag_meanings.keys():
            flag_meanings[test] = strmeanings[test]
            qc_details_val = qc_details_val+detail_bool_test_issue[test]*2**factor_byte
    f.variables[variable+'_qc_details'] = qc_details_val
    f.variables[variable+'_qc_details'].flag_meanings = flag_meanings
#     f.variables[variable+'_qc_details'].flag_masks =  np.array([1,2,4,8,16], dtype=np.int8)
    f.variables[variable+'_qc_level'][:] = qclevel
    f.variables[variable+'_qc_details'][:] = qchistory
    return

def updateHistory(f,variable,indicestoflag,meaning):
    #prepare bite of qc_detail and associated test meanings
    # Mise a jour historique
    qchistory = f.variables[variable+'_qc_details'][:]
    meanings = f.variables[variable+'_qc_details'].flag_meanings.split()
    if len(meanings) == 1:
        meanings.append('spare')
        
    if len(meanings) > 2 and meanings[2] != 'undefined':
        raise Exception('meanings already used') 
    if len(meanings) > 3 and meanings[3] != 'invalid':
        raise Exception('meanings already used')
    if len(meanings) > 4 and meanings[4] != 'duplicated':
        raise Exception('meanings already used')
    if len(meanings) == 2:
        meanings.append('undefined')
        meanings.append('invalid')
        meanings.append('duplicated')
    #valuebit=(nbtest+2)power2
    qchistory[indicestoflag] = _bitTABLE[meaning]
#     qchistory[notexisting] = 4 #=>100 to tell that the test 'notexist' has found something
#     qchistory[invalid] = qchistory[invalid] | 8 #=>1000 modify the 4 digit of the bite to indicate that the invalid test has detected something 
#     qchistory[duplicated] = qchistory[duplicated] | 16 #=>10000
    strmeanings = ""
    for m in meanings: strmeanings += (m + ' ')
    return strmeanings,qchistory

def TestNotExistingData(f,variable):
    """
    
    """
    res = False
#     qclevel=None
    if variable in f.variables:
#         logger.debug('TestNotExistingData test')
        values = ma.array(f.variables[variable][:])
        if (values.mask==False).any() == False:
            res = True
#         qclevel = f.variables[variable+'_qc_level'][:]
#         if type(values.mask) != np.bool_:
#             logger.debug('Data %s has not boolean mask %s',values,values.mask)
#             notexisting = ma.where( values.mask[:] == False )
#             
#             tmp = qclevel[notexisting]
#             tmp[ma.where(tmp > 1)]=1
#             qclevel[notexisting] = tmp
#             strmeanings,qchistory = updateHistory(f,variable,notexisting,'invalid')
#             updatefile(f,qclevel,qchistory,strmeanings,variable)
    return res
    
def testDuplicatedValues(f,variable):
    res = False
    qclevel = None
    if variable in f.variables:
#         logger.debug('testDuplicatedValues test')
        values = ma.array(f.variables[variable][:])
        if len(values)>4:
            qclevel = f.variables[variable+'_qc_level'][:]
#             indices = np.where(values)
            indices = np.arange(len(values))
            indicespair = np.where((indices % 2)==0)
            indicesinpair = np.where((indices % 2)==1)
#             if values[indicespair] == values[indicesinpair]:
            if np.array_equal(values[indicespair], values[indicesinpair]):
                logger.debug('variable %s has duplicated values %s',variable)
                res = True
#                 tmp = qclevel[indicesinpair]
#                 tmp[ma.where(tmp > 3)]=3
#                 qclevel[indicesinpair] = tmp
#                 strmeanings,qchistory=updateHistory(f,variable,indicesinpair,'duplicated')
#                 updatefile(f,qclevel,qchistory,strmeanings,variable)
    return res

def TestSignificanWveHeight(f):
    res = False
    if 'significant_wave_height' in f.variables:
#         logger.debug('significant_wave_height test')
        hs = ma.array(f.variables['significant_wave_height'][:])
        invalid = ma.where((hs <= 0) & (hs >= 40))[0]
        if invalid.size != 0:
            res = True
        qclevel = f.variables['significant_wave_height_qc_level'][:]
        if qclevel.nonzero()[0].size == 0:
            qclevel[:]=4
        tmp = qclevel[invalid]
        tmp[ma.where(tmp > 2)]=2
        qclevel[invalid] = tmp
#         strmeanings,qchistory=updateHistory(f,'significant_wave_height',invalid,'invalid')
#         updatefile(f,qclevel,qchistory,strmeanings,'significant_wave_height')
    return res
    
def testWaveFrequencySpectrum(f,variable='sea_surface_variance_spectral_density'):
    """
    test for directionnal and not directional
    """
    res = False
    if variable in f.variables:
#     if 'wave_frequency_spectrum_spectral_density' in f.variables and (variable == None or variable == 'wave_frequency_spectrum_spectral_density'):
        values = ma.array(f.variables[variable][:])
        qclevel = f.variables[variable+'_qc_level'][:]
#         qclevel = f.variables['wave_frequency_spectrum_qc_level'][:]
        
        if values != None:
            if len(values.shape)>1:
                # verifie qu'au moins une frequence a une valeur valide par pas de temps
                invalid = ma.where( values.sum(1)==0 )[0]
                #print  ma.sum(values[1:,:] == values[0:-1,:],0), len(ma.sum(values[1:,:] == values[0:-1,:], 0))
                # verifie que des spectres ne sont pas dupliques
             
                duplicated = ma.where(ma.sum(values[1:,:] != values[0:-1,:], 1) == 0)[0] + 1
                # verifie l'existence des spectres
                if values.size > 1 or values.mask != False:
                    notexisting = ma.where( values.count(1) == 0 )[0]
                else:
                    notexisting = ma.array([])
                if invalid.size>0 or duplicated.size>0 or notexisting.size>0:
                    logger.debug("Spectral density (Non directional) : invalid %s duplicated %s notexisting %s", invalid, duplicated, notexisting)
                    res = True
            else:
                invalid = ma.where(values==0)[0]
                if invalid.size>0:
                    logger.debug('invalid %s : %s',variable,invalid)
                    res = True
        
#             if qclevel.nonzero()[0].size == 0:
#                 qclevel[:]=4
    
#             tmp = qclevel[duplicated]
#             tmp[ma.where(tmp > 3)]=3
#             qclevel[duplicated] = tmp
#     
#             tmp = qclevel[invalid]
#             tmp[ma.where(tmp > 2)]=2
#             qclevel[invalid] = tmp
    
#             if values.size > 1 or values.mask != False:
#                 tmp = qclevel[notexisting]
#                 tmp[ma.where(tmp > 1)]=1
#                 qclevel[notexisting] = tmp
    
#             strmeanings,qchistory=updateHistory(f,'wave_frequency_spectrum',notexisting,invalid,duplicated)
    #             qchistory = f.variables['wave_frequency_spectrum_qc_details'][:]
    #             meanings = f.variables['wave_frequency_spectrum_qc_details'].flag_meanings.split()
    #             if len(meanings) == 1:
    #                 meanings.append('spare')
    #             if len(meanings) > 2 and meanings[2] != 'undefined':
    #                 raise Exception('meanings already used')
    #             if len(meanings) > 3 and meanings[3] != 'invalid':
    #                 raise Exception('meanings already used')
    #             if len(meanings) > 4 and meanings[4] != 'duplicated':
    #                 raise Exception('meanings already used')
    #             if len(meanings) == 2:
    #                 meanings.append('undefined')
    #                 meanings.append('invalid')
    #                 meanings.append('duplicated')
    #             qchistory[notexisting] = 4
    #             qchistory[invalid] = qchistory[invalid] | 8
    #             qchistory[duplicated] = qchistory[duplicated] | 16
    #             strmeanings = ""
    #             for m in meanings: strmeanings += (m + ' ')
            #if invalid.size>0 or duplicated.size>0 or notexisting.size>0:
            #    print qchistory
    
            # Mise a jour fichier
#             updatefile(f,qclevel,qchistory,strmeanings,'wave_frequency_spectrum')
    #             f._redef()
    #             f.variables['wave_frequency_spectrum_qc_details'].flag_meanings = strmeanings
    #             f.variables['wave_frequency_spectrum_qc_details'].flag_masks =  np.array([1,2,4,8,16], dtype=np.int8)
    #             f.variables['wave_frequency_spectrum_qc_level'][:] = qclevel
    #             f.variables['wave_frequency_spectrum_qc_details'][:] = qchistory
    return res

def testWaveDirectionalSpectrumSpectralDensity(f,variable):
    """
    file with spectral viariables but no frequency and no direction ae not considered as problematic
    """
    res = False
    if variable in f.variables:
#     if 'wave_frequency_spectrum_spectral_density' in f.variables and (variable == None or variable == 'wave_frequency_spectrum_spectral_density'):
        values = ma.array(f.variables[variable][:])
        qclevel = f.variables[variable+'_qc_level'][:]
#     if 'wave_directional_spectrum_spectral_density' in f.variables and (variable == None or variable == 'wave_directional_spectrum_spectral_density'):
#         if 'wave_directional_spectrum_spectral_density' in f.variables:
#             values = ma.array(f.variables['wave_directional_spectrum_spectral_density'][:])
#             qclevel = f.variables['wave_directional_spectrum_qc_level'][:]
    else:
        values = None

    if values != None:
        # verifie qu'au moins une frequence a une valeur valide par pas de temps
        invalid = ma.where( values.sum(1)==0 )[0]
        #print  ma.sum(values[1:,:] == values[0:-1,:],0), len(ma.sum(values[1:,:] == values[0:-1,:], 0))
        # verifie que des spectres ne sont pas dupliques
     
        duplicated = ma.where(ma.sum(values[1:,:] != values[0:-1,:], 1) == 0)[0] + 1
        # verifie l'existence des spectres
        if values.size > 1 or values.mask != False:
            notexisting = ma.where( values.count(1) == 0 )[0]
        else:
            notexisting = ma.array([])
        if invalid.size>0 or duplicated.size>0 or notexisting.size>0:
            logger.debug( "Spectral density (directional) : %s %s %s", invalid, duplicated, notexisting)
            res = True
    
        if qclevel.nonzero()[0].size == 0:
            qclevel[:]=4

        tmp = qclevel[duplicated]
        tmp[ma.where(tmp > 3)]=3
        qclevel[duplicated] = tmp

        tmp = qclevel[invalid]
        tmp[ma.where(tmp > 2)]=2
        qclevel[invalid] = tmp

        if values.size > 1 or values.mask != False:
            tmp = qclevel[notexisting]
            tmp[ma.where(tmp > 1)]=1
            qclevel[notexisting] = tmp

#             qchistory = f.variables['wave_directional_spectrum_qc_details'][:]
#             meanings = f.variables['wave_directional_spectrum_qc_details'].flag_meanings.split()
#             if len(meanings) == 1:
#                 meanings.append('spare')
#             if len(meanings) > 2 and meanings[2] != 'undefined':
#                 raise Exception('meanings already used')
#             if len(meanings) > 3 and meanings[3] != 'invalid':
#                 raise Exception('meanings already used')
#             if len(meanings) > 4 and meanings[4] != 'duplicated':
#                 raise Exception('meanings already used')
#             if len(meanings) == 2:
#                 meanings.append('undefined')
#                 meanings.append('invalid')
#                 meanings.append('duplicated')
# 
#             qchistory[notexisting] = 4
#             qchistory[invalid] = qchistory[invalid] | 8
#             qchistory[duplicated] = qchistory[duplicated] | 16
        #if invalid.size>0 or duplicated.size>0 or notexisting.size>0:
        #        print qchistory

        # Verification des theta1 et stheta1
        # ----------------------------------
        if 'theta1' in f.variables:
            theta1 = ma.array(f.variables['theta1'][:])
            theta2 = ma.array(f.variables['theta2'][:])
            stheta1 = ma.array(f.variables['stheta1'][:])
            stheta2 = ma.array(f.variables['stheta2'][:])
    
            invalid = ma.where( (theta1.sum(1) == 0) | (stheta1.sum(1) == 0) )[0]
            duplicated = ma.where( (ma.sum(theta1[1:,:] != theta1[0:-1,:], 1) == 0) | (ma.sum(stheta1[1:,:] != stheta1[0:-1,:], 1)==0) )[0] + 1
            if (theta1.size > 1) or (theta1.mask != False and stheta1.mask != False):
                notexisting =  ma.where( (theta1.count(1) == 0) | (stheta1.count(1) == 0) )[0]
            else:
                notexisting = ma.array([])
    
            if invalid.size>0 or duplicated.size>0 or notexisting.size>0:
                print invalid, duplicated, notexisting
                res = True
    
    #             if qclevel.nonzero()[0].size == 0:
    #                 qclevel[:]=4
    # 
    #             tmp = qclevel[duplicated]
    #             tmp[ma.where(tmp > 3)]=3
    #             qclevel[duplicated] = tmp
    # 
    #             tmp = qclevel[invalid]
    #             tmp[ma.where(tmp > 2)]=2
    #             qclevel[invalid] = tmp
    # 
    #             if (theta1.size > 1) or (theta1.mask != False and stheta1.mask != False):
    #                 tmp = qclevel[notexisting]
    #                 tmp[ma.where(tmp > 1)]=1
    #                 qclevel[notexisting] = tmp
    
    #             strmeanings,qchistory = updateHistory(f,'wave_directional_spectrum',notexisting,invalid,duplicated)
    #             updatefile(f,qclevel,qchistory,strmeanings,'wave_directional_spectrum')
    #             qchistory[notexisting] = 4
    #             qchistory[invalid] = qchistory[invalid] | 8
    #             qchistory[duplicated] = qchistory[duplicated] | 16
    #             strmeanings = ""
    #             for m in meanings: strmeanings += (m + ' ')
    #             #if invalid.size>0 or duplicated.size>0 or notexisting.size>0:
    #             #    print qchistory
    # 
    #             # Mise a jour fichier
    #             f._redef()
    #             f.variables['wave_directional_spectrum_qc_details'].flag_meanings = strmeanings
    #             f.variables['wave_directional_spectrum_qc_details'].flag_masks =  np.array([1,2,4,8,16], dtype=np.int8)
    #             f.variables['wave_directional_spectrum_qc_level'][:] = qclevel
    #             f.variables['wave_directional_spectrum_qc_details'][:] = qchistory
    return res
           
def controlFile ( filename, variable=None ):
    """
    scheduler of the different test
    """
    
    reason_failure = []
    the_file_is_ok = True
    f = netcdf.Dataset( filename, 'r' )
    variables = f.variables.keys()
    
    res = test_z_dim_in_wave_sensor_files(f, filename)
    
    if res:
        the_file_is_ok = False
        reason_failure.append('z dim for wave sensor')
    logger.debug('test z dim = %s',res)
    
    res = test_pairwise_param(f,filename)
    if res:
        the_file_is_ok = False
        reason_failure.append('pairwise param')
    logger.debug('test pairwise param = %s',res)
        
    res = findWidderPlatformFile(filename)
    if res:
        the_file_is_ok = False
        reason_failure.append('widder time span file existing')
    logger.debug('test widder file existing = %s',res)
    for varvar in variables:
        if varvar not in ['time','lat','lon','depth'] and '_qc_' not in varvar:
            res = TestNotExistingData(f,varvar)
            if res:
                the_file_is_ok = False
                reason_failure.append('%s not existing'%varvar)
            logger.debug('test all values masked %s = %s',varvar,res)
            res = testDuplicatedValues(f,varvar)
            if res:
                the_file_is_ok = False
                reason_failure.append('%s not duplicated'%varvar)
            logger.debug('test duplicate %s = %s',varvar,res)
            values = f.variables[varvar][:]
            flags = f.variables[varvar+'_qc_level'][:]
            res = test_parameter_range(f,variable_name=varvar,values=values,flags=flags)
            if res:
                the_file_is_ok = False
                reason_failure.append('%s exceed range'%varvar)
            logger.debug('test range %s = %s',varvar,res)
        
    logger.debug('specific test')
    res = TestSignificanWveHeight(f)
    logger.debug('TestSignificanWveHeight  = %s',res)
    if res:
        the_file_is_ok = False
        reason_failure.append('TestSignificanWveHeight ')
    res = testWaveFrequencySpectrum(f)
    if res:
        the_file_is_ok = False
        reason_failure.append('testWaveFrequencySpectrum ')
    logger.debug('testWaveFrequencySpectrum  = %s',res)
#     res = testWaveDirectionalSpectrumSpectralDensity(f)
#     if res:
#         the_file_is_ok = False
    f.close()
    logger.debug('quality control performed on %s',filename)
    logger.info('the file is good = %s',the_file_is_ok)
    return the_file_is_ok,reason_failure

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG,format='%(asctime)s %(levelname)-5s %(message)s',datefmt='%d/%m/%Y %I:%M:%S')
    # ex: python make_quality_control /home3/bagan/private/insitu/nodc/globwave.new/wave/1991/01/WMO46042_19910101T0000_19910131T2300_Lat_36.75N_Lon_122.41W.nc
    import argparse
    tmp = logging.getLogger('shapeL2')
    logger = ColoredLogger(tmp)
    logger.info('global test')
    parser = argparse.ArgumentParser(description='Move S-1 WV SAFE product tickets to working apropriate directories')
    parser.add_argument('-i','--input', type=str,help='filepath to test',required=True)
    parser.add_argument('--verbose', action='store_true',default=False)
    args = parser.parse_args()
    output = '/tmp/globwave_quality_check_'+datetime.datetime.now().strftime('%Y%m%d_%H%M')+'.txt'
    fid = open(output,'w')
    if args.verbose:
#         logging.basicConfig(level=logging.DEBUG)
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
#         logging.basicConfig(level=logging.INFO)
    if os.path.isfile(args.input):
        ok,reason = controlFile(args.input)
        if ok ==False:
            fid.write(args.input+';'+(';').join(reason)+'\n')
    else:
        inputs = glob.glob(os.path.join(args.input,"*.nc"))
        for ii,filename in enumerate(inputs):
            logger.info('%s/%s filename : %s',ii+1,len(inputs),filename)
            ok,reason = controlFile(filename)
            if ok ==False:
                fid.write(filename+';'+(';').join(reason)+'\n')
    fid.close()
    logger.info('output log: %s',output)
#     import sys, glob
#     print factors(32)
#     files = glob.glob(sys.argv[1])
#     for f in files:
#         print f
#         controlFile( f )
