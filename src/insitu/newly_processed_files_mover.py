"""
author:Antoine Grouazel
purpose: move globwave files from tmp dir to final dir
date: 22 august 2017 
"""
import os
import time
import sys
import logging
import shutil
import collections
def move_a_sensor_dir(sensor,year,month,src,dest):
    srcdir = os.path.join(src,sensor,year,month)
    destdir = os.path.join(dest,sensor,year,month)
    print 'go'
    move_done = False
    if os.path.exists(srcdir):
        if os.path.exists(destdir):
            logging.info('the dest dir exist and need to be cleaned')
#             shutil.rmtree(destdir)
            os.system('rm -fr %s'%destdir)
        logging.info('move %s -> %s',srcdir,destdir)
        shutil.move(srcdir, destdir)
        move_done = True
    else:
        logging.info('srcdir doesnt exist : %s',srcdir)
    return move_done
if __name__ =='__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Move globwave files to appropriate directories')
    parser.add_argument('--verbose', action='store_true',default=False)
    parser.add_argument('--year', action='store',help='YYYY',required=True)
    parser.add_argument('--month', action='store',help='MM',required=True)
    parser.add_argument('--dir-archive', action='store',help='directory where are stored the globwave files',required=True)
    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
        
    source_tmp_dir = '/home/cercache/project/globwave/soft/tmp/workspace/coriolis_tmp_processing/'
#     destination = '/home/cercache/project/globwave/data/globwave/coriolis/'
    destination = args.dir_archive
    count = collections.defaultdict(int)
    for sensor in os.listdir(source_tmp_dir):
        count
        logging.info('sensor= %s',sensor)
        move_done = move_a_sensor_dir(sensor,args.year,args.month,src=source_tmp_dir,dest=destination)
        if move_done:
            count['done'] += 1
        else:
            count['impossible'] += 1
    logging.info('counters: %s',count)
