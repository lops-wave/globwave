# *-* coding: iso-8859-1 *-*
"""
#Purpose: conversion of MEDS buoy to globwave format
#some function are shared with CDIP script
#author: Antoine Grouazel
test: never done
this version only treat c*_y2d.fb files (spectral files directional or not)
currently the .csv files are in the spool not treated
main idea: 
    1 read all the file
    2 cut the timeseries
    3 create the netcdf files
data info:
c*_y2d.fb file is all the nrt data (spectral) from  a given buoy in the current year (no idea whether we can retrieve previous years)
"""
from datetime import datetime
import os,sys
import glob
import time
import numpy
import copy
from pyexcel_ods import get_data
import json
import pdb
sys.path.append('/home/losafe/users/agrouaze/PROGRAMMES/ROUTINE_PYTHON/my_tool_box')
from colored_logging_lib import ColoredLogger
from collections import OrderedDict
import logging
import math
import netCDF4
format_version = '03/06/2014'
version_converter = '2.0'

from cerbere.datamodel.pointtimeseries import PointTimeSeries
from cerbere.datamodel.variable import Variable
from cerbere.datamodel.field import Field, QCLevel, QCDetail
from cerbere.geo.bathymetry import Bathymetry
from cerform.cfconvention import get_convention
import cerform.wave
from cerbere.mapper.abstractmapper import WRITE_NEW
from cerbere.mapper.ncfile import NCFile
from ceraux.landmask import Landmask
from FileNamer import GlobWaveFileNamer
from variables_conventions import VarConventions,wave_sensor,freqDim,freqVarName,freqRangeName
from shared_convert_methods import time_units,Define_output_filename
from scipy import interpolate

# list_buoys_having_directional_spectra = #cf mail from bruce Bradshaw
list_buoys_having_directional_spectra = ['EN','WD','WC','TR']
INSTRUMENT = {
                  "AE":"AES ODAS buoy. (6m NOMAD, 3m DISCUS or Watchkeeper)",
                  "6N":"MSC Non-directional ODAS buoy. 6m NOMAD",
                  "3D":"MSC Non-directional ODAS buoy. 3m Discus",
                  "12":"MSC Non-directional ODAS buoy. 12m Discus",
                  "AW":"AES buoy data with bad Watchman payload. (Truncated spectra, VCAR=VWH$, VTPK=VTP$)",
                  "MI":"Miros Radar",
                  "EN":"Directional Buoy, Endeco",
                  "HX":"Hexoid buoy",
                  "PC":"Pressure cell",
                  "ST":"Staff gauge",
                  "TG":"Toga buoy",
                  "KG":"Kelk Pressure cell",
                  "TR":"Directional buoy, TriAxys",
                  "SW":"Swartz gauge",
                  "WA":"Non-directional waverider buoy (Datawell)",
                  "WC":"Directional WAVEC buoy (Datawell)",
                  "WD":"Directional Waverider buoy (Datawell)",
                  "WP":"Non-directional WRIPS buoy",
                  "WR":"Non-directional Waverider"
    }
#description from http://www.meds-sdmm.dfo-mpo.gc.ca/isdm-gdsi/waves-vagues/formats/formatb-eng.htm
# VCAR    Characteristic significant wave height (m)
# VWH$    Characteristic significant wave height (reported by the buoy) (m)
# VCMX    Maximum zero crossing wave height (reported by the buoy) (m)
# SHE1    WES sea height (m)
# SWHT    Swell height (m)
# VAV1    Average heave from the non-synoptic part of WRIPS buoy data (m)
# VMNL    Depth of the deepest trough (m)
# VMXL    Height of the highest crest (m)
# VMX1    Maximum zero crossing wave height from the non-synoptic part of WRIPS buoy data (m)
# VST1    Maximum wave steepness
# WAVE PERIOD CODES
# VTPK    Wave spectrum peak period (s)
# VTP$    Wave spectrum peak period (reported by the buoy) (s)
# SEP1    WES sea period (s)
# SWPR    Swell period (s)
# VTD1    Dominant period (s)
# VTZA    Average zero crossing wave period (s)
# VZA1    Average zero crossing period from the non-synoptic part of WRIPS buoy data (s)
# SPECTRAL CODES
# BAND    Bandwidth of spectral estimates
# FREQ    Frequency of spectral estimates
# LCF$    Low frequency cut-off for wave spectra, calculated from the dispersion relation
# VCXX    Autospectrum of north-south tilt (C22)
# VCXY    Cospectrum of north-south and east-west tilt (C23)
# VCYY    Autospectrum of east-west tilt (C33)
# VCZX    Cospectrum of heave and north-south tilt (C12)
# VCZY    Cospectrum of heave and east-west tilt (C13)
# VQXY    Quadspectrum of north-south and east-west tilt (Q23)
# VQZX    Quadspectrum of heave and north-south tilt (Q12)
# VQZY    Quadspectrum of heave and east-west tilt (Q13)
# VSDN    Spectral density (equivalent to C11)
# VSMB    The ratio of spectral moments 0 and 1 (m0/m1)
# METEOROLOGICAL & OCEANOGRAPHIC CODES
# WDIR    Direction from which the wind is blowing (� True)
# WSPD    Horizontal wind speed (m/s)
# WSS$    Horizontal scalar wind speed (m/s)
# GSPD    Gust wind speed (m/s)
# ATMS    Atmospheric pressure at sea level (mbar)
# DRYT    Dry bulb temperature (�C)
# SSTP    Sea surface temperature (�C)
# SLEV    Observed sea level
# SST1    Average sea temperature from the non-synoptic part of WRIPS buoy data (�C)
# HAT$    Water temperature from high accuracy temperature sensor (�C)
# DIRECTION and POSITION CODES
# LTG$    GPS latitude (reported by the buoy) (�)
# LNG$    GPS longitude (reported by the buoy) (�)
# MAGN    Magnetic variation from true north (�)
# SED1    WES sea direction (�)
# SWDR    Direction from which swell is coming (� true)
# VPED    Wave spectrum peak energy direction (� true)
# VSPR    Wave directional spread from cross spectra
# OTHER CODES
# ADNB    Number of fourier transform blocks in analysis
# ADST    DIWAR receiver signal strength
# ADSV    DIWAR receiver signal strength variance
# AST1    Internal temperature from the non-synoptic part of WRIPS buoy data
# AST2    Internal temperature from the synoptic part of WRIPS buoy data
# NBD1    The number of bad samples in a surface elevation time series
# QCF$    The indicator encoding which MEDS QC tests have failed
# QCP$    The indicator encoding which MEDS QC tests have been performed
# RECD    The record number of the tape containing the raw data
# SIDE    The side number of the tape containing the raw data
# TAPE    The tape number of the tape containing the raw data
# UPD$    The update date of the record as YYYYMMDD
# WOI$    The WAVEOB indicator group 00IaImIp from code section 0 of the code.

match_cersat_convention = {'SLEV':('SLEV',True),#source id:(convention id,need QC)
                           'SWDR':('SWDR',True),
                           'VPED':('VPED',True),
                           'VSPR':('VSPR',True),
                           'VWH$':('VAVH',True),#for me is the same 
                           'VCAR':('VAVH',True),#for me is the same 
                           'VCMX':('VZMX',True),
                           'SWHT':('SWHT',True),
                           'VMNL':('VMNL',True),
                           'VMX1':('VMX1',True),
                           'VST1':('VST1',True),
                           'VTPK':('VTPK',True),
                           'SWPR':('SWPR',True),
                           'VTD1':('VTPK',True),#for me is the same 
                           'VTZA':('VTZA',True),
                           'BAND':('BANDWIDTHFREQ',False),
#                            'FREQ':'CENTRALFREQ',
                           'LCF$':('LCF$',False),
                           'WDIR':('WDIR',True),
                           'WSPD':('WSPD',True),
                           'WSS$':('SCALARWSPD',True),
                           'GSPD':('GSPD',True),
                           'ATMS':('ATMP',True),
                           'DRYT':('AIRT',True),#approximation
                           'SSTP':('SST',True),
                           'SST1':('SST',True),
                           'HAT$':('SST',True),
                           'centralFreq':('CENTRALFREQ',False),
                           'lowerFreq':('BINLOWFRQ',False),
                           'freqRange':('BINFRQRANGE',False),
                           'density':('EF',True),
                                          }
# STD_CENTRAL_FREQ = numpy.array([ 0.003906,  0.007813,  0.01172 ,  0.01563 ,  0.01953 ,  0.02344 ,
#         0.02734 ,  0.03125 ,  0.03516 ,  0.03906 ,  0.04297 ,  0.04688 ,
#         0.05078 ,  0.05469 ,  0.05859 ,  0.0625  ,  0.06641 ,  0.07031 ,
#         0.07422 ,  0.07813 ,  0.08203 ,  0.08594 ,  0.08984 ,  0.09375 ,
#         0.09766 ,  0.1016  ,  0.1055  ,  0.1113  ,  0.1191  ,  0.127   ,
#         0.1348  ,  0.1445  ,  0.1563  ,  0.1699  ,  0.1875  ,  0.209   ,
#         0.2344  ,  0.2676  ,  0.3086  ,  0.3652  ,  0.4512  ])
# STD_LOWER_FREQ = numpy.array([ 0.001953 ,  0.00586  ,  0.009767 ,  0.013677 ,  0.017577 ,
#         0.021487 ,  0.025387 ,  0.029297 ,  0.033207 ,  0.037107 ,
#         0.041017 ,  0.044927 ,  0.048827 ,  0.052737 ,  0.056637 ,
#         0.060547 ,  0.064457 ,  0.068357 ,  0.072267 ,  0.076177 ,
#         0.080077 ,  0.083987 ,  0.087887 ,  0.091797 ,  0.095707 ,
#         0.099647 ,  0.103547 ,  0.1073935,  0.1151935,  0.1230935,
#         0.1308935,  0.13864  ,  0.15044  ,  0.162085 ,  0.177735 ,
#         0.19728  ,  0.22073  ,  0.24807  ,  0.287115 ,  0.330045 ,  0.4004   ])
# STD_FREQ_RANGE = numpy.array([ 0.003906,  0.003906,  0.003906,  0.003906,  0.003906,  0.003906,
#         0.003906,  0.003906,  0.003906,  0.003906,  0.003906,  0.003906,
#         0.003906,  0.003906,  0.003906,  0.003906,  0.003906,  0.003906,
#         0.003906,  0.003906,  0.003906,  0.003906,  0.003906,  0.003906,
#         0.003906,  0.003906,  0.003906,  0.007813,  0.007813,  0.007813,
#         0.007813,  0.01172 ,  0.01172 ,  0.01563 ,  0.01953 ,  0.02344 ,
#         0.02734 ,  0.03906 ,  0.04297 ,  0.07031 ,  0.1016  ])
class MEDSMooredBuoy():



    def __init__(self,input_file):
        self.input_file = input_file
        fid = open(input_file,'r')
        
        self.content = fid.readlines()
        fid.close()
        self.spectrumtype = 'nondirectional'
        self._bathymetry = None
        self._landmask = None


    def _readParameters(self, line):
        samplingLength = float(line[42:50])
        samplingFrequency = float(line[50:62])
        qualityCode = int(line[64:66])
        nbParam = int(line[66:70])
        nbWaveHeights = int(line[70:73])
        nbWavePeriods = int(line[73:76])
        nbWaveBins = int(line[76:80])
        return (samplingLength, samplingFrequency, nbParam, nbWaveHeights, nbWavePeriods, nbWaveBins)
    _readParameters=classmethod(_readParameters)
    
    
    def best_measurement_out_of_n(self,samples):
        """
        return the measurement that is the most centered regarding the others
        context: sometime there is 2 anemometers
        """
        if (samples.mask==False).any():
#             logger.debug('best_measurement_out_of_n | samples:%s',samples)
#             med = numpy.median(samples)
#             logger.debug('median %s',med)
#             ind = numpy.where(abs(samples-med)==numpy.amin(abs(samples-med)))[0][0]
    #         logger.debug('best_measurement_out_of_n | ind: %s',ind)
#             res = samples[ind]
            res = numpy.ma.mean(samples)
        else:
            res = samples[0]
        
        return res
    
    def get_all_parameter_available(self,data,intstart,intend):
        list_all_param = []
        for tt in range(intstart,intend+1):
            for var in data[str(tt)].keys():
                if var not in list_all_param:
                    list_all_param.append(var)
        return list_all_param
    
    def std_freq_wave_bins(self,energy_values,centralfreq):
        """
        interpolate the spectrum to get homogeneous Nber of freq bins => no cut in the file
        Args:
            energy_values (vector):
            centralfreq (vector):
        Returns:
            std_val (vector):
        """
        logger.debug('energy_values shape :%s -> %s',energy_values.shape,self.std_central_freq.shape)
#         std_val = None
#         std_val = interpolate.griddata((times, centralfreq), energy_values, (times, STD_CENTRAL_FREQ), method='linear')
        interpolation_func = interpolate.interp1d(centralfreq,energy_values,bounds_error=False)
        std_val = interpolation_func(self.std_central_freq)
        return std_val
    
    def get_ref_freq_param(self,data):
        """
        get the frequencies parameters that are the largest one to be sure thta no data is lost
        Args:
            data (dic): contains each record separatedly with all the parameter
        """
        self.reference_record_max_freq_bin
        self.std_central_freq = data[str(self.reference_record_max_freq_bin)]['centralFreq']
        self.lowerFreq = data[str(self.reference_record_max_freq_bin)]['lowerFreq']
        self.freqRange = data[str(self.reference_record_max_freq_bin)]['freqRange']
        logger.info('the maximum bin freq found is: %s',len(self.std_central_freq))
    
    def pack_data_in_interval(self,data,intstart,intend):
        """
        purpose: put all values of each time dependent variables of the record of a given interval into numpy arrays
        note: since an interval is by definition homogeneous data within can be concatenated
        Args:
            data (dic): contains the values for each fields of a given interval
        """
        new_data = {}
        logger.debug('pack_data_in_interval on indices, %s %s',range(intstart,intend+1),len(range(intstart,intend+1)))
        all_vars = self.get_all_parameter_available(data,intstart,intend)
        
        for tt_moins1 in range(intstart,intend+1):
            tt = tt_moins1
            for var in all_vars:
                if var not in ['centralFreq','lowerFreq','freqRange']:
                    #test existence of the parameter for the record
                    if var in data[str(tt)].keys():
                        valval = numpy.ma.array(data[str(tt)][var])
                    else:#case for parameters only time dependent that are missing
                        valval = numpy.ma.array([numpy.NaN],mask=[True])
                    #homogeneize the parameter with changing dimensions
                    logger.debug('pack_data_in_interval | var:%s valval shape: %s',var,valval.shape )
                    if valval.size>1 and var not in [ 'density','centralFreq','lowerFreq','freqRange']:
                        valval = self.best_measurement_out_of_n(valval)
                    elif var == 'density':#standardize the number of freq bins
    #                     logger.debug('valeuuur :: %s %s %s,',data['centralFreq'],data['lowerFreq'],data['freqRange'])
                        if len(valval)!=len(self.std_central_freq) or (len(valval)==len(self.std_central_freq) and numpy.array_equal(data[str(tt)]['centralFreq'],self.std_central_freq)==False):
                            valval = self.std_freq_wave_bins(valval,data[str(tt)]['centralFreq'])
#                         if len(valval)==len(self.std_central_freq) and numpy.array_equal(data[str(tt)]['centralFreq'],self.std_central_freq)==False:#security debug test
#                             logger.warning('len(valval)==len(STD_CENTRAL_FREQ) but data[str(tt)]["centralFreq"]!=STD_CENTRAL_FREQ:\n %s %s',data[str(tt)]['centralFreq'],self.std_central_freq)
                    if var not in new_data:
                        new_data[var] = valval
                    else:#var is already in new_data
                        logger.debug('pack_data_in_interval | tt_moins1=%s new_data[var] %s (%s) <- data[str(tt)][var] %s (%s)',tt_moins1,new_data[var].shape,type(new_data[var]),valval.shape,type(valval))
                        logger.debug('%s shape valval: %s shape new_data[var] %s',var,valval.shape,new_data[var].shape)
                        new_data[var] = numpy.ma.vstack([new_data[var],valval])
        #case of freq var dependent:
        new_data['centralFreq'] = self.std_central_freq
        new_data['lowerFreq'] = self.lowerFreq
        new_data['freqRange'] = self.freqRange
        return new_data
    
    def create_fields(self,data,intstart,intend,nbWaveBins):
        """
        create variable, dimensions, fields sorted by sensor
        Args:
            data (dic): contain all the data readed in the source file even the one useless
            intstart (int): indice of the starting time interval
            intend (int): indice of the stoping time interval
            nbWaveBins (int): nber of frequencies
        """
        nbWaveBins = len(self.freqRange)
        logger.info('create_fields | nbWaveBins=%s',nbWaveBins)
        ovars = {}
        ofields = {}
        nb_record = intend - intstart +1 
        logger.debug('create_fields| nb record : %s',nb_record)
        #create time field
        val_times = numpy.array([])
#         for tt in data['time'][intstart:intend]:
        for tt_moins1 in range(intstart,intend+1):
            tt = tt_moins1 + 1
#             logger.debug('create_fields | tt : %s',tt)
            tt_dt = data[str(tt)]['time'][0]
            tmp_num = netCDF4.date2num(tt_dt,time_units)
            val_times = numpy.concatenate([val_times,[tmp_num]])
        data_concat = self.pack_data_in_interval(data,intstart,intend)
        vartime = Variable(shortname='time',description='time',standardname='time')
        time_dim = OrderedDict([('time', nb_record)])
        timefield = Field(variable=vartime,dimensions=time_dim,units=time_units,values=val_times)
        for variable in data_concat.keys():
            if variable in match_cersat_convention:
                logger.debug('treat variable %s',variable)
                var_cersat = match_cersat_convention[variable][0]
                vname,longname,description,sensor,stdunits = VarConventions(var_cersat)
                dims = OrderedDict() ## ajout selon JFP
                if variable not in ['centralFreq','lowerFreq','freqRange']:
                    dims['time'] = nb_record #defaut dimensions
                if variable in ['centralFreq','lowerFreq','freqRange','density']:
                    dims[ freqDim[self.spectrumtype] ] = nbWaveBins
    #             if vname not in ovars:
                varobj = Variable(shortname=vname,
                                description= description,
                                authority = get_convention(),
                                standardname = longname )
                ovars[vname] = varobj
                if variable == 'density':
                    if len(data_concat[variable].shape)==1: #case where there only one time step
                        tmp = numpy.ma.reshape(data_concat[variable],(1,)+data_concat[variable].shape)
                        values = tmp
                        logger.debug('values density %s',values.shape)
                    else:
                        values = data_concat[variable]
                    
                elif variable in ['centralFreq','lowerFreq','freqRange']:
                    logger.debug('create_fields | shape of %s = %s',variable,data_concat[variable].shape)
                    if len(data_concat[variable].shape)<=1: #case where there only one time step
#                         tmp = numpy.ma.reshape(data_concat[variable],data_concat[variable].shape+(1,))
                        values = data_concat[variable]
                    else:
                        tmp = data_concat[variable]
                        values = tmp[0,:]#only need the first vector since they are all same
                else:#for normal parameter only depending of the time
                    resampled_values = data_concat[variable]
                    values = resampled_values
                    if len(values.shape)>1:#if the data are not 1D -> squeeze them
                        values = values.squeeze()
                    if isinstance(values,numpy.ndarray)==False:
                        values = numpy.ma.array([values])
#                     values = numpy.ma.array(values)
                    logger.debug('%s values shape %s = %s',variable,values.shape,values)
                if sensor == 'ocean_temperature_sensor' and variable=='sea_surface_temperature':
                    values = values+273.15 #conversion celsius->kelvin
                if variable == 'air_pressure':
                    values = values * 100. #convert hPa -> Pa
                if match_cersat_convention[variable][1]:
                    #TODO: make generic quality test at generation step for all networks and here use QCF$ variable
                    vals_test = numpy.ma.zeros(values.shape)
                    qc_details = QCDetail( values = vals_test,
                                                           dimensions = copy.copy(dims),
                                                           mask = numpy.array([1],dtype='i1'),
                                                           meanings = 'provider_test range_test climstd_test climminmax_test operator_control')
                    vals_qc = numpy.ma.zeros(values.shape)
                    qc_levels = QCLevel( values = vals_qc,
                                                    dimensions = copy.copy(dims),
                                                    levels = numpy.array([0,1,2,3,4],dtype='i1'),
                                                    meanings = "Unknown Unprocessed Bad Suspect Good" )
                else:
                    qc_details = None
                    qc_levels = None
                fieldobj = Field(ovars[vname],
                                             dims,
                                             values = values,
                                             units = stdunits,
                                             qc_levels=qc_levels,
                                             qc_details=qc_details)
                if sensor not in ofields.keys():
                    ofields[sensor] = {}
                ofields[sensor][vname] = fieldobj
            else:
                logger.debug('%s will not be reported in the new file',variable)
        return ofields,timefield
                    
                    
    def find_first_record(self,data):
        """
        since the dictionnary that contains the data is indexed with the first 
        """
        
    def continuity_test(self,data):
        """
        browse the data of the nrt file (year2date= current year up to current date)
         to find discontiunity in the sensor attitude and return the list of time intervals
        """
        intervals = []
        param_2_test = ['depth','lon','lat','samplingLength','samplingFrequency',]
        #nbParam not in because change very often
        #'nbWaveHeights', 'nbWavePeriods' 'nbWaveBins' are no more into the test because it leads to lots of cut into the timerserie => standardization of the spectrum is done above
        param_into_metadata = ['depth','lon','lat','samplingLength','samplingFrequency','nbWaveHeights','nbWavePeriods','nbParam','nbWaveBins','time']
        
#         for pp in param_2_test:
#             previous[pp] = data[pp][0]
        beg = 1
        metadata = {}
        #fill metadata dic with the metadata of the first record
        for pp in param_into_metadata:
            metadata[pp] = data['1'][pp][0]
        #loop over the record to find discontinuity
        number_of_record = len(data.keys())
        maximum_freq_bin = 0
        reference_record_max_freq_bin = 1
        for rec in range(2,number_of_record):#start at 2 since, I need to compare with a first element, and the first record is 1
            breaker = False
            
            if maximum_freq_bin<data[str(rec)]['nbWaveBins'][0]:
                maximum_freq_bin = data[str(rec)]['nbWaveBins'][0]
                reference_record_max_freq_bin = rec
            for pp in param_2_test:
#                 logger.debug('param %s val= %s',pp,len(data[str(rec-1)][pp]))
                if data[str(rec)][pp] != data[str(rec-1)][pp]:
                    breaker = True
                    logger.debug('break because of %s change %s-%s',pp,data[str(rec)][pp],data[str(rec-1)][pp])
            if data[str(rec)]['time'][0].month != data[str(rec-1)]['time'][0].month:
                breaker = True
                pp = 'time'
                logger.debug('break because of month change %s-%s',data[str(rec)]['time'][0].month,data[str(rec-1)]['time'][0].month)
            if breaker:
                logger.debug('break the time serie at %s element because of param %s discontinuity',rec-1,pp)
                intervals.append((beg,rec-1,copy.deepcopy(metadata)))
                beg = rec
                for pp in param_into_metadata:
                    metadata[pp] = data[str(beg)][pp][0]
        #append the last piece of data
        intervals.append((beg,rec,copy.deepcopy(metadata)))
        logger.debug('nb intervals: %s',len(intervals))
        self.reference_record_max_freq_bin = reference_record_max_freq_bin
        return intervals
                    
    def read_source_file(self):
        """
        for 1D spectrum(frequency) files
        read the source file and return the field for each sensor
        agrouaze 99% written
        """
        log_method = logging.getLogger( "{0}.{1}".format(__name__, "read_source_file" ))
        log_method.setLevel(logging.INFO)
        current_line_nb = -1
        data_container_raw = {
                          'lon':[]
                          ,'lat':[]
                          ,'depth':[]
                          ,'samplingLength':[]
                          ,'samplingFrequency':[]
                          ,'nbParam':[]
                          ,'nbWaveHeights':[]
                          ,'nbWavePeriods':[]
                          ,'nbWaveBins':[]
                          }
        
        cpt_loop = 0
        log_method.info('nb lines in source file: %s',len(self.content))
        records = {}
        while current_line_nb<=len(self.content)-1:
            current_line_nb +=1
            log_method.debug('loop nb: %s',cpt_loop)
            cpt_loop += 1
            records[str(cpt_loop)] = copy.deepcopy(data_container_raw)
            line_content = self.GetLine(current_line_nb)
            if line_content is None:
                records.pop(str(cpt_loop))
                break
            # ----------------------------------
            #line 1
            # ----------------------------------
            self.sensorId = line_content[0:10].strip()
            self.longName = line_content[15:35].strip()
            self.institution = 'MEDS'
            self.providerId = line_content[40:50].strip()
            log_method.debug('read_source_file| first line of record Nber: %s',current_line_nb)
            current_line_nb +=1
            line_content = self.GetLine(current_line_nb)
            if line_content is None:
                records.pop(str(cpt_loop))
                break
            lat,lon,depth,measTime = MEDSMooredBuoy._readLocation(line_content)
            log_method.debug('lat %s lon %s depth:%s time:%s',lat,lon,depth,measTime)
 
            log_method.debug('read_source_file| second line of record Nber: %s',current_line_nb)
            if 'time' in records[str(cpt_loop)].keys():
                records[str(cpt_loop)]['time'].append(measTime)
            else:
                records[str(cpt_loop)]['time'] = [measTime]
    #         for sensor in ['anemometer','wave_sensor','barometer']:
            line_content = self.GetLine(current_line_nb)
            if line_content is None:
                records.pop(str(cpt_loop))
                break
            samplingLength, samplingFrequency, nbParam, nbWaveHeights, nbWavePeriods, nbWaveBins = MEDSMooredBuoy._readParameters(line_content)
            log_method.debug('samplingLength %s, samplingFrequency %s, nbParam %s, nbWaveHeights %s, nbWavePeriods %s, nbWaveBins %s',samplingLength, samplingFrequency, nbParam, nbWaveHeights, nbWavePeriods, nbWaveBins)
            records[str(cpt_loop)]['samplingLength'].append(samplingLength)
            records[str(cpt_loop)]['samplingFrequency'].append(samplingFrequency)
            records[str(cpt_loop)]['nbParam'].append(nbParam)
            records[str(cpt_loop)]['nbWaveHeights'].append(nbWaveHeights)
            records[str(cpt_loop)]['nbWavePeriods'].append(nbWavePeriods)
            records[str(cpt_loop)]['nbWaveBins'].append(nbWaveBins)
            records[str(cpt_loop)]['lon'].append(lon)
            records[str(cpt_loop)]['lat'].append(lat)
            records[str(cpt_loop)]['depth'].append(depth)
            # ----------------------------------
            #section 3
            # ----------------------------------
            # skip data decoding
            #read the first 4 lines of data
            for i in range(int(math.ceil(nbParam / 5.))):
                
                current_line_nb += 1
                log_method.debug('read_source_file| time dependent parameter line Nber: %s',current_line_nb)
                line_content = self.GetLine(current_line_nb)
                if line_content is None:
                    records.pop(str(cpt_loop))
                    break
                rest = nbParam-5*i
                if rest >5:
                    rest = 5
                
                for j in range(rest):
                    val = line_content[j*16+0:j*16+12].strip()
                    var = line_content[j*16+12:j*16+16].strip()
                    if val != '' and var != '':
                        value = float(val)
                        variable = str(var)
                        if variable not in records[str(cpt_loop)].keys():
                            records[str(cpt_loop)][variable] = [value]
                        else:
                            records[str(cpt_loop)][variable].append(value)
            #read WAVE param :the line not aligned before spectrum
            for i in range(int(math.ceil((nbWaveHeights + nbWavePeriods)/8.))):
                current_line_nb +=1
                line_content = self.GetLine(current_line_nb)
                if line_content is None:
                    records.pop(str(cpt_loop))
                    return records
                log_method.debug('read_source_file| last time dependent parameter line Nber: %s',current_line_nb)
                rest = (nbWaveHeights + nbWavePeriods)-8*i
                if rest>8:
                    rest=8
    
                for j in range(rest):
                    val = line_content[j*10+0:j*10+6].strip()
                    var = line_content[j*10+6:j*10+10].strip()
                    
                    if val != '' and var != '':
                        value = float(val)
                        variable = str(var)
                        if variable not in records[str(cpt_loop)].keys():
                            records[str(cpt_loop)][variable] = [value]
                        else:
                            records[str(cpt_loop)][variable].append(value)
#                 last_line_nb = curen_line_nb
            # ----------------------------------
            # One-dimensional spectrum record (Bins)
            # ----------------------------------
            ifreq = 0
            centralFreq_all = numpy.array([])
            lowerFreq_all = numpy.array([])
            freqRange_all = numpy.array([])
            density_all = numpy.array([])
            for i in range(int(math.ceil(nbWaveBins / 2.))):
                current_line_nb += 1
                log_method.debug('read_source_file| time,freq dependent parameter line Nber: %s',current_line_nb)
                line_content = self.GetLine(current_line_nb)
                if line_content is None:
                    records.pop(str(cpt_loop))
                    return records
                content = line_content.split()
                for j in range(len(content)/3):
                    singleline = content[j*3] + " " + content[j*3+1] + " " + content[j*3+2]
                    samples = self._readFrequencySamples(singleline,ifreq)
                    centralFreq,lowerFreq,freqRange, density = samples
                    centralFreq_all = numpy.concatenate([centralFreq_all, [centralFreq] ])
                    lowerFreq_all = numpy.concatenate([lowerFreq_all, [lowerFreq] ])
                    freqRange_all = numpy.concatenate([freqRange_all, [freqRange] ])
                    density_all = numpy.concatenate([density_all, [density] ])
                    ifreq = ifreq+1
            if 'centralFreq' in records[str(cpt_loop)].keys():
                logging.debug('centralFreq_all %s %s',centralFreq_all,centralFreq_all.shape)
                records[str(cpt_loop)]['centralFreq'] = numpy.vstack((records[str(cpt_loop)]['centralFreq'],centralFreq_all))
            else:
                records[str(cpt_loop)]['centralFreq'] = centralFreq_all
            if 'lowerFreq' in records[str(cpt_loop)].keys():
                records[str(cpt_loop)]['lowerFreq'] = numpy.vstack((records[str(cpt_loop)]['lowerFreq'],lowerFreq_all))
            else:
                records[str(cpt_loop)]['lowerFreq'] = lowerFreq_all
                
            if 'freqRange' in records[str(cpt_loop)].keys():
                records[str(cpt_loop)]['freqRange'] = numpy.vstack((records[str(cpt_loop)]['freqRange'],freqRange_all))
            else:
                records[str(cpt_loop)]['freqRange'] = freqRange_all
                
            if 'density' in records[str(cpt_loop)].keys():
                records[str(cpt_loop)]['density'] = numpy.vstack((records[str(cpt_loop)]['density'],density_all))
            else:
                records[str(cpt_loop)]['density'] = density_all
            
        return records
    
    def _readFrequencySamples(cls, line, ifreq):

#         res=[]
        content = line.split()
        centralFreq = float(content[0])
        freqRange = float(content[1])
        lowerFreq = centralFreq - freqRange/2
        density = float(content[2])
#         res.append ( (centralFreq,lowerFreq,freqRange,density) )
        res = (centralFreq,lowerFreq,freqRange,density)
        return res
    _readFrequencySamples=classmethod(_readFrequencySamples)

    def _read2DSpectrumBin(cls, line):

        res = []
        freqRange = int(line[4:6].strip())
        centralFreq = float(line[6:17].strip())
        density = float(line[17:28].strip())
        C022 = float(line[28:39].strip())
        C033 = float(line[39:50].strip())
        QD12 = float(line[50:61].strip())
        QD13 = float(line[61:72].strip())
        C023 = float(line[72:83].strip())
        meanDir = float(line[83:94].strip())
        Angular_Spread_at_this_Frequency = float(line[94:105].strip())
        Cosine_spread_factor_at_this_frequency = float(line[105:113].strip())

        a1 = QD12/math.sqrt((C022+C033)*density)
        b1 = QD13/math.sqrt((C022+C033)*density)
        a2 = (C022-C033)/(C022+C033)
        b2 = 2*C023/(C022+C033)
        res.append( (centralFreq, freqRange, a1, b1, a2, b2, density) )
        return res
    _read2DSpectrumBin=classmethod(_read2DSpectrumBin)
    
    
    def _readLocation(cls, line):
        """
        return the location of a measurement read into a MEDS file
        @rtype : tuple(float,float, datetime,float)
        @return : a tuple giving the space/time location (lat,lon, date, depth)
        """
        # Header record
        lat = float(line[0:10])
        lon = float(line[10:20])
        # bug in MEDS file (longitude always positive)
        lon = -lon
        depth = float(line[20:28])
        # time
        year = int(line[29:33])
        month = int(line[33:35])
        day = int(line[35:37])
        hh = line[38:40]
        mm = line[40:42]
        if len(hh.strip()) == 0:
            hour = 0
        else:
            hour = int(line[38:40])   
        if len(mm.strip()) == 0:
            minute = 0
        else:
            minute = int(line[40:42])

        buoyDate = datetime (year, month, day, hour, minute)             
        return (lat, lon, depth, buoyDate)
    _readLocation=classmethod(_readLocation)
    
    
    def GetLine(self,linenbr):
        """
        Returns:
            res (str): sought line of the source file [None if the line doesnt exist]
        """
        if linenbr>len(self.content)-1:
            res = None
        else:
            res = self.content[linenbr]
        return res
    
    
    def read_meta_data_provider(self,buoy_name):
        elev = None
        anemo_heeight = None
        wmo = None
        data = get_data("/home/cercache/project/globwave/data/provider/meds/metadata/buoys_metadata_from_provider.ods")
        header = data[u'Feuille1'][0]
        indices = {}
        list_heads = ['Station name','WMO number','Measurement height (m asl)','Elevation above sea level (m)']
        for hh,heads in enumerate(header):
            for hu in list_heads:
                if heads==hu:
                    indices[hu] = hh
        #find the buoy
        ind_buoy = None
        for lline in range(2,len(data)):
            if data[u'Feuille1'][lline][indices['Station name']] == buoy_name:
                ind_buoy = lline
                break
        if ind_buoy is not None: 
            #find the info:
            elev = data[u'Feuille1'][ind_buoy][indices['Elevation above sea level (m)']]
            anemo_heeight = data[u'Feuille1'][ind_buoy][indices['Measurement height (m asl)']]
            wmo = data[u'Feuille1'][ind_buoy][indices['WMO number']]
#         print(json.dumps(data))
        
        return elev,anemo_heeight,wmo
    
    
    def fill_global_attributes(self,lon,lat,sensor):
        globalmetadata = {}
        globalmetadata['institution'] = "Marine Environmental Data Service (MEDS)"
        globalmetadata['institution_abbreviation'] = "MEDS"
        globalmetadata['naming_authority'] = "WMO"
        globalmetadata['title'] = "Wave buoys observations from MEDS provided for GlobWave project"
        globalmetadata['cdm_feature_type'] = "station"
        globalmetadata['scientific_project'] = "GlobWave"
        globalmetadata['acknowledgement'] = "We are grateful to MEDS for providing these data to GlobWave project"
        globalmetadata['standard_name_vocabulary'] = "CF-1.6"
        globalmetadata['restrictions'] = "Restricted to Ifremer and GlobWave usage"
        globalmetadata['processing_level'] = "0"
        globalmetadata['processor_version'] = version_converter
        globalmetadata['history'] = "1.0 : Processing to GlobWave netCDF format"
        globalmetadata['publisher_name'] = "Ifremer/Cersat"
        globalmetadata['publisher_url'] = "http://cersat.ifremer.fr"
        globalmetadata['publisher_email'] = "antoine.grouazel@ifremer.fr"
        globalmetadata['creator_name'] = "Antoine Grouazel"
        globalmetadata['creator_url'] = "http://cersat.ifremer.fr"
        globalmetadata['creator_email'] = "antoine.grouazel@ifremer.fr"
        globalmetadata['data_source'] = os.path.basename(self.input_file)
        elev,anemo_height,wmo = self.read_meta_data_provider(self.longName)
        if elev is None:
            elev = 'unknown'
        if anemo_height is None:
            anemo_height = 'unknown'
        globalmetadata['wind_gust_sampling_duration_in_seconds'] = int(5)
        globalmetadata['wind_sampling_window_in_seconds'] = int(10)
        globalmetadata['wind_sampling_frequency_in_hertz'] = int(1)
        
        globalmetadata['elevation_abose_sea_level'] = elev
        if sensor == 'wave_sensor':
            sensor_family = INSTRUMENT[self.sensorId]
            globalmetadata['sensor_model'] = sensor_family
        elif sensor == 'anemometer':
            globalmetadata['anemometer_height'] = anemo_height
        if wmo is None:
            globalmetadata['wmo_id'] = ''
        else:
            globalmetadata['wmo_id'] = wmo
        globalmetadata['id'] = self.providerId
        globalmetadata['site_elevation'] = ''
        globalmetadata['station_name'] = self.longName
        globalmetadata['platform_code'] = self.providerId
        if self._bathymetry is None:
            self._bathymetry = Bathymetry(filename='/home/cersat5/application_data/globwawe/static_file/bathy/GridOne.nc')
            logging.debug('lon %s lat %s',lon,lat)
            floorDepth = -self._bathymetry.get_depth(lon,lat)
        else:
            floorDepth = -self._bathymetry.get_depth(lon,lat)
        logging.debug('floordepth type : %s %s',type(floorDepth),floorDepth)
        globalmetadata['sea_floor_depth_below_sea_level'] = math.fabs(floorDepth)
        if self._landmask is None:
            self._landmask = Landmask('/home/losafe/users/agrouaze/git/cerbere/cerbere/geo/resources/NAVO-lsmask-world8-var.dist5.5.nc')
            dist = self._landmask.getDistanceToShore(lat,lon)
        else:
            dist = self._landmask.getDistanceToShore(lat,lon)
        globalmetadata['distance_to_shore_in_meters'] = dist
        globalmetadata['nominal_latitude'] = lat
        globalmetadata['nominal_longitude'] = lon
        return globalmetadata
    
    def pack_sensor_data(self,fields,metadata,timefield,outputdir):
        """
        put the fields into timeserie object and save it as netcdf
        """
        for sensor in fields.keys():
            ts = PointTimeSeries(longitude=metadata['lon'],latitude=metadata['lat'],depth=metadata['depth'],times=timefield)
            for varname in fields[sensor].keys():
                ts.add_field(fields[sensor][varname])
            start_dt = netCDF4.num2date(timefield.get_values()[0],units=time_units)
            end_dt = netCDF4.num2date(timefield.get_values()[-1],units=time_units)
            globalmetadata = self.fill_global_attributes(metadata['lon'],metadata['lat'],sensor)
            if sensor == 'wave_sensor':
                spectral_data = True
            else:
                spectral_data = False
            ofname = Define_output_filename(instr=sensor,lon=metadata['lon'],lat=metadata['lat'],intstart=start_dt,intend=end_dt,globalmetadata=globalmetadata,spectral=spectral_data,outputdir=outputdir)
            if os.path.exists(ofname):
                os.remove(ofname)
            tsncf = NCFile(url=ofname, ncformat='NETCDF4', mode=WRITE_NEW)
            logging.debug("Write file %s" % ofname)
            
            ts.save(tsncf, attrs=globalmetadata)
            tsncf.close()
            logging.info("output file %s" % ofname)
            
        return

if __name__ == "__main__":
    """
    usage: python ~/git/globwave/src/insitu/medsmooredbuoy.py -v -o /tmp/ -i /home/cercache/project/globwave/data/provider/meds/nrt/2015/c45141_y2d.fb
    #     srcFiles = '/home/cercache/project/globwave/data/provider/meds/nrt/2014/'
    """
    logging.basicConfig(level=logging.DEBUG)
    tmp = logging.getLogger('meds')
    logger = ColoredLogger(tmp)
    start = time.time()
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option("-v","--verbose",
                      action='store_true',default=False,
                      help="verbose mode [default is quiet]")
    parser.add_option("-o","--output_dir",
                      action='store',
                      dest="output_dir",
                      help="output directory [mandatory]")
    parser.add_option("-i","--input_filename",
                      action='store',
                      dest='input_filename',
                      help="path of the input file to be converted [mandatory]")
    (options, args) = parser.parse_args()
    if options.verbose:
        logger.setLevel(level=logging.DEBUG)
    else:
        logger.setLevel(level=logging.INFO)
    if options.output_dir is None:
        raise Exception('missing -o option')
    if options.input_filename is None:
        raise Exception('missing -i option')
    buoy = MEDSMooredBuoy(options.input_filename)
    data = buoy.read_source_file()
    if data != {}:
        intervals = buoy.continuity_test(data)
        buoy.get_ref_freq_param(data)
        for kk,inter in enumerate(intervals):
            intstart,intend,metadata = inter
            logger.debug('interval #%s intstart %s,intend %s',kk,intstart,intend)
            fields,timefield = buoy.create_fields(data,intstart,intend,metadata['nbWaveBins'])
            buoy.pack_sensor_data(fields,metadata,timefield,outputdir=options.output_dir)
    else:
        logging.info('no data, -> finished')
    elapse = '%4.0f'%(time.time()-start)
    logger.info('end of processing for MEDS buoy %s in %sseconds',options.input_filename,elapse)

