'''
 @author: agrouaze
 @date: oct 2014
 @purpose: convert files from CETMEF CDOCO (netcdf) to globwave format (netcdf)
 @versions:
 0.1 : version beta decision to stop splitting files within a month
 0.2 : start exploitation + adapt to buoy having only frequency and no direction 26/03/2015
 0.3 : add WMO name if found in file naming 31/03/2015, known bug for time variable on CDOCO side already reported
 NOTE: OBSOLET since the cdoco format is wrong (dimension mistake) and not yet updated
''' 
import os
import logging
# import math
import glob
# import sys
import numpy
import netCDF4
import datetime
import shutil
# import string
# import collections
import pdb
import time
import copy
from optparse import OptionParser
# import traceback
from collections import OrderedDict
from cerbere.datamodel.pointtimeseries import PointTimeSeries
# from cerbere.datamodel.pointcollection import PointCollection
from cerbere.datamodel.variable import Variable
from cerbere.datamodel.field import Field, QCLevel, QCDetail
# import cerbere.science.cfconvention
# import cerbere.science.wave
from cerbere.mapper.abstractmapper import WRITE_NEW
from cerbere.mapper.ncfile import NCFile
# from cerbere.geo.bathymetry import Bathymetry
# from cerbere.geo.landmask import LandMask
# from cerbereutils.ancillary.landmask import Landmask
# from time import gmtime, strftime
# from scipy import interpolate
# from timeserieconcat import ConcatenateFiles, findContigousPlatformFiles, findWidderPlatformFile,sortContigousPlatformFiles
from variables_conventions import VarConventions
soft_version = '0.3'
root_dir_data = '/home/coriolis_exp/spool/co01/co0123/co012303/co01230311/netcdf/'
time_units = 'days since 1950-01-01T00:00:00Z'
wmo_table = {
             'Banyuls':'61188',
             'Roscoff':'62068',
             'Oleron':'62080',
             'Porquerolles':'61021',
             'Alistro':'61295',
             'Ile d-Yeu Nord':'62067',
             'Saint-Jean-de-Luz':'62079',
             'Sainte Lucie':'41099',
             'Fort de France':'41097',
             'Basse Pointe':'41098',
             'Anglet':'62066',
             'Belle-Ile':'62074',
             'Cherbourg':'62059',
             'Le havre':'62060',
             'Plateau du Four':'62078',
             'Sete':'61190',
             'Espiguette':'61431',
             'Pierres-Noires':'62069',
             'Le Planier': '61289',
             'Leucate':'61191',
             'Nice':'61187',
             'Les Minquiers Nord':'62061',
             'Cap Ferret':'',
             
             }
class convertor(object):
    
    def __init__(self,input_dir):
        self.directory = input_dir
        
    def dateGetter(self,path):
        '''get the datetime object from filename'''
        filename = os.path.basename(path)
    #     logging.debug('dateGetter %s',filename)
        tmp = filename[6:21]
        tmp = datetime.datetime.strptime(tmp,'%Y%m%d_%H%M%S')
        tmp = netCDF4.date2num(tmp,time_units)
    #     logging.debug('date only %s',tmp)
        return tmp
    
    def GetStartEndDateSerie(self,file1,file2):
        '''from 2 files get the start and stop time'''
#         nc1 = netCDF4.Dataset(file1,'r')
#         nc2 = netCDF4.Dataset(file2,'r')
#         tm1 = nc1.variables['time'][:]
#         tm2 = nc2.variables['time'][:]
#         concat = numpy.concatenate([tm1,tm2])
#         unit = nc2.variables['time'].getncattr('units')
#         start = min(concat)
#         end = max(concat)
#         start_dt = netCDF4.num2date(start,unit)
#         end_dt = netCDF4.num2date(end,unit)
#         nc1.close()
#         nc2.close()
#         print file1,file2
        file1 = os.path.basename(file1)
        file2 = os.path.basename(file2)
        split1 = file1.split('_')
        split2 = file2.split('_')
        start_dt = datetime.datetime.strptime(split1[1]+'_'+split1[2][0:-3],'%Y%m%d_%H%M%S')
        end_dt = datetime.datetime.strptime(split2[1]+'_'+split2[2][0:-3],'%Y%m%d_%H%M%S')
        return start_dt,end_dt
    
    def DetermineGroups(self,allfields):
        '''group the variable to create separate files for each sensor'''
        groups = {}
        for field in allfields:
            logging.debug('field get groups %s',field)
            if field not in ['depth', 'lat', 'lon', 'time','direction','frequency','station','string16','station_name','spreading']:
                allinfo = VarConventions(field)
                if allinfo is not None:
                    GWname,longname_cv,description_cv,sensor_cv,units_cv = allinfo
                    logging.debug('group %s sensor %s',groups,sensor_cv)
                    if sensor_cv in groups:
                        groups[sensor_cv].append(field)
                    else:
                        groups[sensor_cv] = [field]
                else:
                    logging.error('impossible to determine the sensor group of the variable: %s',field)
        return groups
    
    
    def homogeneize_spectra(self,tmp_val,directions,std_directions,frequencies):
        '''complete spectra values depending of the complete directions'''
        fv = tmp_val.fill_value
        tmp_val_complete = numpy.ma.ones((1,len(frequencies),len(std_directions)))*fv
        tmp_val_complete.mask = numpy.ma.ones(tmp_val_complete.shape)
        didi = directions.astype(int)
        ind_dir = []
        logging.debug('directions %s',didi)
#         pdb.set_trace()
        if (didi<=360).all() and (didi>=0).all():
            for di in didi:
                ind_dir.append(numpy.where(di==std_directions)[0][0])
            tmp_val_complete[:,:,ind_dir] = tmp_val.copy()
            tmp_val_complete[:,:,ind_dir].mask = False
        return tmp_val_complete
    
    def get_wmo_number(self,platform_name):
        res = 'CEREMA-'+platform_name.replace(' ','')
        for possib in wmo_table.keys():
            if possib in platform_name:
                res = 'WMO'+wmo_table[possib]
        
        return res
    
    def ConcatenateFiles(self,listoffiles):
        '''concatenate files when contiguous'''
        logging.debug('enter ConcatenateFiles method')
        std_directions = numpy.arange(0,361)
        first_file = listoffiles[0]
        last_file = listoffiles[-1]
        base1 = os.path.basename(first_file)
    #     base2 = os.path.basename(file2)
        plat = base1.split('_')[0]
    #     start1=base1[indicescor[0]+1:indicescor[1]]
        tmp = base1[6:21]
        logging.debug('first %s last %s',first_file,last_file)
        startn,endn = self.GetStartEndDateSerie(first_file,last_file)
        endnchar = datetime.datetime.strftime(endn, '%Y%m%dT%H%M')
        startnchar = datetime.datetime.strftime(startn, '%Y%m%dT%H%M')
        
        
        ncA = NCFile(first_file)
        source_buoy_name = ncA.read_global_attribute('title')
        allfields = ncA.get_fieldnames()
        groups = self.DetermineGroups(allfields)
    #     ncB = NCFile(file2)
        lon = ncA.read_values('lon')[0][0]
        lat = ncA.read_values('lat')[0][0]
#         lat = numpy.reshape(lat,(1,1))
#         lon = numpy.reshape(lon,(1,1))
        if lon<0:
            lon_str = '%2.2fW' % (abs(lon))
        else:
            lon_str = '%2.2fE' % (lon)
        if lat >=0:
            lat_str = '%2.2fN' % (lat)
        else:
            lat_str = '%2.2fS' % (abs(lat))
        buoyid = self.get_wmo_number(source_buoy_name)
        new_file_name = '%s_%s_%s_Lat_%s_Lon_%s.nc' % (buoyid,startnchar,endnchar,lat_str,lon_str)
        timesA = ncA.read_field('time')
    #     timesB = ncB.read_values('time')
#         time_units = timesA.units
    #     depth = ncA.read_values('depth')[0]
        buoy_with_frequency = False
        buoy_with_direction = False
        if 'frequency' in allfields:
            frequency  = ncA.read_field('frequency')
            allinfo = VarConventions('frequency')
            GWname,longname_cv,description_cv,sensor_cv,units_cv = allinfo
            freq_var = Variable(shortname=GWname,description=description_cv)
            frequency_field = Field(variable=freq_var,dimensions=frequency.dimensions,values=frequency.get_values())
            buoy_with_frequency = True
#         direction.set_values()
        if 'direction' in allfields:
            direction  = ncA.read_field('direction')
            direc_var = direction.variable
            meta_src = direction.get_metadata()
            direction_dim = OrderedDict([('direction',len(std_directions))])
            direction_field = Field(variable=direc_var,dimensions=direction_dim,values=std_directions)
            direction_field.set_metadata(meta_src)
            buoy_with_direction = True
        depth = 0.
        globalmetadata = ncA.read_global_attributes()
        newglobattrib = {}
        newglobattrib['software_version'] = soft_version
        newglobattrib['station_name'] = source_buoy_name
        newglobattrib['time_coverage_start'] = startnchar
        newglobattrib['time_coverage_end'] = endnchar
#         pdb.set_trace()
#         for y, toto in enumerate(globalmetadata):
#             valatt = ncA.read_global_attribute(toto)
#             if toto == 'time_coverage_start':
#                 newglobattrib[toto] = startnchar
#             elif toto == 'time_coverage_stop':
#                 newglobattrib[toto] = endnchar
#             elif toto == 'time_coverage_end':
#                 pass
#             else:   
#                 newglobattrib[toto] = valatt
    #         newglobattrib['starting_date']=startnchar
    #         newglobattrib['ending_date']=endnchar
        newtimes = numpy.array([])
        for ff in listoffiles:
            tmp_time = self.dateGetter(ff)
#             nc = NCFile(ff)
#             tmp_time = nc.read_field('time').get_values()
#             print tmp_time
#             nc.close()
            newtimes = numpy.concatenate([newtimes, [tmp_time]])
#         pdb.set_trace()
        ts_ts = {}
    #     for cf in allfields:
        logging.info('loop over groups %s',groups)
        for grp in groups.keys():
            ofields = {}
            if grp == 'wave_sensor':
                if buoy_with_direction:
                    ofields['direction'] = direction_field
                if buoy_with_frequency:
                    ofields['frequency'] = frequency_field
                
            for cf in groups[grp]   :
                logging.debug('variable %s',cf)
                if cf not in ['depth', 'lat', 'lon', 'time','direction','frequency']:
                    logging.debug('field %s',cf)
                    fiA = ncA.read_field(cf)
    #                 fiB = ncB.read_field(cf)
                    allinfo = VarConventions(cf)
                    GWname,longname_cv,description_cv,sensor_cv,units_cv = allinfo
                    varvar = Variable(shortname=GWname,description=description_cv)
                    logging.debug('dimensions %s',fiA.dimensions)
                    if ('frequency' in fiA.dimensions)==False: #for example Tmer(time,station)
    #                     nbtimeA = ncA.get_dimsize('time')
    #                     nbtimeB = ncB.get_dimsize('time')
                        dimdim = OrderedDict([('time', len(listoffiles))])
    #                     valA = ncA.read_values(cf)
    #                     valB = ncB.read_values(cf)
    #                     newvals = numpy.append(valB, valA)
                        newvals = numpy.ma.array([])
                        for ff in listoffiles:
                            nc = NCFile(ff)
                            tmp_val = nc.read_values(cf)[0]
                            nc.close()
                            newvals = numpy.ma.concatenate([newvals, tmp_val])
                    else:
        #                 for dim in ncA.get_full_dimensions():
        #                     if dim != 'time':
        #                         seconddim = dim
                        lenseconddim = ncA.get_dimsize('frequency')
#                         directiondim = len(ncA.get_dimensions('direction'))
    #                     nbtimeA = ncA.get_dimsize('time')
    #                     nbtimeB = ncB.get_dimsize('time')
                        if buoy_with_direction:
                            dimdim = OrderedDict([('time',len(listoffiles)),('frequency',lenseconddim),('direction',len(std_directions))])
                        else:
                            dimdim = OrderedDict([('time',len(listoffiles)),('frequency',lenseconddim)])
    #                     valA = ncA.read_values(cf)
    #                     valB = ncB.read_values(cf)
    #                     newvals = numpy.append(valB, valA)
#                         newvals = numpy.array([])
                        for jj,ff in enumerate(listoffiles):
                            
                            nc = NCFile(ff)
#                             tmp_val = numpy.squeeze(nc.read_values(cf)[0])
                            tmp_val = nc.read_values(cf)[0]
                            
                            frequencies = nc.read_values('frequency')
                            if buoy_with_direction:
                                directions = nc.read_values('direction')
                                std_val = self.homogeneize_spectra(tmp_val,directions,std_directions,frequencies)
                            else:
                                std_val = tmp_val
#                             std_val = numpy.reshape(std_val, (1,std_val.shape[0],std_val.shape[1]))
#                             print 'tmp',tmp_val.shape,'interoplated',std_val.shape
                            if jj==0:
                                newvals = std_val
                            else:
                                newvals = numpy.ma.concatenate([newvals, std_val])
                            nc.close()
#                             print 'final vals',newvals.shape
                            
#                     newvals = numpy.ma.array(newvals)
                    newfield = Field(varvar,dimdim,units=units_cv,values=newvals)
            #             ofields.append(newfield)
                    ofields[cf] = newfield
            timeobj = (newtimes, time_units)
            newtimeserie = PointTimeSeries(fields=ofields, longitude=lon, latitude=lat, depth=depth, times=timeobj)
            ts_ts[grp] = newtimeserie
        return ts_ts,newglobattrib,new_file_name
    
    def findNearestFile(self,filename,list):
        '''
        get the name of the file with closest date
        '''
        datex = self.dateGetter(filename)
        mindiff = 1000
        for ff in list:
            datey = self.dateGetter(ff)
            if abs(datey-datex)<mindiff:
                mindiff = abs(datey-datex)
                nearest = ff
        if datey-datex>0:
            signus = -1
        else:
            signus = +1
        return nearest,signus
    
    def getCETMEFFiles(self,yearmonth):
        '''give all the files without cutting sub series'''
        filez0 = glob.glob(os.path.join(self.directory,'*'+yearmonth+'*.nc'))
        files1 = sorted(filez0,key=self.dateGetter)
        return files1
    
    def sortContigousCETMEFFiles(self,directory_campagne,yearmonth):
        '''list the files that are close enough in time to be concatenated'''
#         dateformt = '%Y%m%d_%H%M%S'
        filez0 = glob.glob(directory_campagne+'*'+yearmonth+'*.nc')
        files1 = sorted(filez0,key=self.dateGetter)
        logging.info('%s file to be sorted in series',len(files1))
    #     copy_files = sorted(filez0,key=dateGetter)
        series = []
    #     while len(copy_files) != 0
        indices = numpy.ma.array(data=numpy.arange(0,len(filez0)),mask=[False])
    #     maski = indices.mask == False
        while (indices.mask==False).any():
#             logging.debug('indices %s',indices)
            notmasked = numpy.where(indices.mask==False)
#             logging.debug('indice of seed %s',notmasked[0][0])
            seed = files1[notmasked[0][0]]
            base_filename = os.path.basename(seed)
            serie = [base_filename]
    #         del copy_files[0]
            indices.mask[notmasked[0][0]] = True
            endserie = self.dateGetter(seed)
            for ii,ff in enumerate(files1):
#                 logging.debug('file tested %s',ff)
                if ff != seed:
                    fname = os.path.basename(ff)
                    cu_datefile = self.dateGetter(ff)
                    if abs(cu_datefile - endserie) <= 1:
                        if indices.mask[ii] == False:
                            serie.append(fname)
#                             logging.debug('serie content %s',serie)
                            indices.mask[ii] = True
    #                     if ff in copy_files:
    #                         ind = copy_files.index(ff)
    #                         del copy_files[ind]
                        endserie = cu_datefile
            series.append(serie)
#         logging.debug('series %s',len(series))
        
        return series
    
    def convert_a_given_buoy(self,yearmonth,outputdir):
    #     list_of_list = convert.sortContigousCETMEFFiles(input_dir,yearmonth)
        list_to_concat = self.getCETMEFFiles(yearmonth)
        if list_to_concat != []:
            timeserie,globalmetadata,newfilename = self.ConcatenateFiles(list_to_concat)
            logging.debug('%s',timeserie)
            for sensor in timeserie.keys():
                logging.info('sensor %s',sensor)
                out_dir = os.path.join(outputdir,sensor,yearmonth[0:4],yearmonth[4:6])
                out_path = os.path.join(out_dir,newfilename)
                if os.path.exists(out_dir)==False:
                    os.makedirs(out_dir, 0775)
                if os.path.exists(out_path):
                    os.remove(out_path)
        #         tsncf = NCFile(url=out_path, ncformat='NETCDF4', mode=WRITE_NEW)
                tsncf = NCFile(url=out_path, mode=WRITE_NEW)
                timeserie[sensor].save(tsncf, attrs=globalmetadata)
                tsncf.close()
                logging.info(' %s saved',out_path)
        else:
            logging.info('no data for %s in %s',yearmonth,self.directory)
        return
    
def process_all_buoys(yearmonth,outputdir):
    for dirname, dirnames, filenames in os.walk(root_dir_data):
        # print path to all subdirectories first.
        for bb,subdirname in enumerate(dirnames):
            buoy_dir = os.path.join(dirname, subdirname)
    #         for bb,buoy_dir in enumerate(os.listdir(root_dir_data)):
       
            logging.info('%s buoy %s/%s',subdirname,bb,len(dirnames))
            convert = convertor(buoy_dir)
            convert.convert_a_given_buoy(yearmonth,outputdir)
    return
    
def exploit_mode():
    date_5days_before = datetime.datetime.now()-datetime.timedelta(days=5) #to be sure that the data collected with 5days of delay will be converted
    yearmonth = datetime.datetime.strftime(date_5days_before,'%Y%m')
    
    list_months = []
    list_months.append(yearmonth)
    yearmonth = datetime.datetime.now().strftime('%Y%m')
    if yearmonth not in list_months:
        list_months.append(yearmonth)
    for yearmonth in list_months:
        year = yearmonth[0:4]
        month = yearmonth[4:6]
        start = time.time()
        logging.info('start exploitation mode of CETEMEF/CEREMA conversion to Globwave %s %s',year,month)
        temp_dir = os.path.join('/home/cercache/project/globwave/prog/GLOBWAVE/src/insitu/buoynetworks/cetmef/processings',year,month+'/')
        logging.info('temporary dir %s',temp_dir)
        final = '/home/cercache/project/globwave/data/globwave/cetmef/'
        if os.path.exists(temp_dir):
            shutil.rmtree(temp_dir)
            os.makedirs(temp_dir)
        logging.info('start processing cetmef buoys %s',yearmonth)
        
        process_all_buoys(yearmonth,temp_dir)
    
        for param in ['wave_sensor','ocean_temperature_sensor']:
            final_param_dir = os.path.join(final,param,year,month+'/')
            tmp_param_dir = os.path.join(temp_dir,param,year,month+'/')
            if os.path.exists(final_param_dir):
                logging.info('delete previous generated files to avoid doublon')
                shutil.rmtree(final_param_dir)
            os.makedirs(final_param_dir)
            logging.info('set flag processing in progress')
            os.system('touch '+final_param_dir+'processing_in_progress_please_wait')
            logging.info('move new files from %s to %s',tmp_param_dir,final_param_dir)
    #         shutil.move(tmp_param_dir+'*nc',final_param_dir )
            os.system('rsync -avz '+tmp_param_dir+' '+final_param_dir)
            logging.info('remove flag processing in progress')
            os.remove(final_param_dir+'processing_in_progress_please_wait')
        endt = time.time()
        logging.info('total time to convert all the buoy on month %s is: %ss',yearmonth,endt-start)
    return
        
if __name__ == '__main__':
    '''
    @usage: python cetmefglobwaveconvertor.py -o /tmp -q -d 201402 
    '''
    root = logging.getLogger()
    if root.handlers:
        for handler in root.handlers:
            root.removeHandler(handler)

    parser = OptionParser()
    parser.add_option("-i","--input",
                      action="store", type="string",
                      dest="input", metavar="string",
                      help="file(s) to be converted in globWave format (can be directory) [optional]")
    parser.add_option("-o","--outputdir",
                      action="store", type="string",
                      dest="outputdir", metavar="string",
                      help="directory where will be created the sub arborescence sensor/YYYY/MM/file.nc ")
    parser.add_option("-d","--date",
                      action="store", type="string",
                      dest="yearmonth", metavar="string",
                      help="YYYYMM to process")
    parser.add_option("-q","--quiet",
                      action="store_true", default=False,
                      dest="quiet",
                      help="quiet mode")
    parser.add_option("--exploit",
                      action="store_true", default=False,
                      dest="exploit",
                      help="exploitation mode")
    (options, args) = parser.parse_args()
    
    if options.quiet:
        logging.basicConfig(level=logging.INFO)
    else:
        logging.basicConfig(level=logging.DEBUG)
    logging.info('start converting CETMEF netcdf files to globwave format')
    if options.exploit:
        exploit_mode()
    else:
        if options.outputdir is None:
            raise Exception('missing outpudir')
        if options.input is None:
            process_all_buoys(options.yearmonth,options.outputdir)
        else:
            convert = convertor(options.input)
            convert.convert_a_given_buoy(options.yearmonth,options.outputdir)

        logging.info('end of conversion at %s',datetime.datetime.now())
