"""
#soft to convert ECMWF compilation of wave buoys files to GlobeWave netcdf format
#author: maccensi adapted to suit new version of cerbere/naiad by agrouaze
#merged with process_history.py
#first update: 2013/12/30
#last update: 2014/01/28
"""
from datetime import datetime, timedelta
import os,sys, math

from cerbere.geo.bathymetry import Bathymetry
from cerbere.geo.landmask import LandMask
from cerbere.datamodel.pointtimeseries import PointTimeSeries
from cerbere.datamodel.variable import Variable
from cerbere.datamodel.field import Field, QCLevel, QCDetail
import cerbere.science.cfconvention
import cerbere.science.wave
from cerbere.mapper.abstractmapper import WRITE_NEW
from cerbere.mapper.ncfile import NCFile

import glob
#import subprocess
from subprocess import check_output
import logging
import numpy
from collections import OrderedDict

#RECORD_TIME = 0
##RECORD_LOCATION = 1
#RECORD_LAT=1
#RECORD_LON=2
#RECORD_DEPTH=18
#RECORD_SHORE=4
#RECORD_INSTRUMENT = 5
#RECORD_TYPE = 6
#RECORD_BUOY = 7
#RECORD_DATA = 8 #useless
dicVariables = {
    'wind_speed':('wind speed','wind speed','anemometer','m.s-1'),
    'wind_gust':('wind gust','wind gust speed','anemometer','m.s-1'),
    'wind_from_direction':('wind from direction','wind from direction','anemometer','degree'),
    'maximum_wave_height':('maximum wave height','maximum wave height','wave_sensor','m'),
    'wave_from_direction':('wave mean direction','wave mean direction','wave_sensor',"degree"),
    'wave_period':('wave period','wave period','wave_sensor','s'),
    'wave_height':('wave height','wave height','wave_sensor','m'),
    'pressure':('air pressure at sea level','air pressure at sea level','barometer','P'),
    'air_temperature':('mean air temperature at sea surface','mean air temperature at sea surface','air_temperature_sensor','C'),
    'sea_surface_temperature':('sea surface temperature','sea surface temperature','ocean_temperature_sensor','C'),
    }
columndic = {
    'TIME':0,
    'latitude':1,
    'longitude':2,
    'A':3,
    'buoyname':4,
    'wave_from_direction':5,
    'wave_period':6,
    'wave_height':7,
    'wind_from_direction':8,
    'wind_speed':9,
    'pressure':10,
    'air_temperature':11,
    'sea_surface_temperature':12,
    'wind_gust':13,
    'maximum_wave_height':14,
    'depth':15}
sensorList = ['air_temperature_sensor','anemometer','barometer','ocean_temperature_sensor','wave_sensor']

#class ECMWFMooredBuoy(insitu.mooredbuoy.MooredBuoy):
class ECMWFMooredBuoy():
    _bathymetry = None
    INSTRUMENT = {
                "AE":"AES ODAS buoy. (6m NOMAD, 3m DISCUS or Watchkeeper)",
                "AW":"AES buoy data with bad Watchman payload. (Truncated spectra, VCAR=VWH$, VTPK=VTP$)",
                "MI":"Miros Radar",
                "PC":"Pressure cell",
                "ST":"Staff gauge",
                "SW":"Swartz gauge",
                "WA":"Non-directional waverider buoy (Datawell)",
                "WC":"Directional WAVEC buoy (Datawell)",
                "WD":"Directional Waverider buoy (Datawell)",
                "WP":"Non-directional WRIPS buoy",
                "WR":"Non-directional Waverider"}
#dicLinkVar_sensor={'anemometer':('

    def readTimeSeries(self, dataFile): 
        """
        @param dataFile: full path to the input buoy data file
        """
        logging.info("Processing..............%s" % dataFile )
        self.history=['']
        if self._bathymetry == None:
            self._bathymetry = Bathymetry(filename='/home/cercache/users/agrouaze/git/cerbere/cerbere/geo/resources/GridOne.nc')
        #self._bathymetry = Bathymetry()
        # read data records and instanciate measurements
        depth = None
        dist2shore = None
        numberTotaloflines=int(check_output(["wc", "-l", dataFile]).split()[0])
        #print numberTotaloflines
        f = open(dataFile, 'r')
        logging.info("FILE : %s" % dataFile)


        # ----------------------------------
        #HEADER
        # ----------------------------------
        print 'lecture header'
        recordList = []
        # information lines
        line = f.readline()
        while '*' in line:
            line = f.readline()
        
        # ----------------------------------
        #BODY
        # ----------------------------------
        print 'lecture body'
        #line = f.readline()
        #content = line.split()
        cptline=0
        table_data=[]
        while line.strip() != '':
          cptline+=1
          print 'line ',cptline,'/',numberTotaloflines
          content = line.split() 
        # fill not existing content
        if len(content)<14:
            for i in range(len(content)+1,15):
                content.append('-999.00')
        buoyname = content[0]
        date = content[1]
        year = int(date[0:4])
        month = int(date[4:6])
        day = int(date[6:8])
        hour = int(date[8:10])
        minute = int(date[10:12])
        measTime = datetime (year, month, day, hour, minute)
        lat = float(content[2])
        lon = float(content[3])
        #print content[10]
        if buoyname == "21002":
            self.longName = "what"
        elif buoyname == "21004":
            self.longName = "what"
        else:
            self.longName = buoyname
        print lat,lon
        #bathdepth = self._bathymetry.getDepth(lat,lon)
        #bathdepth = -500
        #dist2shore= int(LandMask.getDistanceToShore(lat, lon))
        table_data.extend([[measTime,lat,lon,'A',buoyname,float(content[4]),float(content[5]),float(content[6]),float(content[7]),float(content[8]),float(content[9]),float(content[10]),float(content[11]),float(content[12]),float(content[13]),0]])
        self.institution='ECMWF'
        #self.providerId= buoyname
        line = f.readline()
      
      # ------------------------------------------------------------------
      # Part 2 : browse records and break test
      # ------------------------------------------------------------------
      invalidRecords = []
      breakList = []
      

      # TEST ON CHRONOLOGY
      print 'chronology test ======================'
      previousRecordATime = None
      lastARecord = None  # indice du dernier record A
      for i,rec in enumerate(table_data):
      #print rec,i
      #input()
      if i > 1 and rec[columndic['A']] == 'A':
          if rec[columndic['TIME']] <= previousRecordATime:
          # Dans la cas de duplication ou retour arriere, on ne garde que les dernieres valeurs lues
          # (Choix totalement arbitraire) => invalide le block de mesure precedent
          logging.warning("Duplicated or backward measurement block at : %s (previous = %s) => keep the last one if same time" % ( rec[columndic['TIME']], previousRecordATime))
          for inv in range(lastARecord,i):
              invalidRecords.append( inv )
          print "Backward measurement : ", rec[columndic['TIME']], previousRecordATime
          #raise Exception( "Backward measurement" )
      previousRecordATime = rec[columndic['TIME']]
      if rec[columndic['A']] == 'A':
          lastARecord = i   


      # TEST ON BUOYNAME
      print 'split  buoys ======================'
      previousRecordABuoy = None
      for i,rec in enumerate(table_data):
      if i > 1 and rec[columndic['A']] == 'A':
          if rec[columndic['buoyname']] != previousRecordABuoy:
          breakList.append( i )
          print "New buoy : ", previousRecordABuoy, rec[columndic['buoyname']]
          logging.warning("new buoy : %s (previous = %s)" % (rec[columndic['buoyname']],previousRecordABuoy))
      previousRecordABuoy = rec[columndic['buoyname']]


      # TEST ON MONTH
      print 'split month ======================'
      previousRecordBTime = None
      for i,rec in enumerate(table_data):
      if i > 1 and rec[columndic['A']] == 'A':
          if rec[columndic['TIME']].month != previousRecordBTime.month:
          breakList.append( i )
          print "previous, New month : ", previousRecordBTime, rec[columndic['TIME']]
          logging.warning("new month : %s (previous = %s)" % ( previousRecordBTime, rec[columndic['TIME']]) )
      previousRecordBTime = rec[columndic['TIME']]


      # TEST ON POSITION
      print 'test position ======================'
      #previousRecordALocation = None
      previousLAT=None
      previousLON=None
      changeInLocation = []
      for i,rec in enumerate(table_data):
      #print rec[RECORD_LAT],rec[RECORD_LON]
      if i > 1 and rec[columndic['A']] == 'A':
          #print rec[RECORD_LOCATION][0]
          #if not rec[RECORD_LOCATION].isEqual ( previousRecordALocation, 0.1 ):
          #if abs(rec[RECORD_LOCATION][0]-previousRecordALocation[0])>0.1 and abs(rec[RECORD_LOCATION][1]-previousRecordALocation[1])>0.1 :
          if abs(rec[columndic['longitude']]-previousLON)>0.1 or abs(rec[columndic['latitude']]-previousLAT)>0.1 :
          changeInLocation.append( i )
          breakList.append( i )
          print "Change in location : ", rec[columndic['longitude']],rec[columndic['latitude']],previousLAT,previousLON# previousRecordALocation
          #logging.warning("Change in location : %s (previous = %s)" % ( rec[RECORD_LOCATION], previousRecordALocation ) )
          logging.warning("Change in location : %s%s (previous = %s%s)" % ( rec[columndic['longitude']],rec[columndic['latitude']],previousLAT,previousLON ) )
          #raise Exception( "Location change")
      #previousRecordALocation = rec[RECORD_LOCATION]
      previousLAT=rec[columndic['latitude']]
      previousLON=rec[columndic['longitude']]


      # TEST on DEPTH
      print 'test depth ======================'
      previousRecordADepth = None
      changeInDepth = []
      for i,rec in enumerate(table_data):
      if i > 1 and rec[columndic['A']] == 'A':
        #print rec[RECORD_LOCATION][2]
        if rec[columndic['depth']] != previousRecordADepth:
        #if rec[RECORD_LOCATION].depth != previousRecordADepth:
        changeInDepth.append( i )
        breakList.append( i )
        print 'change in depth'
        logging.warning( "Change in depth : %s (previous = %s)" % (rec[columndic['depth']], previousRecordADepth) )
        #raise Exception( "Change in depth" )
      #previousRecordADepth = rec[RECORD_LOCATION][2]
      previousRecordADepth = rec[columndic['depth']]
      #print recordList
      # ------------------------------------------------------------------
      # Part 3 : Time series creation
      # ------------------------------------------------------------------
      startind=0
      endind=0
      naming = "WMO"
      
      print 'netcdf writting ',len(breakList),' breaks'
      cpt=0
      breakList.sort()
      print 'breakList',breakList
      for i in range(len(breakList)+1):#loop over the breaks
        cpt+=1
    #endind=i-1
    if i==len(breakList):
      endind=len(table_data)
    else:
      endind=breakList[i]-1
    print cpt,'/',len(breakList),' start:  ',startind,':',endind
    if startind!=endind:
      #print columndic['TIME']
      #print table_data[:][columndic['TIME']]
      columnIwant=zip(*table_data)[columndic['TIME']]
      time_measures=columnIwant[startind:endind]
      time_measures=numpy.ma.masked_array(time_measures)
      time_measures[time_measures==-999]=numpy.ma.masked
      #time_measures=table_data[startind:endind][RECORD_TIME]
      time_ordered=OrderedDict([('time',len(range(startind,endind)))])
      #print time_measures
      #time_measures=[uu[0] for uu in recordList]
      #print 'seconde option ',time_measures
      #sensor=recordList[startind][5]
      buoyname=table_data[startind][columndic['buoyname']]#use juste the first
      lon=table_data[startind][columndic['longitude']]
      lat=table_data[startind][columndic['latitude']]
      #print lat,type(int(lat)),type(lat)
      #bathydepth=table_data[startind][3]
      bathydepth = self._bathymetry.getDepth(lon,lat)
      depthies=table_data[startind][columndic['depth']]
      #dist2shories=table_data[startind][columndic['dist2shore']]
      dist2shories= int(LandMask.getDistanceToShore(lat, lon,filename='/home/cercache/users/agrouaze/git/cerbere/cerbere/geo/resources/NAVO-lsmask-world8-var.dist5.5.nc'))
      metadata = [ \
        ('id', id),
        ('naming_authority',naming),
        ('wmo_id', buoyname),# buoy.WMOId),              
        ('institution','European Centre for Medium-Range Weather Forecasts'),
        ('institution_abbreviation','ECMWF'),
        ('buoy_network', 'undefined'),
        ('title','Buoy observation from ECMWF provided for GlobWave project'),
        ('summary',''),
        ('station_name', buoyname),
        ('sea_floor_depth_below_sea_level', bathydepth),
        ('site_elevation',0.),
        ('cdm_feature_type','station'),
        ('scientific_project','GlobWave'),
        ('restrictions','Restricted to Ifremer and GlobWave usage'),
        ('format_version','2.0'),
        ('history',['1.0 : Processing to GlobWave netCDF format']),
        ('publisher_name','Ifremer/Cersat'),
        ('publisher_url','http://cersat.ifremer.fr'),
        ('publisher_email','jfpiolle@ifremer.fr'),
        ('creator_url','http://cersat.ifremer.fr'),
        ('creator_email','jfpiolle@ifremer.fr'),
        ('date_modified', ''),
        ('processing_software','Globwave cerbere python lib v2.0'),
        ('references',''),
        ('data_source',dataFile.split('/')[-1]),
        ('nominal_latitude', lat),
        ('nominal_longitude', lon),
        ('geospatial_lat_min', lat),
        ('geospatial_lat_max', lat),
        ('geospatial_lat_units','degrees'),
        ('geospatial_lon_min', lon),
        ('geospatial_lon_max', lon),
        ('geospatial_lon_units','degrees'),
        ('geospatial_vertical_min', 0),
        ('geospatial_vertical_max', 0),
        ('geospatial_vertical_units','meters above mean sea level'),
        ('distance_to_shore',dist2shories)
        #('sensor_type', sensor),
        #('sensor_description', series.sensor.description),
        #('sensor_manufacturer', series.sensor.manufacturer),
        #('sensor_part_number', series.sensor.part_number),
        #('sensor_serial_number', series.sensor.serial_number),
        #('sensor_install_date', series.sensor.install_date),
        #('sensor_height', series.sensor.elevation),
        #('sensor_sampling_period', series.sensor.samplingLength),
        #('sensor_sampling_rate', series.sensor.samplingRate),
        #('sensor_calibration_date', series.sensor.calibration_date),           
        #('sensor_history', platformHisto)
        ]
      #print metadata
      meta_field={} #dict containing the parameter fields
      for sensenoor in sensorList:
        fields_sensor={}
        for varvar in dicVariables:
          if dicVariables[varvar][2]==sensenoor:
        #print columndic[varvar],varvar
        columnIwant=zip(*table_data)[columndic[varvar]]
        data=numpy.ma.masked_array(columnIwant[startind:endind])
        data[data==-999]=numpy.ma.masked
        #print data
        if any(data[~data.mask]):
          #print 'find something in ',varvar, 'sensor ',sensenoor
          varclass=Variable(shortname=varvar, description=dicVariables[varvar][0],standardname=dicVariables[varvar][1],authority = cerbere.science.cfconvention.get_convention())
          tmp_field=Field(variable=varclass,dimensions=time_ordered,values=data,units=dicVariables[varvar][3])
          fields_sensor[varvar]=tmp_field
        #else:
          #print 'nothing in ',varvar, 'sensor ',sensenoor
        meta_field[sensenoor]=fields_sensor
      
      for sens in sensorList:
        if meta_field[sens]: #test if the sensor field is not empty
          #print 'writting data in ',sens
          #creation of the timeserie
          tmpTS = PointTimeSeries(
                  fields=meta_field[sens],
                  longitude=lon,
                  latitude=lat,
                  depth=depthies,
                  source=dataFile,
                  times=time_measures)
                  
                  
          #start = series._time[0]
          start = time_measures[0]
          #print 'start',start
          #stop = series._time[-1]
          stop = time_measures[-1]
          #print 'stop',stop
          if lat < 0:
          latstr = "%.2fS" % -lat
          else:
          latstr = "%.2fN" % lat
          if lon < 0:
          lonstr = "%.2fW" % -lon
          else:
          lonstr = "%.2fE" % lon


          monthDir =  os.path.join (globwaveOutDir, sens, str(start.year).strip(),"%02d" % start.month)
          if not os.path.exists(monthDir):
          os.makedirs(monthDir)
          filename = "%s/WMO%s_%s_%s_Lat_%s_Lon_%s.nc" % \
            (monthDir,buoyname,#buoy.WMOId,
              start.strftime("%Y%m%dT%H%M"),
              stop.strftime("%Y%m%dT%H%M"),
              latstr,lonstr )
          #if buoy.WMOId == '':
          #id = buoy.providerId
          #naming ="SHOM"
          #else:
          #id =  buoy.WMOId
          
          print sens,'outFile :',filename.split('/')[-1], '\n'
          if os.path.exists(filename):
                  os.remove(filename)
          ts_ncf = NCFile(url=filename, ncformat='NETCDF4', mode=WRITE_NEW)
          #ts_wind.save(ts_wind_ncf, attrs=metadata)
          tmpTS.save(ts_ncf)
          #startind=endind+1
    if i<len(breakList):
      startind=breakList[i]


      return
            
        
#process_history.py piece of code added      
#test /home/cercache/tools/environments/scientific_toolbox_cloudphys_precise/bin/python /home/cercache/users/agrouaze/git/globwave/src/insitu/ecmwfmooredbuoy.py --inputdir /home/cercache/project/globwave/data/provider/ecmwf/2012/raw_hourly_data_2012 --outputdir /home/cercache/users/agrouaze/data/test_globe_wave_convert/

# or
#/home/cercache/users/agrouaze/temporaire/raw_hourly_data_201307_begining0
if __name__ == "__main__":
    if len(sys.argv) < 5 or sys.argv[1] != '--inputdir' or sys.argv[3] != '--outputdir':
        print "Wrong program call : python process_history.py --inputdir <DIR> --outputdir <DIR> --sensor <sensor>"
        #print "example : python process_history.py --inputdir \"/home/data/*\" --outputdir /home/data/out --sensor wave"
        print "example : python process_history.py --inputdir \"/home/data/*\" --outputdir /home/data/out"
        #print "sensor is : wave | sea_temperature | wind | air | pressure | humidity | current | precipitation | salinity"
        exit()
    
    inputDir = sys.argv[2].strip("'")
    globwaveOutDir = sys.argv[4].strip("'")
    #sensorArg = sys.argv[6].strip()
    inFileList = glob.glob(inputDir)
    inFileList.sort()
    
    for inFile in inFileList:
        #for sensorArg in sensorList:
    #LOG_FILENAME = './log/%s.log' % ( sensorArg )
        LOG_FILENAME='/home/cercache/users/agrouaze/temporaire/logecnmwf.log'
        logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG, filemode='w', format='%(asctime)s %(levelname)s %(message)s')
    
        buoy = None
        #buoy = insitu.buoynetworks.ecmwf.ecmwfmooredbuoy.ECMWFMooredBuoy()
        buoy = ECMWFMooredBuoy()
        #inFile= os.path.join(inputDir,ifile)
        print "infile :", inFile.split('/')[-1], '\n'
        timeseries = buoy.readTimeSeries(inFile)