#!/bin/bash -e
#agrouaze
#may 2014
MONTHS=(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec)
source /home/cercache/tools/environments/scientific_toolbox_cloudphys_precise/bin/activate.sh
script='/home/losafe/users/agrouaze/git/globwave/src/insitu/cdipmooredbuoy.py'
outputdir='/home/cercache/project/globwave/data/globwave/cdip/'
year=$(date +%Y)
rep='/home/cercache/project/globwave/data/provider/cdip/'$year'/'
now=$(date)
echo $now
month=$(date +%m)
echo $month
month=`expr $month - 3`
echo $month
monthC=${MONTHS[$month]}
#monthC=${MONTHS[${month:1:1}]}
echo $monthC
monthC=$(echo $monthC | tr '[:upper:]' '[:lower:]')
listplat=$(find $rep -maxdepth 1 -mindepth 1 -type d )
for plat in $listplat ; do
	echo 'plat'$plat
	repc=$plat'/'$monthC'/01/'
	if [[ -d $repc ]] ; then
		echo 'call to CDIP globwave converter! treated month:' $monthC 'directory: '$repc' output dir: '$outputdir
    	python $script --inputdir $repc --outputdir $outputdir  
	else
		echo $repc' doesnt exist'
	fi
done
now=$( date )
echo 'convertion finished at '$now

