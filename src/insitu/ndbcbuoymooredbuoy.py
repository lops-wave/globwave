###########################################
#
# NDBC buoy conversion to globwave product
#
# History : 
#    20140703 CPR Correction stheta calculation. r1 and r2 lines added.
#    20151020 AGR Refactorization of the code. Correction of bug with anemometer height. Args parsing. Suppression of old librairies
#    20160518 CPR Correction bug glob argument2.
#
###########################################
import sys
# sys.path.insert(0,'/home/losafe/users/agrouaze/git/cerbere_gitlab/cerbere')
sys.path.insert(0,'/home/satwave/sources_en_exploitation/cerbere')
sys.path.insert(0,'/home/satwave/sources_en_exploitation/cerform')
sys.path.insert(0,'/home/satwave/sources_en_exploitation/ceraux')
# sys.path.insert(0,'/home/losafe/users/agrouaze/git/cerform')
# sys.path.insert(0,'/home/losafe/users/agrouaze/git/ceraux')
import os
import logging
import math
import collections
import copy
import time
import gc
import pdb
from collections import OrderedDict

import numpy
import netCDF4 as netcdf

from cerbere.datamodel.pointtimeseries import PointTimeSeries
from cerbere.datamodel.variable import Variable
from cerbere.datamodel.field import Field, QCLevel, QCDetail
# import cerbere.science.cfconvention
# sys.path.insert(0,'/home/losafe/users/agrouaze/git/cerbere-utils/cerbereutils/science')
# from cfconvention import get_convention,get_standardname
from cerform.cfconvention import get_convention,get_standardname
# import cerbereutils.science.wave
from cerform.wave import r1r2_to_sth1sth2
from shared_convert_methods import treshold_to_create_new_file,Define_output_filename
# import cerbere.science.wave
from cerbere.mapper.abstractmapper import WRITE_NEW
from cerbere.mapper.ncfile import NCFile
from cerbere.geo.bathymetry import Bathymetry
# from cerbere.geo.landmask import LandMask
# from cerbereutils.ancillary.landmask import Landmask
from ceraux.landmask import Landmask
# print cerbereutils.ancillary.landmask.__file__
processor_version = '2.3'
UNWANTED = ['time',
            'latitude',
            'longitude',
            'frequency',
            'water_level',
            'visibility',
            'rhq',
            'gamma2',
            'gamma3',
            'phih',
            'c11m'
            ]

CERSAT_NAMES = {
    'wind_spd': 'wind_speed',
    'wind_dir': 'wind_direction',
    'air_temperature': 'air_temperature',
    'dewpt_temperature': 'dew_point_temperature',
    'hourly_max_gust': 'hourly_max_gust',
    'direction_of_hourly_max_gust': 'direction_of_hourly_max_gust',
    'continuous_wind_speed': 'continuous_wind_speed',
    'continuous_wind_direction': 'continuous_wind_direction',
    'gust': 'wind_speed_of_gust',
    'air_pressure_at_sea_level': 'air_pressure_at_sea_level',
    'air_pressure': 'air_pressure',
    'sea_surface_temperature': 'sea_surface_temperature',
    'wave_height': 'significant_wave_height',
    'dominant_wpd': 'dominant_wave_period',
    'dominant_dir': 'dominant_wave_direction',
    'average_wpd': 'average_wave_period',
    'mean_wave_dir': 'theta1',
    'spectral_wave_density': 'spectral_wave_density',
    'relative_humidity': 'relative_humidity',
    'principal_wave_dir': 'theta2',
    'wave_spectrum_r1': 'r1',
    'wave_spectrum_r2': 'r2',
    }

UNITS = {'meters/second': 'm s-1',
         'degrees_true': 'angular_degree',
         'degree_Celsius': 'celsius',
         'hPa': 'Pa',
         'meters': 'm',
         'seconds': 's',
         '(meter * meter)/Hz': 'm2 Hz-1'
         }

INSTRUMENTS = {
               'wind_dir': 'anemometer',
               'wind_spd': 'anemometer',
               'gust': 'anemometer',
               'wave_height': 'wave_sensor',
               'dominant_wpd': 'wave_sensor',
               'average_wpd': 'wave_sensor',
               'mean_wave_dir': 'wave_sensor',
               'principal_wave_dir': 'wave_sensor',
               'spectral_wave_density': 'wave_sensor',
               'wave_spectrum_r1': 'wave_sensor',
               'wave_spectrum_r2': 'wave_sensor',

               'air_pressure': 'barometer',
               'air_temperature': 'air_temperature_sensor',
               'sea_surface_temperature': 'ocean_temperature_sensor',
               'dewpt_temperature': 'air_temperature_sensor',
               }

FREQDIM = {'nondirectional': 'frequency',
           'directional': 'frequency'}
FREQVARNAME = {
    'nondirectional': 'wave_frequency_spectrum_central_frequency',\
    'directional': 'wave_directional_spectrum_central_frequency'}
FREQRANGENAME = {
    'nondirectional': 'wave_frequency_spectrum_frequency_range',\
    'directional': 'wave_directional_spectrum_frequency_range'}


class NdbcMooredBuoy(object):
    """Class for decoding and converting a Netcdf file
    from NDBC
    """
    _bathymetry = None
    @classmethod
    def __init__(self,input_file_path,outdir):
        self.provider_file_path = input_file_path
        self.provider_file_handler = netcdf.Dataset(input_file_path, 'r')
        self.groups = []
        self.lon = numpy.NaN
        self.lat = numpy.NaN
        self.anemo_height_metadata = None
        self.globalmetadata = {}
        self.outputdir = outdir
#         self.contain_spectral_data = False
#         self.contain_spectral_directional_data = False
        self.masked_times = []
        logging.debug('successfully init the NdbcMooredBuoy class')

    @classmethod
    def get_cersatname(cls, vname):
        return CERSAT_NAMES[vname]

    @classmethod
    def get_instrument(cls, vname):
        return INSTRUMENTS[vname]

    @classmethod
    def get_units(cls, units):
        if units in UNITS:
            return UNITS[units]
        else:
            raise Exception("Unknown units", units)
    
    @classmethod
    def set_global_metadata(self):
        """
        # Global metadata
        # ---------------
        """
        globalmetadata = {}
        for attr in self.provider_file_handler.ncattrs():
            globalmetadata[attr] = self.provider_file_handler.getncattr(attr)
        globalmetadata.pop('quality')
        globalmetadata.pop('location')
        globalmetadata.pop('url')
        globalmetadata.pop('conventions')
        
        globalmetadata['institution'] = "National Data Buoy Center"
        globalmetadata['institution_abbreviation'] = "NDBC"
        globalmetadata['naming_authority'] = "WMO"
        globalmetadata['summary'] = "The Coastal-Marine Automated Network (C-MAN) was established by NDBC for the NWS in the early 1980\'s.\
         Approximately 50 stations make up the C-MAN and have been installed on lighthouses, at capes and beaches,\
          on near shore islands, and on offshore platforms.\
          Over 100 moored weather buoys have been deployed in U.S. coastal and offshore waters.\
          C-MAN and weather buoy data typically include barometric pressure, wind direction, speed and gust,\
          and air temperature; however, some C-MAN stations are equipped to also measure sea water temperature, waves,\
          and relative humidity. Weather buoys also measure wave energy spectra from which significant wave height,\
          dominant wave period, and average wave period are derived. The direction of wave propagation is also measured on many moored weather buoys."
        globalmetadata['title'] = "Meteorological and Oceanographic Data Collected from the National Data Buoy Center\'s\
         Coastal Marine Automated Network and Weather Buoys, collected for GlobWave project"
        globalmetadata['cdm_feature_type'] = "station"
        globalmetadata['scientific_project'] = "GlobWave"
        globalmetadata['acknowledgement'] = "We are grateful to NDBC for providing these data to GlobWave project"
        globalmetadata['standard_name_vocabulary'] = "CF-1.6"
        globalmetadata['restrictions'] = "Restricted to Ifremer and GlobWave usage"
        globalmetadata['processing_level'] = "0"
        globalmetadata['processor_version'] = processor_version
        globalmetadata['history'] = "1.0 : Processing to GlobWave netCDF format"
        globalmetadata['publisher_name'] = "Ifremer/Cersat"
        globalmetadata['publisher_url'] = "http://cersat.ifremer.fr"
        globalmetadata['publisher_email'] = "jfpiolle@ifremer.fr"
        globalmetadata['creator_name'] = "Jean-Francois Piolle"
        globalmetadata['creator_url'] = "http://cersat.ifremer.fr"
        globalmetadata['creator_email'] = "jfpiolle@ifremer.fr"
        globalmetadata['data_source'] = os.path.basename(self.provider_file_path)
        if self.provider_file_handler.station.isdigit():
            globalmetadata['wmo_id'] = self.provider_file_handler.station
        else:
            globalmetadata['wmo_id'] = ''
        globalmetadata['id'] = str(self.provider_file_handler.station)
        globalmetadata['site_elevation'] = ''
        globalmetadata['station_name'] = globalmetadata['comment']
        globalmetadata.pop('comment')
        globalmetadata.pop('station')
        
        if not 'sea_floor_depth_below_sea_level' in globalmetadata:
            if NdbcMooredBuoy._bathymetry == None:
                NdbcMooredBuoy._bathymetry = Bathymetry(filename='/home/cersat5/application_data/globwawe/static_file/bathy/GridOne.nc')
#             floorDepth = -NdbcMooredBuoy._bathymetry.getDepth(lon, lat)
                floorDepth = -NdbcMooredBuoy._bathymetry.get_depth(self.lon,self.lat)
            else:
                floorDepth = -NdbcMooredBuoy._bathymetry.get_depth(self.lon,self.lat)
        else:
            floorDepth = self.provider_file_handler.sea_floor_depth_below_sea_level
            floorDepth = float(floorDepth[0:-1])
        logging.debug('floordepth type : %s %s',type(floorDepth),floorDepth)
        globalmetadata['sea_floor_depth_below_sea_level'] = math.fabs(floorDepth)
        globalmetadata['distance_to_shore_in_meters'] = self.dist
        globalmetadata['nominal_latitude'] = self.lat
        globalmetadata['nominal_longitude'] = self.lon
        self.globalmetadata = globalmetadata
    
    @classmethod
    def get_anemometer_height(self,buoy_id,year=None,month=None):
        """
        read anemometer height metadata in a csv file
        Args:
            buoy_id (str): WMO or id in upper case
        """
        logging.debug('anemo | year %s',year)
        logging.debug('anemo |month %s',month)
        logging.debug('anemo | buoy_id %s',buoy_id)
        if 'anemometer_height' not in self.globalmetadata:
            
        
            if self.anemo_height_metadata is None:
                fid = open('/home/cercache/project/globwave/prog/GLOBWAVE/src/insitu/buoynetworks/ndbc/buoytabAnHgt.dat','r')
                tmp = fid.readlines()
                meta = {}
                for tx,tt in enumerate(tmp):
                    buoy_id = tt.split(' ')[0]
                    month = tt.split(' ')[4]
                    year = tt.split(' ')[3]
                    if tt.split(' ')[5] != 'NaN\n':
                        vv = tt.split(' ')[6].strip('\n')
#                         logging.debug('vv %s',vv)
                        val = float(vv)
                        if buoy_id not in meta:
                            meta[buoy_id] = {}
                        if year not in meta[buoy_id]:
                            meta[buoy_id][year] = {}
                        meta[buoy_id][year][month] = val
                        
                self.anemo_height_metadata = meta
                fid.close()
            #read the dico
            res = 'unknown'
            if buoy_id in self.anemo_height_metadata:
                if year is not None and month is not None:
                    if year in self.anemo_height_metadata[buoy_id]:
                        if month in self.anemo_height_metadata[buoy_id][year]:
                            res = self.anemo_height_metadata[buoy_id][year][month]
                else:
                    lasty = self.anemo_height_metadata[buoy_id].keys()[-1]
                    lastm = self.anemo_height_metadata[buoy_id][lasty].keys()[-1]
                    res = self.anemo_height_metadata[buoy_id][lasty][lastm]
            #update
            self.globalmetadata['anemometer_height'] = res
            
    
    
    @classmethod
    def get_time_intervals(self,times,instr):
        """
        # determine data intervals
        # ---------------------
        Args:
            times (numpy array): numeric date of each observation
            instr (str): instrument
        """
        # time mask to determine period without any data for this
        tmask = None
        for v in self.groups[instr]:
            if len(self.provider_file_handler.variables[v].dimensions) == 1:
                if tmask == None:
                    tmask = numpy.ma.getmask(self.provider_file_handler.variables[v][:])
                else:
                    tmask = numpy.ma.make_mask(
                        tmask & numpy.ma.getmask(self.provider_file_handler.variables[v][:])
                        )
        masked_times = numpy.ma.array(times, copy=True, mask=tmask)
        intervals = []
        i0 = 0
        i1 = 1
        for i, t in enumerate(masked_times):
            if masked_times.mask[i0]:
                i0 = i
                i1 = i + 1
            elif masked_times.mask[i]:
                pass
            elif (t - masked_times[i1 - 1]).total_seconds() > treshold_to_create_new_file\
                    or t.month != masked_times[i1 - 1].month:
                intervals.append((i0, i1))
                i0 = i
                i1 = i + 1
            else:
                i1 = i + 1
        # last interval or no interval
        intervals.append( (i0,i1) )
        self.masked_times = masked_times
        return intervals,masked_times
    
    @classmethod
    def create_variable(self,variable_name,intstart,intend,qc_levels,qc_details,ofields,ovars,dims):
        # Create Variables and fields
        # ---------------------------
        contain_spectral_data = False
        if variable_name == 'wave_spectrum_r1' or variable_name == 'wave_spectrum_r2':
            contain_spectral_data = True
            values_raw = self.provider_file_handler.variables[variable_name][intstart:intend,:,0,0]
            if isinstance(values_raw,numpy.ma.MaskedArray)==False:
                values_raw = numpy.ma.array(values_raw)
            if (values_raw.mask==False).any():
#             if not isinstance(,numpy.ma.MaskedArray)\
#                 or self.provider_file_handler.variables[variable_name][intstart:intend,:,0,0].count() != 0:
#                 if stheta1 is None: #agrouaze removed since it appears to be useless
                stheta1 = numpy.ma.masked_all_like(self.provider_file_handler.variables['wave_spectrum_r1'][intstart:intend,:,0,0])
                stheta2 = numpy.ma.masked_all_like(self.provider_file_handler.variables['wave_spectrum_r2'][intstart:intend,:,0,0])
                theta1 = numpy.ma.asarray(self.provider_file_handler.variables['mean_wave_dir'][intstart:intend,:,0,0],
                                          dtype='float64')
                theta2 = numpy.ma.asarray(self.provider_file_handler.variables['principal_wave_dir'][intstart:intend,:,0,0],
                                          dtype='float64')
                r1 = numpy.ma.asarray(self.provider_file_handler.variables['wave_spectrum_r1'][intstart:intend,:,0,0],
                                          dtype='float64')
                r2 = numpy.ma.asarray(self.provider_file_handler.variables['wave_spectrum_r2'][intstart:intend,:,0,0],
                                          dtype='float64')
                ts, ss = theta1.shape
                for t in range(ts):
                    for s in range(ss):                                            
                        theta1[t,s], theta2[t,s], stheta1[t,s], stheta2[t,s]\
                            = r1r2_to_sth1sth2(
                                    theta1[t,s],
                                    theta2[t,s],
                                    r1[t,s],
                                    r2[t,s]
                                    )
                if variable_name == 'wave_spectrum_r1':
                    vname = 'stheta1'
                    vals = stheta1
                    descrstr = 'directional spread of the entire sea state in degrees'
                else:
                    vname = 'stheta2'
                    vals = stheta2
                    descrstr = 'directional spread of waves at the peak frequency in degrees'
                varobj = Variable(
                                shortname=vname,
                                description=descrstr,
    #                                                 authority=cerbere.science.cfconvention.get_convention(),
                                authority=get_convention(),
                                standardname=get_standardname(vname)
                                )
                ovars[variable_name] = varobj
                fieldobj = Field(
                                varobj,
                                dims,
                                values=vals,
                                units='angular_degree',
                                qc_levels=qc_levels,
                                qc_details=qc_details)
                ofields[variable_name] = fieldobj
        else:
            vname = NdbcMooredBuoy.get_cersatname(variable_name)
            # attributes
            if vname == 'stheta1':
                descrstr = 'mean direction of the entire sea state in degrees'
            elif vname == 'stheta2':
                descrstr = 'mean direction of waves at the peak frequency in degrees'
            else:
                descrstr = self.provider_file_handler.variables[variable_name].long_name.lower()
            varobj = Variable(
                            shortname=vname,
                            description=descrstr,
                            authority=get_convention(),
                            standardname=get_standardname(vname))
            ovars[variable_name] = varobj
            if 'frequency' in self.provider_file_handler.variables[variable_name].dimensions:
                values = self.provider_file_handler.variables[variable_name][intstart:intend, :, 0, 0]
            else:
                values = self.provider_file_handler.variables[variable_name][intstart:intend, 0, 0]

            #print values.shape, v
            if self.provider_file_handler.variables[variable_name].units == 'hPa':
                values = values * 100.
            if 'units' in self.provider_file_handler.variables[variable_name].ncattrs():
                units = NdbcMooredBuoy.get_units(self.provider_file_handler.variables[variable_name].units)
            else:
                units = None
            if not isinstance(values, numpy.ma.MaskedArray)\
                    or values.count() != 0:
                fieldobj = Field(
                            ovars[variable_name],
                            dims,
                            values=values,
                            units=units,
                            qc_levels=qc_levels,
                            qc_details=qc_details
                            )
                ofields[variable_name] = fieldobj
        return ofields,contain_spectral_data
    
    @classmethod
    def save_time_serie(self,sensor,intstart,intend,ofields,contain_spectral_data):
        """
        put the content of the TimeSerie objet into a netcdf file
        """
        has_any_data = False
        depth = 0 # No information about instrument depth (0 by default)
        for var in self.groups[sensor]:
            if var in ofields:
                has_any_data = True
        if has_any_data:
            ts = PointTimeSeries(
                    fields=ofields,
                    longitude=self.lon,
                    latitude=self.lat,
                    depth=depth,
                    times=(self.provider_file_handler.variables['time'][intstart:intend],
                           self.provider_file_handler.variables['time'].units.replace(' UTC','')))
            start_dt = netcdf.num2date(self.provider_file_handler.variables['time'][intstart],self.provider_file_handler.variables['time'].units)
            end_dt = netcdf.num2date(self.provider_file_handler.variables['time'][intend-1],self.provider_file_handler.variables['time'].units)
            ofname = Define_output_filename(instr=sensor,lon=self.lon,lat=self.lat,intstart=start_dt,intend=end_dt,globalmetadata=self.globalmetadata,spectral=contain_spectral_data,outputdir=self.outputdir)
            if os.path.exists(ofname):
                os.remove(ofname)
            tsncf = NCFile(url=ofname, ncformat='NETCDF4', mode=WRITE_NEW)
            logging.debug("Write file %s" % ofname)
            ts.save(tsncf, attrs=self.globalmetadata)
            gc.collect()
            tsncf.close()
            logging.debug("output file %s" % ofname)
    
    @classmethod
    def determine_sensor_groups(self):
        """
        sort all variables into sensor groups
        """
        groups = {}
        for v in self.provider_file_handler.variables.keys():
            if not v in UNWANTED:
                instr = NdbcMooredBuoy.get_instrument(v)
                if instr in groups:
                    groups[instr].append(v)
                else:
                    groups[instr] = [v]
        for sens in groups.keys():
            logging.debug('sensor: %s',sens)
            for varvar in groups[sens]:
                logging.debug('\t\t variable: %s',varvar)
        self.groups = groups
        
    @classmethod
    def get_geolocation_info(self):
        # get geolocation
        times = netcdf.num2date(
                        self.provider_file_handler.variables['time'][:],
                        self.provider_file_handler.variables['time'].units.replace(' UTC', '')
                        )
        if len(self.provider_file_handler.dimensions['latitude']) != 1:
            raise Exception('latitude dimension is > 1')
        if len(self.provider_file_handler.dimensions['longitude']) != 1:
            raise Exception('longitude dimension is > 1')
        lat = self.provider_file_handler.variables['latitude'][0]
        lon = self.provider_file_handler.variables['longitude'][0]
        LD = Landmask('/home/losafe/users/agrouaze/git/cerbere/cerbere/geo/resources/NAVO-lsmask-world8-var.dist5.5.nc')
        self.dist = LD.getDistanceToShore(lat,lon)
#         dist = int(LandMask.getDistanceToShore(lat, lon,filename='/home/losafe/users/agrouaze/git/cerbere/cerbere/geo/resources/NAVO-lsmask-world8-var.dist5.5.nc'))
        self.lon = lon
        self.lat = lat
        self.times = times
    
    @classmethod
    def prepare_variables_and_dimensions(self,instr,variable_name,intend,intstart,ofields):
        """
        create empty matrix for a given variable plus qc_details and qc_levels
        and prepare dimensions
        """
        nbdata = intend - intstart
        if not 'units' in self.provider_file_handler.variables[variable_name].ncattrs()\
                and variable_name != 'wave_spectrum_r1'\
                and variable_name != 'wave_spectrum_r2':
            raise Exception("Missing units : ", instr, variable_name)
        self.provider_file_handler.variables[variable_name].set_auto_maskandscale(True)
        # build quality fields
        vals = numpy.ma.empty((intend - intstart,), dtype='i1')
        vals[:] = 4
        #print vals, (intend-intstart,), vals.shape
        dims = collections.OrderedDict()
        dims['time'] = nbdata
        vals.mask = numpy.ma.getmask(
                        self.masked_times[intstart:intend]
                        )
        qc_levels = QCLevel(
            values=vals,
            dimensions=copy.copy(dims),
            levels=numpy.array([0,1,2,3,4], dtype='i1'),
            meanings="Unknown Unprocessed Bad Suspect Good"
            )
        vals = numpy.ma.empty((intend - intstart,), dtype='i2')
        vals[:] = 0
        vals.mask = numpy.ma.getmask(
                        self.masked_times[intstart:intend]
                        )
        qc_details = QCDetail(
            values=vals,
            dimensions=copy.copy(dims),
            mask=numpy.array([1], dtype='i1'),
            meanings='flagged_by_provider'
            )
        if 'frequency' in self.provider_file_handler.variables[variable_name].dimensions:
            wavefreq = 'frequency'
        else:
            wavefreq = None
        if wavefreq is not None:
            if 'wave_spectrum_r1' in  self.provider_file_handler.variables:
                spectrumtype = 'directional'
            else:
                spectrumtype = 'nondirectional'
                    
            dims[ FREQDIM[spectrumtype] ]\
                    = len(self.provider_file_handler.variables[wavefreq][:])
            # definition of spectrum bands
            if not wavefreq in ofields:
                varobj = Variable(
                    shortname=FREQVARNAME[spectrumtype],
                    description='central frequency of bin'
                    )
                fieldobj = Field(
                    varobj,
                    OrderedDict([(
                            FREQDIM[spectrumtype],
                            len(self.provider_file_handler.variables[wavefreq][:]))
                            ]),
                    values=self.provider_file_handler.variables[wavefreq][:],
                    units=self.provider_file_handler.variables[wavefreq].units
                    )
                ofields[FREQVARNAME[spectrumtype]] = fieldobj
        return qc_levels,qc_details,dims,ofields
    
    def convert2globwave(self):
        """process a NDBC file to GlobWave format"""
        logging.info("Processing %s", self.provider_file_path)
        self.get_geolocation_info()
        # determine instrument groups
        self.determine_sensor_groups()
        self.set_global_metadata()
        # process output files instrument by instrument
        for instr in self.groups:
            intervals,masked_times = self.get_time_intervals(self.times,instr)
            # loop on time intervals
            # ---------------------
            for interval in intervals:
                intstart, intend = interval
                self.get_anemometer_height(self.globalmetadata['wmo_id'],year=masked_times[intstart].strftime('%Y'),month=masked_times[intstart].strftime('%m'))
                # Create Fields
                # -------------
                ovars = OrderedDict()
                ofields = OrderedDict()
#                 stheta1 = None
                # Distance to the shore is no more a variable but a global attribute
#                 varobj = Variable(
#                         shortname='distance_to_shore',
#                         description='distance to closest land area (shore or island)'
#                         )
#                 fieldobj = Field(
#                         varobj,
#                         collections.OrderedDict([('station', 1)]),
#                         values=numpy.array(dist, dtype='i4'),
#                         units='m',
#                         )
#                 ofields['distance_to_shore'] = fieldobj
                if len(self.groups[instr]) > 0:
                    for v in self.groups[instr]:
                        qc_levels,qc_details,dims,ofields = self.prepare_variables_and_dimensions(instr,v,intend,intstart,ofields)
                        ofields,contain_spectral_data = self.create_variable(v,intstart,intend,qc_levels,qc_details,ofields,ovars,dims)
                    # Create Time Series
                    # ------------------
                    self.save_time_serie(instr,intstart,intend,ofields,contain_spectral_data)
        self.provider_file_handler.close()
    convert2globwave=classmethod(convert2globwave)


if __name__ == "__main__":

    # EXPLOIT version :
    # USAGE :  vpython ndbcbuoymooredbuoy.py outputDir /home/cercache/project/globwave/data/provider/ndbc/meteo/nrt/51003h9999.nc
    # USAGE : python ndbcbuoymooredbuoy.py -o outputDir -i /home/cercache/project/globwave/data/provider/ndbc/meteo/nrt/51003h9999.nc
    import glob
    import sys
    start = time.time()
    root = logging.getLogger()
    if root.handlers:
        for handler in root.handlers:
            root.removeHandler(handler)
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option("-v","--verbose",
                      action='store_true',default=False,
                      help="verbose mode [default is quiet]")
    parser.add_option("-o","--output_dir",
                      action='store',
                      dest="output_dir",
                      help="output directory [mandatory]")
    parser.add_option("-i","--input_filename",
                      action='store',
                      dest='input_filename',
                      help="path of the input file to be converted (can be a directory) [mandatory]")
    (options, args) = parser.parse_args()
    if options.verbose:
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(levelname)-5s %(message)s',
                            datefmt='%d/%m/%Y %I:%M:%S')
    else:
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s %(levelname)-5s %(message)s',
                            datefmt='%d/%m/%Y %I:%M:%S')
    if options.output_dir is None:
        raise Exception('missing -o option')
    if options.input_filename is None:
        raise Exception('missing -i option')
    outdir = options.output_dir
    argument2 = options.input_filename
    logging.debug('argument#2: %s',argument2)
    if os.path.isdir(argument2):
        pattern = os.path.join(argument2,'*nc')
        logging.info('pattern to find the input files : %s',pattern)
        files = glob.glob(pattern)
        logging.info('number of files to process %s',len(files))
        for f in files:
            logging.info("Processing %s", f)
            buoy = NdbcMooredBuoy(f,outdir)
            buoy.convert2globwave()
            gc.collect()
    else:
        buoy = NdbcMooredBuoy(argument2,outdir)
        buoy.convert2globwave()
        gc.collect()
    elapsed = '%3.0f'%(time.time()-start)
    logging.info('end of the run for %s in %s seconds',__file__,elapsed)
 
