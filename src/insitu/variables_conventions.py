# -*- coding: utf-8 -*-
'''
::GLOBEWAVE IFREMER:: convention variable dictionnary (inspired from
 iowaga previmer WW3 CDOCO NCEP conventions) to be merged with xml file from Wenseslaz)
:author: Antoine GROUAZEL antoine.grouazel@ifremer.fr
:source: https://forge.ifremer.fr/plugins/mediawiki/wiki/ww3/index.php/En:parametres_en_sortie
:creation: 2014 feb
:purpose: gives the standard names,input_var_name,units,sensor for a given name variable
:context: used at LOS-CERSAT IFREMER for ESA Projects and operational file production (Pathfinders acidification,ocean-heatflux, and globwave projects)
:rules:
#	gather the param and hourly_param or average_param or param_at_XXX because the precision is given by the dimension
#	it is not true for maximum_param /dominant /average 
#	synonyms must be lower case=> obsolet
#	swell is no longer distinguished from wave
#   having a synonym is not mandatory
#   geolocation variable are not describe in this dictionnary


:python-usage:
 GLOBEWAVE_input_var_name,longname,description,sensor,units = VarConventions('WSPD')
'''
import logging
import time

DICTIONARY_VERSION = '0.0.1'
# import wave
#common words
#sensor designate more the thematic of the data than the specific sensor where come from the data
meteo_sensor = 'meteo_sensors' #to replace anemometer air_humidity_sensor air_temperature_sensor and barometer radiative_sensor
wind_sensor = 'anemometer'
wave_sensor = 'wave_sensor'
# humidity_sensor = 'air_humidity_sensor'
# temperature_air = 'air_temperature_sensor'
temp_sea = 'ocean_temperature_sensor'
salinity = 'ocean_salinity_sensor'
# no_sensor = 'no_sensor'
# derived_param = 'derived_parameters'
tide = 'tide_gage'
chimie = 'chemical_sensor'
turbi = 'turbidimeters'
# radiative = 'radiative_sensor'
current_sensor = 'current_meters'
sensors = [wind_sensor,meteo_sensor,wave_sensor,temp_sea,salinity,tide,chimie,turbi,current_sensor]
# chlorophyle = 'chlorometer'
# fluoro = 'fluorometer'
# oxygen = 'dissolved_oxygen_sensor'
# radioactive = 'radiometric_sensor'

freqDim = {'nondirectional':'frequency','directional':'frequency'}
freqVarName = {'nondirectional':'wave_frequency_spectrum_central_frequency',\
               'directional':'wave_directional_spectrum_central_frequency'}
freqRangeName = {'nondirectional':'wave_frequency_spectrum_frequency_range',\
                 'directional':'wave_directional_spectrum_frequency_range'}
units = {
    'celsius' : 'degree Celsius',
    'micromole' : 'umol',
    'no_unit' : 'unitless',
    'meter_per_second' : 'm s-1',
    'meter' : 'm',
    'watt_per_square_meter' : 'W m-2',
    'second' : 's',
    'degree_clockwise' : 'degree clockwise relative to true North',
    'pascal':'Pa'
}
Ranges = {#min,max ever reached in the whole  Earth planet history
          'WSPD':(0,120),
          'VAVH':(0,30),
          'VHM0':(0,30),
          }
#header : acronym:GLOBEWAVE_name,long_name,description,sensor,units
#correspondance cerbere:
#----------------------
#acronym=>short_name cerbere
#long_name = std_name cerbere (dnas l ideal pas obliger) (dans la pratique le long name _>description cest une implementatin de cerbere on y peux rien
#     et std_name est rempli avec None dans 90%des cas)
#description= descr cerbere
#
RefVars = {
    #'LONGITUDE':('LONGITUDE','degree_east'), #longitude is not in this table since it is processed by cerbere separatly
    #wind###############################
    'WSPD':('wind_speed','wind_speed','wind speed measured',wind_sensor,units['meter_per_second']),
    'WDIR':('wind_direction','average_wind_direction','direction of the wind averaged on a time period',wind_sensor,units['degree_clockwise']),
    'SCALARWSPD':('scalar_wind_speed','scalar_wind_speed','norm of the vector U + V (sqrt(U2+V2))',wind_sensor,units['meter_per_second']),
    'GSPD':('wind_speed_of_gust','wind_speed_gust','wind speed gust',wind_sensor,units['meter_per_second']), #wind_speed_of_gust
    'GDIR':('wind_from_direction_of_gust','wind_from_direction_of_gust','gust wind from direction relative true north',wind_sensor,units['degree_clockwise']),
    'MGST':('maximum_wind_gust_speed','maximum_wind_gust_speed','maximum speed of wind gust',wind_sensor,units['meter_per_second']),
    'DMGST':('direction_maximum_wind_gust_speed','direction_maximum_wind_gust_speed','direction of maximum wind gust',wind_sensor,units['degree_clockwise']),
    'NORTHWS':('northward_wind','northward_wind','northward component of the wind',wind_sensor,units['meter_per_second']),
    'EASTWS':('eastward_wind','eastward_wind','eastward component of the wind',wind_sensor,units['meter_per_second']),
    'WIND_STRESS':('wind_stress','wind_stress','wind stress',wind_sensor,units['pascal']),
    'SURF_DOWN_N_STRESS':('surface_downward_northward_stress','surface_downward_northward_stress','surface downward northward stress',wind_sensor,units['pascal']),
    'SURF_DOWN_E_STRESS':('surface_downward_eastward_stress','surface_downward_eastward_stress','surface downward eastward stress',wind_sensor,units['pascal']),
    #WAVE ENERGY###################################################
    'THETA1':('theta1','mean wave direction','mean wave direction',wave_sensor,units['degree_clockwise']),
    'THETA2':('theta2','principal wave direction','principal wave direction',wave_sensor,units['degree_clockwise']),
    'STHETA1':('stheta1','directional spread of the entire sea state in degrees','directional spread of the entire sea state in degrees',wave_sensor,units['degree_clockwise']),
    'STHETA2':('stheta2','directional spread of waves at the peak frequency in degrees','directional spread of waves at the peak frequency in degrees',wave_sensor,units['degree_clockwise']),
    'A1':('A1','coef_a1','directional Fourier coefficient a1',wave_sensor,units['no_unit']),
    'B1':('B1','coef_b1','directional Fourier coefficient b1',wave_sensor,units['no_unit']),
    'A2':('A2','coef_a2','directional Fourier coefficient a2',wave_sensor,units['no_unit']),
    'B2':('B2','coef_b2','directional Fourier coefficient b2',wave_sensor,units['no_unit']),
    'EF':('sea_surface_variance_spectral_density','sea_surface_wave_variance_spectral_density','wave variance spectral density',wave_sensor,'m2.s'),
    'VEPK':('sea_surface_wave_peak_energy','sea_surface_wave_peak_energy','wave spectrum peak energy',wave_sensor,'m2.s'),#WAVE SPECTRUM PEAK ENERGY
    #WAVE FREQUENCY###################################################
    'CENTRALFREQ':('central_frequency','central_frequency','central frequency',wave_sensor,'Hz'),
    'BANDWIDTHFREQ':('band_width_frequency','bandwidth_frequency','bandwidth of the frequency bin',wave_sensor,'Hz'),
    'BINFRQRANGE':('bin_range_frequency','bin_range_frequency','frequency range of indiviudal bin',wave_sensor,'Hz'),
    'BINLOWFRQ':('bin_lower_frequency','bin_lower_frequency','lower frequency of indiviudal bin',wave_sensor,'Hz'),
    'LCF$':('low_frequency_cutoff','low_frequency_cutoff','Low frequency cut-off for wave spectra, calculated from the dispersion relation',wave_sensor,'Hz'),
#     'WAVE_SPECTRUM_R1':('r1',wave_sensor,'m2.s'), #dont know exactly what is it
#     'WAVE_SPECTRUM_R2':('r2',wave_sensor,'m2.s'),
    #'VAVH_hs':('significant_wave_height',wave_sensor),
    #WAVE HEIGHT###################################################
    'VAVH':('sea_surface_significant_wave_height','significant_wave_height','significant wave height',wave_sensor,units['meter']),#same globwave name but different description
    'VHM0':('spectral_significant_wave_height','spectral_significant_wave_height','spectral significant wave height',wave_sensor,units['meter']),
    'VTDH_H1D3':('wave_height_h1d3','wave_height_h1d3','highest one third average wave height',wave_sensor,units['meter']),  #Average Wave Height (highest one third) (wave_height_h1d3)
    'SWHT':('significant_swell_wave_height','significant_swell_wave_height','significant swell wave height',wave_sensor,units['meter']),
    #'VTDH':('significant_swell_wave_height',wave_sensor), #SIGNIFICANT WAVE HEIGHT (sea_surface_swell_wave_significant_height)
    'VZMX':('sea_surface_zero_crossing_maximum_wave_height','sea_surface_zero_crossing_maximum_wave_height','maxi zero crossing wave height',wave_sensor,units['meter']), # MAXI_ZERO_CROSSING_WAVE_HEIGHT (maximum_waveheight)
    'VZMD':('MAXIMUM_ZERO_DOWN_CROSSING_WAVE_HEIGHT_(HMAX)','MAXIMUM_ZERO_DOWN_CROSSING_WAVE_HEIGHT_(HMAX)','MAXIMUM_ZERO_DOWN_CROSSING_WAVE_HEIGHT_(HMAX)',wave_sensor,units['meter']),
    #'VDIR_th0':('alpha1',wave_sensor),   #Mean Wave Direction
    'VCMX':('sea_surface_crest_trough_maximum_wave_height','sea_surface_crest_trough_maximum_wave_height','max crest trough wave height',wave_sensor,units['meter']),#MAX CREST TROUGH WAVE HEIGHT
    'VMXL':('highest_crest_height','highest_crest_height','Height of the highest crest',wave_sensor,units['meter']),
    'VMNL':('deepest_trough_depth','deepest_trough_depth','Depth of the deepest trough',wave_sensor,'m'),
    'VGHS':('sea_surface_generic_significant_wave_height','sea_surface_generic_significant_wave_height','generic significant wave height (unknown estimator)',wave_sensor,units['meter']),
    'VH110':('sea_surface_wave_mean_height_of_highest_tenth','sea_surface_wave_mean_height_of_highest_tenth','Average height highest 1/10 wave (H1/10)',wave_sensor,units['meter']),
    'VHZA':('sea_surface_wave_mean_height','sea_surface_wave_mean_height','Average zero crossing wave height (Hzm)',wave_sensor,units['meter']),
    #WAVE PERIOD###################################################
#     'AVERAGE_WAVE_PERIOD':('average_wave_period','average_wave_period','average time between two waves peak',wave_sensor,units['second']),
    'VAVT':('sea_surface_zero_crossing_significant_wave_period','sea_surface_zero_crossing_significant_wave_period','aver. period highest 1/3 wave',wave_sensor,units['second']),
    'VTZA':('sea_surface_wave_zero_upcrossing_period','sea_surface_wave_zero_upcrossing_period','mean sea surface swell wave zero upcrossing period',wave_sensor,units['second']), # AVER ZERO CROSSING WAVE PERIOD  (sea_surface_swell_wave_zero_upcrossing_period)
#     'VTZA_TH1D3':('highest_one_third_sea_surface_swell_wave_zero_upcrossing_period','sea_surface_swell_wave_zero_upcrossing_period','highest one third average zero crosssing wave period',wave_sensor,units['meter']), #Aver. Zero Crossing Wave Period (highest one third)  (wave_period_th1d3)
#     'VTZA_TP':('maximum_wave_period','maximum_wave_period','wave period (at spectral maximum) ',wave_sensor,units['meter']),  # Wave Period (at spectral maximum)
#     'VTZA_TM10':('wave_period_at_first_spectral_moment','wave_period_at_first_spectral_moment','Wave Period (at first spectral moment)',wave_sensor,units['second']),
#     'VTZA_TM02':('wave_period_at_second_spectral_moment','wave_period_at_second_spectral_moment','Wave Period (at second spectral moment)',wave_sensor,units['second']),
#     'VTZA_TZ':('sea_surface_wave_zero_upcrossing_period','sea_surface_wave_zero_upcrossing_period','Sea Surface Wave Zero Upcrossing Period',wave_sensor,units['second']),
    'VTPK':('sea_surface_wave_period_at_spectral_density_maximum','sea_surface_wave_period_at_spectral_density_maximum','period of waves at energy peak',wave_sensor,units['second']), # WAVE_SPECTRUM_PEAK_PERIOD (peak_period)
    'SWPR':('sea_surface_swell_wave_period','sea_surface_swell_wave_period','period of swell',wave_sensor,units['second']),
    'VSMC':('sea_surface_wave_mean_period_spectral_second_frequency_moment','sea_surface_wave_mean_period_spectral_second_frequency_moment','spect. moment (0,2) wave period',wave_sensor,units['second']),#SPECT. MOMENT(0,2) WAVE PERIOD
    'VSMB':('sea_surface_wave_mean_period_spectral_first_frequency_moment','sea_surface_wave_mean_period_spectral_first_frequency_moment','spectral moments (0,1) wave period',wave_sensor,units['second']),
    'VSMA':('sea_surface_wave_mean_period_spectral_inverse_frequency_moment','sea_surface_wave_mean_period_spectral_inverse_frequency_moment','spectral moments (-1,0) wave period',wave_sensor,units['second']),
    'VGTA':('sea_surface_generic_averaged_wave_period','sea_surface_generic_averaged_wave_period','generic averaged wave period',wave_sensor,units['second']),
    'VT10':('sea_surface_zero_crossing_averaged_one_tenth_wave_period','sea_surface_zero_crossing_averaged_one_tenth_wave_period','aver. period highest 1/10 wave',wave_sensor,units['second']),
    'VT110':('sea_surface_wave_mean_period_of_highest_tenth','sea_surface_wave_mean_period_of_highest_tenth','Average period highest 1/10 wave (T1/10)',wave_sensor,units['second']),
    'VTMX':('sea_surface_zero_crossing_maximum_wave_period','sea_surface_zero_crossing_maximum_wave_period','maximum wave period',wave_sensor,units['second']),
    'VTM10':('sea_surface_wave_mean_period_from_variance_spectral_density_inverse_frequency_moment','sea_surface_wave_mean_period_from_variance_spectral_density_inverse_frequency_moment','Spectral moments (-1,0) wave period (Tm-10)',wave_sensor,units['second']),
    #WAVE DIRECTION###################################################
    'SWDR':('sea_surface_swell_wave_to_direction','sea_surface_swell_wave_to_direction','direction of the swell',wave_sensor,units['degree_clockwise']),
    'VMDR':('sea_surface_wave_from_mean_direction','sea_surface_wave_from_mean_direction','mean wave direction',wave_sensor,units['degree_clockwise']),
    'VDIR':('dominant_wave_direction','dominant_wave_direction','dominant direction from waves are coming ', wave_sensor,units['degree_clockwise']),
    'VPSP':('sea_surface_wave_directional_spreading_at_spectral_peak','sea_surface_wave_directional_spreading_at_spectral_peak','dir. spreading at wave peak',wave_sensor,units['degree_clockwise']),#DIR. SPREADING AT WAVE PEAK formerly dominant_wave_spreading
    'VPED':('sea_surface_wave_from_direction_at_spectral_peak','dominant_wave_direction','wave direction at energy peak',wave_sensor,units['degree_clockwise']),#WAVE SPECTRUM PEAK ENERGY DIR.
#     'VDIR_TH0':('average_wave_direction','average_wave_direction','average wave direction',wave_sensor,units['degree_clockwise']),
#     'WAVE_MEAN_DIRECTION':('wave_mean_direction','wave_mean_direction','wave mean direction',wave_sensor,units['degree_clockwise']), #WADR invente
    'TH1M':('sea_surface_wave_mean_direction_from_first_directional_moment','sea_surface_wave_mean_direction_from_first_directional_moment','wave mean direction from ef first freq moment',wave_sensor,units['degree_clockwise']),
    'ZDIR':('sea_surface_wave_mean_direction_within_a_given_spectral_bandwidth','sea_surface_wave_mean_direction_within_a_given_spectral_bandwidth','mean wave direction within each band',wave_sensor,units['degree_clockwise']),
    #WAVE ENERGY SPREADING################################################
    'SPRM':('wave_mean_spreading_from_ef_first_freq_moment','wave_mean_spreading_from_ef_first_freq_moment','wave mean spreading from ef first freq moment',wave_sensor,'degree'),
    #'STH1M':('sea_surface_wave_directional_spread_from_first_directional_moment',wave_sensor),
    'WSDS':('sea_surface_wave_spectral_directional_spreading','sea_surface_wave_spectral_directional_spreading','sea surface wave spectral directional spreading',wave_sensor,'degree'),
    'VSPR':('dominant_wave_spreading','dominant_wave_spreading','dominant wave spreading',wave_sensor,'degree'),
    #'VHM0':('significant_wave_height',wave_sensor),#SPECTRAL SIGNIF. WAVE HEIGHT
    #WAVE STAT MOMENTUM GEOMETRY###################################################
    'VST1':('maximum_wave_steepness','maximum_wave_steepness','Maximum wave steepness',wave_sensor,units['no_unit']),
    'SKSK':('wave_skewness','wave_skewness','wave Skewness',wave_sensor,units['no_unit']),
    'KUKU':('wave_kurtosis','wave_kurtosis','wave Kurtosis',wave_sensor,units['no_unit']),
    'VAVA':('wave_variance','wave_variance','wave variance',wave_sensor,units['no_unit']),
    #'ATEM':('air_temperature',temperature_air), # airtemperature
    #OCEANO########################################################
    'VCSP':('upward_sea_water_velocity','upward_sea_water_velocity','Bottom-top current component',current_sensor,units['meter_per_second']),
    'EWCT':('eastward_current_speed','eastward_current_speed','eastward current speed',current_sensor,units['meter_per_second']), # eastward_sea_water_velocity
    'NSCT':('northward_current_speed','northward_current_speed','northward current speed',current_sensor,units['meter_per_second']), # northward_sea_water_velocity    
    'CUSP':('current_speed','current_speed','current speed',current_sensor,'m.-1'), #sea_water_speed
    'SVEL':('speed_of_sound_in_sea_water','speed_of_sound_in_sea_water','sound velocity',current_sensor,units['meter_per_second']),
    'SCDT':('current_direction','current_direction','current direction',current_sensor,units['degree_clockwise']), # SEA SURF CURRENT DIR. REL T. N direction_of_sea_water_velocity
    'PRES':('sea_water_pressure','sea_water_pressure','sea water pressure',temp_sea,units['pascal']), #  sea_water_pressure #previously in manometer but useless
    'CPHL':('chlorophyll_a_total','chlorophyll','chlorophyll concentration',chimie,'mg/m3'),
    'SLEV':('sea_level','sea_level','sea level above LLDW datum',tide,units['meter']),#tbc long name
    'PSAL':('sea_water_salinity','sea_water_salinity','sea water salinity',salinity,'psu'),
    'COND':('sea_water_conductivity','sea_water_conductivity','sea water conductivity',salinity,'S.m-1'),
    'TEMP':('sea_water_temperature','sea_water_temperature','temperature of the sea water',temp_sea,units['celsius']),
    'EQTEMP':('equivalent_temperature','equivalent_temperature','temperature of an air parcel from which all the water vapor has been extracted by an adiabatic process',temp_sea,'kelvin'),
    'SST':('sea_surface_temperature','sea_surface_temperature','sea surface temperature',temp_sea,'kelvin'),
    'DSST':('diurnal_sea_surface_temperature','diurnal_sea_surface_temperature','difference between the maximum and minimum sea surface temperature over a 24-hour',temp_sea,units['celsius']),
    'DOXY':('oxygen_saturation','oxygen_saturation','oxygen saturation',chimie,'micromol.m-3'),
    'DOX1':('dissolved_oxygen','moles_of_oxygen_per_unit_mass_in_sea_water','dissolved oxygen',chimie,units['micromole']+'.kg-1'),#previously in 'micromol.m-3'
    'DOX2':('moles_of_oxygen_per_unit_mass_in_sea_water','moles_of_oxygen_per_unit_mass_in_sea_water','Dissolved oxygen',units['micromole']+'.kg-1'),
    'DIC':('dissolved_inorganic_carbon','dissolved_inorganic_carbon','dissolved inorganic carbon',chimie,units['micromole']+'.kg-1'),
    'TUR2':('turbidity','turbidity','turbidity',turbi,'N.T.U Nephelo Turb. Unit'), # light_attenuation
    'TUR4':('turbidity4','turbidity4','turbidity',turbi,'N.T.U Nephelo Turb. Unit'), # light_attenuation same as milliF.T.U Formaz Turb Unit from TUR6?
    'DENS':('sea_water_sigma_theta','sea_water_sigma_theta','Sea density (sigma-theta)',temp_sea,'kg.m-3'),#a bit the same than SIGMA_THETA ?
    
    'POTENTIAL_TEMPERATURE':('potential_temperature','potential_temperature','temperature of water adiabatically brought to a reference pressure',temp_sea,units['celsius']),
    'SIGMA_THETA':('sigma_theta','sigma_theta_density','density calculated with in situ salinity,potential temperature, and pressure = 0, minus 1000 kg/m3',temp_sea,'kg.m-3'),
    #FLUX############################################
    'ATMS':('air_pressure_at_sea_level','air_pressure_at_sea_level','air pressure at sea level',meteo_sensor,units['pascal']), #air_pressure_at_sea_level
    #'DRYT':('air_temperature',temperature_air), #DRY BULB TEMPERATURE  air_temperature
    'RHOA':('air_density','air_density','density of air at sea level',meteo_sensor,'kg.m-3'),
    'RELH':('relative_humidity','relative_humidity','relative air humidity ay sea level',meteo_sensor,'percent'),#tbc unit
    'DEWPT_TEMPERATURE':('dew_point_temperature','dew_point_temperature','dew point temperature',meteo_sensor,units['celsius']),
    'AIRT':('air_temperature','air_temperature','air temperature at sea level',meteo_sensor,units['celsius']),
    'WETT':('wet_bulb_temperature','wet_bulb_temperature','Air temperature in wet bulb',meteo_sensor,units['celsius']),
    'ATMP':('air_pressure','sea_level_air_pressure','air pressure at sea level',meteo_sensor,units['pascal']),#tbc unit
    'ATPT':('atmospheric_pressure_hourly_tendency','atmospheric_pressure_hourly_tendency','atmospheric pressure hourly tendency',meteo_sensor,units['no_unit']),
    'AIRSEA_T_DIFF':('sea_air_temperature_difference','sea_air_temperature_difference','temperature difference between air and sea surface',meteo_sensor,units['celsius']),
    'ATMCONDWATERCLOUD':('atmosphere_cloud_condensed_water_content','atmosphere_cloud_condensed_water_content','atmosphere cloud condensed water content (ice+liquid)',meteo_sensor,'kg.m-2'),
    'ATMWATERCLOUD':('atmosphere_water_vapor_content','atmosphere_water_vapor_content','atmosphere water vapor content',meteo_sensor,'kg.m-2'),
    'WATEREVAPFLUX':('water_evaporation_flux','water_evaporation_flux','water evaporation flux',meteo_sensor,'mm.day-1'),
    'PRECIPIT_VOLUME':('precipitation_volume','precipitation_volume','precipitation volume',meteo_sensor,'mm.day-1'),
    'SUPWFWFLUX':('surface_upward_fresh_water_flux','surface_upward_fresh_water_flux','surface upward fresh water flux (evaporation - precipitation)',meteo_sensor,'mm.day-1'),
    'LATENT_HEAT_FLUX':('surface_upward_latent_heat_flux','surface_upward_latent_heat_flux','surface upward latent heat flux',meteo_sensor,units['watt_per_square_meter']),
    'SENSIBLE_HEAT_FLUX':('surface_upward_sensible_heat_flux','surface_upward_sensible_heat_flux','surface upward sensible heat flux',meteo_sensor,units['watt_per_square_meter']),
    'LUPNRFLUX':('long_wave_upward_net_radiative_flux','long_wave_upward_net_radiative_flux','daily mean net surface fullsky longwave radiation flux, positive upward',meteo_sensor,units['watt_per_square_meter']),
    'NET_HEAT_FLUX':('upward_net_heat_flux','upward_net_heat_flux','difference between upward and downward heat flux at sea surface level',meteo_sensor,units['watt_per_square_meter']),
    'SWDNRFLUX':('short_wave_downward_net_radiative_flux','short_wave_downward_net_radiative_flux','daily mean net surface fullsky shortwave radiation flux, positive downward',meteo_sensor,units['watt_per_square_meter']),
    'SINC':('surface_downwelling_shortwave_flux','surface_downwelling_shortwave_flux_in_air','Shortwave incoming radiation',meteo_sensor,units['watt_per_square_meter']),
    'LATENT_HEAT_TRANSFERT_COEF':('latent_heat_transfert_coefficient','latent_heat_transfert_coefficient','trasnfert coeffient of latent heat at sea-surface interface',meteo_sensor,units['no_unit']),
    'SEASURF_SPECIFIC_HUMIDITY':('sea_surface_specific_humidity','sea_surface_specific_humidity','sea surface specific humidity (Bentamy et al.; Journal of Climate; 2003; Vol 16; 637-656; formula 3)',meteo_sensor,'g.kg-1'),
    'AIRSURF_SPECIFIC_HUMIDITY':('air_surface_specific_humidity','air_surface_specific_humidity','air surface specific humidity',meteo_sensor,'g.kg-1'),
    'SEA_AIR_SPECIFIC_HUMIDITY_DIFF':('sea_air_specific_humidity_difference','sea_air_specific_humidity_difference','Sea Surface Saturation Specific Humidity - Specific Air Humidity',meteo_sensor,'g.kg-1'),
    'LATTRSFCOEF':('latent_heat_transfer_coefficient','latent_heat_transfer_coefficient','Latent heat transfer coefficient',meteo_sensor,units['no_unit']),
    'ASUPRES':('air_surface_pressure','air_surface_pressure','atmospheric pressure at surface level',meteo_sensor,units['pascal']),
    'PBLH':('planetary_boundary_layer_height','planetary_boundary_layer_height','Planetary boundary layer height',meteo_sensor,units['meter']),
    'LINC':('surface_downwelling_longwave_flux','surface_downwelling_longwave_flux','long-wave incoming radiation',meteo_sensor,units['watt_per_square_meter']),
    'SDFA':('surface_downwelling_shortwave_flux_in_air','surface_downwelling_shortwave_flux_in_air','Solar Irradiation',meteo_sensor,units['watt_per_square_meter']),
    'DOWN_IRRADIANCE490':('DOWN_IRRADIANCE490','DOWN_IRRADIANCE490','Downwelling irradiance at 490 nm',meteo_sensor,"W m-2 nm-1"),
    'DOWN_IRRADIANCE380':('DOWN_IRRADIANCE380','DOWN_IRRADIANCE380','Downwelling irradiance at 380 nm',meteo_sensor,"W m-2 nm-1"),
    'DOWN_IRRADIANCE':('DOWN_IRRADIANCE','DOWN_IRRADIANCE','Downwelling irradiance (mounted on surface buoy)',meteo_sensor,units['watt_per_square_meter']),
    #chimical#####################################################
    'PHPH':('sea_water_ph','sea_water_ph','sea water ph',chimie,units['no_unit']),  #sea_water_ph_reported_in_total_scale
    'NITRATE':('nitrate','nitrate_concentration','water nitrate concentration',chimie,units['micromole']+'.kg-1'),
    'NITRITE':('nitrite','nitrite_concentration','water nitrite concentration',chimie,units['micromole']+'.kg-1'),
    'SILICATE':('silicate','silicate_concentration','silicate concentration',chimie,units['micromole']+'.kg-1'),
    'PHOSPHATE':('phosphate','phosphate_concentration','phosphate concentration',chimie,units['micromole']+'.kg-1'),
    'CFC-11':('trichlorofluoromethane','trichlorofluoromethane_concentration','trichlorofluoromethane concentration',chimie,'pmol.kg-1'),
    'CFC-12':('dichlorodifluoromethane','dichlorodifluoromethane_concentration','dichlorodifluoromethane concentration',chimie,'pmol.kg-1'),
    'SF6':('sulfure_hexafluoride','sulfure_hexafluoride','sulfure hexafluoride(SF6) concentration',chimie,'fmol/kg'),
    'TCO2':('total_CO2','total_CO2','total amount of CO2 in ocean',chimie,units['micromole']+'.kg-1'),
    'PCO2A':('surface_partial_pressure_of_carbon_dioxide_in_air','surface_partial_pressure_of_carbon_dioxide_in_air','surface partial pressure of carbon dioxide in air',chimie,units['pascal']),
    'PCO2W':('surface_partial_pressure_of_carbon_dioxide_in_sea_water','surface_partial_pressure_of_carbon_dioxide_in_sea_water','surface partial pressure of carbon dioxide in sea water',chimie,units['pascal']),
    'FCO2A':('surface_fugacity_of_carbon_dioxide_in_air','surface_fugacity_of_carbon_dioxide_in_air','surface fugacity of carbon dioxide in air',chimie,'atm'),
    'FCO2W':('surface_fugacity_of_carbon_dioxide_in_sea_water','surface_fugacity_of_carbon_dioxide_in_sea_water','surface fugacity of carbon dioxide in sea water',chimie,'atm'),
    'ALKALINITY':('alkalinity','alkalinity','sea alkalinity',chimie,units['micromole']+'.kg-1'),
    'ANTHROPOGENIC_CO2':('anthropogenic_CO2','anthropogenic_CO2','CO2 from anthoropenic sources',chimie,units['micromole']+'.kg-1'),
    'DELTA_C14':('delta_c14','delta_c14','ratio of Carbon-14 isotops',chimie,'o/oo'),
    'DELTA_C13':('delta_c13','delta_c13','ratio of Carbon-13 isotops',chimie,'o/oo'),
    'POTENTIAL_ALKALINITY':('potential_alkalinity','potential_alkalinity','potential quantitative capacity of an aqueous solution to neutralize an acid',chimie,units['micromole']+'.kg-1'),
    'CONV_RC_AGE':('conventional_radiocarbon_age','conventional_radiocarbon_age','conventional radiocarbon age',chimie,units['no_unit']),
    'NATURAL_C14':('natural_c14','natural_c-14','concentration of Carbon-14 isotop in sea from natural sources',chimie,'o/oo'),
    'BOMB_C14':('bomb_c14','bomb_c-14','concentration of Carbon-14 isotop in sea from nuclear weapon testing ',chimie,'o/oo'),
    'PHSWS25':('ph_on_sea_water_scale','ph_on_sea_water_scale','pH on sea water scale sample titration',chimie,units['no_unit']),
    'CFC113':('trichlorotrifluoroethane','trichlorotrifluoroethane','trichlorotrifluoroethane concentration (cfc-113)',chimie,'pmol.kg-1'),
    'FLUO':('fluorescence','fluorescence','sea water fluorescence',chimie,'milligram/m3'),
    'CCL4':('carbon_tetrachloride','carbon_tetrachloride','carbon tetrachloride (CCL4) concentration',chimie,'pmole.kg-1'),
    'H3':('triatomic_hydrogen','triatomic_hydrogen','triatomic hydrogen concentration (H3)',chimie,'TU'),#dont know what means TU
    'HE3':('delta_helium3','delta_helium3','delta helium3 concentration',chimie,'percent'),
    'HE':('helium','helium','helium concentration',chimie,'pmole.kg-1'),
    'PF11':('cfc11_partial_pressure','cfc11_partial_pressure','cfc11 partial pressure',chimie,'parts_per_trillion'),
    'PF12':('cfc12_partial_pressure','cfc12_partial_pressure','cfc12 partial pressure',chimie,'parts_per_trillion'),
    'PF113':('cfc113_partial_pressure','cfc113_partial_pressure','cfc113 partial pressure',chimie,'parts_per_trillion'),
    'PCCL4':('ccl4_partial_pressure','ccl4_partial_pressure','ccl4 partial pressure',chimie,'parts_per_trillion'),
    'PSF6':('sf6_partial_pressure','sf6_partial_pressure','sf6 partial pressure',chimie,'parts_per_trillion'),
    'AOU':('apparent_oxygen_utilization','apparent_oxygen_utilization','apparent oxygen utilization',chimie,units['micromole']+'.kg-1'),
    'OSAT':('fractional_saturation_of_oxygen_in_sea_water','fractional_saturation_of_oxygen_in_sea_water','fractional saturation of oxygen in sea water',chimie,'%'),
    
  }

Synonyms = { #this dict will be at term put in each reader
  'WSPD':('wind_speed','winspd','continuous_wind_speed','wnd10'),
  'NORTHWS':('northward_wind'),
  'WDIR':('wind_dir','wind_direction','Wind_Direction','continuous_wind_direction'),
#   'VTPK':('dominant_wpd','dominant_wave_period','ta','tpic'),
#   'VTZA_TP':('maximum_wave_period'),
#   'VTZA_TH1D3':('vtza_th1d3'),
  'AVERAGE_WAVE_PERIOD':('tp','tm'),
  'ALKALINITY':('total_alkalinity','TA'),#sum of hydroxide,carbonate and biocarbonate alkalinities, 
  #add period from first moment
  #add period from second moment
  'CPHL':('cph1','chlorophyll-a_total'),
#   'VAVH':('wave_height','hs','hm0'), #vavh_hs not in since no example of valid data found (dont know if it is different )
  'ATMP':('air_pressure','air_pressure_at_sea_level','atms','atmospheric_pressure','pppp'),
  'EF':('sea_surface_wave_variance_spectral_density','vepk','wave_variance_spectral_density','wave_frequency_spectrum','spectral_wave_density','spectraldensity','wsed'), #confirme by FA
  'GSPD':('gust'),
  'VPED':('vdir','dominant_dir','dominant_wave_direction','mean_wave_dir','alpha2','principal_wave_dir','dp','dir'), #peut etre aps judicieux de mettre alpha1 et alpha2 ensemble
  'WAVE_MEAN_DIRECTION':('dmean','alpha1'),
  'AIRT':('dryt','atem','air_temperature','tmp2m','air_surface_temperature'),
  'SWHT':('vtdh'),
  'DEWPT_TEMPERATURE':('dptt','dewt'), #invention de DPTT
  'MGST':('max_hust','hourly_max_gust'), #invention pour ne pas avoir de hourly...
  'DMGST':('direction_of_hourly_max_gust'),
#   'TEMP':('ocean_temperature','sea_water_temperature','tmer'),
  'SST':('tmpsf','sea_surface_temperature','diurnally_varying_sea_surface_temperature'),
  'CUSP':('hcsp','current_speed'),
  'SCDT':('current_direction','hcdt'),
  'VCMX':('vzmx','maximum_wave_height'),
  'SPRM':('sprm','sea_surface_wave_directional_spread_from_first_directional_moment','sth1m','vpsp','etal'),
  'RELH':('relative_humidity'),
#   'VTDH_H1D3':('vtdh_h1d3'),
  'WSDS':('wsds'),
  'PSAL':('ocean_salinity','sss'),
  'FLUO':('FLU2'),
#   'A1':('a1'),
#   'B1':('b1'),
#   'B2':('b2'),
#   'A2':('a2'),
  'THETA1':('theta1'),
  'THETA2':('theta2'),
  'STHETA2':('stheta2'),
  'STHETA1':('stheta1'),
  'CENTRALFREQ':('centralfrequency','centralfreq','frequency'),
  'BANDWIDTHFREQ':('bandwidth','band_width'),
  'PRES':('sea_water_pressure'),
  'NET_HEAT_FLUX':('qnet','net_heat_flux'),
  'SWDNRFLUX':('nswrs','shortwave_net_flux'),
  'LUPNRFLUX':('nlwrs','longwave_net_flux'),
  'SENSIBLE_HEAT_FLUX':('shtfl','sensible_heat_flux'),
  'LATENT_HEAT_FLUX':('lhtfl','latent_heat_flux'),
  'AIRSURF_SPECIFIC_HUMIDITY':('hum2m','surface_air_specific_humidity','specific_humidity'),
  'WATEREVAPFLUX':('evapr','evaporation'),
  'NITRATE':('no3'),
  'NITRITE':('no2'),
  'DOX1':('oxygen','dox2'),
  'CFC-12':('dichlorodifluoromethane'),
  'CFC-11':('trichlorofluoromethane'),
  'TCO2':('total_co2'),
  'SIGMA_THETA':('sigma_theta_density'),
}

def testname(synonym_tested,input,tested_synonym_id):
    '''
    synonym_tested (str) synonyme of a reference variable
    input (str) tested variable name
    tested_synonym_id (str) ref variable'''
#     logging.debug('synonym_tested %s input: %s %s %s %s',synonym_tested,input,type(synonym_tested),type(input),len(synonym_tested))
#     logging.debug('test %s',input.lower() == synonym_tested)
    if input.lower() == synonym_tested:
#         logging.debug('%s input.lower() %s possible_synonyms %s',input,input.lower(),possible_synonyms)
        match = tested_synonym_id
        return match
    else:
        return None
    
def look_into_RefVars_names(input_variable):
    res = None
    for varid in RefVars.keys():
        globwave_varname = RefVars[varid][0]
        if globwave_varname == input_variable:
            res = varid
    return res

def look_into_synonyms(input_var_name):
    match = None
    for tested_synonym_id in Synonyms: #tested_synonym_id < len(Synonyms):
#             logging.debug('variable_convention | tested_synonym_id %s',tested_synonym_id)
        if not match is None:
            break
        possible_synonyms = Synonyms[tested_synonym_id]
        if isinstance(possible_synonyms,tuple):
            for synonym_tested in possible_synonyms:
                match = testname(synonym_tested,input_var_name,tested_synonym_id)
                if match != None:
                    break
        else:
            synonym_tested = possible_synonyms
            match = testname(synonym_tested,input_var_name,tested_synonym_id)
    return match

def VarConventions(input_var_name,return_accro=False):
    '''
    function to retrieve the full attributes of a parameter
    Args:
        input_var_name (str): name of the parameter you seek
    Return:
        meta_res (tuple): information of the parameter 
        '''
    start = time.time()
    match = None
    meta_res = None
    if RefVars.has_key( input_var_name.upper() ): #direct match with var_id (rare)
        logging.debug('direct match for var %s',input_var_name.upper())
#         return RefVars[input_var_name.upper()]
        match = input_var_name.upper()
    if match is None:
        match = look_into_RefVars_names(input_var_name)
#     if match is None:
#         match = look_into_synonyms(input_var_name)
    if match is not None:
        res = RefVars[match.upper()]
        if len(res) < 5 or len(res)>5:
            logging.warning('variables_conventions | parameter %s do not have a full description',match.upper())
            raise Exception('%s not valid reference',input_var_name)
        meta_res = res
    else:
        logging.debug('%s variable is not referenced in CERSAT variables_conventions.py',input_var_name)
#     logging.debug('analyse the variable %s in %1f seconds',input_var_name,time.time()-start)
    if return_accro:
        if meta_res is None:
            return None
        else:
            return meta_res+(match,)
    else:
        return meta_res
        
def checkNumberofentries():
    if len(RefVars)!=len(Synonyms):
        logging.info('warning the number of entries in RefVars is %d Synonyms dictionnary is %d',len(RefVars),len(Synonyms))
    else:
        logging.info('tables in variables_conventions.py are okay')

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(levelname)-5s %(message)s',
                        datefmt='%d/%m/%Y %I:%M:%S')
    #GWname,longname_cv,description_cv,sensor_cv,units_cv=VarConventions('ta')
    print(VarConventions('sSs'))
    print(VarConventions('ATMS'))