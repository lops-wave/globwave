#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#

"""
Processing cdipmooredbuoy from directory listing.

Cmd :
ssh cerhouse1
source /home/cercache/tools/environments/scientific_toolbox_cloudphys_precise/bin/activate.csh
setenv PYTHONPATH /home1/typhon/cprevost/DEV/python/cerbere_deployed
./cdipReprocess2.py 

Runner :
/home1/typhon/cprevost/DEV/shell/run_python_cerbere_CPR.sh ./cdipReprocess2.py 

globwaveOutDir = /home/cercache/project/globwave/data/globwave/cdip
fileList = /home/cercache/project/globwave/data/provider/cdip/spool/cdip_to_GW.list

NOTICE :
 Do not forget '' if using '/path/*', otherwise last month only will be treated !
 
History : 20141020 CPR Creation

"""



import glob
import sys
import os
import subprocess
import datetime



globwaveOutDir = '/home/cercache/project/globwave/data/globwave/cdip'
print "globwaveOutDir : ",globwaveOutDir
fileList = '/home/cercache/project/globwave/data/provider/cdip/spool/cdip_to_GW.list'

# Open directory listing (written by archiving process)
if os.path.isfile(fileList):
    fl = open(fileList, 'r')
    monthdata = fl.readlines() 
    fl.close()
    
    
    for m in monthdata:
        month = m.strip()
    
        if month[-1] == '*':
            inDirs = glob.glob(month)
        elif month.split('/')[-1] in ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec']:
            inDirs = [month]
        else : 
            raise Exception( "[CDIP REPROCESS] inputdir format should be as /path/YYYY/platformNb/{month|*} with no \n at the end.")
            exit(1)
        
        
        
        print inDirs
        start = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
        channelCounter = 0
        for inputDir in inDirs:
            channelDirList = os.listdir(inputDir)
            if 'p2' in channelDirList:
                channelDir = 'p2'
            elif '01' in channelDirList and (len(channelDirList)==1 or (len(channelDirList)==2 and 'fix' in channelDirList)):
                channelDir = '01'
            elif '01' in channelDirList:
                 channelDir = '01'
            elif len(channelDirList)==1 and '07' in channelDirList:
                 channelDir = '07'
            elif len(channelDirList)==1 and '02' in channelDirList:
                 channelDir = '02'
            elif '04' in channelDirList:  # TO BE VERIFIED !!! #
                 channelDir = '04'        # TO BE VERIFIED !!! #
            elif len(channelDirList)==1 and '05' in channelDirList: # TO BE VERIFIED !!! #
                 channelDir = '05'      # TO BE VERIFIED !!! #
            elif len(channelDirList)==0:
                 print 'no channel for : ', inputDir
                 continue
            else:
                print 'channel :', channelDirList
                raise Exception( "ERROR : ambiguity with channels")
        
            print "channelDir %s/%s" % (inputDir,channelDir)
          
            channelCounter += 1
            retcode = os.system("python  /home1/typhon/cprevost/DEV/python/GIT/globwave/src/insitu/cdipmooredbuoy.py --inputdir "+inputDir+"/"+channelDir+" --outputdir "+globwaveOutDir)
            print ">>%s<<"%retcode
            
            
            if retcode!=0:
                raise Exception('[CDIP REPROCESS] Error %s when reprocessing CDIP data from : %s/%s ' % (retcode,inputDir,channelDir))
                exit(1)
            
            print "START : ",start
            print "channelCounter = ",channelCounter
            print "END : ", datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
    
    # Treatment ok, remove listing
    os.remove(fileList)
else:
    print "No file to process !"
