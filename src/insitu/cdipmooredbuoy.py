# *-* coding: iso-8859-1 *-*

#############################################################
#
#  Convert CDIP data file (Fortran) in Globwave format,
#  using cerbere libs.
#
#  author : agrouaze, cprevost
#
#
#############################################################



format_version='09/10/2014'
PROCESSOR_VERSION='2.3'  # Increase version for major evolution



from datetime import datetime
import os,sys
import glob
import numpy
import copy
from collections import OrderedDict
import logging
import traceback

#temporary code (to remove when FileNamer and variables_conventions will be in GlobeWave git)
# sys.path.insert(0, '/home/cercache/users/agrouaze/git/cerbere') #cerbere mapper etc

from cerbere.datamodel.pointtimeseries import PointTimeSeries
from cerbere.datamodel.pointcollection import PointCollection
from cerbere.datamodel.variable import Variable
from cerbere.datamodel.field import Field, QCLevel, QCDetail
# import cerbere.science.cfconvention
# import cerbere.science.wave as wavescience
from cerform.cfconvention import get_convention,get_standardname
from cerform import cfconvention
# import cerbereutils.science.wave
from cerform.wave import r1r2_to_sth1sth2,moments2dirspread
from cerbere.mapper.abstractmapper import WRITE_NEW
from cerbere.mapper.ncfile import NCFile
from ceraux.landmask import Landmask
# from cerbere.geo.landmask import LandMask
#from cerbereutils.ancillary.bathymetry import Bathymetry

from FileNamer import GlobWaveFileNamer
from variables_conventions import VarConventions

wanted_as_variable=['hs','tp','dp','ta'] #gives the name of the varaible to transform into field from metadata of provided files

#dico to convert cdip variabl to classic globwave names
# infos format http://cdip.ucsd.edu/?nav=documents&sub=index&xitem=description&xdoc=sp_format
corresp_dico = {
    'bandwidth':'BANDWIDTHFREQ',
    'spectralDensity':'VEPK',
    'dmean':"ZDIR",
    'hs':'VAVH',
    'tp':'VTPK', #peak period
    'dp':'VMDR', #mean direction
    'ta': 'VGTA',#average period
    }


class CDIPMooredBuoy():
    def __init__(self, id=None):
        self.model=None
        self.history=None
        self.elevation=None
        self.depth=None
        self.lon=None
        self.lat=None
        self.dist2shore=None
        self.spectrumType='Directional'
        self.spectrumTypePrev=''
        self.sampleRate=None
        self.samplelength=None
        self.institution='CDIP'
        self.WMOId=''
        self.providerId=None
        self.longName=None
        self.measTime = None
                
    def initSeriesArray(self):
        big_table_measures={}
        jo=numpy.ma.masked_array([])
        big_table_measures['centralfreq']=jo
        big_table_measures['bandwidth']=jo
        big_table_measures['dmean']=jo
        big_table_measures['spectralDensity']=jo
        big_table_measures['theta1']=jo
        big_table_measures['theta2']=jo
        big_table_measures['stheta1']=jo
        big_table_measures['stheta2']=jo
        big_table_metadata={}
        big_table_metadata['datetime']=jo
        big_table_metadata['station_name']=jo
        big_table_metadata['lon']=jo
        big_table_metadata['lat']=jo
        big_table_metadata['sensortype']=jo
        big_table_metadata['waterdepth']=jo
        big_table_metadata['sensordepth']=jo
        big_table_metadata['sensorelev']=jo
        big_table_metadata['shorenormal']=jo
        big_table_metadata['sourcefile']=jo
        big_table_metadata['samplelength']=jo
        big_table_metadata['samplerate']=jo
        big_table_metadata['hs']=jo
        big_table_metadata['tp']=jo
        big_table_metadata['dp']=jo
        big_table_metadata['ta']=jo
        big_table_metadata['spectrumtype']=jo
        return big_table_metadata,big_table_measures

    def appendData2Timeserie(self,big_table_metadata,big_table_measures,allSamples):
        #list_metadata=['datetime','station_name','location','model','depth','elevation','normalToShore','Hs','Tp','Dp','Ta','sampleLength','sampleRate']
        #for meme in list_metadata:
        big_table_metadata['datetime']=numpy.append(big_table_metadata['datetime'],self.measTime)
        big_table_metadata['station_name']=numpy.append(big_table_metadata['station_name'],self.longName)
        big_table_metadata['lon']=numpy.append(big_table_metadata['lon'],self.lon)
        big_table_metadata['lat']=numpy.append(big_table_metadata['lat'],self.lat)
        big_table_metadata['sensortype']=numpy.append(big_table_metadata['sensortype'],self.model)
        big_table_metadata['waterdepth']=numpy.append(big_table_metadata['waterdepth'],self.depth)
        big_table_metadata['sensorelev']=numpy.append(big_table_metadata['sensorelev'],self.elevation)
        big_table_metadata['shorenormal']=numpy.append(big_table_metadata['shorenormal'],self.normalToShore)
        big_table_metadata['sourcefile']=numpy.append(big_table_metadata['sourcefile'],self.dataFile)
        big_table_metadata['samplelength']=numpy.append(big_table_metadata['samplelength'],self.sampleLength)
        big_table_metadata['samplerate']=numpy.append(big_table_metadata['samplerate'],self.sampleRate)
        big_table_metadata['hs']=numpy.append(big_table_metadata['hs'],self.Hs)
        big_table_metadata['tp']=numpy.append(big_table_metadata['tp'],self.Tp)
        #big_table_metadata['dp']=numpy.append(big_table_metadata['dp'],self.Dp)
        big_table_metadata['ta']=numpy.append(big_table_metadata['ta'],self.Ta)
        big_table_metadata['spectrumtype']=numpy.append(big_table_metadata['spectrumtype'],self.spectrumType)
        
        
        if self.spectrumType=='directional':   # CPR 20140915 dp is not applicable for non-directional buoy
            big_table_metadata['dp']=numpy.append(big_table_metadata['dp'],self.Dp)
        
        for i, sample in enumerate(allSamples):
            if self.spectrumType=='directional':
#                 centralFreq,freqRange,a1,b1,a2,b2,density,meanDir = sample
                centralFreq,freqRange,theta1,theta2,stheta1,stheta2,density,meanDir = sample
            else:
                centralFreq,freqRange,density  = sample

            #add the samples to spectrum record (if a value is defined)
            if density != None:
                if self.spectrumType=='directional':   
                    big_table_measures['centralfreq']=numpy.append(big_table_measures['centralfreq'],centralFreq)
                    big_table_measures['bandwidth']=numpy.append(big_table_measures['bandwidth'],freqRange)
                    big_table_measures['dmean']=numpy.append(big_table_measures['dmean'],meanDir)
                    big_table_measures['spectralDensity']=numpy.append(big_table_measures['spectralDensity'],density)
                    big_table_measures['theta1']=numpy.append(big_table_measures['theta1'],theta1)                                       
                    big_table_measures['theta2']=numpy.append(big_table_measures['theta2'],theta2)
                    big_table_measures['stheta1']=numpy.append(big_table_measures['stheta1'],stheta1)
                    big_table_measures['stheta2']=numpy.append(big_table_measures['stheta2'],stheta2)
                
                else:
                    big_table_measures['centralfreq']=numpy.append(big_table_measures['centralfreq'],centralFreq)
                    big_table_measures['bandwidth']=numpy.append(big_table_measures['bandwidth'],freqRange)
                    big_table_measures['spectralDensity']=numpy.append(big_table_measures['spectralDensity'],density)
        return big_table_measures,big_table_metadata
    
    def findWMO(self):
        fWMO = open('/home/cercache/project/globwave/prog/GLOBWAVE/src/insitu/buoynetworks/cdip/resources/cdip_WMOids', 'r')
        line = fWMO.readline()
        val=None
        logging.debug('self.providerId %s',self.providerId)
        logging.debug('line content:%s',line)
        while line != '' and line != None and val==None:
            if line.strip().split()[0] == self.providerId:
                val=line.strip().split()[1]

            line = fWMO.readline()

        fWMO.close()
        return val
                
    def readTimeLine(self,content):
        
        year = int(content[7:11])
        logging.debug('year = %s',year)
        month = int(content[11:13])
        logging.debug('month = %s',month)
        day = int(content[13:15])
        logging.debug('day = %s',day)
        hour = int(content[15:17])
        logging.debug('hour = %s',hour)
        minute = int(content[17:19])
        logging.debug('minutes %s',minute)
        measTime = datetime (year, month, day, hour, minute)
        logging.debug('measTime %s',measTime)
        return measTime
    
    def defineBuoyModel(self,model):
        if model == 'Waverider Drctnl Buoy' or model == 'Spherical Drctnl Buoy':
            buoyModel='Waverider MarkII directional'
        elif model == 'Pressure Sensor':
            buoyModel='Unknown'

        elif  model == 'Spherical Non-Drctnl Buoy':
            buoyModel='Waverider non-directional'

        else:
            raise Exception ("ERROR : Unknown platform model : %s" % model)
        return buoyModel
    
    def readData(self,maxline):
#         line = fhandler.readline()
        logging.debug('reading data')
        beg=10
        allSamples=[]

        for linenb in range(beg,maxline):
            line=self.GetLine(linenb)
            content = line.split()
            centralFreq = float(content[0])
            freqRange = float(content[1])
            density = float(content[2])
            if self.spectrumType=='directional':
                # DIRECTIONAL SPECTRUM
                if len(content) == 3:
                    a1 = None
                    a2 = None
                    b1 = None
                    b2 = None
                    meanDir = None
                else:
                    #fullSpectrum = True
                    if '*' not in content[4] and content[4].strip() != '.':
                        a1 = float(content[4])
                    else:
                        a1 = None
                    if '*' not in content[6] and  content[6].strip() != '.':
                        a2 = float(content[6])
                    else:
                        a2 = None
                    if '*' not in content[5] and  content[5].strip() != '.':
                        b1 = float(content[5])
                    else:
                        b1 = None
                    if '*' not in content[7] and  content[7].strip() != '.':
                        b2 = float(content[7])
                    else:
                        b2 = None
                    if '*' not in content[3] and  content[3].strip() != '.':
                        meanDir = float(content[3])
                    else:
                        meanDir = None
                if meanDir !=None:
                    theta1,theta2,stheta1,stheta2 = moments2dirspread( a1,b1,a2,b2 )
                    sample = [(centralFreq, freqRange, theta1, theta2, stheta1, stheta2, density, meanDir)]
                else:
                    sample = [(centralFreq, freqRange, None, None, None, None, density, None)]
                
            else: # non-directional
                # FREQUENCY SPECTRUM
                sample = [(centralFreq, freqRange,density)]
                
            allSamples += sample
            line=self.GetLine(linenb)
            content = line.split()
        return allSamples
    
    def GetPosition(self):
        line=self.GetLine(2)
        content = line.split()
        lat = float(content[1]) + float(content[2])/60.
        if (content[3] == 'S'):
            lat = -lat
        lon = float(content[4]) + float(content[5])/60.
        if (content[6] == 'W'):
            lon = -lon
        logging.debug('lon %s lat %s',lon,lat)
        return lon,lat
            
    def readHeaderInformation(self):
        breakSeries=False
        line=self.GetLine(0)
        content = line.split()
        if (self.providerId != None and self.providerId != content[2][2:5]):
            raise Exception ("ERROR : provider Id change!")
        self.providerId = content[2][2:5]
        #may replace it by something integrated
        val=self.findWMO()
        if val != None:
            self.WMOId = val
        else:
            if (self.WMOId == '' or self.WMOId == None) and self.providerId == None:
                logging.warning('WARNING : no WMO id found')
        if (content[0] == "File"):
            histoEntry = ""
            
            #line 1
            logging.debug('content[2] %s',content[2])
            measTime = self.readTimeLine(content[2])
            self.measTime=measTime
            
            #line 2
            line=self.GetLine(1)
            content = line.split(':')
            self.longName = content[1].split(',')[0].strip()
            logging.debug('self.longName %s',self.longName)
            #line 3
            lon,lat=self.GetPosition()
            line=self.GetLine(2)
            model = line.split(':')[2].strip()
            buoyModel = self.defineBuoyModel(model)
            logging.debug('model %s buoy category %s',model,buoyModel)
            # check buoy change
            if (self.model != None and self.model != buoyModel):
                self.model = buoyModel
                logging.warning('WARNING : Platform model changed')
                if histoEntry == "":
                    histoEntry = "%s: Platform model change" % measTime.strftime("%Y-%m-%dT%H:%M:%S")
                    if not histoEntry in self.history:
                        self.history.append(histoEntry)
                else:
                    self.history[-1] =  self.history[-1] + ", Platform model change"
                breakSeries = True
                
            elif self.model == None :
                self.model = buoyModel
               
            #line 4
            line=self.GetLine(3)
            content = line.split(':')
            depth = -int(content[1].split()[0].strip())
            logging.debug('self.depth %s depth %s',self.depth,depth)
            if self.depth != None and self.depth != depth:
                logging.warning("Depth changed! %s %s",self.depth,depth)
                breakSeries = True
                if histoEntry == "":
                    histoEntry = "%s: Depth changed" % measTime.strftime("%Y-%m-%dT%H:%M:%S")
                    if not histoEntry in self.history:
                        self.history.append(histoEntry)
                else:
                    self.history[-1] =  self.history[-1] + ", Depth changed"
                self.depth = depth
            else:
                self.depth = depth
            elevation = float(content[-1].strip()) + self.depth
            logging.debug('elevation %s',elevation)
            logging.debug('depth %s',self.depth)
            if self.elevation == None:
                self.elevation = elevation
            if self.elevation != elevation:
                logging.warning('Elevation changed! %s  <> %s ',self.elevation, elevation)
                if histoEntry == "":
                    histoEntry = "%s: Elevation change" % measTime.strftime("%Y-%m-%dT%H:%M")
                    if not histoEntry in self.history:
                        self.history.append(histoEntry)
                else:
                    self.history[-1] =  self.history[-1] + ", Elevation change"
                breakSeries = True
                self.elevation = elevation
                
            if self.lon == None:
                self.lon=lon
                self.lat=lat
                
                # INITIAL 
                # distance in meters
                # (alternative) /home/cercache/tools/environments/scientific_toolbox_cloudphys_precise/lib/python2.7/site-packages/cerbereutils/ancillary/resources/NAVO-lsmask-world8-var.dist5.5.nc
                # (alternative) /home/cercache/users/medspi/cerbere/src/geo/resources/NAVO-lsmask-world8-var.dist5.5.nc
                inst_ld = Landmask('/home/cercache/tools/environments/scientific_toolbox_cloudphys_precise/lib/python2.7/site-packages/cerbereutils/ancillary/resources/NAVO-lsmask-world8-var.dist5.5.nc')
#                 self.dist2shore= int(Landmask.getDistanceToShore(lat, lon,filename='/home/cercache/tools/environments/scientific_toolbox_cloudphys_precise/lib/python2.7/site-packages/cerbereutils/ancillary/resources/NAVO-lsmask-world8-var.dist5.5.nc'))
                self.dist2shore= inst_ld.getDistanceToShore(lat,lon)
                
                # Other method :
                #dist2shore = int( Bathymetry('gridone_dist2shore.nc').get_distance_to_shore(lon, lat) ) * 1000
                
                
                #buoyTimeSeries.location = geo.location.Location( lat, lon, depth, dist2shore)
            elif self.lon!=lon or self.lat!=lat:
                logging.warning("Location changed! %s %s <> %s %s",self.lat,self.lon, lat,lon)
                if histoEntry == "":
                    histoEntry = "%s: Location change" % measTime.strftime("%Y-%m-%dT%H:%M")
                    if not histoEntry in self.history:
                        self.history.append(histoEntry)
                else:
                    self.history[-1] =  self.history[-1] + ", Location change"
                breakSeries = True
                self.lon=lon
                self.lat=lat
                
            #line 5
            line=self.GetLine(4)
            content = line.split()
            normalToShore = content[2]
            self.normalToShore = normalToShore
            #line 6
            line=self.GetLine(5)
            content = line.split()
            sampleLength = float(content[2].strip()) / 60
            sampleRate = float(content[5].strip())
            
            #line 7
            line=self.GetLine(6)
            content = line.split()
            logging.debug('content %s',content)
            Hs=content[1]
            if Hs.strip() != '':
                Hs=float(Hs)
            Tp=content[3]
            if Tp.strip() != '' and Tp.strip() != '*****':
                Tp=float(Tp)
            Dp=content[5]
            if Dp.strip() != '' and Dp.strip() != '.' and Dp.strip() != 'N/A':
                Dp=float(Dp)
            Ta=content[7]
            if Ta.strip() != '':
                Ta=float(Ta)
            logging.debug('Hs %s Tp %s Dp %s Ta %s',Hs,Tp,Dp,Ta)
            self.Hs = Hs
            self.Dp = Dp
            self.Tp = Tp 
            self.Ta = Ta
            line=self.GetLine(8)

            content = line.split()
            
            if self.spectrumType != 'unknown':
                if len(content) > 3 and self.spectrumType == 'non-directional':
                    logging.warning("WARNING : Change in spectrum type : non-directional => directional")
                    breakSeries = True
                    if histoEntry == "":
                        histoEntry = "%s: Change in spectrum type (non-directional => directional)" % measTime.strftime("%Y-%m-%dT%H:%M")
                        if not histoEntry in self.history:
                            self.history.append(histoEntry)
                    else:
                        self.history[-1] =  self.history[-1] + ", Change in spectrum type (non-directional => directional)"                           
                    
                    self.spectrumTypePrev = 'non-directional'
                    self.spectrumType = 'directional'
                elif len(content) == 3 and self.spectrumType == 'directional':
                    logging.warning("WARNING : Change in spectrum type : directional => non-directional")
                    breakSeries = True
                    if histoEntry == "":
                        histoEntry = "%s: Change in spectrum type (directional => non-directional)" % measTime.strftime("%Y-%m-%dT%H:%M")
                        if not histoEntry in self.history:
                            self.history.append(histoEntry)
                    else:
                        self.history[-1] =  self.history[-1] + ", Change in spectrum type (directional => non-directional)"
                    self.spectrumTypePrev = 'directional'
                    self.spectrumType = 'non-directional'
 
            logging.debug('content %s',content)
            if len(content) > 3:
                self.spectrumType = 'directional'
            elif len(content) == 3:
                self.spectrumType = 'non-directional'
            line=self.GetLine(8) 
            logging.debug('-spectrumType %s',self.spectrumType)
            # check sampling duration
            if self.sampleRate!=None:
                if (sampleRate != self.sampleRate or    sampleLength != self.sampleLength):
                    breakSeries = True
                    logging.warning("Different sampling time (length,rate) : %s %s <> %s %s",self.sampleLength,self.sampleRate,sampleLength, sampleRate)
                    if self.sampleLength != sampleLength:
                        histoEntry = "%s: Wave sampling length change" % measTime.strftime("%Y-%m-%dT%H:%M")
                    else:
                        histoEntry = "%s: Wave sampling rate change" % measTime.strftime("%Y-%m-%dT%H:%M")
                    if not histoEntry in self.history:  
                        self.history.append(histoEntry)
                        self.sampleRate=sampleRate
                        self.sampleLength=sampleLength
                    else:
                        self.sampleRate=sampleRate
                        self.sampleLength=sampleLength
            else:
                self.sampleRate=sampleRate
                self.sampleLength=sampleLength
                    
            logging.debug('sample rate: %s ',sampleRate)
            logging.debug('sample lenght: %s ',sampleLength)
               
        return breakSeries
    
    def GetLine(self,linenbr):
        return self.content[linenbr]
    
    def AddQCvars(self,valuesSHAPE):
        dims = OrderedDict() ## ajout selon JFP
        if len(valuesSHAPE) == 2:
            dims['time'] = valuesSHAPE[0] ## ajout selon JFP
            dims['frequency'] = valuesSHAPE[1]
        else:
            dims['time'] = valuesSHAPE[0] ## ajout selon JFP
        vals = numpy.ma.empty(valuesSHAPE ,dtype='i1')
        vals[:] = 4
        qc_levels = QCLevel( values = vals,
                                            dimensions = copy.copy(dims),
                                            levels = numpy.array([0,1,2,3,4],dtype='i1'),
                                            meanings = "Unknown Unprocessed Bad Suspect Good" )
        vals = numpy.ma.empty(valuesSHAPE,dtype='i2')
        vals[:] = 1
        qc_details = QCDetail( values = vals,
                                                   dimensions = copy.copy(dims),
                                                   mask = numpy.array([1],dtype='i1'),
                                                   meanings = 'flagged_by_provider')
        return qc_levels,qc_details
    
    def CreateTimeSerie(self,big_table_measures,big_table_metadata,cptfile,allSamples,spectrumType):
        fields_sensor={}
        start=big_table_metadata['datetime'][0]
        stop=big_table_metadata['datetime'][-1]
        # data of time serie to be saved
        lat=big_table_metadata['lat'][0]
        lon=big_table_metadata['lon'][0]
        depth=big_table_metadata['waterdepth'][0]

        if self.WMOId != '':
            CorrespondingFileName=GlobWaveFileNamer('WMO'+self.WMOId,outputdir,'wave_sensor',start,stop,lat,lon)
        else:
            CorrespondingFileName=GlobWaveFileNamer(self.providerId,outputdir,'wave_sensor',start,stop,lat,lon)
            
        # Distinct filename for directional data 
        if spectrumType =='directional':
            CorrespondingFileName = CorrespondingFileName.replace(".nc", "_spectrum.nc")
            
        logging.info('new file %s will be created',CorrespondingFileName)
    #filenames_container[CorrespondingFileName]=[]#indexing the futur file 
    #variable agrouaze GLOBEWAVE_varname,longname,description,sensor,units
    #sauvegarde dans des fields et variables agrouaze

        dims = OrderedDict() ## ajout selon JFP
        dims['time'] = cptfile ## ajout selon JFP
        dims['frequency'] = len(allSamples) #for example 74 differnt frequencies
        dimstime = OrderedDict()
        dimstime['time'] = cptfile
                #dimFrequency=OrderedDict([('frequency',len(allSamples))])
        for varvar in big_table_measures:
            if varvar in corresp_dico:
                corresponding_var = corresp_dico[varvar]
            else:
                corresponding_var = varvar
            GWname,longname_cv,description_cv,sensor_cv,units_cv=VarConventions(corresponding_var)
            logging.debug('declare 2DField for var %s',GWname)
            #logging.debug('input: %s gwname: %s long %s descr %s sensor %s unit %s',varvar,GWname,longname_cv,description_cv,sensor_cv,units_cv)
            varclass=Variable(shortname=GWname, description=description_cv,standardname=longname_cv,authority =cfconvention.get_convention())
            valval=big_table_measures[varvar]
            
            # Non-directional buoys case if spectrumType=='directional':
            if len(valval) > 0:
                logging.debug('shape of valval %s var %s.',len(valval),varvar)
                logging.debug('time dim length %s',dims['time'])
                logging.debug('frequency dim length %s',dims['frequency'])
                valval=valval.reshape((dims['time'],dims['frequency']))
                qc_levels,qc_details = self.AddQCvars(valval.shape)
                
                # if GWname in ['theta1','theta2','wave_mean_direction', 'stheta1','stheta2']
                if valval.dtype == 'object':
                    # Force type to float64 if object (presence of None within values)
                    tmp_field=Field(variable=varclass,dimensions=dims,values=valval,units=units_cv,qc_levels=qc_levels, qc_details=qc_details,\
                                     datatype=numpy.dtype(numpy.float64))
                else:
                    tmp_field=Field(variable=varclass,dimensions=dims,values=valval,units=units_cv,qc_levels=qc_levels, qc_details=qc_details)
                fields_sensor[varvar]=tmp_field
            else :
                print "Variable does not exist in source file : ",varvar
            
            # Add Distance to the shore
            varobj = Variable(
                    shortname='distance_to_shore',
                    description='distance to closest land area (shore or island)'
                    )
            fieldobj = Field(
                    varobj,
                    OrderedDict([('station', 1)]),
                    values=numpy.array(self.dist2shore, dtype='i4'),
                    units='m',
                    )
            fields_sensor['distance_to_shore'] = fieldobj
            
            
        for varvar in wanted_as_variable:
            # If non-directional, Dp (dominant_wave_direction not available : N/A)
            if spectrumType == 'non-directional' and varvar == 'dp':
                pass
            else:
                if varvar in corresp_dico:
                    corresponding_var = corresp_dico[varvar]
                else:
                    corresponding_var = varvar
                allinfo=VarConventions(corresponding_var)
                GWname,longname_cv,description_cv,sensor_cv,units_cv = allinfo
                logging.debug('declare 1DField for var %s %s',GWname,varvar)
                varclass=Variable(shortname=GWname, description=description_cv,standardname=longname_cv,authority =cfconvention.get_convention())
                qc_levels,qc_details = self.AddQCvars(big_table_metadata[varvar].shape)
                tmp_field=Field(variable=varclass,dimensions=dimstime,values=big_table_metadata[varvar],units=units_cv,qc_levels=qc_levels, qc_details=qc_details)
                fields_sensor[varvar]=tmp_field

        tmpTS = PointTimeSeries(
                                fields=fields_sensor,
                                longitude=lon,
                                latitude=lat,
                                depth=depth,
                                source=big_table_metadata['sourcefile'],
                                times=big_table_metadata['datetime'])            
        
        return tmpTS,CorrespondingFileName
        
    def Prepare_raw_data(self, dataSeriesPath,outputdir,filepattern):
        """
        Return the list of measurements read in the input file
        @type dataSeriesPath: string
        @param dataSeriesPath: full path to the input buoy data file or directory (for a single buoy)
        @rtype: list of TimeSeries
        @return: list of time series. A new TimeSeries is created each time the buoy location changes.
        """
        lineNumbr=0
        if os.path.isdir(dataSeriesPath):
            # filepattern : sp*
            filelist = glob.glob(dataSeriesPath+'/'+filepattern) #to ignore .Z files if any
            filelist.sort()
        elif os.path.isfile(dataSeriesPath):
            filelist=[dataSeriesPath]
        else:
            raise Exception("ERROR : file/directory not found")
        if filelist == []:
            return []
        self.history=['']
        filenames_container={}
        big_table_metadata,big_table_measures=self.initSeriesArray()
        self.spectrumType = 'unknown'
        cptfile=1
        
        for ii,dataFile in enumerate(filelist):
            breakSeries = False
            if os.path.isdir(dataSeriesPath):
                dataFile = os.path.join( dataSeriesPath,dataFile )
            self.dataFile = dataFile
            logging.debug('\n %s/%s file %s',cptfile,len(filelist),dataFile)
            f = open(dataFile, 'r')
            # Read header
            self.content=f.readlines()
            maxline=len(self.content)
            logging.debug('maxline %s',maxline)
            content = self.GetLine(lineNumbr)
            logging.debug('first line %s',content)
            breakSeries = self.readHeaderInformation()
            
            if breakSeries:
                #save the previous data in a TS
                logging.info('break in TS, register data to a given filename')            
                
                tmpTS,newfilename = self.CreateTimeSerie(big_table_measures, big_table_metadata, cptfile-1, allSamples,big_table_metadata['spectrumtype'][-1] ) ## CEDRIC 
                filenames_container[newfilename]=tmpTS
                cptfile=1
                #init a new TS
                logging.info('init a new TS for current data ')
                allSamples = self.readData(maxline)
                big_table_metadata,big_table_measures=self.initSeriesArray()
                big_table_measures,big_table_metadata=self.appendData2Timeserie(big_table_metadata, big_table_measures, allSamples)
            else:
                allSamples = self.readData(maxline)
                logging.debug('no break in TS, append data')
                big_table_measures,big_table_metadata=self.appendData2Timeserie(big_table_metadata, big_table_measures, allSamples)
                            
            #if it is the last file to process: save it
            if dataFile == filelist[-1]:
                logging.info('end of the TS, register data to a given filename')
                tmpTS,newfilename = self.CreateTimeSerie(big_table_measures,big_table_metadata,cptfile,allSamples, self.spectrumType)    # big_table_metadata['spectrumtype'][-1]
                filenames_container[newfilename] = tmpTS
            cptfile=cptfile+1
        logging.debug('end of Prepare_raw_data \n\n')
        
        return (filenames_container)
            
# =====================================================
# test /home/cercache/project/globwave/data/provider/cdip/2014/146/jan/01/
# =====================================================  
# /home/cercache/tools/environments/scientific_toolbox_cloudphys_precise/bin/python /home7/revolin/coriolis/ANTOINE/git/globwave/src/insitu/cdipmooredbuoy.py --inputdir /home/cercache/users/agrouaze/temporaire/cdip_test_data/  --outputdir /home/cercache/users/agrouaze/data/test_globe_wave_convert/
if __name__ == "__main__":
#     modedebug=False
    
    import argparse
    parser = argparse.ArgumentParser(description='CDIP buoy conversion to globwave')
    parser.add_argument('--verbose', action='store_true',default=False,help='turn on verbose mode and also DEBUG mode (no try/catch)')
    parser.add_argument('--inputdir', action='store',help='input directory (ex: /home/cercache/project/globwave/data/provider/cdip/2015/166/apr/01/ )',required=True)
    parser.add_argument('--outputdir', action='store',help='outputdir directory (ex: /tmp/ )',required=True)
    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG,format='%(asctime)s %(levelname)-5s %(message)s',
                        datefmt='%d/%m/%Y %I:%M:%S')
    else:
        logging.basicConfig(level=logging.INFO,format='%(asctime)s %(levelname)-5s %(message)s',
                        datefmt='%d/%m/%Y %I:%M:%S')
    
#     print 'modedebug',modedebug
#     if modedebug:
#         logging.basicConfig(level=logging.DEBUG,
#                         format='%(asctime)s %(levelname)-5s %(message)s',
#                         datefmt='%d/%m/%Y %I:%M:%S')
#     else:
#         logging.basicConfig(level=logging.INFO,
#                         format='%(asctime)s %(levelname)-5s %(message)s',
#                         datefmt='%d/%m/%Y %I:%M:%S')
#     if len(sys.argv) != 5 or sys.argv[1] != '--inputdir' or sys.argv[3] != '--outputdir':
#         logging.error("Wrong program call : python cdipmooredbuoy.py --inputdir <DIR> --outputdir <DIR>")
#         exit()
    srcFile = args.inputdir
    outputdir = args.outputdir
#     srcFile = sys.argv[2]
#     outputdir = sys.argv[4]
    
    buoy = CDIPMooredBuoy()
    
    timeseries = None
    filepattern='sp?????????????????'
#     if modedebug==False:
    if args.verbose==False: #on considere que si on est en mode non verbeux cest quon fait de lexploit et quon va vouloir que ca marche pour le maximum de fichier
        try:
            timeseries = buoy.Prepare_raw_data(srcFile,outputdir,filepattern)
        except Exception, e:
            logging.error(str(e))
    else:
        logging.debug('Prepare_raw_data')
        timeseries = buoy.Prepare_raw_data(srcFile,outputdir,filepattern)
    #buoyModel = None
    #idxSensor = -1
    if timeseries != None:
        logging.debug('number of files that will be created %s',len(timeseries))
        logging.debug('file names %s',timeseries)
        for i,filename in enumerate(timeseries):
            structure_data=timeseries[filename]
#            if len(buoy.history) != len(timeseries):
#                logging.error('buoy history %s',buoy.history)
#                raise Exception("History of platform has more entries than series")
#            platformHisto = buoy.history
            #overloadding of ncfile.py class object (cerbere) with additionnal metadata
            if buoy.providerId != '':
                #id = buoy.providerId
                naming ="CDIP"
            else:
                #id =  buoy.WMOId
                naming = "WMO"      
            metadata={}
            #metadata = { \
            metadata['id'] = buoy.WMOId
            metadata['naming_authority'] = naming
            metadata['wmo_id'] = buoy.WMOId              
            metadata['institution'] = 'Coastal Data Information Program'
            metadata['institution_abbreviation'] = 'CDIP'
            metadata['buoy_network'] = buoy.institution
            metadata['title'] = 'Wave Buoy observations from CDIP, provided for GlobWave project'
            metadata['summary'] = ''
            metadata['processor_version'] = PROCESSOR_VERSION
            metadata['provider'] = 'CDIP'
            metadata['platform_name'] = buoy.longName
            metadata['site_elevation'] = 0.
            metadata['cdm_feature_type'] = 'station'
            metadata['scientific_project'] = 'GlobWave'
            metadata['restrictions'] = 'Restricted to Ifremer and GlobWave usage'
            metadata['format_version']=  format_version
            metadata['history'] = '1.0 : Processing to GlobWave netCDF format'
            metadata['publisher_name'] = 'Ifremer/Cersat'
            metadata['publisher_url'] = 'http://cersat.ifremer.fr'
            metadata['publisher_email'] = 'jfpiolle@ifremer.fr'
            metadata['creator_url'] = 'http://cersat.ifremer.fr'
            metadata['creator_email'] = 'jfpiolle@ifremer.fr'
            metadata['date_modified'] = ''
            metadata['processing_software'] = 'Globwave python lib v1.0'
            metadata['references'] = ''
            metadata['data_source'] = 'CDIP archive'
            metadata['nominal_latitude'] = structure_data.get_lat()
            metadata['nominal_longitude'] = structure_data.get_lon()
            metadata['geospatial_lat_min'] = structure_data.get_lat()
            metadata['geospatial_lat_max'] = structure_data.get_lat()
            metadata['geospatial_lat_units'] = 'degree'
            metadata['geospatial_lon_min'] = structure_data.get_lon()
            metadata['geospatial_lon_max'] = structure_data.get_lon()
            metadata['geospatial_lon_units'] = 'degree'
            metadata['geospatial_vertical_min'] = 0.
            metadata['geospatial_vertical_max'] = 0.
            metadata['geospatial_vertical_units'] = 'meters above mean sea level'    
                       
            monthDir=os.path.dirname(filename)
            
            if not os.path.exists(monthDir):
                os.makedirs(monthDir)
            if os.path.exists(filename):
                os.remove( filename )
            ts_ncf = NCFile(url=filename, ncformat='NETCDF4', mode=WRITE_NEW)
            print "glagla",structure_data
            for kk in structure_data.get_fieldnames():
                print kk,structure_data.get_values(kk).shape,structure_data.get_values(kk)
            structure_data.save(ts_ncf, attrs=metadata)
            logging.info('file %s saved',filename)
    logging.info('end processing %s',srcFile)
    