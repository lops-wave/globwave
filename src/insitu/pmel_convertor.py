"""
@author: Antoine Grouazel
@purpose: convert pmel netcdf files into globwave format
15/06/2015: creation
24/08/2016: modification suggested by Bentamy to skip all oceano parameters except sst
 to avoid all issues concerning the resampling (indeed all atmo sensors are 10min sampling)
25/08/2016: decision to aggregate all sensor together
@note: no change of standard (variable name or units)
 only cut data into sensor directory and monthly dirs
 -it seems that files begining with met... are only gathering parameters existing besides
 
 si je ne prend que les 10min sample: -> solution tres simple mais perte d'interet quand on veut comparer des flux! (ce choix va avec le 1d only)
    ok10min ['sst', 'rain', 's', 'airt', 't', 'w', 'rh']
    not in 10min ['tau', 'bf', 'qnet', 'evap', 'rad', 'sss', 'swnetnc', 'lwnetnc', 'qsennc', 'ssd', 'qlatnc', 'rfnc', 'taunc', 'qsen', 'qlat', 'd', 'lwnet', 'evapnc', 'swnet', 'bfnc', 'qnetnc', 'lw', 'rf', 'empnc', 'bp', 'emp']
    how many byuoys
    total buoy distinct 135
    only 10min buoy distinct 103 (on perd 32 bouees)
probleme persistant: gestion des valeurs de temps en double ex 01:00:00 <-hourly et 01:00:00 <- from 10min ... (actuellement les valeur s ecrasent)

"""
import os
import logging
import math
import time
import glob
import sys
# sys.path.insert(0,'/home/losafe/users/agrouaze/git/cerbere')
# sys.path.insert(0,'/home/losafe/users/agrouaze/git/cerbere-utils')
# sys.path.insert(0,'/opt/python_venv/dev/lib/python2.7/site-packages/scipy')
import numpy
from scipy.interpolate import interp1d,PchipInterpolator,InterpolatedUnivariateSpline
# from scipy.interpolate import 
import netCDF4 as netcdf
import datetime
import collections
import calendar
import re
import pdb
import copy
from optparse import OptionParser
import traceback
from collections import OrderedDict
import cerbere
vers_file = os.path.join(os.path.dirname(os.path.dirname(cerbere.__file__)),'VERSION.txt')
version_cerbere = open(vers_file,'r').readlines()[0].replace('\n','')
from cerbere.datamodel.pointtimeseries import PointTimeSeries
# from cerbere.datamodel.sectiontimeseries import SectionTimeSeries
from cerbere.datamodel.pointcollection import PointCollection
from cerbere.datamodel.variable import Variable
from cerbere.datamodel.field import Field, QCLevel, QCDetail
# from cerbereutils.science.wave import r1r2_to_sth1sth2
from cerform.wave import r1r2_to_sth1sth2
from cerform import cfconvention
# from cerform.science import cfconvention
# from cerbereutils.science import cfconvention
from cerbere.mapper.abstractmapper import WRITE_NEW
from cerbere.mapper.ncfile import NCFile
from cerbere.geo.bathymetry import Bathymetry
# from cerbereutils.ancillary.landmask import Landmask
from ceraux.landmask import Landmask
from timeserieconcat import ConcatenateFiles, findContigousPlatformFiles, findWidderPlatformFile
from variables_conventions import VarConventions
from oceansitesbuoymooredbuoy import OceansitesMooredBuoy
from pmel_list_param import PARAM
from globwave.src.insitu.find_buoy_file.stat_pmel_provider import list_param
processor_version = '0.1'
STD_DEPTH = numpy.arange(2,1000,10)
# land_mask_file = '/home/losafe/users/agrouaze/git/cerbere-utils/cerbereutils/ancillary/resources/mwf-ers-mask.nc'
#PARAM links name of files with globwave sensors
#acro PMEL: acro LOPS,QC acro
DICO = {'T_20':('SST','QT_5020'),
        'T_25':('SST','QT_5025'),
         'RH_910':('RELH','QRH_5910'),
         'RN_485':('PRECIPIT_VOLUME','QRN_5485'),
         'AT_21':('AIRT','QAT_5021'),
         'S_41':('PSAL','QS_5041'),
         'WU_422':('EASTWS',None),
         'WV_423':('NORTHWS',None),
         'WS_401':('WSPD','QWS_5401'),
         'WD_410':('WDIR','QWD_5410'),
          }
VAR_NAMES = {
             'Ql_136':'LONGWAVE_UP_NET_RADIATIVE_FLUX',
             #to be continued!!!!
             }
TIME_UNIT = 'days since 1899-01-01 00:00:00'
class convertor(object):
    def __init__(self,input_dir,output_dir,date):
        self.dir_input = input_dir
        self.output_dir = output_dir
        self.date = date
        return
    
    
    
    def DefineGlobalMetaData(self,ifd,lon,lat):
        # Global metadata
        logging.info("global metadata copy")
        # ---------------
        globalmetadata = {}
        globalmetadata['array'] = ifd.getncattr('array')
#         for attr in ifd.ncattrs():
#             globalmetadata[attr] = ifd.getncattr(attr)
        globalmetadata['institution'] = 'Pacific Marine Environmental Laboratory NOAA'
        globalmetadata['institution_abbreviation'] = 'PMEL'
        globalmetadata['naming_authority'] = "WMO"
        globalmetadata['summary'] = "PMEL data from moored buoys"
        globalmetadata['title'] = "Meteorological and Oceanographic data at ocean surface and above"
        globalmetadata['cdm_feature_type'] = "station"
        globalmetadata['scientific_project'] = "GlobWave"
        globalmetadata['acknowledgement'] = "We are grateful to PMEL (NOAA) for providing these data to GlobWave project"
        globalmetadata['standard_name_vocabulary'] = "CF-1.6"
        globalmetadata['restrictions'] = "Restricted to Ifremer and GlobWave usage"
        globalmetadata['processing_software'] = "cerbere "+version_cerbere+" (CERSAT)"
        globalmetadata['processing_level'] = "0"
        globalmetadata['processor_version'] = processor_version
        globalmetadata['history'] = "1.0 : Processing to GlobWave netCDF format"
        globalmetadata['publisher_name'] = "Ifremer/Cersat"
        globalmetadata['publisher_url'] = "http://cersat.ifremer.fr"
        globalmetadata['publisher_email'] = "antoine.grouazel@ifremer.fr"
        globalmetadata['creator_name'] = "Antoine Grouazel"
        globalmetadata['creator_url'] = "http://cersat.ifremer.fr"
        globalmetadata['creator_email'] = "antoine.grouazel@ifremer.fr"
        globalmetadata['geospatial_lat_min'] = str(lat) 
        globalmetadata['geospatial_lat_max'] = str(lat) 
        globalmetadata['geospatial_lon_min'] = str(lon)
        globalmetadata['geospatial_lon_max'] = str(lon)
        if 'wmo_platform_code' in ifd.ncattrs():
            globalmetadata['wmo_id'] = ifd.wmo_platform_code
        globalmetadata['site_elevation'] = ''
        if not 'sea_floor_depth_below_sea_level' in globalmetadata:
            bathy = Bathymetry(filename='/home/cersat5/application_data/globwawe/static_file/bathy/GridOne.nc')
            floorDepth = -bathy.get_depth(lon,lat)
        else:
            floorDepth = ifd.sea_floor_depth_below_sea_level        
        globalmetadata['sea_floor_depth_below_sea_level'] = math.fabs(floorDepth)
        landmask_instance = Landmask()
        dist = int(landmask_instance.getDistanceToShore(lat,lon,filename='/home/losafe/users/agrouaze/git/cerbere/cerbere/geo/resources/NAVO-lsmask-world8-var.dist5.5.nc'))
#         dist = int(LandMask.getDistanceToShore(lat,lon,filename='/home/cercache/users/agrouaze/git/cerbere/cerbere/geo/resources/NAVO-lsmask-world8-var.dist5.5.nc'))
        globalmetadata['distance_to_shore']=str(dist)+'m'
        globalmetadata['nominal_latitude'] = lat
        globalmetadata['nominal_longitude'] = lon
        return globalmetadata
    

    
    def list_sensor(self,list_parameters):
        """
        from list param deduce the list of the sensor to treat
        """
        list_sensors = []
        for param in list_parameters:
            if param in PARAM.keys():
                logging.debug('%s -> %s',param,PARAM[param])
                sensor = PARAM[param][1]
                if sensor not in list_sensors:
                    list_sensors.append(sensor)
            else:
                logging.debug('%s -> %s',param,None)
        return list_sensors

    def get_lon_lat(self,handler):
        lons = handler.variables['lon'][0]
        lons = numpy.mod(lons,360)
#         lons[lons>180] = lons[lons>180] - 360.
        if lons>180:
            lons = lons-360.
        lats = handler.variables['lat'][0]
#         lats = numpy.squeeze(lats)
#         lons = numpy.squeeze(lons)
#         lons = lons[0]
#         lats = lats[0]
        return lons,lats
    
    def get_times(self,handler,start,stop):
        """
        return the times of a given file in datenum format
        """
        times_newformat = None
        indsta = None
        indsto = None
        
        start_measu_num = handler.variables['time'][0]
        stop_measu_num = handler.variables['time'][-1]
        src_unit = handler.variables['time'].units
        
        sta_num = netcdf.date2num(start,src_unit)
        sto_num = netcdf.date2num(stop,src_unit)
        flag_interset = False
        if (start_measu_num<=sto_num) and (start_measu_num>=sta_num):
            flag_interset = True
        if stop_measu_num<=sto_num and stop_measu_num>=sta_num:
            flag_interset = True
        if stop_measu_num>=sto_num and start_measu_num<=sta_num:
            flag_interset = True
        if flag_interset == True:
            times = handler.variables['time'][:]
            indsta = numpy.where(abs(times-sta_num)==numpy.min(abs(times-sta_num)))[0][0]
            indsto = numpy.where(abs(times-sto_num)==numpy.min(abs(times-sto_num)))[0][0]
    #         logging.debug('indsta %s indsto %s',indsta,indsto)
    #         logging.debug('times %s',times)
            if indsta!=indsto:
                times_dt = netcdf.num2date(times,src_unit)
                times_newformat =  netcdf.date2num(times_dt[indsta:indsto],TIME_UNIT)
        else:
#             times = handler.variables['time'][:]
#             times_dt = netcdf.num2date(times,src_unit)
            start_measu = netcdf.num2date(handler.variables['time'][0],src_unit)
            stop_measu = netcdf.num2date(handler.variables['time'][-1],src_unit)
            logging.debug('buoy starting measu: %s stoping :%s',start_measu,stop_measu)
        return times_newformat,indsta,indsto
    
    def get_buoy_identifier(self,handler):
        if 'wmo_platform_code' in handler.ncattrs():
            identifier = 'WMO'+str(handler.getncattr('wmo_platform_code'))
        else:
            identifier = handler.getncattr('platform_code')
        return identifier
    
    def get_depth(self,handler):
        z = handler.variables['depth'][:]#to fit to pointtime
        z  = numpy.reshape(z,(len(z),))
#         if z.size==1:
#             z = z[0]
#         vatime = 
#         time_field = Field(values=times,units=TIME_UNIT,variable=vatime,dimensions=collections.OrderedDict([('time',len(times))]))
        va_z = Variable(shortname='depth',description='depth',standardname='depth')
        z_dim = collections.OrderedDict([('z',len(z))])
        z_field = Field(values=z,units='m',variable=va_z,dimensions=z_dim)
        return z
    
    def define_output_filename(self,lon,lat,start,stop,buoy_id,instr=None):
        if lon < 0:
            strlon = "%.02fW" % abs(lon)
        else:
            strlon = "%.02fE" % lon
        if lat < 0:
            strlat = "%.02fS" % abs(lat)
        else:
            strlat = "%.02fN" % lat
        if instr is None:
            instr = ''
        outdir = os.path.join(self.output_dir, instr, start.strftime('%Y/%m'))
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        ofname =  outdir+'/'+buoy_id + '_%s_%s_Lat_%s_Lon_%s.nc' % \
                     (\
                      start.strftime('%Y%m%dT%H%M'),\
                      stop.strftime('%Y%m%dT%H%M'),\
                      strlat,
                      strlon)
        logging.debug(ofname)
        return ofname
    
    def interpolate_depth(self,depth_val,param_val):
        logging.debug('source depth %s',depth_val)
        for tt in range(param_val.shape[0]):
#             func_inter = PchipInterpolator(depth_val,param_val[tt,:],extrapolate=True)
#             func_inter = interp1d(depth_val,param_val[tt,:])
            func_inter = InterpolatedUnivariateSpline(depth_val, param_val[tt,:], k=1)
            tmp = func_inter(STD_DEPTH)
            if tt == 0:
                interpolated_param_val = tmp
            else:
                interpolated_param_val = numpy.ma.vstack([interpolated_param_val,tmp])
        
        return interpolated_param_val
    
    def copy_fields(self,ofields,handler,indice_start,indice_stop,times_glob,times_file):
        """
        organize the values and the fields resampled on a single time sampling scheme
        Args:
            ofields (list):
            handler (netcdf obj):
            indice_start (int):
            indice_stop (int):
            times_glob (ndarray):
            times_file (datetime array?):
        """
        fields = handler.variables.keys()
        fields.pop(fields.index('lon'))
        fields.pop(fields.index('lat'))
        fields.pop(fields.index('time'))
        fields.pop(fields.index('depth'))
        
        depth_val = handler.variables['depth'][:]
#         depth_length =  len(handler.dimensions['depth'])
        time_length = len(times_glob)
        for ff in fields:
#             logging.debug('field %s',ff)
#             pdb.set_trace()
            values = handler.variables[ff][indice_start:indice_stop,:,:,:]
            values = numpy.ma.squeeze(values)
            
            #reorganize data following final time vector
            if len(values.shape)==2:#time,depth
                interp_val = self.interpolate_depth(depth_val,values)
                blanko = numpy.ma.array(data=9999.*numpy.ones((len(times_glob),len(STD_DEPTH))),mask=numpy.ones((len(times_glob),len(STD_DEPTH))))
            else:
                blanko = numpy.ma.array(data=9999.*numpy.ones(len(times_glob)),mask=numpy.ones(len(times_glob)))
            ind_in_final = []
            for tt in times_file:
                ind_in_final.append(numpy.where(tt==times_glob)[0][0])
            if len(values.shape)==2:
                blanko[ind_in_final,:] = interp_val
                blanko.mask[ind_in_final,:] = False
            else:
                blanko[ind_in_final] = values
                blanko.mask[ind_in_final] = False
#             time_length = values.shape[0]
            dims_2d = collections.OrderedDict([('time',time_length),('depth',len(STD_DEPTH))])
            dims_1d = collections.OrderedDict([('time',time_length)])
            if len(values.shape)==1:
                dims = dims_1d
            else:
                dims = dims_2d
            lg = handler.variables[ff].long_name
            std = handler.variables[ff].generic_name
            name = handler.variables[ff].name
            vava = Variable(shortname=ff,standardname=std,description=lg)
            units = handler.variables[ff].units
            fifi = Field(dimensions=dims,variable=vava,units=units,values=blanko)
            if ff not in ofields.keys():
                ofields[ff] = fifi
        return ofields
    
    def copy_field_simplified(self,ofields,handler,indice_start,indice_stop):
        fields = handler.variables.keys()
        fields.pop(fields.index('lon'))
        fields.pop(fields.index('lat'))
        fields.pop(fields.index('time'))
        fields.pop(fields.index('depth'))
        
        depth_val = handler.variables['depth'][:]
        ind_surface = numpy.where(abs(depth_val-0)==numpy.amin(abs(depth_val-0)))
    #         depth_length =  len(handler.dimensions['depth'])
#         time_length = len(times_glob)
        for ff in fields:
    #             logging.debug('field %s',ff)
            raw_val = handler.variables[ff][:]
            if len(raw_val.shape)>1:
                values = raw_val[indice_start:indice_stop,ind_surface]
            else:
                values = raw_val[indice_start:indice_stop]
            values = numpy.ma.squeeze(values)
            
            #reorganize data following final time vector
#             if len(values.shape)==2:#time,depth
#                 interp_val = self.interpolate_depth(depth_val,values)
#                 blanko = numpy.ma.array(data=9999.*numpy.ones((len(times_glob),len(STD_DEPTH))),mask=numpy.ones((len(times_glob),len(STD_DEPTH))))
#             else:
#                 blanko = numpy.ma.array(data=9999.*numpy.ones(len(times_glob)),mask=numpy.ones(len(times_glob)))
#             ind_in_final = []
#             for tt in times_file:
#                 ind_in_final.append(numpy.where(tt==times_glob)[0][0])
#             if len(values.shape)==2:
#                 blanko[ind_in_final,:] = interp_val
#                 blanko.mask[ind_in_final,:] = False
#             else:
#                 blanko[ind_in_final] = values
#                 blanko.mask[ind_in_final] = False
    #             time_length = values.shape[0]
#             dims_2d = collections.OrderedDict([('time',values.shape[0]),('depth',len(STD_DEPTH))])
            dims_1d = collections.OrderedDict([('time',len(values))])
#             if len(values.shape)==1:
            dims = dims_1d
#             else:
#                 dims = dims_2d
            conv_info = VarConventions(DICO[ff][0],return_accro=True)
#             if conv_info is not None:
            vname,longname,description,sensor,stdunits,acronym = conv_info
            logging.debug('translate %s ->%s-> %s',ff,DICO[ff],vname)
            stdname = cfconvention.get_standardname(vname)
            #logging.debug("original name:",v,"globawave name", vname,'stdanard name: ',stdname)
                # attributs !!!
            varobj = Variable(shortname=acronym,
                                    description= longname,
                                    authority = cfconvention.get_convention(),
                                    standardname = stdname )
            units = handler.variables[ff].units
            if stdunits != units:
                logging.warn('units are not the same pmel: %s ifremer: %s',units,stdunits)
            lg = handler.variables[ff].long_name
            std = handler.variables[ff].generic_name
            name = handler.variables[ff].name
#             vava = Variable(shortname=ff,standardname=std,description=lg)
            raw_qc = handler.variables[DICO[ff][1]][:]
            if len(raw_val.shape)>1:
                qc_levels = raw_qc[indice_start:indice_stop,ind_surface]
            else:
                qc_levels = raw_qc[indice_start:indice_stop]
            fifi = Field(dimensions=dims,
                         variable=varobj,
                         qc_levels=qc_levels,
                         units=units,values=values,
                         attributes={
                                    'pmel_long_name':lg,
                                    "pmel_generic_name":std,
                                    'pmel_acronym':ff
                                                             })
            if ff not in ofields.keys():
                ofields[ff] = fifi
        return ofields
#     def list_buoys(self):
        
#         return list_buoys
    def list_files_containing_data_from_a_given_sensor(self,sensor,all_cdf_files):
        files_sensor = []
        for papa in PARAM.keys():
            if PARAM[papa][1]==sensor:
                
#                     files_sensor += glob.glob(os.path.join(self.dir_input,papa+'*.cdf'))
                for ff in all_cdf_files:
                    basenam_file = os.path.basename(ff)
                    m = re.search("\d", basenam_file)
                    file_sensor = basenam_file[0:m.start()]
                    if file_sensor == papa:
                        files_sensor.append(ff)
        return files_sensor

    def get_complete_timeline(self,list_files,first_dt,last_dt):
        """
        return metadata of a buoy (geolocation) and also a timeline including all the sample of the 
        files that intersect the date sought
        """
        flag_done = False
        times_file = {}
        files_sensor_buoy_date = []
        indices = {}
        lon = None
        lat = None
        buoy_id = None
        metadata = {}
        times = numpy.array([])
        for xha,file_inp in enumerate(list_files):
            handler = netcdf.Dataset(file_inp,mode='r')
            t0 = time.time()
            tmp_time,sta,sto = self.get_times(handler,first_dt,last_dt)
            print 'time to get time %s',time.time()-t0
            logging.debug('tmp_time: %s',tmp_time)
            if tmp_time is not None: #meaning the date sought intersect with the period sampled by the sensor
                if flag_done==False:
                    
                    lon,lat = self.get_lon_lat(handler)
                    metadata = self.DefineGlobalMetaData(handler,lon,lat)
                    buoy_id = self.get_buoy_identifier(handler)
                    flag_done = True
#                         depths_tmp = self.get_depth(handler)
#                         if len(depths_tmp)>len(depths):
#                             depths = depths_tmp
                times_file[file_inp] = tmp_time
                indices[file_inp] = (sta,sto)
                times = numpy.union1d(times,tmp_time)
#                         logging.debug('time length %s',len(times))
                files_sensor_buoy_date.append(file_inp)
            handler.close()
        return files_sensor_buoy_date,indices,times,lon,lat,buoy_id,metadata,times_file
    
    def orchestrator_nosensor(self,pattern_file=None):
        """
        same has orchestrator but without sensor separation
        treat all the files in a given input directory
        date_sought (str) YYYYMM
        Args:
            pattern_file (str): pattern of the input file to be used (mainly used to get a specific sampling rate)
        """
        counters = collections.defaultdict(int)
        counters['nber_files_written'] = 0
        if pattern_file is None:
            pattern_file = '*10m.cdf' #previously *.cdf"
        date_sought = self.date
        year_int = int(date_sought[0:4])
        month_int = int(date_sought[4:6])
        first_dt = datetime.datetime(year_int,month_int,1)
        first,last = calendar.monthrange(year_int,month_int)
        last_dt = datetime.datetime(year_int,month_int,last)
        list_parameters,list_buoys = list_param(self.dir_input,pattern_file=pattern_file)
        all_cdf_files = glob.glob(os.path.join(self.dir_input,pattern_file))
#         if sensor_wanted is not None:
#             sensors = [sensor_wanted]
#         else:
#             sensors = self.list_sensor(list_parameters)
#         for sens in sensors:
#             logging.info('sensor: %s',sens)
#         files_sensor = self.list_files_containing_data_from_a_given_sensor(sens,all_cdf_files)
        for vbvb,buoy in enumerate(list_buoys):
            logging.info('buoy %s %s/%s',buoy,vbvb,len(list_buoys))
            
#                 depths = []
            ofields = collections.OrderedDict()
#             files_sensor_buoy = []
            files_buoy = []
            for ff in all_cdf_files:
                if buoy in ff:
                    files_buoy.append(ff)
#             for bb in files_sensor:
#                 if buoy in bb:
#                     files_sensor_buoy.append(bb)
            logging.debug('files of buoy %s: %s',buoy,files_buoy)
            if files_buoy != []:
                interesting_files,indices,times,lon,lat,buoy_id,metadata,times_file = self.get_complete_timeline(files_buoy,first_dt,last_dt)
                
                logging.debug('end of first loop over buoys')
                logging.info('Nber of files for buoy :%s date: %s = %s',buoy,self.date,len(interesting_files))
                if interesting_files != []:
                    for file_inp in interesting_files:
                        handler = netcdf.Dataset(file_inp,mode='r')
                        if pattern_file == '*10m.cdf':
                            ofields = self.copy_field_simplified(ofields,handler,indices[file_inp][0],indices[file_inp][1],times_file[file_inp])
                        else:
                            ofields = self.copy_fields(ofields,handler,indices[file_inp][0],indices[file_inp][1],times,times_file[file_inp])
                        
                        handler.close()
                    outpath = self.define_output_filename(lon,lat,first_dt,last_dt,buoy_id)
                    if os.path.exists(outpath)==True:
                        os.remove(outpath)
                    vatime = Variable(shortname='time',description='time',standardname='time')
                    time_field = Field(values=times,units=TIME_UNIT,variable=vatime,dimensions=collections.OrderedDict([('time',len(times))]))
                    va_depth = Variable(shortname='depth',description='depth down positive',standardname='depth')
                    depth_field = Field(values=STD_DEPTH,units='m',variable=va_depth,dimensions=collections.OrderedDict([('z',len(STD_DEPTH))]))
                    ts = PointTimeSeries( fields = ofields,
                                    longitude = lon,
                                    latitude = lat,
                                    depth = 0,
                                    times = time_field)
#                     ts = SectionTimeSeries( fields = ofields,
#                                     longitude = lon,
#                                     latitude = lat,
#                                     depths = STD_DEPTH,
#                                     times = time_field)
                    new_handler = NCFile(outpath,mode='w')
                    logging.debug('ts before writting %s',ts)
#                     for ff in ts.get_fieldnames():
#                         print ts.get_field(ff).variable.shortname
                    ts.save(new_handler,metadata)
                    new_handler.close()
                    counters['nber_files_written'] += 1
                    logging.info(' %s ',outpath)
                else:
                    logging.info('no intersecting data within the month: %s',date_sought)
        logging.info('counters : %s',counters)
        return


    def orchestrator(self,sensor_wanted=None,pattern_file=None):
        """
        treat all the files in a given input directory
        date_sought (str) YYYYMM
        """
        if pattern_file is None:
            pattern_file = '*10m.cdf' #previously *.cdf"
        date_sought = self.date
        year_int = int(date_sought[0:4])
        month_int = int(date_sought[4:6])
        first_dt = datetime.datetime(year_int,month_int,1)
        first,last = calendar.monthrange(year_int,month_int)
        last_dt = datetime.datetime(year_int,month_int,last)
        list_parameters,list_buoys = list_param(self.dir_input,pattern_file=pattern_file)
        all_cdf_files = glob.glob(os.path.join(self.dir_input,pattern_file))
        if sensor_wanted is not None:
            sensors = [sensor_wanted]
        else:
            sensors = self.list_sensor(list_parameters)
        for sens in sensors:
            logging.info('sensor: %s',sens)
            files_sensor = self.list_files_containing_data_from_a_given_sensor(sens,all_cdf_files)
            for vbvb,buoy in enumerate(list_buoys):
                logging.info('buoy %s %s/%s',buoy,vbvb,len(list_buoys))
                
#                 depths = []
                ofields = collections.OrderedDict()
                files_sensor_buoy = []
                for bb in files_sensor:
                    if buoy in bb:
                        files_sensor_buoy.append(bb)
                logging.debug('files of buoy %s, that will be merge into %s param files: %s',buoy,sens,files_sensor_buoy)
               
                flag_done = False
                if files_sensor_buoy != []:
                    flag_done,files_sensor_buoy_date,indices,times,lon,lat,buoy_id,metadata,times_file = self.get_complete_timeline(files_sensor_buoy,first_dt,last_dt,flag_done)
                    logging.debug('end of first loop over buoys')
                    logging.info('Nber of files for sensor: %s buoy :%s date: %s = %s',sens,buoy,self.date,len(files_sensor_buoy_date))
                    if files_sensor_buoy_date != []:
                        for file_inp in files_sensor_buoy_date:
                            handler = netcdf.Dataset(file_inp,mode='r')
                            if pattern_file == '*10m.cdf':
                                ofields = self.copy_field_simplified(ofields,handler,indices[file_inp][0],indices[file_inp][1],times_file[file_inp])
                            else:
                                ofields = self.copy_fields(ofields,handler,indices[file_inp][0],indices[file_inp][1],times,times_file[file_inp])
                            
                            handler.close()
                        outpath = self.define_output_filename(lon,lat,first_dt,last_dt,buoy_id,instr=sens)
                        if os.path.exists(outpath)==True:
                            os.remove(outpath)
                        vatime = Variable(shortname='time',description='time',standardname='time')
                        time_field = Field(values=times,units=TIME_UNIT,variable=vatime,dimensions=collections.OrderedDict([('time',len(times))]))
                        va_depth = Variable(shortname='depth',description='depth down positive',standardname='depth')
                        depth_field = Field(values=STD_DEPTH,units='m',variable=va_depth,dimensions=collections.OrderedDict([('z',len(STD_DEPTH))]))
                        ts = PointTimeSeries( fields = ofields,
                                        longitude = lon,
                                        latitude = lat,
                                        depth = 0,
                                        times = time_field)
    #                     ts = SectionTimeSeries( fields = ofields,
    #                                     longitude = lon,
    #                                     latitude = lat,
    #                                     depths = STD_DEPTH,
    #                                     times = time_field)
                        new_handler = NCFile(outpath,mode='w')
                        logging.debug('ts before writting %s',ts)
    #                     for ff in ts.get_fieldnames():
    #                         print ts.get_field(ff).variable.shortname
                        ts.save(new_handler,metadata)
                        new_handler.close()
                        logging.info(' %s ',outpath)
                    else:
                        logging.info('no intersecting data within the month: %s',date_sought)
        return
if __name__ == '__main__':
    input_dir = '/home/cercache/project/globwave/data/provider/pmel/unziped/'
    output_dir = '/home/cercache/project/globwave/data/globwave/pmel/'
    parser = OptionParser()
    parser.add_option("-d","--date",
                      action="store", type="string",
                      dest="date", metavar="string",
                      help="month to convert YYYYMM")
    parser.add_option("-e","--exploit",
                      action="store_true", default=False,
                      dest="exploit",
                      help="exploitation mode")
    parser.add_option("-q","--quiet",
                      action="store_true", default=False,
                      dest="quiet",
                      help="quiet mode")
    parser.add_option("-o","--outputdir",
                      action="store", type="string",
                      dest="outputdir", metavar="string",
                      help="directory where will be created the sub arborescence sensor/YYYY/MM/file.nc [optional]")
    parser.add_option("-s","--sensor",
                      action="store", type="string",
                      dest="sensor", metavar="string",
                      help="sensor to analyse (ex: ocean_temperature_sensor)")
    parser.add_option("-i","--input-file",
                      action="store", type="string",
                      dest="input", metavar="string",
                      help="input file to convert (basename only) [optional, default = *.cdf in %s]"%input_dir)
    (options, args) = parser.parse_args()
    if options.quiet==True:
        logging.basicConfig(level=logging.INFO)
    else:
        logging.basicConfig(level=logging.DEBUG)
    if options.exploit ==True:
        date_wanted  = datetime.datetime.now().strftime('%Y%m')
    else:
        date_wanted = options.date
    if options.outputdir is None:
        out_d = output_dir
    else:
        out_d = options.outputdir
    inst = convertor(input_dir,output_dir=out_d,date=date_wanted)
#     inst.orchestrator(sensor_wanted=options.sensor,pattern_file=options.input)
    inst.orchestrator_nosensor(pattern_file=options.input)
    logging.info('end of script')