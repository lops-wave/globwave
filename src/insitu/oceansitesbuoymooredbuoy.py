"""
@author: Antoine Grouazel
@purpose: convert buoy data in oceansites netcdf format from myocean (CORIOLIS database) project to globwave netcdf format
#30/01/2014 agrouaze SPT file taken into account
#06/05/2014 agrouaze correct bugs when multi Z levels + handle files comming from latest dir (daily processing)
#23/10/2014 agrouaze split each action to ease debuging and processor_version
#17/12/2014 agrouaze correct time_coverage_end attribut
#31/03/2015 agrouaze use variables_convention.py script to standardize the variables v0.6
#01/04/2015 agrouaze choose to have a unique file per month per buoy (no cut) v0.7
#09/04/2015 agrouaze handle 2D variables to be copied entierely into the new file v0.8
#09/07/2015 agrouaze choose to avoid all wave data that are not surface data when DEPTH dimension is != 1, and squeeze when ==1 
#06/01/2015 agrouaze do not create file is all the variables contains masked values
#29/03/2016 agrouaze hide some warning for units that are no written in the same way
#30/03/2016 agrouaze use the DICO var as a proxy to avoid adding synonyms in variables_conventions
"""
import os
import logging
import math
import traceback
import glob
import sys
import numpy
import netCDF4 as netcdf
import string
import collections
import pdb
import copy
from optparse import OptionParser
from cerbere.datamodel.pointtimeseries import PointTimeSeries
from cerbere.datamodel.variable import Variable
from cerbere.datamodel.field import Field, QCLevel, QCDetail
# import cerbere.science.cfconvention
# import cerbere.science.wave
# from cerbereutils.science.wave import r1r2_to_sth1sth2
from cerform.wave import r1r2_to_sth1sth2
from shared_convert_methods import GetFinalNameOfTheBuoy,treshold_to_create_new_file,Define_output_filename,time_units
# from cerbereutils.science import cfconvention
from cerform import cfconvention
from cerbere.mapper.abstractmapper import WRITE_NEW
from globwave.src.insitu.globwave_shared_infos import DetermineInstrumentGroups,DICO
from cerbere.mapper.ncfile import NCFile
# from cerbere.geo.bathymetry import Bathymetry
# from cerbere.geo.landmask import LandMask
from ceraux.landmask import Landmask
from ceraux.landmask_resources import LAND_MASK_PATH
from time import gmtime, strftime
from timeserieconcat import ConcatenateFiles, findContigousPlatformFiles, findWidderPlatformFile
from variables_conventions import VarConventions,freqDim,freqVarName,freqRangeName
from collections import OrderedDict
from globwave.src.insitu.read_buoys_data.coriolis_common_create_field import UNITS,CreateFieldcomon,WriteGWOceansiteFile,DefineGlobalMetaData
processor_version = '0.8'
unwanted_files = ['TS_RF','TS_DB']
FLAGS_GEN = []        
#gathering dico INSUTRMENTS and CERSAT_NAMES in one unique table
#DICO gives the traduction of the coriolis variables name into the LOPS vocabulary
#this is to avoid changing a lot of things into variables conventions



# UNWANTEDVAR = ['TIME','DC_REFERENCE','LATITUDE','DEPTH','DEPH','LONGITUDE','POSITIONING_SYSTEM','SVEL','DATA_MODE_CORA','DIRECTION']





EXCEPT_LIST = "/home/cercache/users/agrouaze/resultats/globwave_monitore/list_oceansites_files_not_converted_"+strftime("%Y%m%d_%Hh", gmtime())+".txt"
TREATED_FILES = "/home/cercache/users/agrouaze/resultats/globwave_monitore/list_oceansites_files_converted_"+strftime("%Y%m%d_%Hh", gmtime())+".txt"
NOT_STABLE_FILES = "/home/cercache/users/agrouaze/resultats/globwave_monitore/list_oceansites_files_not_stable_enough_"+strftime("%Y%m%d_%Hh", gmtime())+".txt"
class OceansitesMooredBuoy(object):
    '''
    class to gather all the function to convert oceansites buoys netCDF file
    '''
    _bathymetry = None
    #logging.basicConfig(filename='example.log',level=logging.INFO)
    #logging.basicConfig(filename='/home/cercache/users/agrouaze/temporaire/oceansite_globwave.log',level=logging.DEBUG,filemode='w', format='%(asctime)s %(levelname)s %(message)s')
    
    def __init__(self):                 
        pass

#     def get_cersatname( self, vname ):
#         #return CERSAT_NAMES[vname]
#         return DICO[vname][0]
#     get_cersatname=classmethod(get_cersatname)
#     
#     def get_instrument( self, vname ):
#         #return INSTRUMENTS[vname]
#         return DICO[vname][1]
#     get_instrument=classmethod(get_instrument)
    
    def get_units(self, units):
        '''attribute the convention on unit naming'''
        if UNITS.has_key(units):
            return UNITS[units]
        elif units in UNITS.values():
            return units
        elif units == ' ':
            return 'unitless'
        else:
            
            logging.warning('unknown units : %s',units)
            return units
#             raise Exception()
            #return units
#     get_units=classmethod(get_units)
            
#             for root, dirnames, filenames in os.walk(thedir):
#                 for filename in fnmatch.filter(dirnames,patt):
            #match.append(os.path.join(root, filename))
            
    def CutTimeSerie(self,tmask,times):
        ''' determine data intervals within the timeserie'''
        # ---------------------
#            if (tmask==False): 
        if tmask.size == 1:                
            masked_times  = numpy.ma.array( times, copy=True, mask=False )
        else:
            masked_times  = numpy.ma.array( times, copy=True, mask=tmask )
            
        intervals = []
        i0 = 0
        i1 = 1
        # Beyond 24 hours, another file is created
        for i,t in enumerate(masked_times):
            if masked_times.mask[i0]:
                i0 = i
                i1 = i + 1
            elif masked_times.mask[i]:
                pass
            elif (t - masked_times[i1-1]).total_seconds() > 60*86400 or t.month != masked_times[i1-1].month:
                intervals.append( (i0,i1) )
                i0 = i
                i1 = i+1
            else:
                i1 = i+1
        # last interval or no interval
        intervals.append( (i0,i1) )
        return intervals,masked_times
#     CutTimeSerie = classmethod(CutTimeSerie)
    
    def UseFlagFromProvider(self,ifd):
        ''' validate data with QC flag from file and return a mask '''
        # ------------------------------------
        fmaskSuspect = None
        timeSize = ifd.variables['TIME'].size
        for v in FLAGS_GEN:
            #logging.debug("Flags : ",v)
            
            # RULE : Flag position, time, depth  equal to 3, 4, 5, 8 and 9 => suspect 
            # ------
            # flag 0, 1, 2, 7 => good
            # fmaskSuspect ON ONE AXIS ONLY
            #if ifd.variables[v].ndim == 1:
            if ifd.variables[v].ndim == 1 and v[0:2] != 'DEP': #ag we dont want to use Z flags because it is sometime suspect flag on level above surface
                if ifd.variables[v].size == timeSize :
                    fmask3 = numpy.ma.masked_greater_equal(ifd.variables[v][:],3) # flag >= 3  suspect a true  
                    fmask7 = numpy.ma.masked_not_equal(ifd.variables[v][:],7)
                    
                    if fmaskSuspect == None:
                        fmaskSuspect = numpy.ma.make_mask( fmask3.mask & fmask7.mask , shrink=False)       # minus flag 7
                    else:
                        fmaskSuspect = numpy.ma.make_mask( fmaskSuspect | numpy.ma.make_mask( fmask3.mask & fmask7.mask , shrink=False) , shrink=False)
                else:
                    # suspect flags 
                    if ifd.variables[v][0] in [3, 4, 5, 8, 9]:
                        fmask = numpy.ma.getmaskarray(numpy.ma.masked_all((timeSize,)) )  # all positions suspect
                        if fmaskSuspect == None:                                
                            fmaskSuspect = fmask
                        else:                    
                            fmaskSuspect = numpy.ma.make_mask( fmaskSuspect | fmask , shrink=False)
            else:                    
                logging.debug(" flag_qc dimension not supported or Z flags not used by purpose %s" % v)
            
            # transpose to 2 axis
        fs = numpy.ma.expand_dims(fmaskSuspect, axis=1)
        return fs,fmaskSuspect
#     UseFlagFromProvider = classmethod(UseFlagFromProvider)
        
#     def DefineGlobalMetaData(self,ifd,f,lon,lat):
#         # Global metadata
#         logging.debug("global metadata copy")
#         # ---------------
#         globalmetadata = {}
#         for attr in ifd.ncattrs():
#             globalmetadata[attr] = ifd.getncattr(attr)
#         globalmetadata['institution'] = ifd.getncattr('institution')
#         globalmetadata['institution_abbreviation'] = ''
#         globalmetadata['naming_authority'] = "WMO"
#         globalmetadata['summary'] = "MyOcean data in OceanSITES format obtained from moored buoys"
#         globalmetadata['title'] = "Meteorological and Oceanographic data obtained from the MyOcean project, collected for GlobWave project"
#         globalmetadata['cdm_feature_type'] = "station"
#         globalmetadata['scientific_project'] = "GlobWave"
#         globalmetadata['acknowledgement'] = "We are grateful to MyOcean for providing these data to GlobWave project"
#         globalmetadata['standard_name_vocabulary'] = "CF-1.6"
#         globalmetadata['restrictions'] = "Restricted to Ifremer and GlobWave usage"
#         globalmetadata['processing_software'] = "cerbere 1.0 (CERSAT)"
#         globalmetadata['processing_level'] = "0"
#         globalmetadata['processor_version'] = processor_version
#         globalmetadata['history'] = "1.0 : Processing to GlobWave netCDF format"
#         globalmetadata['publisher_name'] = "Ifremer/Cersat"
#         globalmetadata['publisher_url'] = "http://cersat.ifremer.fr"
#         globalmetadata['publisher_email'] = "antoine.grouazel@ifremer.fr"
#         globalmetadata['creator_name'] = "Antoine Grouazel"
#         globalmetadata['creator_url'] = "http://cersat.ifremer.fr"
#         globalmetadata['creator_email'] = "antoine.grouazel@ifremer.fr"
#         globalmetadata['geospatial_lat_min'] = ifd.getncattr('geospatial_lat_min') 
#         globalmetadata['geospatial_lat_max'] = ifd.getncattr('geospatial_lat_max') 
#         globalmetadata['geospatial_lon_min'] = ifd.getncattr('geospatial_lon_min')
#         globalmetadata['geospatial_lon_max'] = ifd.getncattr('geospatial_lon_max')
#         globalmetadata['geospatial_vertical_min'] = ifd.getncattr('geospatial_vertical_min')
#         globalmetadata['geospatial_vertical_max'] = ifd.getncattr('geospatial_vertical_max') 
#         globalmetadata['time_coverage_start'] = ifd.getncattr('time_coverage_start')
#         globalmetadata['time_coverage_end'] = ifd.getncattr('time_coverage_end')
# #         if 'Z' in ifd.getncattr('time_coverage_end'):
# #             coverage_end = datetime.datetime.strptime( ifd.getncattr('time_coverage_end'),'Y%m%dT%H%M%SZ')
# #         else:
# #             coverage_end = datetime.datetime.strptime( ifd.getncattr('time_coverage_end'),'Y%m%dT%H%M%S')
# #         globalmetadata['time_coverage_end'] = coverage_end
# #         globalmetadata['data_source'] = os.path.basename(f)
#         globalmetadata['source'] = os.path.basename(f)
#         if 'wmo_platform_code' in globalmetadata:
#             if ifd.wmo_platform_code != "no_value" or ifd.wmo_platform_code==' ':
#                 globalmetadata['wmo_id'] = ifd.wmo_platform_code
#             else :
#                 globalmetadata['wmo_id'] = ''
#             del globalmetadata['wmo_platform_code'] #useless since we have wmo_id
#         else:
#             globalmetadata['wmo_id'] = ''
#         globalmetadata['id'] = ifd.site_code 
#         globalmetadata['platform_code'] = ifd.platform_code
#         globalmetadata['site_elevation'] = ''
#         if 'platform_name' in ifd.ncattrs():
#             globalmetadata['station_name'] = ifd.platform_name
#         else:
#             globalmetadata['station_name'] = ''
#             
#         # voir avec JFP :             sea_floor_depth_below_sea_level ?
#         if not 'sea_floor_depth_below_sea_level' in globalmetadata:
#             if self._bathymetry == None:
#                 #self._bathymetry = Bathymetry(filename='/home/cercache/users/agrouaze/git/cerbere/cerbere/geo/resources/GridOne.nc')
#                 self._bathymetry = Bathymetry(filename='/home/cersat5/application_data/globwawe/static_file/bathy/GridOne.nc')
#             floorDepth = -self._bathymetry.get_depth(lon,lat)
#         else:
#             floorDepth = ifd.sea_floor_depth_below_sea_level        
#         globalmetadata['sea_floor_depth_below_sea_level'] = math.fabs(floorDepth)
#         landmask_instance = Landmask(os.path.join(LAND_MASK_PATH,'landmask_025.nc'))
#         dist = int(landmask_instance.getDistanceToShore(lat,lon,filename=os.path.join(LAND_MASK_PATH,'NAVO-lsmask-world8-var.dist5.5.nc')))
# #         dist = int(LandMask.getDistanceToShore(lat,lon,filename='/home/cercache/users/agrouaze/git/cerbere/cerbere/geo/resources/NAVO-lsmask-world8-var.dist5.5.nc'))
#         globalmetadata['distance_to_shore']=str(dist)+'m'
#         globalmetadata['nominal_latitude'] = lat
#         globalmetadata['nominal_longitude'] = lon
#         return globalmetadata
    
#     def DetermineInstrumentGroups(self,ifd):
#         # determine instrument groups
#         logging.debug("determine instrument groups ")
#         groups = {}
#         for v in ifd.variables.keys():
#             #if not v in UNWANTED and not v in FLAGS_GEN:
#             logging.debug('variables found %s',v)
#             if v not in UNWANTEDVAR and v[-3:]!='_QC' and  v[-3:] != '_DM' and v[0:4]!='GPS_':
#                 v_std = DICO[v][0]
#                 logging.debug('%s -> %s',v,v_std)
#                 res = VarConventions(v_std)
#                 if res is not None:
#                     GLOBEWAVE_input_var_name,longname,description,sensor,units = res
#     #             if v in DICO and not v in FLAGS_GEN:
#                     #logging.debug("wanted", v)
#     #                 sensor = self.get_instrument(v)
#                     if groups.has_key(sensor):
#                         groups[sensor].append(v)
#                     else:
#                         groups[sensor] = [v]
#                 else:
#                     logging.warning('variable %s (comming from %s) is not referenced in dictionary ',v_std,v)
#         return groups
#     DetermineInstrumentGroups = classmethod(DetermineInstrumentGroups)
    
    def ReadGeolocationInformations(self,ifd,filename):
        '''read times,lon,lat,depth,and assoicated QC position ...'''
        num_dates = ifd.variables['TIME'][:]
        if time_units != ifd.variables['TIME'].units:
            logging.debug('change time units to globwave units')
            dates_dt = netcdf.num2date(num_dates,ifd.variables['TIME'].units)
            num_dates = netcdf.date2num(dates_dt,time_units)
        num_dates.sort()
        times = netcdf.num2date(num_dates,ifd.variables['TIME'].units)
        
        logging.debug("find lon and lat ")
        if len(ifd.dimensions['LATITUDE']) != 1 or len(ifd.dimensions['LONGITUDE']) != 1 or len(ifd.dimensions['POSITION']) != 1 :
            # add filename to except file list
            
            #logging.warning("Latitude or longitude (or position) dimension not fixed. Add new file %s to except file list" % f)
            #list = open(EXCEPT_LIST, 'a')
            #list.write(f+'\n')
            #list.close()
            #raise Exception
            #ifd.close()
            if any(ifd.variables['LATITUDE'][:]!=ifd.variables['LATITUDE'][0]) or any(ifd.variables['LONGITUDE'][:]!=ifd.variables['LONGITUDE'][0]):
            #check that the position doesnt change too much in the month
                if numpy.std(ifd.variables['LATITUDE'][:])<0.1 and  numpy.std(ifd.variables['LONGITUDE'][:])<0.1:
                    lat = numpy.mean(ifd.variables['LATITUDE'][:])
                    lon = numpy.mean(ifd.variables['LONGITUDE'][:])
                else:
                    logging.warning('position are not stable enough for file %s',filename)
                    fid = open(NOT_STABLE_FILES,'a')
                    fid.write(filename+'\n')
                    fid.close()
                    return
#                       raise Exception
            else:
                lat = ifd.variables['LATITUDE'][0]
                lon = ifd.variables['LONGITUDE'][0]
            # Move treated file
            #shutil.move(f, TREATED_FILES)
            
            #return
        else:
            lat = ifd.variables['LATITUDE'][0]
            lon = ifd.variables['LONGITUDE'][0]
        #TODO add a check between GPS_LATITUDE and LATITUDE 
        # DEPTH or DEPH or PRES?
        logging.debug('lon: %s',lon)
        logging.debug('lat: %s',lat)
        logging.debug("find Z dimension ")
        logging.debug("find Z variable ")
        if 'DEPTH' in ifd.variables.keys():
            depth = ifd.variables['DEPTH'][0]
            if isinstance(depth,numpy.ndarray):
                depth = depth[0]
#             depth = numpy.squeeze(depth)
#             depth = numpy.reshape(depth,(1,))
            logging.debug('depth shape %s',depth)
            FLAGS_GEN = ['TIME_QC','POSITION_QC','DEPTH_QC'] # Flags to be applied to all values
        elif 'DEPH' in ifd.variables.keys():
            depth = ifd.variables['DEPH'][0,0]
            #depth=depth.T
            FLAGS_GEN = ['TIME_QC','POSITION_QC','DEPH_QC'] # Flags to be applied to all values
        else :
            # no information about instrument depth
            depth = 0
            FLAGS_GEN = ['TIME_QC','POSITION_QC'] # Flags to be applied to all values

        position_qc = ifd.variables['POSITION_QC'][0]
        return FLAGS_GEN,position_qc,lon,lat,times,depth
#     ReadGeolocationInformations=classmethod(ReadGeolocationInformations)
    
    def CreateTimeMaskInstrument(self,ifd,instr,groups):
        '''create a time mask to determine period without any data for this instrument
        (we cumulate the mask of each sensor)'''
        tmask = None
        for v in groups[instr]:
                #logging.debug(v)
            logging.debug("create time mask for instrument %s",instr)
            if ifd.variables[v].ndim == 1:
                if tmask is None:
                    tmask = numpy.ma.getmask(ifd.variables[v][:])
                else:
                    tmask = numpy.ma.make_mask( tmask & numpy.ma.getmask(ifd.variables[v][:]), shrink=False )
            if ifd.variables[v].ndim == 2:
                logging.debug("time mask: 2D variables ")
                if tmask is None:
                    #tmask = numpy.ma.getmask(ifd.variables[v][:])
                    tmask = numpy.ma.MaskedArray(ifd.variables[v][:]).all(axis=1).mask
                else:
                    # cumulating masks
                    #tmask = numpy.ma.make_mask( tmask & numpy.ma.getmask(ifd.variables[v][:]) , shrink=False)
                    tmask = numpy.ma.make_mask( tmask & numpy.ma.MaskedArray(ifd.variables[v][:]).all(axis=1).mask, shrink=False)
                
            if ifd.variables[v].ndim == 3:                    
                logging.warning("Error : 3D variable not supported !")
                raise Exception
        return tmask
#     CreateTimeMaskInstrument = classmethod(CreateTimeMaskInstrument)
    
    def BuildQualityFields(self,ifd,variable,intstart,intend,fs,fmaskSuspect,masked_times,dims,sensor):
        ''' Build quality fields, qc_levels and qc_details'''
        # --------------------
        #vals = numpy.ma.empty((intend - intstart,), dtype='i1')
#         variable_nb_dimension = ifd.variables[variable].ndim
        variable_nb_dimension = len(dims)
        
        if string.join((variable,'_QC'),'') in ifd.variables.keys():
#                            var_qc = ifd.variables[string.join((variable,'_QC'),'')][:]

#             var_qc = ifd.variables[string.join((variable,'_QC'),'')][intstart:intend][:,0:1]
            if len(ifd.variables[string.join((variable,'_QC'),'')][:].squeeze().shape)>1:
                var_qc = ifd.variables[string.join((variable,'_QC'),'')][intstart:intend,0]
            else:
                var_qc = ifd.variables[string.join((variable,'_QC'),'')][intstart:intend]
            var_qcSize = var_qc.size # for if conditions
            # initial array
            vals = numpy.ma.array(var_qc ,dtype='i1')                                                
            mask012 = numpy.ma.masked_outside(vals,0,2) # mask for oceansites qc 0, 1 , 2
            #mask7=numpy.ma.masked_not_equal(var_qc,7) # mask for oceansites qc 7       # NOT relevant                  
            mask34 = numpy.ma.masked_outside(vals,3,4) # mask for oceansites qc 3, 4
            mask5789 = numpy.ma.masked_outside(vals,5,9) # mask for oceansites qc 5,7,8,9 

            # convert initial flag array
            vals[numpy.ma.where(mask012)] = 4  # which is not masked converts to 4
            vals[numpy.ma.where(mask34)] = 2
##                        if mask7.mask.size == var_qcSize: vals[numpy.ma.where(mask7)]=4  # NOT relevant
            vals[numpy.ma.where(mask5789)] = 3
                
            # fmaskSuspect was transposed => fs
            if fmaskSuspect != None:
                # RULE : if varflag good (4) and general QC flag suspect (3), then varflag converted to suspect (3)
                # -----
                fmaskGood = numpy.ma.masked_equal(vals,4) # good are masked
                
                maskFinalVar = numpy.ma.make_mask( fmaskGood.mask & fs[intstart:intend],   shrink=False)
                                           
                vals[numpy.ma.where(maskFinalVar)] = 3
        else: #if there is no field_QC in netcdf
            if variable_nb_dimension == 1 :
                
                vals = numpy.ma.ones(intend-intstart ,dtype='i1')*4
            elif variable_nb_dimension == 2:
                dimension_name = ifd.variables[variable].dimensions[1]
                logging.debug('dimension_name %s',dimension_name)
                shape_second_dim = len(ifd.dimensions[dimension_name])
                logging.debug('value of the second dimension %s',shape_second_dim)
                vals = numpy.ma.ones((intend-intstart,shape_second_dim) ,dtype='i1')*4
            else:
                logging.error('the number of dimension is > 2 and it is an unexpected case')
                raise
#         if self.curretnVarNbDim==1:
#             vals = numpy.ma.array(numpy.ones(numpy.shape(ifd.variables[variable][intstart:intend])) ,dtype='i1')
#         else:
#             vals = numpy.ma.array(numpy.ones(numpy.shape(ifd.variables[variable][intstart:intend,:])) ,dtype='i1')

                            
        # Adjust according to dimensions (as physical parameters)
        if variable_nb_dimension == 1:
            vals = vals[:]
        elif variable_nb_dimension == 2:
            vals = vals[:,:]
        elif variable_nb_dimension == 3:
            vals = vals[:,0,0]
        else :
            logging.error("QC dimensions : dimensions of variable %s > 3" % variable)
            raise Exception
        if sensor == 'wave_sensor' and len(vals.shape)==2:
            vals = numpy.ma.squeeze(vals[:,0])
            vals = numpy.ma.reshape(vals,(dims['time']))
            
        
        vals.mask = numpy.ma.getmask(masked_times[intstart:intend])
        
        logging.debug('build QC levels field associated to %s with shape %s',variable,vals.shape)
        qc_levels = QCLevel( values = vals,
                                            dimensions = copy.copy(dims),
                                            levels = numpy.array([0,1,2,3,4],dtype='i1'),
                                            meanings = "Unknown Unprocessed Bad Suspect Good" )
#         vals = numpy.ma.empty((intend-intstart,),dtype='i2')
        vals_qc = numpy.ma.zeros(vals.shape)
#         vals_qc[:] = 0
#         vals.mask = numpy.ma.getmask(masked_times[intstart:intend])
        qc_details = QCDetail( values = vals_qc,
                                                   dimensions = copy.copy(dims),
                                                   mask = numpy.array([1],dtype='i1'),
                                                   meanings = 'flagged_by_provider')
        logging.debug('build QC details field associated to %s with shape %s',variable,vals_qc.shape)
        return qc_details,qc_levels
#     BuildQualityFields = classmethod(BuildQualityFields)
    
    def CreateSpectralFields(self,ofields,wavefreq,ifd,intstart,intend,dims,spectrumtype):
        '''create spectral fields for frequency'''
        #TODO: use the offical variable conventions of CERSAT and check for any values update
        #                            dims.append( freqDim[spectrumtype] )
        #dims[ freqDim[spectrumtype] ] =  len(ifd.variables[wavefreq][:])
        
        # definition of spectrum bands
        if not wavefreq in ofields:
            varobj = Variable( shortname=freqVarName[spectrumtype],
                                                  description= 'central frequency of bin' )
        #                                fieldobj = Field(variable = varobj,
        #                                                                 values = ifd.variables[wavefreq][:],
        #                                                                 units = ifd.variables[wavefreq].units,
        #                                                                 dimensions = [freqDim[spectrumtype]])
            fieldobj = Field( varobj,dims,
                                              #OrderedDict( [(freqDim[spectrumtype],len(ifd.variables[wavefreq][:]))] ),
                                              values = ifd.variables[wavefreq][intstart:intend,:],
                                              units = ifd.variables[wavefreq].units
                                              )
            ofields[freqVarName[spectrumtype]] = fieldobj
            wavefreq_bnds = wavefreq+'_bnds'
            varobj = Variable( shortname=freqRangeName[spectrumtype],
                                                  description= 'frequency range of bin' )
            fieldobj = Field(variable = varobj,
                                             values = ifd.variables[wavefreq_bnds][:,1] - ifd.variables[wavefreq_bnds][:,0],
                                             units = ifd.variables[wavefreq_bnds].units,
                                             dimensions = [freqDim[spectrumtype]])                                
            ofields[freqRangeName[spectrumtype]] = fieldobj
        return ofields
#     CreateSpectralFields = classmethod(CreateSpectralFields)
    
    def GetValues2D(self,ifd,v,intstart,intend):
        '''get good values of a geophysical parameter in 2D array 
        (this function return a vector because it was designed for TEMP(time,depth)->SST(time) '''
#         if '_FillValue' in ifd.variables[v].ncattrs():
#             fillval = ifd.variables[v]._FillValue
#         else:
#             fillval = 9.96921e+36
        logging.debug('GetValues2D() %s ',ifd.variables[v].dimensions)
        if 'frequency' in  ifd.variables[v].dimensions:
            totalvalues= ifd.variables[v][intstart:intend,:]
            totalvalues = numpy.ma.array(totalvalues)
        else: #dans tout les uatres cas on ne garde que la colonne de la matrice avec des valeurs
            filled_2dim = 0
            for posd in range(ifd.variables[v][:].shape[1]):
                ttmp = ifd.variables[v][0,posd]
                if numpy.isfinite(ttmp):
                    filled_2dim = posd
#             t0_1 = ifd.variables[v][0,0]
#             t0_2 = ifd.variables[v][0,2]
#             if numpy.isfinite(t0_1) and numpy.isfinite(t0_2)==False:
            totalvalues = ifd.variables[v][intstart:intend,filled_2dim]
#             else:
#                 totalvalues = ifd.variables[v][intstart:intend,1]
#         if ifd.variables
#         LEVELindice=totalvalues < abs(fillval)
#         goodLEVELS=numpy.logical_and(LEVELindice==True,numpy.ma.getmask(totalvalues)==False)
#         indicesgoodlevels=numpy.where(goodLEVELS==True)
#         if len(indicesgoodlevels[0])>0 :
#             if len(indicesgoodlevels)==2:
#                 firstgoodindicelevel=indicesgoodlevels[1][0]
#             else:
#                 firstgoodindicelevel=indicesgoodlevels[0][0]
#         else:
#             firstgoodindicelevel=0
#         logging.debug('firstgoodindicelevel %s indice ',firstgoodindicelevel)
#         values = ifd.variables[v][intstart:intend,firstgoodindicelevel]
#         values=values.T
        #return the whole 2D array
#         values = totalvalues
        return totalvalues
#     GetValues2D = classmethod(GetValues2D)
    
#     def ConversionUnits(self,src_unit,variable_name,values,std_unit):
#         '''convert values in function of units and get globwave units convention '''
#         flag_warning = False
#         if std_unit != src_unit:
#             if src_unit in UNITS:
#                 if UNITS[src_unit] != std_unit:
#                     flag_warning = True
#             else:
#                 flag_warning = True
#             if flag_warning:
#                 logging.warning('units are different std:%s != src:%s',std_unit,src_unit)
#             # Convert to Pascal
# #             src_unit = ifd.variables[variable_name].units
#             if src_unit in ['hPa','hectopascal','hectoPascal'] :
#                 values = values * 100.
#             # Convert to m s-1
#             if src_unit in ['cm/second'] :
#                 logging.debug("cm/second -> conversion m s-1")
#                 values = values / 100.
#         # if ifd.variables[v].ndim
# #         if 'units' in ifd.variables[v].ncattrs():
# #             logging.debug('variable %s units %s',v,ifd.variables[v].units)     
# #             units = self.get_units(ifd.variables[v].units)
# #         else:
# #             units = None
#         if variable_name =='PRRT':
#             cst = conversion[variable_name][1]
#             operator = conversion[variable_name][0]
#             values = operator(values,cst)
#         return values
# #     ConversionUnits = classmethod(ConversionUnits)
#     GetFinalNameOfTheBuoy = classmethod(GetFinalNameOfTheBuoy)
    
    def CreateFinalTimeSerie(self,interval,instr,ofields,lon,lat,depth,intstart,intend,ifd):
        ''' Create Time Series and define final name of the file netCDF'''
        # ------------------
        ts = None
        if ofields != {}:
            logging.debug('create final time serie for this timeinterval %s in this instrument %s',interval,instr)
            untis_time = ifd.variables['TIME'].units
            logging.debug('time units before write %s',untis_time)
            ts = PointTimeSeries( fields = ofields,
                                    longitude = lon,
                                    latitude = lat,
                                    depth = depth,
                                    times = (ifd.variables['TIME'][intstart:intend],untis_time ) )
        # times = (ifd.variables['TIME'][intstart:intend],ifd.variables['TIME'].units )
        #times = (times[intstart:intend])
        #ts=PointCollection(fields=ofields,longitudes=lon,latitudes=lat,depths=depth,times=(ifd.variables['TIME'][intstart:intend],ifd.variables['TIME'].units))
        return ts
#     CreateFinalTimeSerie = classmethod(CreateFinalTimeSerie)
    
    def CreateFieldsWaveSpectrum(self,ifd,ofields,v,intstart,intend,dims,fs,fmaskSuspect,masked_times):
        '''create fields wave_spectrum_r1 and wave_spectrum_r2'''
        logging.debug("WARNING - NEED DE SCIENCE.WAVE LIB")
#         raise Exception
        stheta1 = None
        if not isinstance(ifd.variables[v][intstart:intend,:,0,0],numpy.ma.MaskedArray) or ifd.variables[v][intstart:intend,:,0,0].count() != 0:
            if stheta1 is None:
                stheta1 = numpy.ma.masked_all_like(ifd.variables['wave_spectrum_r1'][intstart:intend,:,0,0])
                stheta2 = numpy.ma.masked_all_like(ifd.variables['wave_spectrum_r2'][intstart:intend,:,0,0])
                theta1 = ifd.variables['mean_wave_dir'][intstart:intend,:,0,0]
                theta2 = ifd.variables['principal_wave_dir'][intstart:intend,:,0,0]
                ts,ss = theta1.shape
                # Commentaires a supprimer si variable wave_spectrum...
                for t in range(ts):
                    for s in range(ss):
                        theta1[t,s],theta2[t,s],stheta1[t,s],stheta2[t,s] = r1r2_to_sth1sth2(
                                                                          theta1[t,s],
                                                                          theta2[t,s],
                                                                          ifd.variables['wave_spectrum_r1'][t,s],
                                                                          ifd.variables['wave_spectrum_r2'][t,s]
                                                                          )
            if v == 'wave_spectrum_r1':
                vname = 'stheta1'
                vals = stheta1
            else:
                vname = 'stheta2'
                vals = stheta2
            varobj = Variable(shortname=vname,
                                    description= '',
                                    authority = cfconvention.get_convention(),
                                    standardname = cfconvention.get_standardname(vname) )
            qc_levels,qc_details = self.BuildQualityFields(ifd,v,intstart,intend,fs,fmaskSuspect,masked_times,dims)
            fieldobj = Field(varobj,
                                             dims,
                                             values = vals,
                                             units = 'angular_degree',
                                             qc_levels=qc_levels, qc_details=qc_details)
            ofields[v] = fieldobj
        return ofields
    
    def CreateField(self,ifd,v,intstart,intend,ofields,dims,qc_levels,qc_details):
        '''
        instanciate cerbere class Field with geophysical variables of the provider netCDF
        Args:
            ifd (netCDF4 obj): file handler of input file
            v (str): variable name
            intstart (int):
            intend (int):
            ofields (OrderedDict):
            dims (OrderedDict):
            qc_levels (cerbere field):
            qc_details (cerbere field):
        
        '''
        logging.debug('get variable %s informations ',v)
#         vname = self.get_cersatname(v)
        if v in DICO:
            v_short = DICO[v][0]
            conv_info = VarConventions(v_short)
            if conv_info is not None:
#                 vname,longname,description,sensor,stdunits = conv_info
#                 logging.debug('translate %s ->%s-> %s',v,v_short,vname)
#                 stdname = cfconvention.get_standardname(vname)
                #logging.debug("original name:",v,"globawave name", vname,'stdanard name: ',stdname)
                    # attributs !!!
#                 varobj = Variable(shortname=vname,
#                                         description= ifd.variables[v].long_name.lower(),
#                                         authority = cfconvention.get_convention(),
#                                         standardname = stdname )
                var_nb_dimension = ifd.variables[v].ndim
#                 var_nb_dimension = len(dims) #les dimension souhaite
                var_dimension_names = ifd.variables[v].dimensions
                logging.debug('%s has %s dimensions',v,var_nb_dimension)
                if 'frequency' in var_dimension_names or 'FREQUENCY' in var_dimension_names:
                    logging.debug('variable nb dimensions %s %s',v,self.curretnVarNbDim)
                    if self.curretnVarNbDim == 2:
                        values = ifd.variables[v][intstart:intend,:]
                    elif self.curretnVarNbDim == 3:
                        values = ifd.variables[v][intstart:intend,:,0]
                    elif self.curretnVarNbDim == 4:
                        values = ifd.variables[v][intstart:intend,:,0,0]
                else:                            
                    # Adjust according to dimensions
                    if var_nb_dimension == 1:
                        values = ifd.variables[v][intstart:intend]
                    elif var_nb_dimension == 2:
                        values = self.GetValues2D(ifd,v,intstart,intend)
                    elif var_nb_dimension == 3:
                        values = ifd.variables[v][intstart:intend,:,0,0]
                    else:
                        logging.error("Dimensions of variable %s > 3" % v)
                        raise Exception
                    if len(values.shape)==2: # and sensor=='wave_sensor':
                        values = numpy.ma.squeeze(values[:,0])
                    values = numpy.ma.reshape(values,tuple(dims.values()))
                    logging.debug('values of %s : %s => field %s',v,values.shape,dims.values())
    #                         values = numpy.ma.squeeze(values)
                src_unit = ifd.variables[v].units
                input_descr = ifd.variables[v].long_name
#                 values = self.ConversionUnits(src_unit,v,values,stdunits)
                #values = ConversionUnits(src_unit=src_unit,variable_name=v,values=values,std_unit=stdunits) #dja fait dans CreateFieldcomon()
                logging.debug('shape of values for variable %s is %s ',v,values.shape)
#                 values = numpy.ma.masked_array(values)
                if values.size != 0:
                    fieldobj = CreateFieldcomon(v,dims,qc_levels,qc_details,values,input_descr,src_unit)
#                     fieldobj = Field(ovars[v],
#                                                      dims,
#                                                      values = values,
#                                                      units = stdunits,
#                                                      qc_levels=qc_levels,
#                                                      qc_details=qc_details)
                    ofields[v] = fieldobj
                else:
                    logging.warning('variable %s has not been added to the new netcdf because there is no useable values',v)
                    logging.debug('type of values :%s   values.count:%s',type(values),values.count())
            else:
                logging.warning('%s is not defined in VarConventions',DICO[v])
        else:
            logging.warning('%s is not defined in DICO',v)
        return ofields
#     CreateField = classmethod(CreateField)
    
    def processOneVariable(self,ofields,v,intstart,intend,ifd,fs,fmaskSuspect,masked_times,wavefreq,spectrum_type,sensor):
        '''get the values, qc, and store them into Field and Variable cerbere classes'''
        logging.debug('treated variable: %s',v)
        dims = self.DefineDimensionOfVariable(ifd,intend,intstart,v,wavefreq,spectrum_type,sensor)
        self.curretnVarNbDim = len(ifd.variables[v].dimensions)
        logging.debug('number of dimension for variable %s is %s',v,self.curretnVarNbDim)
        if not 'units' in ifd.variables[v].ncattrs() and v != 'wave_spectrum_r1' and v != 'wave_spectrum_r2':
            logging.error("Missing units for variable: %s", v)
            raise Exception()
        ifd.variables[v].set_auto_maskandscale(True)
        qc_details,qc_levels = self.BuildQualityFields(ifd,v,intstart,intend,fs,fmaskSuspect,masked_times,dims,sensor)
#         if 'FREQUENCY' in ifd.variables[v].dimensions:
#             wavefreq = 'FREQUENCY'
#             spectral = True
#         else:
#             wavefreq = None
#             spectral = False
        if wavefreq == True:
            ofields = self.CreateSpectralFields(ofields,wavefreq,ifd,intstart,intend,spectrum_type,fs,fmaskSuspect,masked_times)
        # Create Variables and fields
        # ---------------------------
        if v == 'wave_spectrum_r1' or v == 'wave_spectrum_r2':
            ofields = self.CreateFieldsWaveSpectrum(ifd,ofields,v,intstart,intend,dims)
        else: #nominal case for variable integration
            ofields = self.CreateField(ifd,v,intstart,intend,ofields,dims,qc_levels,qc_details)
        return ofields
    
    def DefineDimensionOfVariable(self,ifd,intend,intstart,v,wavefreq,spectrumtype,sensor):
        '''define the dimensions of the new file for a given sensor
        here there is a choice: it is impossible to hvae variable in 2D except for frequency param dependant 
        '''
        dims = collections.OrderedDict()
        dims['time'] = intend - intstart #defaut dimensions
        if 'FREQUENCY' in ifd.variables[v].dimensions:
            dims[ freqDim[spectrumtype] ] = len(ifd.dimensions[wavefreq])
        #if sensor != 'wave_sensor':#we dont want z dimension for wave fields
#             if 'DEPTH' in ifd.variables[v].dimensions:
#                 dims['z'] = len(ifd.dimensions['DEPTH'])
        logging.debug('dimensions of the variable are %s',dims)
        return dims
#     DefineDimensionOfVariable = classmethod(DefineDimensionOfVariable)
    
    def DetermineNatureOfNewFile(self,groups,instr,ifd):
        """
        Returns:
            wavefreq (str): None or 'FREQUENCY' tells if there are frequency in the variable dimensions
            spectral (bool): redondant with wavefreq but in boolean
        """
        if 'wave_spectrum_r1' in  ifd.variables:
            spectrumtype = 'directional'
        else:
            spectrumtype = 'nondirectional'
        for v in groups[instr]:
            if 'FREQUENCY' in ifd.variables[v].dimensions:
                wavefreq = 'FREQUENCY'
                spectral = True
                
            else:
                wavefreq = None
                spectral = False
        return wavefreq,spectral,spectrumtype
#     DetermineNatureOfNewFile = classmethod(DetermineNatureOfNewFile)

    def test_ofields(self,ofields):
        containsomething = False
        for kk in ofields:
            if ofields[kk] is not None:
                containsomething = True
        return containsomething
    
    def convert2globwave(self, f, outputdir):
        '''
        process a oceansites file to GlobWave format
        Args:
            f (str): full path of myocean input file
            outputdir (str): path where to write the file created in globwave format
        '''
#         logging.info("Processing %s" % f)
        listOfFilesCreated = []
        ifd = netcdf.Dataset( f, 'r' )
        input_fieldnames = ifd.variables.keys()
        # get geolocation
        geoloc_info = self.ReadGeolocationInformations(ifd,f)
#         logging.info('geoloc: %s',geoloc_info)
        if geoloc_info is not None:
            FLAGS_GEN,position_qc,lon,lat,times,depth = geoloc_info
#             nc = NCFile(f)
#             feature = PointTimeSeries(identifier='', title='', description='', source='',
#                                metadata=metadata, fields=fields_container,
#                                 longitude=lon, latitude=lat, depth=depths, times=time_field)
#             feature = PointTimeSeries()
#             feature.load(nc,checkstorage=False)
#             groups = self.DetermineInstrumentGroups(ifd)
            groups = DetermineInstrumentGroups(input_fieldnames)
            
            alloriginalglobalattr = {}
            for attr in ifd.ncattrs():
                alloriginalglobalattr[attr] = ifd.getncattr(attr)
            starttimeserie =ifd.getncattr('time_coverage_start')
            endtimeserie = ifd.getncattr('time_coverage_end')
            globalmetadata = DefineGlobalMetaData(f,alloriginalglobalattr,starttimeserie,endtimeserie,lat,lon,processor_version)
#             globalmetadata = self.DefineGlobalMetaData(ifd,f,lon,lat)
            # process output files instrument by instrument
            logging.debug('loop over instrument in groups %s',groups)
            for instr in groups:
                logging.debug('========= instrument %s  from %s ============',instr,f)
                tmask = self.CreateTimeMaskInstrument(ifd,instr,groups)
                logging.debug("create flag mask with time_qc ,position_qc")
                fs,fmaskSuspect = self.UseFlagFromProvider(ifd)
                intervals,masked_times = self.CutTimeSerie(tmask,times)
                # loop on time intervals
                # ---------------------
                logging.debug("intervals : %s",intervals)
                for interval in intervals:
                    logging.debug("interval %s", interval)
                    intstart,intend = interval
                    # Create Fields
                    # -------------
                    #ovars = {}
                    ofields = collections.OrderedDict()
                    if len(groups[instr]) > 0: ## ajout selon JFP
                        logging.debug('loop over variables')
                        wavefreq,spectral,spectrumtype = self.DetermineNatureOfNewFile(groups,instr,ifd)
                        for v in groups[instr]:
                            ofields = self.processOneVariable(ofields,v,intstart,intend,ifd,fs,fmaskSuspect,masked_times,wavefreq,spectrumtype,instr)
                        if self.test_ofields(ofields):
                            ts = self.CreateFinalTimeSerie(interval,instr,ofields,lon,lat,depth,intstart,intend,ifd)
                        
#                         if ts:
                            logging.debug('ts %s',ts)
                            logging.debug('fields: %s',ts.get_fieldnames())
                            logging.debug('lat values shape %s',ts.get_geolocation_field('lat').get_values().shape)
                            start_dt = netcdf.num2date(ifd.variables['TIME'][intstart],ifd.variables['TIME'].units)
                            end_dt = netcdf.num2date(ifd.variables['TIME'][intend-1],ifd.variables['TIME'].units)
                            ofname = Define_output_filename(instr,lon,lat,start_dt,end_dt,globalmetadata,spectral,outputdir)
                            listOfFilesCreated = WriteGWOceansiteFile(ofname,listOfFilesCreated,ts,globalmetadata)
                        #uncomment following block for exploit
                        #try:
                            ##tsncf = NCFile( url = ofname, mode=WRITE_NEW )
                            #tsncf = NCFile(url=ofname, mode=WRITE_NEW, ncformat='NETCDF4')
                            #logging.debug( "Write file %s" % ofname )
                            ##ts.save( tsncf, attrs=dict(globalmetadata.items()) )
                            #ts.save( tsncf, attrs=globalmetadata )
                        #except Exception, e:
                            #logging.debug( "Error when writing output files : %s" % e) 
                            #ifd.close()
                            ## keep trace of excepted files
                            ##open(os.path.join(EXCEPTED_FILES,os.path.basename(f)), 'w').close()
                            
                            ##shutil.move(f, EXCEPTED_FILES)
                            #return
                
        ifd.close()
        # Move treated file
        #shutil.move(f, TREATED_FILES)
        return listOfFilesCreated
#     convert2globwave=classmethod(convert2globwave)


    
#     def WriteGWOceansiteFile(self,ofname,listOfFilesCreated,ts,globalmetadata):
#         '''
#         function to write the timeserie in a final nertcdf file 
#         (note that the test on existing file won't affect the process
#          if the data previously processed has been removed)
#          '''
#         existingfile = findWidderPlatformFile(ofname)
#         if existingfile == None:
#             if os.path.exists(ofname):
#                 logging.info('removing the existing version of %s',ofname)
#                 os.remove( ofname )
#             tsncf = NCFile(url=ofname, mode=WRITE_NEW, ncformat='NETCDF4')
#             listOfFilesCreated.append(ofname)
#             ts.save( tsncf, attrs=globalmetadata )
#             tsncf.close()
#             logging.info('file written %s',ofname)
#         else:
#             logging.info('file %s not created because %s already exist',ofname,existingfile)
#         return listOfFilesCreated
#     WriteGWOceansiteFile=classmethod(WriteGWOceansiteFile)
    
#     def ProcessDataFromScratch(self,outdir,f):
        
#         return
#     ProcessDataFromScratch = classmethod(ProcessDataFromScratch)
    
    def ProcessDataWithMergingFunctions(self,outdir,f):
        '''process a provider file using the function allowing to find contigous files'''
        filescreated = self.convert2globwave( f, outputdir=outdir )
        for fcrea in filescreated:
            relative=findContigousPlatformFiles(fcrea)
            if relative != None:
                ConcatenateFiles(fcrea,relative)
    
    def convert_a_whole_directory(self,inputdir,outdir,trymode=False,buoyid=None):
        if ('*' in inputdir) == False:
            inputdir = os.path.join(inputdir, '*.nc')
        logging.info('pattern to find monthly buoys files : %s',inputdir)
        files = glob.glob(inputdir)
        logging.info('all files before filtering %s',len(files))
        if buoyid is not None:
            files = [x for x in files if buoyid in x]
        logging.info('recup all files from dir %s => %s',inputdir,len(files))
#         cpt_saved = 0
        counter = collections.defaultdict(int)
        counter['total_input'] = len(files)
        for cptfile,f in enumerate(files):
            type_file_source = os.path.basename(f)[10:15]
            if  (type_file_source in unwanted_files)==False:
                logging.info(" %s/%s Processing %s $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$",cptfile,len(files), f)
                if trymode==False:
#                     self.ProcessDataFromScratch(outdir,f)
                    filescreated = self.convert2globwave( f, outputdir=outdir )
#                     cpt_saved += len(filescreated)
                    counter['cpt_saved'] += len(filescreated)
    #                buoy.ProcessDataWithMergingFunctions(outdir,f)
                else:
                    try:
#                         self.ProcessDataFromScratch(outdir,f)
                        filescreated = self.convert2globwave( f, outputdir=outdir )
#                         cpt_saved += len(filescreated)
                        counter['cpt_saved'] += len(filescreated)
                        for ffuy in filescreated:
                            if 'wave_sensor' in ffuy:
                                counter['cpt_saved_wave_sensor'] += 1
    #                     buoy.ProcessDataWithMergingFunctions(outdir,f)
                    except:
                        counter['failed'] += 1
                        print(traceback.format_exc())
                        list_exc = open(EXCEPT_LIST, 'a')
                        list_exc.write(f+'\n')
                        list_exc.close()
#                         logging.error('converting failed for file %s',f)
#                         logging.error('traceback %s',traceback.print_exc())
                logging.info("process finished for %s",f)
        logging.info('%s files have been generated for this run from %s source files',counter['cpt_saved'],counter['total_input'])
        return counter

if __name__ == "__main__":

    root = logging.getLogger()
    if root.handlers:
        for handler in root.handlers:
            root.removeHandler(handler) 
    buoy = OceansitesMooredBuoy()
    parser = OptionParser()
    parser.add_option("-i","--input",
                      action="store", type="string",
                      dest="input", metavar="string",
                      help="file(s) to be converted in globWave format (can be directory) ")
    parser.add_option("-o","--outputdir",
                      action="store", type="string",
                      dest="outputdir", metavar="string",
                      help="directory where will be created the sub arborescence sensor/YYYY/MM/file.nc ")
    parser.add_option('-q','--quiet',
                      action='store_true',default=False,
                      dest='quiet',
                      help='quiet mode [default verbose]')
    (options, args) = parser.parse_args()
    if options.quiet==True:
        logging.basicConfig(level=logging.INFO,format='%(asctime)s %(lineno)d %(levelname)s %(message)s')
    else:
        logging.basicConfig(level=logging.DEBUG,format='%(asctime)s %(lineno)d %(levelname)s %(message)s')
    logging.info('start converting data from MyOcean distribution in oceansites format----')
    if options.input is None:
        logging.error('missing -i input argument')
        raise
    if options.outputdir is None:
#         logging.error('missing -o outputdir argument')
        raise Exception('missing -o outputdir argument')
    outdir = options.outputdir
    input_file = options.input
    if os.path.isfile(input_file):
        buoy.convert2globwave( input_file, outputdir=outdir )
    else:
        buoy.convert_a_whole_directory(input_file,outdir,trymode=True)
    logging.info('end of script')