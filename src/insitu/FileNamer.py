#FileNamer
#agrouaze
#feb 2014
#purpose:harmonize the way to organise and store globewave data
import os
def GlobWaveFileNamer(station_name,outputdir,sensor,start,stop,lat,lon):
  #station_name (str)
  #outputdir (str)
  #sensor (str)
  #start,stop (datetime)
  #lat,lon (float)
  #GlobWaveFileNamer('joj','/toto/gre/','wave_sensor',start,stop,'15.48','0.12')
  #if outputdir[-1]!='/':
    #outputdir=outputdir+'/'
  #if len(month)!=2:
    
  #outputdir_full=outputdir+sensor+'/'+year+'/'+month
  if lat < 0:
      latstr = "%.2fS" % -lat
  else:
      latstr = "%.2fN" % lat
  #lon = series.location.lon
  if lon < 0:
      lonstr = "%.2fW" % -lon
  else:
      lonstr = "%.2fE" % lon
  monthDir =  os.path.join (outputdir,sensor,str(start.year).strip(),"%02d" % start.month)

  filename = "%s_%s_%s_Lat_%s_Lon_%s.nc" % (station_name,start.strftime("%Y%m%dT%H%M"),stop.strftime("%Y%m%dT%H%M"),latstr,lonstr )
  fullpath=monthDir+'/'+filename
  return fullpath