#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#

"""
Processing cdipmooredbuoy from input directory and output directory as arguments.


# inputDir : path without channel (ie p2, 01, 02, 04, 05, 07) : 
# ex.: /home/cercache/project/globwave/data/provider/cdip/2004/183/may

Cmd :
ssh cerhouse1
source /home/cercache/tools/environments/scientific_toolbox_cloudphys_precise/bin/activate.csh
setenv PYTHONPATH /home1/typhon/cprevost/DEV/python/cerbere_deployed
./cdipReprocess.py --outputdir './' --inputdir '/home/cercache/project/globwave/data/provider/cdip/2004/183/may'
or
./cdipReprocess.py --outputdir './' --inputdir '/home/cercache/project/globwave/data/provider/cdip/2004/183/*'


NOTICE :
 Do not forget '' if using '/path/*', otherwise last month only will be treated !

"""



import glob
import sys
import os
import subprocess
import datetime



globwaveOutDir = sys.argv[2]
print "globwaveOutDir : ",globwaveOutDir

inputDir = sys.argv[4]
if inputDir[-1] == '*':
    inDirs = glob.glob(inputDir)
elif inputDir.split('/')[-1] in ['jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec']:
    inDirs = [inputDir]
else : 
    raise Exception( "[CDIP REPROCESS] inputdir format should be as /path/YYYY/platformNb/{month|*}")
    exit(1)



print inDirs
start = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
channelCounter = 0
for inputDir in inDirs:
    channelDirList = os.listdir(inputDir)
    if 'p2' in channelDirList:
        channelDir = 'p2'
    elif '01' in channelDirList and (len(channelDirList)==1 or (len(channelDirList)==2 and 'fix' in channelDirList)):
        channelDir = '01'
    elif '01' in channelDirList:
         channelDir = '01'
    elif len(channelDirList)==1 and '07' in channelDirList:
         channelDir = '07'
    elif len(channelDirList)==1 and '02' in channelDirList:
         channelDir = '02'
    elif '04' in channelDirList:  # TO BE VERIFIED !!! #
         channelDir = '04'        # TO BE VERIFIED !!! #
    elif len(channelDirList)==1 and '05' in channelDirList: # TO BE VERIFIED !!! #
         channelDir = '05'      # TO BE VERIFIED !!! #
    elif len(channelDirList)==0:
         print 'no channel for : ', inputDir
         continue
    else:
        print 'channel :', channelDirList
        raise Exception( "ERROR : ambiguity with channels")

    print "channelDir %s/%s" % (inputDir,channelDir)
    #ret = subprocess.call(["/bin/csh","source", "/home/cercache/tools/environments/scientific_toolbox_cloudphys_precise/bin/activate.csh"])
    #sys.path.append('')
    #os.environ["PYTHONPATH"] = "/home1/typhon/cprevost/DEV/python/cerbere_deployed"
    #print "PYTHONHOME 2: ", os.environ['PYTHONHOME'].split(os.pathsep)
    #print "PYTHONPATH 2: ", os.environ['PYTHONPATH'].split(os.pathsep)
    
    channelCounter += 1
    retcode = os.system("python  /home1/typhon/cprevost/DEV/python/GIT/globwave/src/insitu/cdipmooredbuoy.py --inputdir "+inputDir+"/"+channelDir+" --outputdir "+globwaveOutDir)
    print ">>%s<<"%retcode
    
    
    if retcode!=0:
        raise Exception('[CDIP REPROCESS] Error %s when reprocessing CDIP data from : %s/%s ' % (retcode,inputDir,channelDir))
    
    print "START : ",start
    print "channelCounter = ",channelCounter
    print "END : ", datetime.datetime.now().strftime("%Y-%m-%d %H:%M")

                           
