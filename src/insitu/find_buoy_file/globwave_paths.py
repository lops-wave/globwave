"""
author: Antoine Grouazel
purpose: list the path of globwave files answer a given request
date of creation: 12/11/2015
"""
import os
import time
import logging
import glob
import datetime
# import fnmatch
# print 'version april 2018 path'
import sys
# sys.path.append('/home/losafe/users/agrouaze/PROGRAMMES/ROUTINE_PYTHON/my_tool_box')
# from colored_logging_lib import ColoredLogger
sys.path.append('/home/losafe/users/agrouaze/git/globwave/src/insitu')
from globwave_shared_infos import active_networks,GLOBWAVE_ROOT
from variables_conventions import sensors
# logging.basicConfig(level=logging.DEBUG)
# tmp = logging.getLogger('finder')
# logger = ColoredLogger(tmp)
def list_paths_glob(outputdir,startperiod,stopperiod,network,buoyid=None,sensors_wanted=sensors):
    """
    return the path of the list created to store all the path of the globwave files
    Args:
        outputdir (str): directory where the listing will be written
        network (list):
        startperiod (datetime):
        network (list): [CORIOLIS,NDBC,...]
        buoyid (str): WMO13224,WMO1645
        sensors_wanted (list):
    """
    assert 'WMO' in buoyid
    cpt = {}
    list_paths = []
    if startperiod is not None:
        styear = startperiod.year
    else:
        styear = 1940
    if stopperiod is not None:
        stopyear = stopperiod.year
    else:
        stopyear = 2050
    list_valid_years = range(styear,stopyear+1)
    for net in network:
        if net not in cpt:
            cpt[net] = {}
#         print net,active_networks
        net_dir = active_networks[net]
        logging.info('network : %s -> %s',net,net_dir)
        for sens in sensors_wanted:
            if sens not in cpt[net].keys():
                cpt[net][sens] = 0
            logging.info('sensor : %s',sens)
            sens_dir = os.path.join(net_dir,sens)
            if os.path.exists(sens_dir):
                logging.debug('sens_dir %s',sens_dir)
                for year_dir in os.listdir(sens_dir):
                    if int(year_dir) in list_valid_years:
                        logging.info('year: %s',year_dir)
                        year_direc = os.path.join(sens_dir,year_dir)
                        logging.debug('year_direc %s',year_direc)
                        if buoyid is None:
                            buoyid_str = ['*']
                        else:
                            buoyid_str = buoyid.split(',')
                        
                        for buoy in buoyid_str:
                            pattern = buoy+'_*.nc'
                            ful_pat = os.path.join(year_direc,'*',pattern)
                            logging.debug('full pattern : %s',ful_pat)
                            list_brute = glob.glob(ful_pat)
                            for filename in list_brute:
                                if startperiod is not None or stopperiod is not None:
                                    start_file = datetime.datetime.strptime(os.path.basename(filename).split('_')[1],'%Y%m%dT%H%M')
                                    flag_val = (start_file>=startperiod) & (start_file<=stopperiod)
                                    logging.debug('flag_val: %s %s %s %s',flag_val,startperiod,start_file,stopperiod)
                                else:
                                    flag_val = True
                                if flag_val:
                                    cpt[net][sens] +=1
                                    list_paths.append(filename)
#                             logging.debug('add %s %s files ',filename,len(list_paths))
    list_paths_loc = os.path.join(outputdir,'listing_globwave_paths.lst')
    fid = open(list_paths_loc,'w')
    for ff in list_paths:
        fid.write(ff+'\n')
    fid.close()
    logging.info('output: %s',list_paths_loc)
    logging.info('Nber of files found: %s',len(list_paths))
    logging.info('Counter: %s',cpt)
    return list_paths_loc,list_paths


if __name__ == '__main__':
    #usage python globwave_paths.py -v -o /tmp -n CORIOLIS -s 20140201 -e 20140512 --sensor wave_sensor
#     python globwave_paths.py -v -o /tmp -n MEDS -s 20140101 -e 20151212 -b C45139,C45159,C45135,disw3,stdm4,pilm4,C45136,C45132,C45142,C45137,C45154,C45149,C45143
# rsync -avzR `cat /tmp/listing_globwave_paths.lst` /media/agrouaze/Dell\ Portable\ Hard\ Drive/GPM_data/buoys/
    start = time.time()
#     logging.basicConfig(level=logging.DEBUG)
#     tmp = logging.getLogger('finder')
#     logger = ColoredLogger(tmp)
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option("-v","--verbose",
                      action='store_true',default=False,
                      help="verbose mode [default is quiet]")
    parser.add_option("-o","--output_dir",
                      action='store',
                      dest="output_dir",
                      help="output directory [option, default is /tmp ]")
    parser.add_option("-n","--network-name",
                      action='store',type="choice",choices=active_networks.keys(),
                      dest='network',
                      help="name of the network to browse %s [optional, default is all active network]"%active_networks.keys())
    parser.add_option("-b","--buoy-id",
                      action='store',
                      dest="buoyid",
                      help="identifier of the buoy (WMO62134 or sdf45) [optional, default is all buoys ] if many buoys add , separator between them")
    parser.add_option("-s","--start-period",
                      action='store',
                      dest="startperiod",
                      help="starting date of the period of interest YYYYMMDD [optional, default is all dates ]")
    parser.add_option("-e","--end-period",
                      action='store',
                      dest="endperiod",
                      help="ending date of the period of interest YYYYMMDD [optional, default is all dates ]")
    parser.add_option("--sensor",
                      action='store',
                      dest="sensor",
                      help="sensor you are interested in (%s) [optional, default is all sensors ]"%sensors)
    (options, args) = parser.parse_args()
    if options.output_dir is None:
        outdir = '/tmp'
    else:
        outdir = options.output_dir
    if options.verbose:
        logging.basicConfig(level=logging.DEBUG)
#         logging.setLevel(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
#         logging.setLevel(level=logging.INFO)
    if options.sensor is None:
        sensorss = sensors
    else:
        if ',' in options.sensor:
            sensorss= options.sensor.split(',')
        else:
            sensorss = [options.sensor]
    if options.startperiod is not None:
        start_dt = datetime.datetime.strptime(options.startperiod,'%Y%m%d')
    else:
        start_dt = datetime.datetime(1940,1,1)
    if options.endperiod is not None:
        stop_dt = datetime.datetime.strptime(options.endperiod,'%Y%m%d')
    else:
        stop_dt = datetime.datetime(2050,1,1)
    if options.network is None:
        netw = active_networks.keys()
    else:
        netw = [options.network]
    list_paths_glob(outdir,startperiod=start_dt,stopperiod=stop_dt,network=netw,buoyid=options.buoyid,sensors_wanted=sensorss)
    elapse = '%4.0f'%(time.time()-start)
    logging.info('end of script (time for research: %sseconds',elapse)
    sys.exit(0)
