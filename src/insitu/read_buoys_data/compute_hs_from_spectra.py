"""
purpose: compute hs from spectra given in NDBC globwave netcdf files
context: it seems that some times there is no variable Hs and moreover we want to apply 2d cu toff from SAR for instance
date: 30 april 2018
author: Antoine Grouazel
"""
import numpy as np
import numpy
import copy
from cerform.wave import period2wavelength #pythonpath cerform
from compute_2D_cutoff import get_WV_2D_cutoff
import logging

from interpolation_polar_spectrum import interp_polar_spectrum #pythonpath on qualitycheck
def get_buoy_hs_total(phi,k,spec):
    """
    :note: this method gives results plausible but needs to be validated against the values provided in NDBC for instance 
    :args:
        phi (1d nd.array):(direction)
        k (1d nd.array):(wavenber vector)
        spec (2d nd.array): tested with the one returned by wavetoolbox globwave reader
    hstot = 4xsqrt(sum (spectre x k x dk x dphi))
    """
    #print "k",k.shape
    #print 'phi',phi.shape
    #print 'spec',spec.shape
    #Nk = len(f)
    Nk = len(k)
    #fmin = numpy.amin(f)
    #fmax = numpy.amax(f)
    #kmin = (2*numpy.pi*fmin)**2/g
    #kmax = (2*numpy.pi*fmax)**2/g
    kmin = k.min()
    kmax = k.max()
    #titi = numpy.argsort(theta)
    #theta_ordered = phi[titi]
    #k = numpy.logspace(numpy.log10(kmin), numpy.log10(kmax),Nk)
    dk = numpy.log(10)*(numpy.log10(kmax)-numpy.log10(kmin))/(Nk-1)*k
    #dphi = abs(numpy.concatenate([numpy.diff(phi),[numpy.diff(phi)[-1]]]))
    dphi = numpy.diff(phi)[0]
    kdkdphi = k*dk*dphi
    #print "kdkdphi",kdkdphi.shape
    if spec.shape[0] == len(kdkdphi):
#         print('get_buoy_hs_total | transpose spec')
        spec  = spec.T
    hstot = 4.0*np.sqrt(np.sum(spec*kdkdphi))
    return hstot
    
def compute_buoy_hs_from_spectra_on_freq_range(buoy_spec,buoy_k,buoy_phi,freqrange):
    """
    uathor Antoine Grouazel
    to be validated
    values given on one example seems ok:
    WMO41043_20160601T0000_20160630T2300_Lat_21.02N_Lon_64.85W_spectrum.nc
    frequances plage 1 (0.04, 0.08)
    Hs 0.0979796027297 m
    frequances plage 3 (0.15, 0.228)
    Hs 1.174393419 m
    frequances plage 2 (0.08, 0.15)
    Hs 1.49244770081 m
    frequances plage 4 (0.0, 0.3)
    Hs 1.99924986627 m
    """
    fmin,fmax = freqrange
    if fmin!=0:
        
        Tmax = 1./fmin
    else:
        Tmax = 30 #arbitrary value I dont know what could be a possible max period
    Tmin = 1./fmax
    wlmin = period2wavelength(Tmax)
    wlmax = period2wavelength(Tmin)
#     print wlmin,wlmax
    kb = 2.*np.pi/wlmax
    ka = 2.*np.pi/wlmin
    if ka>=kb:
        kmin = kb
        kmax = ka
    else:
        kmin = ka
        kmax = kb
    logging.debug("kmin %s",kmin)
    logging.debug("kmax %s",kmax)
    indices_k_onfrange = np.where((buoy_k<kmax) & (buoy_k>kmin))
    logging.debug("indices_k_onfrange %s",indices_k_onfrange)
    dphi_sar = np.radians(np.array(abs(buoy_phi[1]-buoy_phi[0])))
    dk_buoy = np.ma.zeros(buoy_k.size)
    dk_buoy[0] = buoy_k[1]-buoy_k[0]
    dk_buoy[-1] = buoy_k[-1]-buoy_k[-2]
    dk_buoy[1:-1] = (buoy_k[2:]-buoy_k[0:-2])/2
    logging.debug("dk_buoy %s",dk_buoy.shape)
    logging.debug("dphi_sar %s %s",dphi_sar.shape,dphi_sar)
    logging.debug('buoy_k %s',buoy_k.shape)
    logging.debug('buoy_phi %s',buoy_phi.shape)
    kdkdphi_sar = np.array((np.tile(buoy_k[indices_k_onfrange]*dk_buoy[indices_k_onfrange]*dphi_sar, (buoy_phi.size, 1))))
    kdkdphi_sar = kdkdphi_sar.squeeze()
    if buoy_spec.shape[0] == len(kdkdphi_sar):
        buoy_spec = buoy_spec.T
    if buoy_spec.shape != kdkdphi_sar.shape:
        kdkdphi_sar = kdkdphi_sar.T
    energy_transpo = buoy_spec[indices_k_onfrange,:].squeeze()
    hs_particular_frge = 4*np.sqrt(np.sum(energy_transpo*kdkdphi_sar))
    return hs_particular_frge

def compute_sar_hs_from_spectra_on_freq_range(buoy_spec,buoy_k,buoy_phi,freqrange):
    """
    same as above but changing dimensions order
    uathor Antoine Grouazel
    to be validated
    values given on one example seems ok:
    WMO41043_20160601T0000_20160630T2300_Lat_21.02N_Lon_64.85W_spectrum.nc
    frequances plage 1 (0.04, 0.08)
    Hs 0.0979796027297 m
    frequances plage 3 (0.15, 0.228)
    Hs 1.174393419 m
    frequances plage 2 (0.08, 0.15)
    Hs 1.49244770081 m
    frequances plage 4 (0.0, 0.3)
    Hs 1.99924986627 m
    """
    buoy_spec = buoy_spec.T
    fmin,fmax = freqrange
    if fmin!=0:
        
        Tmax = 1./fmin
    else:
        Tmax = 30 #arbitrary value I dont know what could be a possible max period
    Tmin = 1./fmax
    wlmin = period2wavelength(Tmax)
    wlmax = period2wavelength(Tmin)
#     print wlmin,wlmax
    kb = 2.*np.pi/wlmax
    ka = 2.*np.pi/wlmin
    if ka>=kb:
        kmin = kb
        kmax = ka
    else:
        kmin = ka
        kmax = kb
    logging.debug("kmin %s",kmin)
    logging.debug("kmax %s",kmax)
    indices_k_onfrange = np.where((buoy_k<kmax) & (buoy_k>kmin))
    logging.debug("indices_k_onfrange %s",indices_k_onfrange)
    dphi_sar = np.radians(np.array(abs(buoy_phi[1]-buoy_phi[0])))
    dk_buoy = np.ma.zeros(buoy_k.size)
    dk_buoy[0] = buoy_k[1]-buoy_k[0]
    dk_buoy[-1] = buoy_k[-1]-buoy_k[-2]
    dk_buoy[1:-1] = (buoy_k[2:]-buoy_k[0:-2])/2
    logging.debug("dk_buoy %s",dk_buoy.shape)
    logging.debug("dphi_sar %s %s",dphi_sar.shape,dphi_sar)
    logging.debug('buoy_k %s',buoy_k.shape)
    logging.debug('buoy_phi %s',buoy_phi.shape)
    kdkdphi_sar = np.array((np.tile(buoy_k[indices_k_onfrange]*dk_buoy[indices_k_onfrange]*dphi_sar, (buoy_phi.size, 1))))
    kdkdphi_sar = kdkdphi_sar.squeeze()
    energy_transpo = buoy_spec[indices_k_onfrange,:].T.squeeze()
    hs_particular_frge = 4*np.sqrt(np.sum(energy_transpo*kdkdphi_sar))
    return hs_particular_frge
    
def compute_buoy_hs_effective_SAR_imaging_domain(buoy_spec,buoy_k,buoy_phi,file_handler_s1,osw=None):
    """
    imaging SAR domain restricted to cut off in azimuth and range
    copy/paste from get_hs_effective_2D_elipsoid_cutoff
    :args:
        buoy_spec (2d):
        buoy_k (1d):
        buoy_phi (1d): degree clockwise relative to geo north
    """
    #version_reread_s1file = True
    #if version_reread_s1file:
    if osw is None:
        k_eff = 2*np.pi/file_handler_s1.variables['oswSpecRes'][:].squeeze()
        k_sar = file_handler_s1.variables['oswK'][:]
        phi_sar   = np.array(file_handler_s1.variables['oswPhi'][:].squeeze())
        k_cutoff = 2*np.pi/file_handler_s1.variables['oswAzCutoff'][0][0]
        s1a_spec = file_handler_s1.variables['oswPolSpec'][:].squeeze()
        partoch = file_handler_s1.variables['oswPartitions'][:].squeeze()
    else:
        k_sar = osw['oswK']
        phi_sar = osw['oswPhi']
        k_cutoff = 2*np.pi/osw['oswAzCutoff']
        s1a_spec = osw['oswPolSpec']
        k_eff = 2*np.pi/osw['oswSpecRes']
        partoch = osw['oswPartitions']
        
#   
    if isinstance(k_cutoff,np.ma.core.MaskedConstant):
        swh_eff_2d_buoy = np.nan
        swh_buoy = np.nan
        buoywavelengthpeak = np.nan
        buoywavelengthpeak_eff = np.nan
        buoydirectionpeak = np.nan
        buoydirectionpeak_eff = np.nan
    else:
        mask = s1a_spec*0.
        toto_ksar = copy.copy(k_sar)
        k_sar_2D = np.array((np.tile(toto_ksar, (phi_sar.size, 1))))
        mask[k_sar_2D<k_cutoff] = 1
        #logging.debug('pct ok k sar %1.1f%%',100.0*np.sum(k_sar.mask)/k_sar.size)
        logging.debug('pct ok k sar %1.1f%%',100.0*np.sum(np.isnan(k_sar))/k_sar.size)
        logging.debug('phi sar: %s',phi_sar)
        logging.debug('phi buoy %s',buoy_phi)
        logging.debug('buoy spec: %s',100.0*np.sum(np.isnan(buoy_spec))/buoy_spec.size)
        buoy_spec_interp = interp_polar_spectrum(toto_ksar, phi_sar, 
                                                    buoy_spec, buoy_k, buoy_phi)
        logging.debug('interpolated spec: %s',100.0*np.sum(np.isnan(buoy_spec_interp))/buoy_spec_interp.size)
        logging.debug('k buoy: %s',buoy_k)
        k_eff_2D = np.array((np.tile(k_eff, (toto_ksar.size, 1))).T)
        mask_eff = s1a_spec*0.
        mask_eff[k_sar_2D<k_eff_2D] = 1
    #     k_sar.fill(np.nan)
        dk_sar = np.ma.zeros(k_sar.size)
        logging.debug('k_sar %s dk_sar %s ',type(k_sar),type(dk_sar))
    #     pdb.set_trace()
        dk_sar[0] = k_sar[1]-k_sar[0]
        dk_sar[-1] = k_sar[-1]-k_sar[-2]
        dk_sar[1:-1] = (k_sar[2:]-k_sar[0:-2])/2
    
        kdk_sar = np.array((np.tile(k_sar*dk_sar, (phi_sar.size, 1))))
        dphi_sar = np.array(abs(phi_sar[1]-phi_sar[0])*np.pi/180.)
        kdphi_sar = np.array((np.tile(k_sar*dphi_sar, (phi_sar.size, 1))))
        kdkdphi_sar = np.array((np.tile(k_sar*dk_sar*dphi_sar, (phi_sar.size, 1))))
        buoy_spec_interp_azc = buoy_spec_interp*mask
        buoy_spec_interp_eff = buoy_spec_interp*mask_eff
        # k-1D Spectra
        spk_buoy = np.sum(buoy_spec_interp*mask*kdphi_sar,0)
        spk_buoy_eff = np.sum(buoy_spec_interp*mask_eff*kdphi_sar,0)
        # Phi-1D Spectra
        spp_buoy = np.sum(buoy_spec_interp*mask*kdk_sar,1)
        spp_buoy_eff = np.sum(buoy_spec_interp*mask_eff*kdk_sar,1)
        # Significant Wave Height
        swh_buoy = 4*np.sqrt(np.sum(buoy_spec_interp*kdkdphi_sar))
        swh_eff_2d_buoy = 4*np.sqrt(np.sum(buoy_spec_interp*kdkdphi_sar*mask_eff))
        
        #peak variable
    #     pdb.set_trace()
        if not np.isnan(np.max(spk_buoy)):
            _ind = np.where(spk_buoy==np.nanmax(spk_buoy))[0][0]
            buoywavelengthpeak = 2*np.pi/k_sar[_ind]
        else:
            buoywavelengthpeak = np.nan
        logging.debug('spp_buoy = %s',spp_buoy)
        logging.debug('np.nanmax(spp_buoy) = %s',np.nanmax(spp_buoy))
        if np.nanmax(spp_buoy) != np.nan:
            buoydirectionpeak = np.nan
        else:
            _ind = np.where(spp_buoy==np.nanmax(spp_buoy))[0][0]
            buoydirectionpeak = phi_sar[_ind]
        # Peak Parameters with  variable cut off
        if not np.isnan(np.max(spk_buoy_eff)):
            _ind = np.where(spk_buoy_eff==np.nanmax(spk_buoy_eff))[0][0]
            buoywavelengthpeak_eff = 2*np.pi/k_sar[_ind]
        else:
            buoywavelengthpeak_eff = np.nan
        #calcul wavelength on partition ponderated
        energy_not_sumed = buoy_spec_interp*mask_eff*kdphi_sar
        #here i reuse the partition of ESA SAR product and apply it to buoy
        #for simplicity I use only the first SAR partition label=1
        maskpartition  = (partoch!=1)
        energy_not_sumed[maskpartition] = 0 #je met a zero tout les bins d energies qui ne sont pas dans la partition
        denominater = np.sum(energy_not_sumed)
        buoywl_partSAR1_weightened = np.sum(energy_not_sumed*2*np.pi/k_sar)/denominater
        #end 
            
        logging.debug('spp_buoy_eff = %s',spp_buoy_eff)
        logging.debug('np.nanmax(spp_buoy_eff) = %s',np.nanmax(spp_buoy_eff))
        if not np.isnan(np.max(spp_buoy_eff)):
            _ind = np.where(spp_buoy_eff==np.nanmax(spp_buoy_eff))[0][0]
            buoydirectionpeak_eff = phi_sar[_ind]
        else:
            buoydirectionpeak_eff = np.nan
    return swh_eff_2d_buoy,swh_buoy,buoywavelengthpeak,buoywavelengthpeak_eff,buoydirectionpeak,buoydirectionpeak_eff,buoywl_partSAR1_weightened
    
