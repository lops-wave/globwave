"""
context: monthly rep of myocean is more updated every day (only once per month)
creation: 30/05/2016
purpose: read/map the daily data in latest, concat the 30 last days for the current month and then convert it to globwave
note: big drawback with pointtimeseries wa cannot have 2D variable that depends on Z dimension because the  get_geolocation_field_dimnames('depth') return station...
"""
import sys
import os
import glob
import logging
import collections
import numpy

import calendar
import datetime
import copy
import netCDF4
import traceback
import string
from cerbere.datamodel.pointtimeseries import PointTimeSeries
from cerbere.datamodel.field import Field, QCLevel, QCDetail
from cerbere.datamodel.variable import Variable
from cerbere.mapper.ncfile import NCFile
from cerform.cfconvention import get_convention,get_standardname
from cerform import cfconvention
from globwave.src.insitu.variables_conventions import VarConventions,freqDim,freqVarName,freqRangeName
from globwave.src.insitu.timeserieconcat import ConcatenateFiles, findContigousPlatformFiles, findWidderPlatformFile
from globwave.src.insitu.oceansitesbuoymooredbuoy import OceansitesMooredBuoy
from globwave.src.insitu.read_buoys_data.coriolis_common_create_field import WriteGWOceansiteFile,DefineGlobalMetaData,CreateFieldcomon
from globwave.src.insitu.globwave_shared_infos import UNWANTEDVAR,DICO,DetermineInstrumentGroups
from ceraux.landmask import Landmask
from globwave.src.insitu.shared_convert_methods import GetFinalNameOfTheBuoy,treshold_to_create_new_file,Define_output_filename,time_units
from ceraux.landmask_resources import LAND_MASK_PATH
from time import gmtime, strftime 
processor_version = '0.9'
FLAGS_GEN = []
unwanted_files = ['TS_RF','TS_DB','TS_DC','PR_PF','PR_GL','PR_CT']
EXCEPT_LIST = "/home/cercache/users/agrouaze/resultats/globwave_monitore/list_coriolis_files_not_converted_"+strftime("%Y%m%d_%Hh", gmtime())+".txt"
TREATED_FILES = "/home/cercache/users/agrouaze/resultats/globwave_monitore/list_coriolis_files_converted_"+strftime("%Y%m%d_%Hh", gmtime())+".txt"
NOT_STABLE_FILES = "/home/cercache/users/agrouaze/resultats/globwave_monitore/list_coriolis_files_not_stable_enough_"+strftime("%Y%m%d_%Hh", gmtime())+".txt"
INPUT_DATA_DIR = '/home/datawork-coriolis-public/exp/co05/co0524/co052409/co05240907/co0524090706/nrt/global/latest/'
INPUT_DATA_DIR_MONTHLY = '/home/datawork-coriolis-public/exp/co05/co0524/co052409/co05240907/co0524090706/nrt/global/monthly/MO/'
def wrapper_daily_createfield(feature,v,intstart,intend,dims,qc_levels,qc_details):
    sl = {'time':slice(intstart,intend,1)}
    inputvalues = copy.deepcopy(feature.get_values(v,slices=sl))
    inputVarDescription = feature.get_field(v).variable.description
    input_units = feature.get_field(v).units
    fieldojb = CreateFieldcomon(v,dims,qc_levels,qc_details,inputvalues,inputVarDescription,input_units)
    return fieldojb

def list_distinct_buoys_in_latest(buoy_id=None):
    """
    return a structure with the buoy name and associated filepath
    """
    res = {}
    if buoy_id:
        pattern_add = '*'+buoy_id
    else:
        pattern_add = ''
    # list_nc = glob.glob('/home/coriolis_exp/spool/co01/co0134/co013407/co01340701/latest/*/'+pattern_add+'*.nc')
    pattern = os.path.join(INPUT_DATA_DIR,'*',pattern_add+'*.nc')
    logging.info('pattern = %s',pattern)
    list_nc = glob.glob(pattern)
    logging.info('there is currently %s TS_MO in the latest dir',len(list_nc))
    for uf in list_nc:
        # type_data = os.path.basename(uf)[10:15]
        type_data = os.path.basename(uf)[3:8]
#         if type_data not in unwanted_files:
        if type_data in ['TS_MO'] :#and 'TG' not in uf:
            id_buoy = os.path.basename(uf).split('_')[4]
            if id_buoy not in res:
                res[id_buoy] = [uf]
            else:
                res[id_buoy].append(uf)
#         else:
#             logging.debug('%s doesnt come from  a mooring',uf)
    if buoy_id and res=={}:
        logging.info('No files found matching with the pattern: %s',buoy_id)
    return res

def concat_monthly_data(yearmonth,buoy,list_file_struc):
    """
    return a struct containing all the data for a given buoy
    mapping of source data
    """
    logging.debug('concat for buoy %s month %s',buoy,yearmonth)
    logging.debug('Nber of files to read :%s',len(list_file_struc[buoy]))
    
    res = collections.OrderedDict()
    fields_container = collections.OrderedDict()
#     df = pd.DataFrame()
    list_files = list_file_struc[buoy]
    list_files.sort()
    logging.debug('first : %s',list_files[0])
    logging.debug('last : %s',list_files[-1])
    if len(list_files)>0:
#         df_previous_day = pd.DataFrame({'dates':[]})
        all_fields = []
        for jj,ff in enumerate(list_files):
            logging.debug('file ;: %s',ff)
            nc = NCFile(ff,'r')
#             nc = netCDF4.Dataset(ff,'r')
            timesu = nc.read_values('TIME')
            time_unit = nc.read_field('TIME').units
            if 'time' not in res:
                res['time'] = timesu
            else:
                res['time'] = numpy.concatenate([res['time'],timesu])
            fields = nc.get_fieldnames()
            for gg in fields:
                if gg not in all_fields:
                    all_fields.append(gg)
#             dico_for_df = {'dates':timesu}
            for vv in all_fields:
                
                    
                if vv not in ['TIME','DEPH','LONGITUDE','LATITUDE']+UNWANTEDVAR:
                    
                    if vv not in fields:#si un champs nest pas present un jour donne
                        dimension_block = (len(timesu),res[vv].shape[1])
                        values = numpy.ma.empty(dimension_block,dtype=res[vv].dtype)
                        values.mask=numpy.ones(values.shape)
                    else:
                        logging.debug('variable: %s',vv)
        #                 values = nc.variables[vv][:]
                        field_i = nc.read_field(vv)
                        if vv not in fields_container:
                            fields_container[vv] = field_i
                        values = nc.read_values(vv)
#                     logging.debug('type of values: %s %s',type(values),values.shape)
                    if vv not in res: #rattrapage si un champs apparait au bout du xiem jour
                        if len(values.shape)>1:
                            dimension_block = (len(res['time']),values.shape[1])
                        else:
                            dimension_block = (len(res['time']))
                        tmp_filler = numpy.ma.empty(dimension_block,dtype=values.dtype)
                        tmp_filler[-len(values):] = values
                        tmp_filler[0:-len(values)-1].mask = True
                        res[vv] = tmp_filler
                    else:
                        res[vv] = numpy.ma.concatenate([res[vv],values])
                    res[vv] = numpy.ma.masked_where(res[vv]==field_i.fillvalue, res[vv], copy=True)
#                     dico_for_df[vv] = values #original
#                     dico_for_df[vv] = values.squeeze() #for test 1D -> pandas
#             print dico_for_df.keys()
#             for hk in dico_for_df.keys():
#                 print hk,dico_for_df[hk].shape
#             print dico_for_df['VAVH'].shape
#             df_daily = pd.DataFrame(dico_for_df)
#             df_daily.index = df_daily.dates
#             print dico_for_df['dates']
#             print df_daily.dates
#             print df_previous_day.dates
#             df_merged = pd.merge(df_daily,df_previous_day,on='dates',how='outer')
#             df_previous_day = df_merged
            if jj==0:
                attr_list = nc.read_global_attributes()
                metadata = collections.OrderedDict()
                for att in attr_list:
                    metadata[att] = nc.read_global_attribute(att)
                lon = nc.read_values('LONGITUDE')
                lat = nc.read_values('LATITUDE')
                if 'DEPH' in fields:
                    depths = nc.read_values('DEPH') 
                    depths = depths[0,0].squeeze()#to take only one time step
                elif 'DEPTH' in fields:
                    depths = nc.read_values('DEPTH')
                    depths = depths[0].squeeze()
                else:
                    depths = 0
                if not numpy.isfinite(depths):
                    depths = 0
                logging.debug('depths read from source: %s %s',depths,type(depths))
#                 print nc.getAttributes()
            nc.close()
    time_dim = collections.OrderedDict([('time',len(res['time']))])
    time_var = Variable(shortname='time', description='time', authority=get_convention(), standardname=get_standardname('time'))
    time_field = Field(variable=time_var, dimensions=time_dim, values=res['time'],units=time_unit)
    logging.debug('first time: %s last time: %s',netCDF4.num2date(res['time'][0],time_unit),netCDF4.num2date(res['time'][-1],time_unit))
#     print fields_container.keys()
#     print df_merged.keys()
    for paramo in fields_container:
        if paramo not in ['TIME']:
            fields_container[paramo].set_values(res[paramo])
            #same but using pandas
#             values_concat = df_merged[paramo].values
#             values_concat = numpy.ma.masked_where(numpy.isnan(values_concat),values_concat,copy=True)
#             fields_container[paramo].set_values(values_concat)
            source_dim = fields_container[paramo].dimensions
            if 'TIME' in source_dim or 'POSITION' in source_dim:
#                 source_dim.popitem('TIME')
#                 source_dim['time'] = len(res['time'])
#                 source_dim.update(('time',len(res['time'])))
                extra_dims = [('time',len(res['time']))]
#                 extra_dims = [('time',len(df_merged))]
                if 'DEPTH' in source_dim:
                    
                    extra_dims.append(('depth',source_dim['DEPTH']))
                if 'FREQUENCY' in source_dim:
                    extra_dims.append('frequency' ,source_dim['frequency'] )
                for ii in source_dim:
#                     print ii
                    if ii not in  ['TIME','POSITION','DEPTH']:
#                         print source_dim[ii]
                        extra_dims.append((ii,source_dim[ii]))
#                 extra_dims.append()
                new_dim = collections.OrderedDict(extra_dims)
                fields_container[paramo].dimensions = new_dim
    
    
    feature = PointTimeSeries(identifier='', title='', description='', source='',
                               metadata=metadata, fields=fields_container,
                                longitude=lon, latitude=lat, depth=depths, times=time_field)

    return feature

# def DetermineInstrumentGroups(feature):
#     # determine instrument groups
#     logging.debug("determine instrument groups ")
#     groups = {}
#     for v in feature.get_fieldnames():
#         #if not v in UNWANTED and not v in FLAGS_GEN:
#         logging.debug('variables found %s',v)
#         if v not in UNWANTEDVAR and v[-3:]!='_QC' and  v[-3:] != '_DM' and v[0:4]!='GPS_':
#             res = VarConventions(DICO[v][0],return_accro=True)
#             if res is not None:
#                 GLOBEWAVE_input_var_name,longname,description,sensor,units,acronym = res
# #             if v in DICO and not v in FLAGS_GEN:
#                 #logging.debug("wanted", v)
# #                 sensor = self.get_instrument(v)
#                 if groups.has_key(sensor):
#                     groups[sensor].append(v)
#                 else:
#                     groups[sensor] = [v]
#             else:
#                 logging.warning('variable %s is not referenced in dictionary ',DICO[v][0])
#     return groups
    
    
def CreateTimeMaskInstrument(feature,instr,groups):
    '''
    create a time mask to determine period without any data for this instrument
    (we cumulate the mask of each sensor)
    '''
    tmask = None
    for v in groups[instr]:
            #logging.debug(v)
        values = feature.get_values(v,copy=True)
        dimensions = feature.get_field(v).dimensions
        logging.debug("create time mask for instrument %s",instr)
        if len(dimensions) == 1:
            if tmask is None:
                tmask = numpy.ma.getmask(values)
            else:
                tmask = numpy.ma.make_mask( tmask & numpy.ma.getmask(values), shrink=False )
        if len(dimensions) == 2:
            logging.debug("time mask: 2D variables ")
            if tmask is None:
                #tmask = numpy.ma.getmask(ifd.variables[v][:])
                tmask = numpy.ma.MaskedArray(values).all(axis=1).mask
            else:
                # cumulating masks
                #tmask = numpy.ma.make_mask( tmask & numpy.ma.getmask(ifd.variables[v][:]) , shrink=False)
                tmask = numpy.ma.make_mask( tmask & numpy.ma.MaskedArray(values).all(axis=1).mask, shrink=False)
            
        if len(dimensions) == 3:                    
            logging.warning("Error : 3D variable not supported !")
            raise Exception
        return tmask

def UseFlagFromProvider(feature):
    '''
    validate data with QC flag from source file and return a mask 
    Args:
        feature (cerbere datamodel): contains mapping of source data
    Returns:
        flag_suspect_2d (nd.array): 
    '''
    #TODO: revoir entierement var je ne comprend pas ce a quoi sert le mask creer et comment il est creer
    fmaskSuspect = None
    flag_suspect_2d = None
    timeSize = len(feature.get_times())
    for v in FLAGS_GEN:
        #logging.debug("Flags : ",v)
        
        # RULE : Flag position, time, depth  equal to 3, 4, 5, 8 and 9 => suspect 
        # ------
        # flag 0, 1, 2, 7 => good
        # fmaskSuspect ON ONE AXIS ONLY
        #if ifd.variables[v].ndim == 1:
        values = feature.get_values(v).copy()
        if values.shape[0] == 1 and v[0:2] != 'DEP': #ag we dont want to use Z flags because it is sometime suspect flag on level above surface
            if values.size == timeSize : #parameter 1D
                fmask3 = numpy.ma.masked_greater_equal(values,3) # flag >= 3  suspect a true  
                fmask7 = numpy.ma.masked_not_equal(values,7)
                
                if fmaskSuspect == None:
                    fmaskSuspect = numpy.ma.make_mask( fmask3.mask & fmask7.mask , shrink=False)       # minus flag 7
                else:
                    fmaskSuspect = numpy.ma.make_mask( fmaskSuspect | numpy.ma.make_mask( fmask3.mask & fmask7.mask , shrink=False) , shrink=False)
            else:
                # suspect flags 
                if values[0] in [3, 4, 5, 8, 9]:
                    fmask = numpy.ma.getmaskarray(numpy.ma.masked_all((timeSize,)) )  # all positions suspect
                    if fmaskSuspect == None:                                
                        fmaskSuspect = fmask
                    else:                    
                        fmaskSuspect = numpy.ma.make_mask( fmaskSuspect | fmask , shrink=False)
        else:                    
            logging.debug(" flag_qc dimension not supported or Z flags not used by purpose %s" % v)
        
    # transpose to 2 axis
    if fmaskSuspect is not None:
        flag_suspect_2d = numpy.ma.expand_dims(fmaskSuspect, axis=1)
#     return fs,fmaskSuspect
    return flag_suspect_2d

def CutTimeSerie(tmask,feature,current_month):
    '''
    determine data intervals within the timeserie
    '''
    # ---------------------
#            if (tmask==False): 
    src_ti = copy.deepcopy(feature.get_times())
    logging.debug('lenth scr_ti %s shape: %s type %s',len(src_ti),src_ti.shape,type(src_ti))
    time_unit = feature.get_geolocation_field('time').units
#     times = []
#     for tt in range(len(src_ti)):
#         times.append(netCDF4.num2date(src_ti,time_unit))
    src_ti = numpy.array(src_ti)
    logging.debug('time units: %s',time_unit)
    logging.debug('times: %s',src_ti)
    times = netCDF4.num2date(src_ti,time_unit)
    
    logging.debug('lenth times %s',len(times))
    logging.debug('Nber of time step :%s first : %s last: %s',len(times),times[0],times[-1])
    current_month = datetime.datetime.strptime(current_month,'%Y%m')
    d0,de = calendar.monthrange(current_month.year,current_month.month)
    startday = current_month.replace(day=1)
    stopday = current_month.replace(day=de)
#     startday_num = netCDF4.date2num(startday,time_unit)
#     stopday_num = netCDF4.date2num(stopday,time_unit)
    if tmask.size == 1:
        masked_times  = numpy.ma.array( times, copy=True, mask=False )
    else:
        masked_times  = numpy.ma.array( times, copy=True, mask=tmask )
    masked_times = copy.deepcopy(numpy.ma.masked_where((times<startday) | (times>stopday),masked_times))
    intervals = []
    if (masked_times.mask==False).any():
        i0 = 0
        i1 = 1
        # Beyond 24 hours, another file is created
        for i,t in enumerate(masked_times):
#             logging.debug('i0 %s',i0)
#             logging.debug('masked_times.mask[i0] %s',masked_times.mask[i0])
#             logging.debug('month 0 %s motnh i %s',t.month,masked_times[i0].month) 
            if masked_times.mask[i0]:
                i0 = i
                i1 = i + 1
            elif masked_times.mask[i]:
                pass
            elif gap_intertaval_test(t,masked_times,i1,i0):
                intervals.append( (i0,i1) )
                i0 = i
                i1 = i+1
            else:
                i1 = i+1
        # last interval or no interval
        intervals.append( (i0,i1) )
    else:
        logging.info('no data for month: %s',current_month)
    return intervals,masked_times

def gap_intertaval_test(t,masked_times,i1,i0):
    """
    return True if new interval has to be created
    """
    need_interval = False
    if masked_times.mask[i0]==False:
        if t.month != masked_times[i0].month:
            need_interval = True
        
    if (t - masked_times[i1-1]).total_seconds() > 60*86400:
        need_interval = True
    return need_interval

def DetermineNatureOfNewFile(groups,instr,feature):
    """
    Returns:
        wavefreq (str): None or 'FREQUENCY' tells if there are frequency in the variable dimensions
        spectral (bool): redondant with wavefreq but in boolean
    """
    if 'wave_spectrum_r1' in feature.get_fieldnames():
        spectrumtype = 'directional'
    else:
        spectrumtype = 'nondirectional'
    for v in groups[instr]:
        if 'FREQUENCY' in feature.get_field(v).dimensions:
            wavefreq = 'FREQUENCY'
            spectral = True
            
        else:
            wavefreq = None
            spectral = False
    return wavefreq,spectral,spectrumtype

# def DefineDimensionOfVariable(feature,intend,intstart,v,wavefreq,spectrumtype,sensor):
#         '''define the dimensions of the new file for a given sensor'''
#         dims = collections.OrderedDict() ## ajout selon JFP
#         dimensions_field = feature.get_field(v).dimensions
# #         dims['time'] = intend - intstart #defaut dimensions
# #         if 'FREQUENCY' in dimensions_field:
# #             dims[ freqDim[spectrumtype] ] = len(dimensions_field[wavefreq])
# #         if sensor != 'wave_sensor':#we dont want z dimension for wave fields
# #             if 'DEPTH' in dimensions_field:
# #                 dims['z'] = len(dimensions_field['DEPTH'])
#         logging.debug('dimensions of the variable are %s',dims)
#         return dims

def BuildQualityFields(feature,variable,intstart,intend,fmaskSuspect,masked_times,dims,sensor):
    ''' Build quality fields, qc_levels and qc_details'''
    # --------------------
    #vals = numpy.ma.empty((intend - intstart,), dtype='i1')
    #variable_nb_dimension = ifd.variables[variable].ndim
    variable_nb_dimension = len(dims)
    fields = feature.get_fieldnames()
    dimensions_var = copy.deepcopy(feature.get_field(variable).dimensions)
    sl = {'time':slice(intstart,intend,1),'z':slice(0,0,1)}
    if string.join((variable,'_QC'),'') in fields:
#                            var_qc = ifd.variables[string.join((variable,'_QC'),'')][:]

#             var_qc = ifd.variables[string.join((variable,'_QC'),'')][intstart:intend][:,0:1]
#             var_qc = ifd.variables[string.join((variable,'_QC'),'')][intstart:intend,:]
        var_qc = copy.copy(feature.get_values(string.join((variable,'_QC'),''),slices=sl))
        var_qcSize = var_qc.size # for if conditions
        # initial array
        vals = numpy.ma.array(var_qc ,dtype='i1')
        logging.debug('initnal qc : %s',vals.shape)
        mask012 = numpy.ma.masked_outside(vals,0,2) # mask for oceansites qc 0, 1 , 2
        #mask7=numpy.ma.masked_not_equal(var_qc,7) # mask for oceansites qc 7       # NOT relevant                  
        mask34 = numpy.ma.masked_outside(vals,3,4) # mask for oceansites qc 3, 4
        mask5789 = numpy.ma.masked_outside(vals,5,9) # mask for oceansites qc 5,7,8,9 
        # convert initial flag array
        vals[numpy.ma.where(mask012)] = 4  # which is not masked converts to 4
        vals[numpy.ma.where(mask34)] = 2
##                        if mask7.mask.size == var_qcSize: vals[numpy.ma.where(mask7)]=4  # NOT relevant
        vals[numpy.ma.where(mask5789)] = 3
            
        # fmaskSuspect was transposed => fs
        if fmaskSuspect is not None:
            # RULE : if varflag good (4) and general QC flag suspect (3), then varflag converted to suspect (3)
            # -----
            fmaskGood = numpy.ma.masked_equal(vals,4) # good are masked
            logging.debug('intstart %s intend %s',intstart,intend)
            logging.debug('fmaskGood %s fmaskSuspect %s',fmaskGood.mask.shape,fmaskSuspect[intstart:intend].shape)
            maskFinalVar = numpy.ma.make_mask( fmaskGood.mask & fmaskSuspect[intstart:intend],shrink=False)
                                       
            vals[numpy.ma.where(maskFinalVar)] = 3
    else: #if there is no field_QC in netcdf
        if variable_nb_dimension == 1 :
            
            vals = numpy.ma.ones(intend-intstart ,dtype='i1')*4
        elif variable_nb_dimension == 2:
            
            dimension_name = dimensions_var.keys()[1]
            logging.debug('dimension_name %s',dimension_name)
            shape_second_dim = dimensions_var[dimension_name]
            logging.debug('value of the second dimension %s',shape_second_dim)
            vals = numpy.ma.ones((intend-intstart,shape_second_dim) ,dtype='i1')*4
        else:
            logging.error('the number of dimension is > 2 and it is an unexpected case')
            raise
#         if self.curretnVarNbDim==1:
#             vals = numpy.ma.array(numpy.ones(numpy.shape(ifd.variables[variable][intstart:intend])) ,dtype='i1')
#         else:
#             vals = numpy.ma.array(numpy.ones(numpy.shape(ifd.variables[variable][intstart:intend,:])) ,dtype='i1')

                        
    # Adjust according to dimensions (as physical parameters)
    if variable_nb_dimension == 1:
        vals = vals[:]
    elif variable_nb_dimension == 2:
        vals = vals[:,:]
    elif variable_nb_dimension == 3:
        vals = vals[:,0,0]
    else :
        logging.error("QC dimensions : dimensions of variable %s > 3" % variable)
        raise Exception
    if 'z' in dimensions_var and len(vals.shape)==2: #could be improve with slice to generalize to grater dimension number
        logging.debug('var: %s vals: %s',variable,vals.shape)
        vals = numpy.ma.squeeze(vals[:,0])
        vals = numpy.ma.reshape(vals,intend-intstart)
        
    
    vals.mask = numpy.ma.getmask(masked_times[intstart:intend])
    
    logging.debug('build QC levels field associated to %s with shape %s',variable,vals.shape)
    qc_levels = QCLevel( values = vals,
                                        dimensions = copy.copy(dims),
                                        levels = numpy.array([0,1,2,3,4],dtype='i1'),
                                        meanings = "Unknown Unprocessed Bad Suspect Good" )
#         vals = numpy.ma.empty((intend-intstart,),dtype='i2')
    vals_qc = numpy.ma.zeros(vals.shape)
#         vals_qc[:] = 0
#         vals.mask = numpy.ma.getmask(masked_times[intstart:intend])
    qc_details = QCDetail( values = vals_qc,
                                               dimensions = copy.copy(dims),
                                               mask = numpy.array([1],dtype='i1'),
                                               meanings = 'flagged_by_provider')
    logging.debug('build QC details field associated to %s with shape %s',variable,vals_qc.shape)
    return qc_details,qc_levels




# def CreateField(feature,v,intstart,intend,dims,qc_levels,qc_details):
#     '''instanciate cerbere class Field with geophysical variables of the provider netCDF
#     what to do on values:
#     squeeze the one with depth=1 ?
#     take only the interval time for the ones having time dimensions
#     
#     '''
#     logging.debug('get variable %s informations ',v)
# #         vname = self.get_cersatname(v)
# #     field_container = collections.OrderedDict()
# #     var_dimension_names = copy.deepcopy(feature.get_field(v).dimensions)
#     fieldobj = None
#     if v in DICO:
#         conv_info = VarConventions(DICO[v][0],return_accro=True)
#         if conv_info is not None:
#             vname,longname,description,sensor,stdunits,acronym = conv_info
#             logging.debug('translate %s ->%s-> %s',v,DICO[v],vname)
#             stdname = cfconvention.get_standardname(vname)
#             #logging.debug("original name:",v,"globawave name", vname,'stdanard name: ',stdname)
#                 # attributs !!!
#             descr = feature.get_field(v).variable.description
#             varobj = Variable(shortname=acronym,
#                                     description= longname,
#                                     authority = cfconvention.get_convention(),
#                                     standardname = stdname )
# #                 ovars[v] = varobj
# #             var_nb_dimension = ifd.variables[v].ndim
#             var_nb_dimension = len(dims)
#             logging.debug('%s has %s dimensions',v,var_nb_dimension)
# #             sl = {'time':slice(intstart,intend,1),'z':slice(0,0,1)}
#             sl = {'time':slice(intstart,intend,1)}
# #             values = feature.get_values(v)
#             values = copy.deepcopy(feature.get_values(v,slices=sl))
#             logging.debug('values of %s : %s => field %s',v,values.shape,dims.values())
# #                         values = numpy.ma.squeeze(values)
#             src_unit = feature.get_field(v).units
#             values = ConversionUnits(src_unit,v,values,stdunits)
#             logging.debug('shape of values for variable %s is %s ',v,values.shape)
#             values = numpy.ma.masked_array(values)
#             if values.count() != 0 and (values.mask==False).any():
#                 fieldobj = Field(varobj,
#                                                  copy.copy(dims),
#                                                  values = values,
#                                                  units = stdunits,
#                                                  qc_levels=qc_levels,
#                                                  qc_details=qc_details,
#                                                  attributes={'description':description,
#                                                              "description_provider":descr
#                                                              }
#                                                  )
# #                 field_container[v] = fieldobj
#             else:
#                 logging.warning('variable %s has not been added to the new netcdf because there is no useable values',v)
#                 logging.debug('type of values :%s   values.count:%s',type(values),values.count())
#         else:
#             logging.warning('%s is not defined in VarConventions',DICO[v])
#     else:
#         logging.warning('%s is not defined in DICO',v)
#     return fieldobj


def processOneVariable(feature,v,intstart,intend,fmaskSuspect,masked_times,wavefreq,spectrum_type,sensor):
    '''
    get the values, qc, and store them into Field and Variable cerbere classes
    returns:
        field_instance (cerbere datamodel instance):
    '''
    field_instance = None
    logging.debug('treated variable: %s',v)
#     dims = DefineDimensionOfVariable(intend,intstart,v,wavefreq,spectrum_type,sensor)
    dims = copy.copy(feature.get_field(v).dimensions)
    dims['time'] = intend-intstart
    logging.debug('var: %s dimn : %s',v,dims)

    qc_details,qc_levels = BuildQualityFields(feature,v,intstart,intend,fmaskSuspect,masked_times,dims,sensor)
    if wavefreq == True:
        logging.info('pour le moment je nai aucune bouee coriolis avec des spectres')
        raise NotImplemented
#         ofields = self.CreateSpectralFields(ofields,wavefreq,ifd,intstart,intend,spectrum_type,fs,fmaskSuspect,masked_times)
    # Create Variables and fields
    # ---------------------------
    if v == 'wave_spectrum_r1' or v == 'wave_spectrum_r2':
        logging.info('pour le moment je nai aucune bouee coriolis avec des spectres')
        raise NotImplemented
#         ofields,ovars = self.CreateFieldsWaveSpectrum(ifd,ofields,v,intstart,intend,dims)
    else: #nominal case for variable integration
        field_instance = wrapper_daily_createfield(feature,v,intstart,intend,dims,qc_levels,qc_details)
#         field_instance = CreateField(feature,v,intstart,intend,dims,qc_levels,qc_details)
#     if 'z' in dims:
#         dims.popitem('z')
#     pdb.set_trace()
    if field_instance is not None:
        field_instance.dimensions = copy.copy(dims)
    return field_instance

def CreateFinalTimeSerie(interval,instr,ofields,feature):
    '''
    Create Time Series and define final name of the file netCDF
    '''
    ts = None
    if ofields != {}:
        logging.debug('create final time serie for this timeinterval %s in this instrument %s',interval,instr)
#         untis_time = ifd.variables['TIME'].units
        time_field = copy.deepcopy(feature.get_geolocation_field('time'))
        intstart = interval[0]
        intend = interval[1]
        time_values = copy.deepcopy(feature.get_times()[intstart:intend])
        time_field.dimensions = collections.OrderedDict([('time',len(time_values))])
        time_field.set_values(time_values)
        logging.debug('timeserie first date: %s',netCDF4.num2date(time_values[0],time_field.units))
        lon = feature.get_lon()
        lat = feature.get_lat()
        depth = feature.get_z()
#         depth = numpy.squeeze(feature.get_z(slices={'time':slice(0,0,1)}))
#         depth = list(depth)
#         depth = 0
        logging.debug('lon : %s lat: %s depth: %s',lon,lat,depth)
        ts = PointTimeSeries( fields = ofields,
                                longitude = lon,
                                latitude = lat,
                                depth = depth,
                                times = time_field)
    return ts,time_field



def convert_coriolis_to_globwave(ts_source,month,outputdir,files_source):
    """
    treat one input file to generate N globwave files
    """
    listOfFilesCreated = []
#     ifd = netcdf.Dataset( f, 'r' )
    # get geolocation
#     geoloc_info = self.ReadGeolocationInformations(ifd,f)
#     if geoloc_info is not None:
#         FLAGS_GEN,position_qc,lon,lat,times,depth = geoloc_info
#     times = ts_source.get_times()
    input_fields = ts_source.get_fieldnames()
    groups = DetermineInstrumentGroups(input_fields)
    alloriginalglobalattr = ts_source.get_metadata()
    starttimeserie = ts_source.get_start_time()
    endtimeserie = ts_source.get_end_time()
    lat = ts_source.get_lat()
    lon = ts_source.get_lon()
    globalmetadata = DefineGlobalMetaData(files_source[0],alloriginalglobalattr,starttimeserie,endtimeserie,lat,lon,processor_version)
#     globalmetadata = DefineGlobalMetaData(files_source[0],ts_source)
    # process output files instrument by instrument
    logging.debug('loop over instrument in groups %s',groups)
#     logging.info('Nber of groups: %s',groups)
    for instr in groups:
        logging.debug('========= instrument %s  from %s ============',instr,files_source[0])
        tmask = CreateTimeMaskInstrument(ts_source,instr,groups)
        logging.debug("create flag mask with time_qc ,position_qc")
        fmaskSuspect = UseFlagFromProvider(ts_source)
        intervals,masked_times = CutTimeSerie(tmask,ts_source,month)
        # loop on time intervals
        # ---------------------
        logging.debug("intervals : %s",intervals)
        logging.info('Nber of intervals to treat: %s',len(intervals))
        for interval in intervals:
            logging.debug("interval %s", interval)
            intstart,intend = interval
            # Create Fields
            # -------------
#             ovars = {}
            ofields = collections.OrderedDict()
            if len(groups[instr]) > 0: ## ajout selon JFP
                logging.debug('loop over variables')
                wavefreq,spectral,spectrumtype = DetermineNatureOfNewFile(groups,instr,ts_source)
                for v in groups[instr]:
                    new_field = processOneVariable(ts_source,v,intstart,intend,fmaskSuspect,masked_times,wavefreq,spectrumtype,instr)
                    if new_field:
                        ofields[v] = new_field
                    else:
                        logging.debug('%s field has not been added to the final feature',v)
                ts,timefield = CreateFinalTimeSerie(interval,instr,ofields,copy.deepcopy(ts_source))
                
                if ts:
                    logging.debug('ts %s',ts)
                    logging.debug('fields: %s',ts.get_fieldnames())
                    logging.debug('lat values shape %s',ts.get_geolocation_field('lat').get_values().shape)
                    units = timefield.units
                    start_dt = netCDF4.num2date(timefield.get_values()[0],units)
                    end_dt = netCDF4.num2date(timefield.get_values()[-1],units)
                    lon  = ts_source.get_lon()
                    lat = ts_source.get_lat()
                    ofname = Define_output_filename(instr,lon,lat,start_dt,end_dt,globalmetadata,spectral,outputdir)
                    listOfFilesCreated = WriteGWOceansiteFile(ofname,listOfFilesCreated,ts,globalmetadata)
                else:
                    logging.info('timeserire %s %s is empty',instr,interval)
            else:
                logging.info('there is no variable in %s %s',instr,interval)
    return listOfFilesCreated

def secure_time_dim_vs_val(feature):
    """
    it can append that a variable is not present in all daily files thus the dimensions time will be higher than the shape of thz variable
    the workaround is to interpolate the variable on the fat time vector
    note: cest mieux que rien mais on perd les param qui ne sont pas dans tout les fichiers daily et ca cest naze...
    """
    new_fature = PointTimeSeries(identifier=feature.identifier,
                                  title=feature.title,
                                   description=feature.description,
#                                     source=feature.source,
                                     metadata=feature.metadata,
#                                       fields,
                                       longitude=feature.get_lon()[0][-1]
                                    , latitude=feature.get_lat()[0][-1],
                                     depth=feature.get_z(),
                                      times=feature.get_geolocation_field('time'))
#     fields_ok = collections.OrderedDict()
    for pp in feature.get_fieldnames():
        tval = feature.get_values(pp).shape[0]
        tdim = feature.get_field(pp).dimensions['time']
        if  tval!=tdim :
            logging.warning(' shap of values : %s is not equal to dimensions time :%s',tval,tdim)
        else:
            fiedl_param = copy.deepcopy(data_buoy.get_field(pp))
            new_fature.add_field(fiedl_param)
#             fields_ok[pp] = data_buoy.get_field(pp)
#     new_fature.set_fields(fields_ok)
    return new_fature

if __name__ == '__main__':
    """
    usage with 
    setenv PYTHONPATH /home/losafe/users/agrouaze/git/cerbere_gitlab/cerbere:/home/losafe/users/agrouaze/git/cerform:/home/losafe/users/agrouaze/git/globwave/src/insitu:/home/losafe/users/agrouaze/git:/home/losafe/users/agrouaze/git/ceraux
    """
    root = logging.getLogger()
    if root.handlers:
        for handler in root.handlers:
            root.removeHandler(handler) 
    import argparse
    parser = argparse.ArgumentParser(description='coriolis globwave')
#     parser.add_argument('-a','--action', type=str, choices=['in_progress','finished','failed'])
    parser.add_argument('--verbose', action='store_true',default=False)
    parser.add_argument('--no-failure', action='store_true',default=False)
    parser.add_argument('-m','--month',required=False,help='month YYYYMM')
    parser.add_argument('-b','--buoy',required=False,help='buoy ID (ex: 62149)')
    parser.add_argument('-o','--outputdir',required=False,help='where to save the netcdf (sensor/year/month/*nc)')
    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    if args.month:
        wanted_month = datetime.datetime.strptime(args.month,'%Y%m')
        dates = [wanted_month]
    else:#in operational i prefer not to use the schedul date but only the execution date to avoid uncontrolled input/output issues
        dates = []
        months = []
        current_month = datetime.datetime.today()#.strftime('%Y%m')
        dates.append(current_month)
        if False:
            daysago = (datetime.datetime.today() - datetime.timedelta(days=7))#.strftime('%Y%m') #we need this second date because insitu data can take time to come# update april19 the M-1 processing will be treated separatly
            if daysago.strftime('%Y%m') not in months:
                dates.append(daysago)
                months.append(daysago.strftime('%Y%m'))
#     current_month = '201605'
#     buoy_id = '62149'
#     buoy_id = None
    buoy_id = args.buoy
    if buoy_id is None:
        logging.info('no specific buoy asked')
    else:
        logging.info('specific buoy asked %s',buoy_id)
    counters = collections.defaultdict(int)
    if args.outputdir:
        outdir = args.outputdir
    else:
        outdir = '/tmp/new_convertion_coriolis'
    list_file_struc = list_distinct_buoys_in_latest(buoy_id)
    logging.info('There is %s distinct buoys present in the latest directory,',len(list_file_struc))
    list_converted = []
    for month_to_treat in dates:
        logging.info('treat the month: %s',month_to_treat)
        today = datetime.datetime.today()
#         month_to_treat_dt = datetime.datetime.strptime(month_to_treat,'%Y%m')
        month_to_treat_dt = month_to_treat #fix agrouaze to avoid cases when the previous month is considered as higher than 32 days but 
        if today-month_to_treat_dt>datetime.timedelta(days=32): #last 32 days in the /lastest dir
            #lorsque la date est passe les donnes sont supprime du repertoire latest myocen et il faut exploiter les donnees du monthly
            logging.warning('More than 31 days separated the current date and the month to treat, switch to OceansitesMooredBuoy method and monthly data')
            # input_file = os.path.join('/home/coriolis_exp/spool/co01/co0134/co013407/co01340701/monthly/mooring/',month_to_treat.strftime('%Y%m'))
            input_file = os.path.join(INPUT_DATA_DIR_MONTHLY,month_to_treat.strftime('%Y%m'))
            buoy = OceansitesMooredBuoy()
            counters = buoy.convert_a_whole_directory(input_file,outdir,trymode=True,buoyid=args.buoy)
        else:
            #attention au cas ou la date de schedule {{ ds }} est anterieur de 1 mois par rapport a la date effective du run ce qui amene a lire dans latest la ou il ny a plus de data
            for mm,uu in enumerate(list_file_struc.keys()):
                logging.info('buoy %s %s/%s',uu,mm+1,len(list_file_struc.keys()))
        #         print uu,len(list_file_struc[uu])
                try:
                    data_buoy = concat_monthly_data(month_to_treat.strftime('%Y%m'),uu,list_file_struc)
        
            #     print len(data_buoy['TIME'])
        #         print data_buoy
                    data_buoy = secure_time_dim_vs_val(data_buoy)
                    if args.verbose:
                        for pp in data_buoy.get_fieldnames():
                            print(pp,data_buoy.get_values(pp).shape,data_buoy.get_field(pp).dimensions)
            #     for pp in data_buoy.keys():
            #         logging.debug('%s %s',pp,data_buoy[pp].shape)
        
                    list_converted = convert_coriolis_to_globwave(ts_source=data_buoy,month=month_to_treat.strftime('%Y%m'),outputdir=outdir,files_source=list_file_struc[uu])
                    counters['conversion_ok'] +=1
                except KeyboardInterrupt:
                    sys.exit(1)
                except:
                    list_converted = []
                    logging.error('%s',traceback.format_exc())
                    list_exc = open(EXCEPT_LIST, 'a')
                    list_exc.write(list_file_struc[uu][0]+'\n')
                    list_exc.close()
                    counters['conversion_failed'] +=1
                    if args.no_failure:
                        sys.exit(1)
    logging.info('Nber of files converted :%s Nber of inputs buoys (distinct): %s',len(list_converted),len(list_file_struc))
    logging.info('%s',counters)
    logging.info('failures: %s',EXCEPT_LIST)