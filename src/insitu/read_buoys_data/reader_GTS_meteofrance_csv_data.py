"""
uatho: antoine Grouzael
creation: april 2019
for gascogne and brittany buoys
#ff = '/home/agrouaze/Documents/buoy_gascogne/Gascogne-TRIAXYS_20190201_20190330.csv'
ff = '/home/cercache/project/globwave/data/provider/GTS_via_meteofrance/Brittany-TRIAXYS_20161215_20170125.csv'
tests in http://br156-191.ifremer.fr:8888/notebooks/workspace_leste/sentinel1_perso/reader_buoy_gascogne.ipynb
"""
import pandas as pd
import os
import sys
import datetime
import logging
import glob
import numpy as np
import cerform.wave_formula as wowo
from matplotlib import pyplot as plt
import matplotlib
from dateutil import relativedelta,rrule
from globwave_shared_infos import active_networks


#given from read me
frequencies =  np.array([0.03, 
               0.035,
                0.04,
               0.045,
                0.05,
               0.055,
                0.06,
               0.065,
                0.07,
               0.075,
                0.08,
               0.085,
                0.09,
               0.095,
               0.101,
                0.11,
                0.12,
                0.13,
                0.14,
                0.15,
               0.163,
                0.18,
                 0.2,
                0.22,
                0.24,
                0.27,
                0.31,
                0.35,
                0.39,
               0.445,
                0.52,
                 0.6])
dirs = np.arange(0,365,1) #arbitratry choice for spectra directions
def reader_GTS(filepath,startdate,stopdate):
    """
    :args:
        filepath (str):
        startdate (dt):
        stopdate (dt):
    :returns:
        df (pdans.DataFrame): containing the raw data as columns
        mat_spec (nd.array): 3D (freq,dirs,time) wave height spectra reconstructed using method
            Maximum Entropy Method - Lygre & Krogstad (1986 - JPO)
    """
#     fid = open(filepath)
#     data = fid.readlines()
#     fid.close()
    
    df = pd.read_csv(filepath,delimiter=';',header=0,parse_dates=[1])
#     print df.keys()
#     print df.count()
    mat_spec = None
    times = df['Date OBS'].dt.to_pydatetime()
    indices_time_ok = (times>=startdate) & (times<=stopdate)
    logging.info('Nb indices time OK in the file = %s',np.sum(indices_time_ok))
    mat = {}
    for vv in ['a1','b1','b2','a2']:
        for kk in df.keys():
            if 'Four' in kk and vv in kk:
                #print(kk)
                if vv not in mat:
                    mat[vv] = df[kk][indices_time_ok]
                    #print mat[vv].shape
                else:
                    mat[vv] = np.vstack([mat[vv],df[kk][indices_time_ok]])
    for pp in df.keys():
        if 'E' in pp:
#             logging.debug('%s', pp)
            if 'sf' not in mat:
                mat['sf'] = df[pp][indices_time_ok]
                logging.debug('%s', mat['sf'].shape)
            else:
                mat['sf'] = np.vstack([mat['sf'],df[pp][indices_time_ok]])
#                 logging.debug('%s', mat['sf'].shape)
    logging.info('mat["sf"] %s',mat['sf'].shape)
    for tt in range(mat['sf'].shape[1]):
        #could also test the wav_utils.py method from Wang He to double check. exactly same code
        sp2d,D = wowo.buoy_spectrum2d(mat['sf'][:,tt],mat['a1'][:,tt],mat['a2'][:,tt],mat['b1'][:,tt],mat['b2'][:,tt], dirs = dirs)
        #print sp2d.shape
        if tt==0:
            mat_spec = sp2d
        else:
            mat_spec = np.dstack([mat_spec,sp2d])
            if tt%50==0:
                logging.debug('%s %s',mat_spec.shape ,mat['sf'].shape[1])
    
    subtimes = times[indices_time_ok]
    logging.info('shape a1 = %s',mat['a1'].shape)
    return df,mat_spec,mat,subtimes


def find_right_file_for_a_month(buoy_id,year,month):
    """
    for a given month I have about 25 files between 1month and 2 month long
    I will take with the period the closest to 1 month
    :args:
        buoy_id (str): name of the buoy brittany or gascogne
        year (int)
        month (int)
    """
    month_sougth = datetime.datetime(year,month,1)
    nextmonth = month_sougth + relativedelta.relativedelta(months=1)
    pattern = os.path.join(active_networks['METEOFRANCE'],buoy_id+'-TRIAXYS_'+month_sougth.strftime(('%Y%m%d'))+'*.csv')
    logging.info("pattern search buoy %s",pattern)
    list_files = glob.glob(pattern)
    logging.info('all files found for %s = %s',buoy_id,len(list_files))
    closest_diff = 5000000
    best = None
    for ff in list_files:
        if '__' not in os.path.basename(ff):#remove empty files
            stop_date = datetime.datetime.strptime(os.path.basename(ff).split('_')[2].replace('.csv',''),'%Y%m%d')
            diff = abs((stop_date-nextmonth).total_seconds())
            if diff<closest_diff:
                closest_diff = diff 
                best = ff
    logging.info('fin tentative classic %s',best)
    if best is None:#je regarde pour les fichier qui commence un mois ou 2 ois avant et qui se termine dans le mois recherche
            pattern = os.path.join(active_networks['METEOFRANCE'],buoy_id+'-TRIAXYS_*'+month_sougth.strftime(('%Y%m'))+'*.csv')
            logging.info("tentative 2 pattern search buoy %s",pattern)
            list_files = glob.glob(pattern)
            logging.info('tentative 2 all files found for %s = %s',buoy_id,len(list_files))
            closest_diff = 5000000
            best = None
            for ff in list_files:
                if '__' not in os.path.basename(ff):#remove empty files
                    stop_date = datetime.datetime.strptime(os.path.basename(ff).split('_')[2].replace('.csv',''),'%Y%m%d')
                    diff = abs((stop_date-nextmonth).total_seconds())
                    if diff<closest_diff:
                        closest_diff = diff 
                        best = ff
    logging.debug('best candidate is %s with a diff of %s seconds',best,closest_diff)
    if closest_diff>60*60*24:
        logging.warn("hmm the best candaidate for %s GTS meteofrance file is may be weird %s",month_sougth,best)
    return best
    

def reader_GTS_wanghe_compliant(id,startDate,stopDate,provider):
    """
    in automated script we will run the script from the begining to the end of a month
    but in general we expect to be able to run on whatever period we need
    :args:
        id (str): name of the buoy brittany or gascogne
        startDate (dt):
        stopDate (dt):
        provider (str): meteofrance GTS
    """
    
    #step 0 find indices and files concerned by the period
    files_concerned = []
    cpt = 0 
    big_mat = {}
    f,sf,time,a1,a2,b1,b2 = None,None,None,None,None,None,None
    for mm in  rrule.rrule(rrule.MONTHLY,dtstart=startDate,until=stopDate):
        
        one_file = find_right_file_for_a_month(buoy_id=id,year=mm.year,month=mm.month)
        logging.info('month %s file found : %s',mm,one_file)
        if one_file is not None:
            files_concerned.append(one_file)
            #step 1 read the data
            df,mat_spec,mat,times = reader_GTS(one_file,startDate,stopDate)
            #step 2) using the indices and filepath read the data
    #         indices_files = {}
            if mat_spec is not  None:
                
                if cpt==0:
                    big_times = times
                    for vv in ['sf','a1','a2','b1','b2']:
                        big_mat[vv] = mat[vv]
                    cpt += 1
                else:
                    for vv in ['sf','a1','a2','b1','b2']:
                        big_mat[vv] = np.hstack([big_mat[vv],mat[vv]])
                    big_times = np.hstack([big_times,times])
                    cpt += 1
            else:
                logging.info('no valid data found')
        else:
            logging.info('no files found for  %s to %s',startDate,stopDate)
    if 'a1' in big_mat:
        logging.info('a1 big_mat = %s',big_mat['a1'].shape)
        f,sf,time,a1,a2,b1,b2 = frequencies,big_mat['sf'].T,big_times,big_mat['a1'].T,big_mat['a2'].T,big_mat['b1'].T,big_mat['b2'].T
    else:
        return None
    return time,f,sf,a1,a2,b1,b2




def plot_mean_spectra(mat_spec,ff=None,title=None,title2=None):

    cdict = {'red': ((0., 1, 1),
                     (0.05, 1, 1),
                     (0.11, 0, 0),
                     (0.66, 1, 1),
                     (0.89, 1, 1),
                     (1, 0.5, 0.5)),
             'green': ((0., 1, 1),
                       (0.05, 1, 1),
                       (0.11, 0, 0),
                       (0.375, 1, 1),
                       (0.64, 1, 1),
                       (0.91, 0, 0),
                       (1, 0, 0)),
             'blue': ((0., 1, 1),
                      (0.05, 1, 1),
                      (0.11, 1, 1),
                      (0.34, 1, 1),
                      (0.65, 0, 0),
                      (1, 0, 0))}
    my_cmap = matplotlib.colors.LinearSegmentedColormap('my_colormap', cdict, 256)
    mean_stacked = np.mean(mat_spec,axis=2)
    mean_stacked.shape
    dirs = np.arange(0,365,1)
    plt.figure(figsize=(12,10),dpi=150)
    ax = plt.subplot(1,2,1,polar=True)
    ax.set_theta_direction(-1) #clokwise\n",
    ax.set_theta_offset(np.pi/2.) #north on the top
    #ax.set_rscale(10.)
    
    min_wavelength = 50
    period = 1./np.array(frequencies)
    g = 9.81
    wavelength = (g*period**2)/(2. * np.pi)
    if ff :
        plt.title('mean spectra\n%s'%os.path.basename(ff),fontsize=11)
    elif title:
        plt.title(title)
    else:
        plt.title('mean spectra')
    #ax.set_rmin(0.333)
    ax.set_ylim(0,2.0*np.pi/min_wavelength)
    plt.contourf(np.radians(dirs),2*np.pi/wavelength,mean_stacked,60,cmap=my_cmap)
    plt.colorbar()
    
    0
    
    #variance du spectre
    variance = np.std(mat_spec,axis=2)/mean_stacked
    #plt.figure(figsize=(8,6),dpi=150)
    ax = plt.subplot(1,2,2,polar=True,rmax=0.5)
    ax.set_theta_direction(-1) #clokwise\n",
    ax.set_theta_offset(np.pi/2.) #north on the top
    #ax.set_rscale(10.)
    #ax.set_rmax(0.6)
    period = 1./np.array(frequencies)
    g = 9.81
    wavelength = (g*period**2)/(2. * np.pi)
    if ff :
        
        plt.title('variance spectra\n%s'%os.path.basename(ff))
    elif title2:
        plt.title(title2)
    else:
        plt.title('variance spectra')
    #ax.set_rmin(0.333)
    plt.contourf(np.radians(dirs),2*np.pi/wavelength,variance,60,cmap=my_cmap)
    plt.colorbar()
    ax.set_ylim(0,2.0*np.pi/min_wavelength)
    plt.show()
    
    
