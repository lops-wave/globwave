"""
purpose: generate pickle files from both S1 dataframe and buoy files from globwave
context: provide prepared data for bokeh scatter Q/Q plot and timeseries for the ~80 buoys NDBC spectral data
date: may 2018
author: Antoine grouazel
inspired from  http://br156-191.ifremer.fr:8888/notebooks/workspace_leste/sentinel1_perso/create_listing_WV_L2_directional_NDBCbuoy_colocs.ipynb
"""
import logging
import sys
import os
import numpy as np
import pandas as pd
import time
import pdb
import datetime
import glob
path_datacol = '/home/agrouaze/git/mpc-sentinel/mpc-sentinel/mpcsentinellibs/data_collect/'
if path_datacol not in sys.path:
    sys.path.append(path_datacol)
import get_full_path_from_measurement
import traceback
import phoenixdb
import math
path_cerb = "/home/agrouaze/git/mpc-sentinel/mpc-sentinel/mpcsentinellibs/qualitycheck/"
if path_cerb not in sys.path:
    sys.path.append(path_cerb)
from cross_assignment import best_matchup,best_matchup_energyweigthed #already in mpc git qualitycheck
path_glo_hs = '/home/agrouaze/git/globwave/src/insitu/read_buoys_data/'
if path_glo_hs not in sys.path:
    sys.path.append(path_glo_hs)
import compute_hs_from_spectra
path_wave = '/home/agrouaze/git/wavetoolbox/'
import netCDF4
path_cerb = "/home/agrouaze/git/cerbere_gitlab/cerbere/"
if path_cerb not in sys.path:
    sys.path.append(path_cerb)
path_cerb = "/home/agrouaze/git/cerform/"
if path_cerb not in sys.path:
    sys.path.append(path_cerb)
import cerform.wave as waveao
path_cerb = "/home/agrouaze/git/mpc-sentinel/mpc-sentinel/mpcsentinellibs/colocation/"
if path_cerb not in sys.path:
    sys.path.append(path_cerb)
path_cerb = "/home/agrouaze/git/mpc-sentinel/mpc-sentinel/mpcsentinellibs/indexation_in_hbase/"
if path_cerb not in sys.path:
    sys.path.append(path_cerb)

if path_wave not in sys.path:
    sys.path.append(path_wave)
import SpectrumReaderBuoys
path_globwa = '/home/agrouaze/git/globwave/src/insitu/visualize_buoy_data/'
if path_globwa not in sys.path:
    sys.path.append(path_globwa)
path_globwa = '/home/agrouaze/git/globwave/src/insitu/find_buoy_file/'
if path_globwa not in sys.path:
    sys.path.append(path_globwa)
path_globwa = '/home/agrouaze/git/'
if path_globwa not in sys.path:
    sys.path.append(path_globwa)
    
sys.path.append('/home/agrouaze/PROGRAMMES/ROUTINE_PYTHON/my_tool_box/')
import datetime_vect_difference
import globwave_paths
import display_timeseries
import collections
import get_full_path_from_measurement
from globwave_shared_infos import active_networks,GLOBWAVE_ROOT
from compute_Hs_total import compute_hs_total_sar_from_nc_file
from wavelength_wv_s1_computation import compute_wavelength_eff_weightned_test_version_v2,compute_wavelength_eff_weightned_test_version_v2_ww3
from wave_direction_estimate_on_spectra import compute_wavedir_eff_weightned_test_version_v2,compute_wavedir_eff_weightned_test_version_v2_ww3 #{qualitycheck}

from compute_period_from_ndbc_archive_globwave_nc_file import compute_periode_from_ndbc_file
from dirs_analysis_buoy_tools import PATH_L2C_BUOY
# ROOTDIR = "/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/"

from dirs_analysis_buoy_tools import PATH_L2B_BUOY

def give_buoy_time_resolution(handler_netcdf_buoy):
    units = handler_netcdf_buoy.variables['time'].units
    t0 = netCDF4.num2date(handler_netcdf_buoy.variables['time'][0],units)
    t1 = netCDF4.num2date(handler_netcdf_buoy.variables['time'][1],units)
    res = (t1-t0).total_seconds()
    return res
    
    

def compute_hs_tot_and_eff_on_buoys_spectra(list_paths,dataframe_sar,cirteria_time_color,buoy_id):
    """
    #critere de une heure car 99% des bouees directionnelle NDBC archive envoie un spectre par heure: 
    verifie http://br156-191.ifremer.fr:8888/notebooks/workspace_leste/globwave_tests/find_champion_buoy.ipynb
    """
    #cmpute the hs_tot for the whole serie stratus
    cpt = collections.defaultdict(int)
    hs_stratus = []
    time_stratus = []
    period_stratus = []
    
    hs_pair_eff = []
    hs_pair_total = []
    dir_buoy_eff = []
    wl_buoy_eff_bary = []
    dates_pair = []
    dates_buoy=  []
    all_lon_buoy = []
    all_lat_buoy = []
    all_buoy_id = []
    all_appaired_dominant_wave_period = [] #peak period
    hs_freq = {'plag1':[],'plag2':[],'plag3':[],'plag4':[]}
    instance_globwavereader = SpectrumReaderBuoys.SpectrumReaderBuoyGlobwave()
    logging.info('start looping over buoy files')
    for cc,ff in enumerate(list_paths):#loop over buoy files
        logging.info('buoy netcdf file %s/%s',cc+1,len(list_paths))
        nc = netCDF4.Dataset(ff)
        nb_time_step = len(nc.dimensions['time'])
        units = nc.variables['time'].units
        times_one_file = netCDF4.num2date(nc.variables['time'][:],units)
        lonbuoy = nc.variables['lon'][:]
        latbuoy = nc.variables['lat'][:]
        if 'dominant_wave_period' in nc.variables:
            dominant_wave_period = nc.variables['dominant_wave_period'][:]
        else:
            dominant_wave_period = compute_periode_from_ndbc_file(nc)
#             wave_directional_spectrum_central_frequency = nc.variables['wave_directional_spectrum_central_frequency'][:]
#             spectral_wave_density = nc.variables['spectral_wave_density'][:]
#             ind_peak_time,ind_peak_freq = np.where(spectral_wave_density==np.amax(spectral_wave_density))
#             dominant_wave_period = [1./wave_directional_spectrum_central_frequency[titi] for titi in ind_peak_freq]
            logging.info('recup dominant period = %s',len(dominant_wave_period))
        
        try:
            for tt in range(nb_time_step):
                if 'ndbc' in ff:
                    readermode = 'buoyglobwavendbc'
                else:
                    readermode = 'buoyglobwavecdip'
                spectra_wavetoolbox_obj = instance_globwavereader.read_spectrum(nc,tt,mode=readermode)
                phi_radian = np.radians(spectra_wavetoolbox_obj.spec_data.phi)
                phi_deg = np.degrees(phi_radian)
                buoy_spec = spectra_wavetoolbox_obj.spec_data.sp
                buoy_k = spectra_wavetoolbox_obj.spec_data.k
                buoy_phi = spectra_wavetoolbox_obj.spec_data.phi
                #dans certain cas je peux aussi trouver le Hs donne par le NDBC mais pour stratus je navais pas cette variable en NRT globwave (mois de mai 2016)
                hstot = compute_hs_from_spectra.get_buoy_hs_total(phi_radian,spectra_wavetoolbox_obj.spec_data.k,buoy_spec)
                hs_stratus.append(hstot)
                time_stratus.append(times_one_file[tt])
                period_stratus.append(dominant_wave_period[tt])
                cpt['ok_spectra'] += 1
                #hs eff si proche du sar dans le temps:
                if True:
                    sar_dt = pd.to_datetime(dataframe_sar.FDATEDT)
                    diffo = (sar_dt-times_one_file[tt])
                    #print 'diffo',diffo
                    diffo_seconds = diffo.apply(datetime.timedelta.total_seconds)
                    #print diffo_seconds.values
                    #print sar_dt
                    #kjdlk
                    condi = abs(diffo_seconds.values)<cirteria_time_color 
                    any_match = (condi).any()
                    if any_match:
#                         indice_sar = dataframe_sar[condi]
                        if len(dataframe_sar[condi].FNAME.values)>1:
                            logging.info('y a plusieurs SAR qui match avec la timeserie %s',np.sum(condi))
                        for one_match_sar in range(len(dataframe_sar[condi])):
                            fname_sar = dataframe_sar[condi].FNAME.values[one_match_sar]
                            
                            logging.debug('%s',fname_sar)
                            
                            sar_full_path = get_full_path_from_measurement.get_full_path_from_measu(fname_sar)
                            if os.path.exists(sar_full_path):
                                file_handler_s1 = netCDF4.Dataset(sar_full_path)
                                try:
                                    start_sar_dt = datetime.datetime.strptime(
                                            file_handler_s1.getncattr('firstMeasurementTime'),
                                            '%Y-%m-%dT%H:%M:%S.%fZ')
                                except:
                                    start_sar_dt= datetime.datetime.strptime(
                                            file_handler_s1.getncattr('firstMeasurementTime'),
                                            '%Y-%m-%dT%H:%M:%SZ')
                                
                                logging.debug('paf jai un poisson')
                                cpt['matching_timestep'] += 1
                                hs_sar_total = compute_hs_total_sar_from_nc_file(file_handler_s1)
                                swh_eff_2d_buoy,swh_buoy,buoywavelengthpeak,buoywavelengthpeak_eff,buoydirectionpeak,buoydirectionpeak_eff,buoywl_partSAR1_weightened = compute_hs_from_spectra.compute_buoy_hs_effective_SAR_imaging_domain(buoy_spec,spectra_wavetoolbox_obj.spec_data.k,phi_deg,file_handler_s1,osw=None)
                                plages_freq_nrl = {'1':(0.04,0.08),
                                  '2':(0.08,0.15),
                                  '3':(0.15,0.228),
                                  '4':(0.0,0.3)
                                  }
                                sar_spec = file_handler_s1.variables['oswPolSpec'][:].squeeze()
                                sar_k = file_handler_s1.variables['oswK'][:].squeeze()
                                sar_phideg = file_handler_s1.variables['oswPhi'][:].squeeze()
                                for frqrange in plages_freq_nrl:
                                    hs_buyoy_frequencies = compute_hs_from_spectra.compute_buoy_hs_from_spectra_on_freq_range(buoy_spec,buoy_k,buoy_phi,plages_freq_nrl[frqrange])
                                    hs_sar_frequencies = compute_hs_from_spectra.compute_sar_hs_from_spectra_on_freq_range(sar_spec,sar_k,sar_phideg,plages_freq_nrl[frqrange])
                                    tupi = (hs_buyoy_frequencies,hs_sar_frequencies)
                                    #print tupi,plages_freq_nrl[frqrange]
                                    hs_freq['plag'+frqrange].append(tupi)
                                    #to continue
#                                 dates_pair.append(times_one_file[tt])
                                dates_pair.append(start_sar_dt) #i prefere to use SAR data with is more high resolution compare to hourly buoy data
                                dates_buoy.append(times_one_file[tt])
                                sar_hseff = dataframe_sar[condi].S1_EFFECTIVE_HS_2DCUTOFF.values[0]
                                dir_buoy_eff.append(buoydirectionpeak_eff)
                                wl_buoy_eff_bary.append(buoywl_partSAR1_weightened)
                                all_lon_buoy.append(lonbuoy)
                                all_lat_buoy.append(latbuoy)
                                all_buoy_id.append(buoy_id)
                                all_appaired_dominant_wave_period.append(dominant_wave_period[tt])
    #                             print "sar_hseff",sar_hseff
                                hs_pair_eff.append((swh_eff_2d_buoy,sar_hseff,dataframe_sar[condi].iloc[one_match_sar]))
                                hs_pair_total.append((hstot,hs_sar_total))
                            else:
                                logging.error('trouve pas le fichier sar')
                                cpt['cannot_find_SAR'] += 1
               
        except:
            cpt['ko_file'] += 1
            print ff
            print traceback.format_exc()
        #if cc==2:
        #    break
            
        nc.close()
    logging.info('counters: %s',cpt)
    logging.info('combien de pair %s',len(hs_pair_eff))
    aparaied_df = pd.DataFrame({'dates':dates_pair,
                                'dates_buoy_appered':dates_buoy,
                                'hs_eff':hs_pair_eff,
                                'hs_total':hs_pair_total,
                                'hs_freq1':hs_freq['plag1'],
                                'hs_freq2':hs_freq['plag2'],
                                'hs_freq3':hs_freq['plag3'],
                                'hs_freq4':hs_freq['plag4'],
                                "dir_buoy_eff":dir_buoy_eff,
                                "wl_buoy_eff_bary":wl_buoy_eff_bary,
                                "dominant_wave_period_buoy":all_appaired_dominant_wave_period,
                                "lonbuoy":all_lon_buoy,
                                "latbuoy":all_lat_buoy,
                                "buoy_id":all_buoy_id})
    print 'Nb period buoy',len(period_stratus),'Nb hs total buoy=',len(hs_stratus)
    return aparaied_df,time_stratus,hs_stratus,period_stratus




def compute_hs_tot_and_eff_on_buoys_spectra_storm_tracked(filepath_buoytracked,dataframe_sar,cirteria_time_color,buoy_id):
    """
    #critere de une heure car 99% des bouees direcitonnelle NDBC archive envoie une spectre par heure: 
    verifie http://br156-191.ifremer.fr:8888/notebooks/workspace_leste/globwave_tests/find_champion_buoy.ipynb
    note:
        25 juin 2018: il  manque les spectres et une variable time ecrite de maniere classique dans le netcdf en sortie du code de storm tracking
        5 juillet 2018: maintenant cest bon le time est OK et les spectre sont dedans
    :args:
        filepath_buoytracked (str):
        dataframe_sar (pd.DataFrame):
        cirteria_time_color (int): seconds
        buoy_id (str):
    """
    #compute the hs_tot for the whole serie stratus
    cpt = collections.defaultdict(int)
    hs_stratus = []
    time_stratus = []
    hs_partition_buoy = []
    period_stratus = []
    
    hs_pair_eff = []
    hs_pair_total = []
    dir_buoy_eff = []
    wl_buoy_eff_bary = []
    dates_pair = []
    spectra_pair = []
    dates_buoy = []
    all_lon_buoy = []
    all_lat_buoy = []
    all_buoy_id = []
    all_appaired_dominant_wave_period = [] #peak period
    class_wv = []
    proba_wv = []
    mini_baryucentric_spect_dst = []
    mini_spect_dst_simple = []
    xassigned_simple_hs = []
    barycentric_xassigned_hs = []
    xassigned_simple_wl = []
    barycentric_xassigned_wl = []
    xassigned_simple_dirdeg = []
    barycentric_xassigned_dirdeg = []
    
    count_nan_wl_sar = []
    
    hs_freq = {'plag1':[],'plag2':[],'plag3':[],'plag4':[]}
    instance_globwavereader = SpectrumReaderBuoys.SpectrumReaderBuoyGlobwave()

    nc = netCDF4.Dataset(filepath_buoytracked)
    nb_time_step = len(nc.dimensions['time'])
    units = nc.variables['time'].units

    times_one_file = netCDF4.num2date(nc.variables['time'][:],units)
    
    lonbuoy = nc.getncattr("station_lon")
    latbuoy = nc.getncattr("station_lat")
    st_hs = nc.variables['hs'][:] #hs calcule par la moulinette de partitioning de spectre sur une partition
    st_dirdeg = nc.variables['dirdeg'][:]
    st_fp = nc.variables['fp'][:]
    st_wl = waveao.period2wavelength(1./st_fp)
    st_label = nc.variables['sysid'][:]
#     lonbuoy = nc.variables['lon'][:]
#     latbuoy = nc.variables['lat'][:]
    if 'dominant_wave_period' in nc.variables:
        dominant_wave_period = nc.variables['dominant_wave_period'][:]
    elif 'fp' in nc.variables:
        fp = nc.variables['fp'][:]
        dominant_wave_period = 1./fp
    readermode = 'storm_tracked'
    logging.info('start looping over the %s spectra from the buoy',nb_time_step)
    logging.info('first buoy spectra is %s and last is %s',times_one_file[0],times_one_file[-1])
    try:
        for tt in range(nb_time_step):#loop over the storm tracked pts of the buoy
            if tt%200==0:
                logging.info('loop over buoys obs: %s/%s',tt+1,nb_time_step)
#             spectra_wavetoolbox_obj = instance_globwavereader.read_spectrum(nc,tt,mode=readermode)
            buoy_spec = nc.variables['spectra_buoy'][tt,:,:].squeeze() #phi x k
            parts_buoy = nc.variables['parts_buoy'][tt,:,:].squeeze() #phi x k
            buoy_phi = nc.variables['phi'][:]
            indice_partition = nc.variables['indice_partition'][tt].squeeze()
            phi_radian = np.radians(buoy_phi)
            phi_deg = np.degrees(phi_radian)
            buoy_k = nc.variables['k'][:]
            #storm tracked system

            
            hstot = compute_hs_from_spectra.get_buoy_hs_total(phi_radian,buoy_k,buoy_spec)
            hs_stratus.append(hstot)
            hs_partition_buoy.append(st_hs[tt])
            time_stratus.append(times_one_file[tt])
            period_stratus.append(dominant_wave_period[tt])
            cpt['ok_spectra'] += 1
            #hs eff si proche du sar dans le temps:
            if True:
                sar_dt = pd.to_datetime(dataframe_sar.FDATEDT)
                diffo = (sar_dt-times_one_file[tt])
                #print 'diffo',diffo
#                 diffo_seconds = diffo.apply(datetime.timedelta.total_seconds)
#                 diffo_seconds = datetime_vect_difference.compute_datetime_vector_difference(vect1=sar_dt,date_sought=times_one_file[tt])
                diffo_seconds = np.divide(diffo, np.timedelta64(1, 's'))
                #print diffo_seconds.values
                #print sar_dt
                #kjdlk
                condi = abs(diffo_seconds.values)<cirteria_time_color 
                
                any_match = (condi).any()
                if any_match:
                    cpt['buoy_spectra_with_at_least_one_SAR_matchup'] += 1
#                         indice_sar = dataframe_sar[condi]
                    if len(dataframe_sar[condi].FNAME.values)>1:
                        logging.info('y a plusieurs SAR qui match avec la timeserie %s',np.sum(condi))
                    for one_match_sar in range(len(dataframe_sar[condi])):
                        fname_sar = dataframe_sar[condi].FNAME.values[one_match_sar]
                        
                        logging.debug('%s',fname_sar)
                        
                        sar_full_path = get_full_path_from_measurement.get_full_path_from_measu(fname_sar)
                        if os.path.exists(sar_full_path):
                            file_handler_s1 = netCDF4.Dataset(sar_full_path)
                            try:
                                start_sar_dt = datetime.datetime.strptime(
                                        file_handler_s1.getncattr('firstMeasurementTime'),
                                        '%Y-%m-%dT%H:%M:%S.%fZ')
                            except:
                                start_sar_dt= datetime.datetime.strptime(
                                        file_handler_s1.getncattr('firstMeasurementTime'),
                                        '%Y-%m-%dT%H:%M:%SZ')
                            
                            logging.debug('paf jai un poisson')
                            cpt['matching_timestep'] += 1
                            hs_sar_total = compute_hs_total_sar_from_nc_file(file_handler_s1)
                            logging.debug('compute some param from the buoy spectra')
                            buoy_wave_p = compute_hs_from_spectra.compute_buoy_hs_effective_SAR_imaging_domain(buoy_spec,buoy_k,phi_deg,file_handler_s1,osw=None)
                            swh_eff_2d_buoy,swh_buoy,buoywavelengthpeak,buoywavelengthpeak_eff,buoydirectionpeak,buoydirectionpeak_eff,buoywl_partSAR1_weightened = buoy_wave_p
                            logging.debug('read some buoy param from the storm tracking analysis')
                            
                            plages_freq_nrl = {'1':(0.04,0.08),
                              '2':(0.08,0.15),
                              '3':(0.15,0.228),
                              '4':(0.0,0.3)
                              }
                            sar_spec = file_handler_s1.variables['oswPolSpec'][:].squeeze()
                            sar_k = file_handler_s1.variables['oswK'][:].squeeze()
                            sar_phideg = file_handler_s1.variables['oswPhi'][:].squeeze()
                            sar_partitions = file_handler_s1.variables['oswPartitions'][:].squeeze()
                            
                                #to continue
#                                 dates_pair.append(times_one_file[tt])
                            
#                             dates_pair_buoy.append(times_one_file[tt])
                            
#                             partitions_pair.append((obj_spectra_buoy))
                            if False: #replace sar_hseff because of multiple matchup 
                                sar_hseff = dataframe_sar[condi].S1_EFFECTIVE_HS_2DCUTOFF.values[0]
                            else:
                                sar_hseff = dataframe_sar[condi].S1_EFFECTIVE_HS_2DCUTOFF.values[one_match_sar]
                            
                            
                            #DO the X-ASSIGNMENT SAR storm tracked vs BUOY (added July 2018)
                            s1_struct_all = []
                            hs_esa_sar = file_handler_s1.variables['oswHs'][:].squeeze()
                            wl_esa_sar = file_handler_s1.variables['oswWl'][:].squeeze()
                            dirdeg_esa_sar = file_handler_s1.variables['oswDirmet'][:].squeeze()
                            
                            nb_partitions_sar = len(hs_esa_sar)
                            logging.info('oswHs = %s',hs_esa_sar)
                            for sar_p in range(nb_partitions_sar):
                                if hs_esa_sar[sar_p]>0:
                                    _wl_ew_eff,_ = compute_wavelength_eff_weightned_test_version_v2(file_handler_s1,partition_label=sar_p,lowfreqfilter=True,avec_plot=False)
                                    dirdeg_ew_eff,_,_ = compute_wavedir_eff_weightned_test_version_v2(file_handler_s1,partition_label=sar_p,lowfreqfilter=True,avec_plot=False)
                                    if np.isnan(_wl_ew_eff):
                                        #pdb.set_trace()#je veux comprendre les nan (august 2018)
                                        cpt['nan_wl_eff_bary_sar'] += 1
                                        count_nan_wl_sar.append(sar_full_path)
    #                                     if sar['oswHs'].squeeze()[bp]>0:
    #                                         _wl  = sar['oswWl'].squeeze()[bp]
    #                                         _wl_ew_eff,_ = compute_wavelength_eff_weightned_test_version_v2(ncsarhandler,partition_label=bp,lowfreqfilter=True,avec_plot=False)
    #                                         _dirdeg_ew_eff,_,_ = compute_wavedir_eff_weightned_test_version_v2(ncsarhandler,partition_label=bp,lowfreqfilter=True,avec_plot=False)
    #                                         _dirrad_ew_eff = np.radians(_dirdeg_ew_eff)
    #                                         _phi = np.radians(sar['oswDirmet'].squeeze()[bp])
    #                                         _hs  = sar['oswHs'].squeeze()[bp]
    #                                         _iconf = sar['oswIconf'].squeeze()[bp]
    #                                         s1_struct_all.append(struct(wl=_wl,wl_ew_eff=_wl_ew_eff,dirad=_phi,dirrad_ew_eff=_dirrad_ew_eff,hs=_hs,iconf=_iconf,partition_label=bp+1,p2br=_p2br_sar[bp]))
                                    assert dirdeg_esa_sar[sar_p]<360
                                    s1_struct_all.append(struct(wl=wl_esa_sar[sar_p],wl_eff=_wl_ew_eff,
                                             wl_ew_eff=_wl_ew_eff,
                                             dirad=np.radians(dirdeg_esa_sar[sar_p]),
                                             dirad_eff=np.radians(dirdeg_ew_eff),
                                             dirrad_ew_eff=np.radians(dirdeg_ew_eff),hs=hs_esa_sar[sar_p],partition_label=sar_p))
                                else:
                                    cpt['sar_partition_with_Hs_zero'] += 1
                            logging.info('Nb SAR partition to test for X-assign... %s',len(s1_struct_all))
                            #for buoy
                            buoy_struct_closest = []
                            buoy_struct_closest.append(struct(wl=st_wl[tt],
                                     wl_eff=st_wl[tt],
                                 wl_ew_eff=st_wl[tt],
                                 dirad=np.radians(st_dirdeg[tt]), 
                                 dirad_eff=np.radians(st_dirdeg[tt]), #here I give the partition given by the storm tracking analysis from Wang He 
                                 #where params are peak params but nothing prevent us to use barycentric param in the future (agrouaze July 2018)
                                 wd_ew_eff_rad=np.radians(st_dirdeg[tt]),
                                 dirrad_ew_eff=np.radians(st_dirdeg[tt]),
                                     hs=st_hs[tt],
                                     partition_label=st_label[tt]))
                            
                            
                            if (wl_esa_sar!=-999.0).any() and (dirdeg_esa_sar!=-999.0).any():
                                ind_s1_0,ind_buoy_0,min_sp_dist_0 = best_matchup(s1_struct_all,buoy_struct_closest)
            #                 else: #not implemented yet (we have to check how Gilles compute the pseudo peak param of the partitions
                                ind_s1,ind_buoy,min_sp_dist = best_matchup_energyweigthed(s1_struct_all,buoy_struct_closest)
                                
                                obj_spectra_sar = {'sp':sar_spec,'k':sar_k,'phideg':sar_phideg,'partitions':sar_partitions,'indice':ind_s1}
                                obj_spectra_buoy = {'sp':buoy_spec,'k':buoy_k,'phideg':phi_deg,'partitions':parts_buoy,'indice':indice_partition}
                                logging.debug('start to append infos to listings')
                                #start appending part
                                
                                for frqrange in plages_freq_nrl:
                                    hs_buyoy_frequencies = compute_hs_from_spectra.compute_buoy_hs_from_spectra_on_freq_range(buoy_spec,buoy_k,buoy_phi,plages_freq_nrl[frqrange])
                                    hs_sar_frequencies = compute_hs_from_spectra.compute_sar_hs_from_spectra_on_freq_range(sar_spec,sar_k,sar_phideg,plages_freq_nrl[frqrange])
                                    tupi = (hs_buyoy_frequencies,hs_sar_frequencies)
                                    #print tupi,plages_freq_nrl[frqrange]
                                    hs_freq['plag'+frqrange].append(tupi)
                                spectra_pair.append((obj_spectra_sar,obj_spectra_buoy))
                                #TODO save the cross assignment in tuples
                                mini_spect_dst_simple.append(min_sp_dist_0)
                                mini_baryucentric_spect_dst.append(min_sp_dist)
                                pair_xassigned_simple_hs = (s1_struct_all[ind_s1_0].hs,buoy_struct_closest[ind_buoy_0].hs) #this one is the hs per partition Xassigned
    #                             hs_pair_partition.append((st_hs[tt],))
                                pair_barycentric_xassigned_hs = (s1_struct_all[ind_s1].hs,buoy_struct_closest[ind_buoy].hs)
                                
                                pair_xassigned_simple_wl = (s1_struct_all[ind_s1_0].wl,buoy_struct_closest[ind_buoy_0].wl)
                                pair_barycentric_xassigned_wl = (s1_struct_all[ind_s1].wl_ew_eff,buoy_struct_closest[ind_buoy].wl)
                                pair_xassigned_simple_dirdeg = (np.degrees(s1_struct_all[ind_s1_0].dirad),np.degrees(buoy_struct_closest[ind_buoy_0].dirad))
                                pair_barycentric_xassigned_dirdeg = (np.degrees(s1_struct_all[ind_s1].dirrad_ew_eff),np.degrees(buoy_struct_closest[ind_buoy].dirad))
                                
                                xassigned_simple_hs.append(pair_xassigned_simple_hs)
                                barycentric_xassigned_hs.append(pair_barycentric_xassigned_hs)
                                xassigned_simple_wl.append(pair_xassigned_simple_wl)
                                barycentric_xassigned_wl.append(pair_barycentric_xassigned_wl)
                                xassigned_simple_dirdeg.append(pair_xassigned_simple_dirdeg)
                                barycentric_xassigned_dirdeg.append(pair_barycentric_xassigned_dirdeg)
                                dates_pair.append(start_sar_dt) #i prefere to use SAR data with is more high resolution compare to hourly buoy data
                                class_wv.append(dataframe_sar[condi].CLASS_1.values[one_match_sar])
                                proba_wv.append(dataframe_sar[condi].PROB_1.values[one_match_sar])
                                dir_buoy_eff.append(buoydirectionpeak_eff)
                                wl_buoy_eff_bary.append(buoywl_partSAR1_weightened)
                                all_lon_buoy.append(lonbuoy)
                                all_lat_buoy.append(latbuoy)
                                all_buoy_id.append(buoy_id)
                                dates_buoy.append(times_one_file[tt])
                                all_appaired_dominant_wave_period.append(dominant_wave_period[tt])
    #                             print "sar_hseff",sar_hseff
                                hs_pair_eff.append((swh_eff_2d_buoy,sar_hseff,dataframe_sar[condi].iloc[one_match_sar])) #je cache un peu de la metadata dans ce tuple...
                                hs_pair_total.append((hstot,hs_sar_total))
                            else:
                                logging.info('case %s with all the wl or all the direction to -999',times_one_file[tt])
                                cpt['sar_case_with_dir_or_wl_default_value'] += 1
                        else:
                            logging.error('trouve pas le fichier sar')
                            cpt['cannot_find_SAR'] += 1
                else:
                    cpt['buoy_spectra_with_no_SAR_matchup'] += 1
    except:
        cpt['ko_file'] += 1
        logging.info(filepath_buoytracked)
        logging.info(traceback.format_exc())
    #if cc==2:
    #    break
        
    nc.close()
    logging.info('counters: %s',cpt)
    logging.info('combien de pair SAR vs buoy %s',len(hs_pair_eff))
    logging.info('combien de longitudes buoy: %s',len(all_lon_buoy))
    logging.info('%s %s %s',len(dates_buoy),len(hs_pair_eff),len(hs_pair_total))
    logging.info('%s %s %s',len(hs_freq['plag1']),len(hs_freq['plag2']),len(hs_freq['plag3']))
    logging.info('%s %s %s',len(dir_buoy_eff),len(spectra_pair),len(wl_buoy_eff_bary))
    logging.info('%s %s %s',len(all_appaired_dominant_wave_period),len(hs_pair_eff),len(hs_pair_total))
    logging.info('%s %s %s',len(all_lon_buoy),len(all_buoy_id),len(mini_baryucentric_spect_dst))
    logging.info('%s %s %s',len(mini_spect_dst_simple),len(xassigned_simple_hs),len(barycentric_xassigned_hs))
    logging.info('%s %s %s',len(class_wv),len(proba_wv),len(barycentric_xassigned_dirdeg))
    aparaied_df = pd.DataFrame({'dates':dates_pair,#s1
                                
                                'dates_buoy_appered':dates_buoy,
                                'hs_eff':hs_pair_eff,
                                'hs_total':hs_pair_total,
                                'hs_freq1':hs_freq['plag1'],
                                'hs_freq2':hs_freq['plag2'],
                                'hs_freq3':hs_freq['plag3'],
                                'hs_freq4':hs_freq['plag4'],
                                "dir_buoy_eff":dir_buoy_eff,
                                "spectra_pair":spectra_pair,
                                "wl_buoy_eff_bary":wl_buoy_eff_bary,
                                "dominant_wave_period_buoy":all_appaired_dominant_wave_period,
                                "lonbuoy":all_lon_buoy,
                                "latbuoy":all_lat_buoy,
                                "buoy_id":all_buoy_id,
                                "mini_baryucentric_spect_dst":mini_baryucentric_spect_dst,
                                "mini_spect_dst_simple":mini_spect_dst_simple, #simple means that the cross assignment has been done using the wl and direction of the SAR grid and not energy weighted params
                                "xassigned_simple_hs":xassigned_simple_hs,
                                "barycentric_xassigned_hs":barycentric_xassigned_hs,
                                "xassigned_simple_wl":xassigned_simple_wl,
                                "barycentric_xassigned_wl":barycentric_xassigned_wl,
                                "xassigned_simple_dirdeg":xassigned_simple_dirdeg,
                                "barycentric_xassigned_dirdeg":barycentric_xassigned_dirdeg,
                                "class_1":class_wv,
                                "prob_1":proba_wv,
                                }
                                )
    logging.info('Nb period buoy %s Nb hs total buoy= %s',len(period_stratus),len(hs_stratus))
    logging.info('bad wl barycentric SAR : %s',count_nan_wl_sar)
    return aparaied_df,time_stratus,hs_stratus,period_stratus,hs_partition_buoy

class struct:
    def __init__(self,**strdata):
        self.__dict__.update(strdata)


def add_distance_tobuoy(rowdf,lonbuoy,latbuoy):
    lonpt = rowdf.LON
    latpt = rowdf.LAT
    distance_to_buoy = haversine(lon1=lonpt,lat1=latpt,lon2=lonbuoy,lat2=latbuoy)
    return distance_to_buoy

def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(np.radians, [lon1, lat1, lon2, lat2])
    # haversine formula 
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = np.sin(dlat/2)**2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon/2)**2
    c = 2 * math.asin(np.sqrt(a))
    km = 6367 * c
    return km

def get_dataframe_sar_from_phoenix(lonmin,lonmax,latmin,latmax):
    """
    :args:
        bbox of the area around the buoy coordinates
    """
    logging.info('start query in phoenix database')
    database_url = 'http://br156-175:8765/'
    conn = phoenixdb.connect(database_url, autocommit=True)
#     sql = "select * from INDEX_COLLECTIONFILES_PHX4 where fid like '%-029' and OSWLON<-84.574 and OSWLON>-85.574 and LAT<-18.93 and LAT>-19.93"
    sql = "select * from INDEX_COLLECTIONFILES_PHX4 where OSWLON<%1.4f and OSWLON>%1.4f and LAT<%1.4f and LAT>%1.4f"%(lonmax,lonmin,latmax,latmin)
    #sql = "select * from INDEX_COLLECTIONFILES_PHX4 where FID like '%009961-0120dd-029%'"
    logging.info('query = %s',sql)
    df = pd.read_sql(sql, conn)
    conn.close()
    df.index = df.FDATEDT
    df = df.sort_index()
    logging.info('Nbr of SAR file matching the query: %s',len(df))
    logging.info('keys %s',df.keys())
    return df

def save_listing_sar_match_given_buoy(buoy_name,df_sar,output_pickle_file):
    """
    in fact the listing saved here is just a spatial matchup not necessarly a temporal matchup
    :args:
        output_pickle_file (str): path of the pickle that contains the dataframe of mathcing SAR measurements
    """
    logging.info('save a listing of all the SAR measu matching the query on the buoy')
    fileout = "/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/listing_sentinel1_wv_ocn_file_matching_buoy_%s.txt"%buoy_name
    fid = open(fileout,'w')
    fnames =df_sar.FNAME.values
    for ff in fnames:
        fid.write(ff+'\n')
    fid.close()
    logging.info(fileout) 
    #also save the pickle dataframe sar
    
    df_sar.to_pickle(output_pickle_file)
    logging.info('output %s',output_pickle_file)

def save_pickle_files(buoyname,df_sar,df_buoy,aparaied_df,cirteria_time_color,start_dt,stop_dt,storm_tracked=False,filepath_buoytracked_L2B=None):
    """
    :args:
        storm_tracked (bool): to choose the name of outputfile  
        filepath_buoytracked_L2B (str): full path of the file generated with the Wang He analysis main_work_ndbc.py L2B
    """
    final_path_sar,final_path_buoy,Xassigned_output_pickle_file = get_output_Xassigned_buoy_s1_pickle_file(buoyname,cirteria_time_color,
                                                            start_dt,stop_dt,storm_tracked=storm_tracked,filepath_buoytracked_L2B=filepath_buoytracked_L2B)
    df_sar.to_pickle(final_path_sar)
    df_buoy.to_pickle(final_path_buoy) #que les data buoy
    aparaied_df['filepath_buoytracked'] = filepath_buoytracked_L2B
    aparaied_df.to_pickle(Xassigned_output_pickle_file)
    logging.info('the most important one: %s',Xassigned_output_pickle_file)
    
def get_output_Xassigned_buoy_s1_pickle_file(buoyname,cirteria_time_color,start_dt,stop_dt,storm_tracked=False,filepath_buoytracked_L2B=None):
    """
    :args:
        storm_tracked (bool): to choose the name of outputfile  
        filepath_buoytracked (str): full path of the file generated with the Wang He analysis main_work_ndbc.py
    """
    if start_dt is None:
        temporality_info = 'all_measurements'
    else:
        
        start_dt_str = start_dt.strftime('%Y%m%d')
        stop_dt_str = stop_dt.strftime('%Y%m%d')
        temporality_info = '%s_%s'%(start_dt_str,stop_dt_str)
    #save df buoy and df phoenix sar in pickle files in order to continue devlelop bokh plot on another notebook
    cirteria_time_colocstr = 'time_crit_coloc_'+str(int(cirteria_time_color))
#     out_path = os.path.dirname(filepath_buoytracked)
    
    if '3h_spectra_averaging' in filepath_buoytracked_L2B:
        
        average_spectr_or_not = '3h_spectra_averaging'
    else:
        average_spectr_or_not = 'no_spectra_averaging'
    if 'original_dectection_param' in filepath_buoytracked_L2B:
        qualification_param = 'original_dectection_param'
    elif 'tuned_detection_param' in filepath_buoytracked_L2B:
        qualification_param = 'tuned_detection_param'
    dir_out_path = os.path.join(PATH_L2C_BUOY,netw,average_spectr_or_not,qualification_param)
    
    final_dir = os.path.join(dir_out_path,cirteria_time_colocstr)
#     final_dir = os.path.join(ROOTDIR,cirteria_time_colocstr,buoyname)
    if storm_tracked:
        suffix = 'storm_tracked'
    else:
        suffix = ''
    final_path_sar = os.path.join(final_dir,'phoenix_%s_%s_sar_dataframe_s1a_s1b_%s.pkl'%(buoyname,temporality_info,suffix)) #que les data SAR
    final_path_buoy = os.path.join(final_dir,'df_buoy_%s_%s_%s.pkl'%(buoyname,temporality_info,suffix))
    Xassigned_output_pickle_file = os.path.join(final_dir,'phoenix_%s_%s_sar_df_hs_eff_apaired_%s.pkl'%(buoyname,temporality_info,suffix)) #les cross assignment
    return final_path_sar,final_path_buoy,Xassigned_output_pickle_file

def get_buoy_lon_lat(buoy_name,network):
    """
    pour les storm tracked on peu aussi lire les attributs :station_lon :station_lat
    """
#     dir_globwave_ndbc_archive = '/home/cercache/project/globwave/data/globwave/ndbc/archive/wave_sensor/'
#     if 'data/globwave' in active_networks[network]:
    network  ='NDBC_ARCHIVE' #force ntwork since the only valuable information sought is lon;lat
    dir_globwave = os.path.join(active_networks[network],'wave_sensor')
#     else:
#         dir_globwave = os.path.join(active_networks[network])
    for root, dirs, files in os.walk(dir_globwave, topdown=False):
        for name in files:
            #print(os.path.join(root, name))
            if buoy_name in name:
                latstr = name.split('_')
                fullpath  = os.path.join(root, name)
                nc = netCDF4.Dataset(fullpath)
                lon = nc.variables['lon'][:][0]
                lat = nc.variables['lat'][:][0]
                return lon,lat

def prepare_one_buoy(buoy_id,netw,start_dt,stop_dt,redo=False,cirteria_time_color=600):
    """
    main function to call the otherone
    """
    logging.info('start to prepare the data from the buoy : %s',buoy_id)
    t0 = time.time()
    if os.path.exists(os.path.join(ROOTDIR,'phoenix_%s_sar_dataframe_s1a_s1b.pkl'%buoy_id))==False or redo:
        sensor_cv = 'wave_sensor'
#         delta = 0.5 #deg
        delta = 1.0 #like navy study
        logging.info('coloc spatial criteria: %s degree',delta)
        lonb,latb = get_buoy_lon_lat(buoy_id,netw)
        logging.info('longitude,latitude of the buoy: %s,%s',lonb,latb)
        area_buoys = [lonb-delta,lonb+delta,latb-delta,latb+delta]
        lonmin,lonmax,latmin,latmax = area_buoys
        output_pickle_file = '/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/phoenix_df_sentinel1_wv_ocn_matching_position_buoy_%s_crit_%s.pkl'%(buoy_id,int(delta))
        force_phoenix_query = True#ajouter car si le pickle a ete fait par un env conda par exeple je peut plus le lire avec un venv de worker...
        if os.path.exists(output_pickle_file) and force_phoenix_query==False:
            logging.info('listing SAR matching the buoy already exists %s',output_pickle_file)
            df_sar = pd.read_pickle(output_pickle_file)
        else:
            df_sar = get_dataframe_sar_from_phoenix(lonmin,lonmax,latmin,latmax)
            if len(df_sar)>0:
                save_listing_sar_match_given_buoy(buoy_id,df_sar,output_pickle_file)
        if len(df_sar)>0:
            df_sar['distancebuoy'] = df_sar.apply(add_distance_tobuoy, args=(lonb,latb,),axis=1)
            logging.info('fetch filepath of the CDIP buoy %s',buoy_id)
            _,list_paths = display_timeseries.list_paths_glob(outputdir='/tmp',startperiod=start_dt,stopperiod=stop_dt,network=[netw],buoyid=buoy_id,sensors_wanted=[sensor_cv])
            filtered_fpath = []
            for ff in list_paths:
                if 'spectrum' in ff:
                    filtered_fpath.append(ff)
            logging.info('length buoy listing before  %s -> %s',len(list_paths),len(filtered_fpath))
            aparaied_df,time_stratus,hs_stratus,dominant_period_stratus = compute_hs_tot_and_eff_on_buoys_spectra(filtered_fpath,df_sar,cirteria_time_color,buoy_id)
            df_buoy = pd.DataFrame({'dates':time_stratus,'hstot':hs_stratus,'dominant_period':dominant_period_stratus})
            save_pickle_files(buoy_id,df_sar,df_buoy,aparaied_df,cirteria_time_color,start_dt,stop_dt)
        else:
            logging.info('no coloc for buoy %s',buoy_id)
    else:
        logging.info('prepared data already exists for %s',buoy_id)
    logging.info('elapsed time to prepare one buoy %1.1f seconds',time.time()-t0)

def prepare_one_buoy_stormtracked(buoy_id,netw,start_dt,filepath_buoytracked_L2B,stop_dt,redo=False,cirteria_time_coloc=600):
    """
    starting the coloc from buoys storm tracked with Wang He 's code
    /home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/ndbc_nrt/original_dectection_param/BuoySwell32012from2017_05_01_16to2017_05_28_19.nc
    output is a pkl file
    :args:
        filepath_buoytracked_L2B (str): netcdf files generated by the storm tracking analysis (subset of the whole measurement from the buoy)
        cirteria_time_coloc (int): nb seconds in the time window (600s by default)
        netw (str): parameter with only informative utility
        buoy_id (str): parameter with only informative utility
        start_dt (datetime): start_dt can reduce the period provided into the netcdf used as input
    """
    logging.info('start to prepare the data from the buoy : %s',buoy_id)
    t0 = time.time()
    aparaied_df,time_stratus,hs_stratus,dominant_period_stratus = None,None,None,None
    final_path_sar,final_path_buoy,Xassigned_output_pickle_file = get_output_Xassigned_buoy_s1_pickle_file(buoy_id,cirteria_time_coloc,
                                                            start_dt,stop_dt,storm_tracked=True,filepath_buoytracked_L2B=filepath_buoytracked_L2B)
#     if os.path.exists(os.path.join(ROOTDIR,'phoenix_%s_sar_dataframe_s1a_s1b.pkl'%buoy_id))==False or redo:
    if os.path.exists(final_path_sar)==False or redo:
        sensor_cv = 'wave_sensor'
#         delta = 0.5 #deg
        delta = 1.0 #like navy study
        logging.info('coloc spatial criteria: %s degree',delta)
        lonb,latb = get_buoy_lon_lat(buoy_id,netw)
        logging.info('longitude,latitude of the buoy: %s,%s',lonb,latb)
        area_buoys = [lonb-delta,lonb+delta,latb-delta,latb+delta]
        lonmin,lonmax,latmin,latmax = area_buoys
#         dir_out_path = os.path.dirname(filepath_buoytracked_L2B)#previously calquer sur le L2B
        
        
        
#         output_pickle_file = '/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/phoenix_df_sentinel1_wv_ocn_matching_position_buoy_%s_crit_%s.pkl'%(buoy_id,int(delta))
        dir_out_path = os.path.dirname(final_path_sar)
        output_pickle_file = os.path.join(dir_out_path,'phoenix_df_sentinel1_wv_ocn_matching_position_buoy_%s_crit_%s.pkl'%(buoy_id,int(delta)))
        force_phoenix_query = True#ajouter car si le pickle a ete fait par un env conda par exeple je peut plus le lire avec un venv de worker...
        if os.path.exists(output_pickle_file) and force_phoenix_query==False:
            logging.info('listing SAR matching the buoy already exists %s',output_pickle_file)
            df_sar = pd.read_pickle(output_pickle_file)
        else:
            df_sar = get_dataframe_sar_from_phoenix(lonmin,lonmax,latmin,latmax)
            if len(df_sar)>0:
                save_listing_sar_match_given_buoy(buoy_id,df_sar,output_pickle_file)
        if len(df_sar)>0:
            df_sar['distancebuoy'] = df_sar.apply(add_distance_tobuoy, args=(lonb,latb,),axis=1)
            if False: #those listings are already existing no need to rewrite them, but when it will be automated may be it can be useful to turn this ON
                save_listing_sar_match_given_buoy(buoy_id,df_sar)
            aparaied_df,time_stratus,hs_stratus,dominant_period_stratus,hs_partition_buoy = compute_hs_tot_and_eff_on_buoys_spectra_storm_tracked(filepath_buoytracked_L2B,df_sar,cirteria_time_coloc,buoy_id)
            df_buoy = pd.DataFrame({'dates':time_stratus,'hstot':hs_stratus,'dominant_period':dominant_period_stratus,'hs_partition_buoy':hs_partition_buoy})#param without coloc S1
            save_pickle_files(buoy_id,df_sar,df_buoy,aparaied_df,cirteria_time_coloc,start_dt,stop_dt,storm_tracked=True,filepath_buoytracked_L2B=filepath_buoytracked_L2B)
        else:
            logging.info('no coloc Sentinel-1 for buoy %s',buoy_id)
    else:
        
        logging.info('prepared data already exists for %s',buoy_id)
    logging.info('elapsed time to prepare one buoy %1.1f seconds',time.time()-t0)
    return aparaied_df,time_stratus,hs_stratus,dominant_period_stratus

def get_dates_id_from_filename(filepath_buoytracked):
    netw = filepath_buoytracked.split('/')[10]
    if 'from' in os.path.basename(filepath_buoytracked):
        buoy_id = os.path.basename(filepath_buoytracked).split('from')[0].replace('BuoySwell','')
        start_dt = datetime.datetime.strptime(os.path.basename(filepath_buoytracked).split('from')[1][2:13],'%y_%m_%d_%H')
        stop_dt = datetime.datetime.strptime(os.path.basename(filepath_buoytracked).split('to')[1][2:13],'%y_%m_%d_%H')
    else: #case all_measurements.nc
        buoy_id = os.path.basename(filepath_buoytracked).split('_')[0].replace('BuoySwell','')
        start_dt = None
        stop_dt = None
    return netw,buoy_id,start_dt,stop_dt

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='prepare SAR and buoy dataframe to look a the data')
    parser.add_argument('--verbose', action='store_true',default=False)
    parser.add_argument('--fromStormTrackedBuoy', action='store',default=None)
    parser.add_argument('--provider', action='store', default=None,choices=['ndbc_arc','ndbc_nrt','ndbc_txt','ndbc_globwave','cdip_globwave','meteofrance'],required=True, help='provider of buoy data?')
    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)



#     cirteria_time_coloc= 3600.0
    cirteria_time_coloc = 1200 #I took 30min to have only one buoy measurement matching the SAR
#     classic_approach = False
#     if classic_approach:
    if args.fromStormTrackedBuoy is None:
        if False:
            start_dt = datetime.datetime(2014,1,1)
            stop_dt = datetime.datetime(2020,1,1)
    #     start_dt = datetime.datetime(2016,5,1)
    #     stop_dt = datetime.datetime(2016,5,30)
    
    #     netw = 'NDBC_'
    #     netw = 'CDIP'
            logging.info('classic approach without storm tracking before')
            logging.info('analysis period: %s to %s',start_dt,stop_dt)
            #     buoy_id = 'WMO46246'
            buoy_id = 'WMO32012'
            netw = 'NDBC_ARCHIVE'
            netw = 'NDBC' #globwave NRT NC spectra
            prepare_one_buoy(buoy_id,netw,start_dt,stop_dt,redo=True,cirteria_time_color=cirteria_time_coloc)
        else: #cas je les fait toutes celles qui sont dispo!!!
            dir_l2b = os.path.join(PATH_L2B_BUOY,args.provider,'3h_spectra_averaging','tuned_detection_param')
            pat = os.path.join(dir_l2b,'BuoySwell*_all_measurements.nc')
            logging.info('pattern L2B = %s',pat)
            list_L2B_available = glob.glob(pat)
            logging.info('Nb file L2B found %s',len(list_L2B_available))
            cpt_pairs ={}
            for buoy_cpt,filepath_buoytracked_l2b in enumerate(list_L2B_available):
                netw,buoy_id,start_dt,stop_dt = get_dates_id_from_filename(filepath_buoytracked_l2b)
                
                logging.info('%s/%s input : %s',buoy_cpt,len(list_L2B_available),filepath_buoytracked_l2b)
                aparaied_df,time_stratus,hs_stratus,dominant_period_stratus = prepare_one_buoy_stormtracked(buoy_id,netw,start_dt,filepath_buoytracked_l2b,stop_dt,redo=False,cirteria_time_coloc=cirteria_time_coloc)
                if aparaied_df is not None:
                    nb_pair = len(aparaied_df)
                else:
                    nb_pair = 0
                cpt_pairs[buoy_id] = nb_pair
            logging.info('Nb pairs %s',cpt_pairs)
    else: #for proto stormtracking approach
        filepath_buoytracked = '/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/ndbc_txt/original_dectection_param/BuoySwell32012from2016_08_26_01to2016_08_29_08.nc'
        filepath_buoytracked = '/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/ndbc_nrt/original_dectection_param/BuoySwell32012from2017_05_01_16to2017_05_28_19.nc'
        filepath_buoytracked = '/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/ndbc_nrt/original_dectection_param/BuoySwell32012from2016_05_06_04to2016_06_01_16.nc'
        filepath_buoytracked = '/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/ndbc_globwave/original_dectection_param/BuoySwell32012from2016_05_02_23to2016_05_30_23.nc'
        #file for may 2016
        filepath_buoytracked = '/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/ndbc_globwave/tuned_detection_param/BuoySwell32012from2016_05_02_23to2016_05_30_23.nc'
#         filepath_buoytracked = '/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/ndbc_globwave/original_dectection_param/BuoySwell32012from2016_05_01_03to2016_05_30_23.nc'
        #tte la period sentinel1
#         filepath_buoytracked = '/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/ndbc_globwave/original_dectection_param/BuoySwell32012from2014_06_01_07to2018_01_01_17.nc'
        filepath_buoytracked = '/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/ndbc_globwave/original_dectection_param/BuoySwell32012from2014_06_01_07to2018_02_01_01.nc' #avec les event en plus + partitionID
        #pour test avec les events ajoutes et le path ci dessous
#         filepath_buoytracked = '/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/ndbc_globwave/original_dectection_param/BuoySwell32012from2016_05_06_04to2016_05_30_23.nc'
#         filepath_buoytracked = '/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/ndbc_globwave/original_dectection_param/BuoySwell32012from2016_05_01_03to2016_05_30_23.nc'
        filepath_buoytracked = '/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/ndbc_txt/no_spectra_averaging/original_dectection_param/BuoySwell32012from2016_01_01_02to2016_01_31_03.nc'
        filepath_buoytracked = '/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/ndbc_txt/no_spectra_averaging/original_dectection_param/BuoySwell32012from2016_01_04_08to2016_01_31_03.nc' #txt no averaging and discard threshold at 5%
#         filepath_buoytracked = '/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/ndbc_globwave/no_spectra_averaging/original_dectection_param/BuoySwell32012from2014_01_01_01to2018_01_30_04.nc'
        #filepath_buoytracked = "/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/ndbc_globwave/no_spectra_averaging/original_dectection_param/BuoySwell51004from2014_07_07_16to2018_09_16_20.nc"
#         filepath_buoytracked = "/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/ndbc_globwave/no_spectra_averaging/original_dectection_param/BuoySwell51004from2014_07_09_22to2018_09_16_20.nc"
#         filepath_buoytracked = "/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/ndbc_globwave/3h_spectra_averaging/original_dectection_param/BuoySwell32012from2014_01_06_02to2018_02_01_01.nc"
        filepath_buoytracked = '/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/ndbc_txt/3h_spectra_averaging/original_dectection_param/BuoySwell32012from2016_01_04_08to2016_01_31_03.nc'
        filepath_buoytracked = "/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/ndbc_txt/3h_spectra_averaging/original_dectection_param/BuoySwell32012from2016_01_10_02to2016_01_29_03.nc"
        filepath_buoytracked = "/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/ndbc_globwave/3h_spectra_averaging/original_dectection_param/BuoySwell32012_all_measurements.nc"
        filepath_buoytracked = '/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/ndbc_globwave/no_spectra_averaging/original_dectection_param/BuoySwell51004_all_measurements.nc'
        filepath_buoytracked = '/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/ndbc_txt/3h_spectra_averaging/original_dectection_param/BuoySwell32012_all_measurements.nc'
        filepath_buoytracked = '/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/ndbc_txt/3h_spectra_averaging/original_dectection_param/BuoySwell51004_all_measurements.nc'
        filepath_buoytracked = '/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/cdip_globwave/3h_spectra_averaging/original_dectection_param/BuoySwell46246from2016_01_04_06to2016_01_31_11.nc'
        filepath_buoytracked = '/home/cercache/users/agrouaze/temporaire/sentinel1/coloc/buoydata/output_WangHe_tracking_code/cdip_globwave/3h_spectra_averaging/original_dectection_param/BuoySwell46246_all_measurements.nc'
        filepath_buoytracked = args.fromStormTrackedBuoy
        #:TODO:
        #il faudra une petite methode pour trouver le bon fichier netcdf en fontion du buoyid du reseau et des dates quand on aura un dataset complet
        #en attendant je fais l'inverse en recuperant ces infos depuis le fichier selectionne a la mano
        netw,buoy_id,start_dt,stop_dt = get_dates_id_from_filename(filepath_buoytracked)
        
        logging.info('input : %s',filepath_buoytracked)
        aparaied_df,time_stratus,hs_stratus,dominant_period_stratus = prepare_one_buoy_stormtracked(buoy_id,netw,start_dt,filepath_buoytracked,stop_dt,redo=False,cirteria_time_coloc=cirteria_time_coloc)
        #do the plot in time series about Hs
    logging.info('execution finished %s %s',time.time(),datetime.datetime.today())
#         pdb.set_trace()
        
        
        
        
        
        
