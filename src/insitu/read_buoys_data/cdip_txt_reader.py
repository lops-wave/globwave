import os
import glob
from datetime import datetime
import numpy as np
from cerform.wave import buoy_spectrum2d,spfphi2kphi,spfrom2to
from data_path import cdip_txt_Path


def read_cdip_pos(id,time_wd):
	filelist = findFilename( id , time_wd)
	# import pdb; pdb.set_trace() 

	if filelist !=[]:
		lon,lat,name,depth = Getpos(filelist[0])

		lon = lon+(lon<0)*360;

		return lon,lat,name,depth
	else:
		return None,None,None,None

def read_cdip_txt_2(id,time_wd,phi = np.arange(0,365,10),con = 'to'):
	filelist = findFilename( id , time_wd)

	if filelist !=[]:
		f,sf,time,a1,a2,b1,b2,lon,lat = GetAllData(filelist)
		spfphi = Get2dSpec(sf,a1,a2,b1,b2,phi)		

		if con=='to':
			spfphi = spfrom2to(spfphi,phi)
		return lon,lat,time,f,sf,spfphi	
	else:
		return None,None,None,None,None,None


		

def read_cdip_txt(id,time_wd,phi = np.arange(0,365,10),con = 'to'):
	filelist = findFilename( id , time_wd)

	if filelist !=[]:
		f,sf,time,a1,a2,b1,b2,lon,lat = GetAllData(filelist)
		spfphi = Get2dSpec(sf,a1,a2,b1,b2,phi)
		sp2d,k =  spfphi2kphi(spfphi,f)

		if con=='to':
			sp2d = spfrom2to(sp2d,phi)
		return lon,lat,time,f,k,sf,sp2d	
	else:
		return None,None,None,None,None,None,None




def findFilename( id, time_wd):
	filepattern = 'sp'+id+'??????????????'

	filelist,filelist2,time = [],[],[]  

	if time_wd['type'] == 't0dt':
		time_wd2 = {'type':'t1t2',
			'startDate': time_wd['time0']-time_wd['dt_max'],
			'stopDate': time_wd['time0']+time_wd['dt_max']}
	else:
		time_wd2 = time_wd

	startDate,stopDate = time_wd2['startDate'],time_wd2['stopDate']
	archivePath = cdip_txt_Path

	curDate = datetime(startDate.year,startDate.month,1)
	while curDate < stopDate:
		monthDir = os.path.join(archivePath, 
					str(curDate.year), 
					id,	curDate.strftime('%b').lower(),'01' )

		if os.path.exists(monthDir):
			filelist = filelist+glob.glob(monthDir+'/'+filepattern)
		if curDate.month == 12:
			curDate = curDate.replace(year=curDate.year+1,month=1)
		else:
			curDate = curDate.replace(month=curDate.month+1)

	filelist.sort()
	for i,f in enumerate(filelist):
		fn = os.path.basename(f)
		ftime = datetime.strptime(fn[7:19], '%Y%m%d%H%M')
		if startDate<ftime<stopDate:
			filelist2.append(f)
			time.append(ftime)

	time = np.array(time)
	if time_wd['type'] == 't0dt' and len(filelist)>1 and len(time)>1:
		time0,dt_max= time_wd['time0'],time_wd['dt_max']
		# import pdb; pdb.set_trace() 
		dt = min(abs(time-time0))
		if dt<dt_max :
			filelist2 = [filelist2[np.argmin(abs(time-time0))]]
		else:
			filelist2 = []


	return filelist2

def GetPosition(line):
	content = line.split()
	lat = float(content[1]) + float(content[2])/60.
	if (content[3] == 'S'):
		lat = -lat
	lon = float(content[4]) + float(content[5])/60.
	if (content[6] == 'W'):
		lon = -lon
	return lon,lat

def Getdata(filepath):
	f,sf,a1,a2,b1,b2 = [],[],[],[],[],[]
	file = open(filepath, 'r')
	# import pdb; pdb.set_trace()
	all_content=file.readlines()
	maxline=len(all_content)


	line=all_content[0];	content = line.split()
	timestr = content[2]

	time = [datetime.strptime(timestr[7:19], '%Y%m%d%H%M')]
	lon,lat = GetPosition(all_content[2])
	beg = 10
	for linenb in range(beg,maxline):
		line = all_content[linenb]
		content = line.split()
		f.append( float(content[0]))
		# freqRange = float(content[1])
		sf.append( float(content[2]))
		a1.append( float(content[4]))
		a2.append( float(content[6]))
		b1.append( float(content[5]))
		b2.append(  float(content[7]))
		# meanDir = float(content[3])
	f =  np.array(f)
	sf = np.array(sf).reshape((1,-1))
	time = np.array(time)
	a1 = np.array(a1).reshape((1,-1))
	a2 = np.array(a2).reshape((1,-1))
	b1 = np.array(b1).reshape((1,-1))
	b2 = np.array(b2).reshape((1,-1))
	lon = np.array(lon).reshape((1,))
	lat = np.array(lat).reshape((1,))

	return f,sf,time,a1,a2,b1,b2,lon,lat

def Getpos(filepath):

	file = open(filepath, 'r')
	# import pdb; pdb.set_trace()
	all_content=file.readlines()

	lon,lat = GetPosition(all_content[2])
	name = all_content[1].split(':')[1][0:-2]

	depth = -int(all_content[3].split(':')[1].split()[0].strip())


	return lon,lat,name,depth

def GetAllData(filelist):
	# import pdb; pdb.set_trace() 

	f,sf,time,a1,a2,b1,b2,lon,lat = Getdata(filelist[0])
	for buoyFile in filelist[1::]:
		# print buoyFile
		_,isf,itime,ia1,ia2,ib1,ib2,ilon,ilat = Getdata(buoyFile)
		# import pdb; pdb.set_trace() 

		sf = np.concatenate((sf,isf), axis=0)
		a1 = np.concatenate((a1,ia1), axis=0)
		a2 = np.concatenate((a2,ia2), axis=0)
		b1 = np.concatenate((b1,ib1), axis=0)
		b2 = np.concatenate((b2,ib2), axis=0)
		# import pdb; pdb.set_trace()
		time = np.array(list(time)+list(itime))
		lon  = np.array(list(lon )+list(ilon))
		lat  = np.array(list(lat )+list(ilat))
	return f,sf,time,a1,a2,b1,b2,lon,lat			

def Get2dSpec(sf,a1,a2,b1,b2,phi):
	spfphi = []
	
	for vsf,va1,va2,vb1,vb2 in zip(sf,a1,a2,b1,b2):
		# import pdb; pdb.set_trace() 		
		v,_ = buoy_spectrum2d(vsf,va1,va2,vb1,vb2, dirs = phi)
		spfphi.append(v)

	return spfphi
if __name__ == '__main__':
	from datetime import timedelta 
	id = '168'
	time_wd = {'type':'t0dt','time0': datetime(2014,7,1,0,20),'dt_max':timedelta(hours = 0.5)}
	# time_wd = {'type':'t1t2','startDate': datetime(2014,6,28,0,2),'stopDate':datetime(2014,7,5,0,52)}

	phi = np.arange(0,365,10)
	

	# lon,lat,time,f,k,sf,sp_list = read_cdip_txt(id,time_wd,phi = phi,con = 'to')

	# from sp_plot import spec_plot1
	# cut_off = 100.
	# dir_out = 'f:'
	# if len(sp_list):
	# 	for sp,t in zip(sp_list,time):
	# 		fn = id+'_'+t.strftime('%Y%m%d%H%M')
	# 		spec_plot1(sp,k,phi, 
	# 				title = id, sub_title =t.strftime('%d-%b-%Y %H:%M:%S'),
	# 				file_out=os.path.join(dir_out,fn),cut_off=cut_off)	


	lon,lat = read_cdip_pos(id,time_wd)