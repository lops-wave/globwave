import os
import glob
import logging
from datetime import datetime
import netCDF4 as netcdf
import numpy as np
import pdb
from dateutil import rrule
from cerform.wave import buoy_spectrum2d, spfphi2kphi, th2ab,ar2ab,spfrom2to
# from data_path import cdip_globwave_Path, ndbc_globwave_Path,ndbc_nrt_Path,ndbc_archive_Path,ndbc_nrt_wind_Path,ndbc_globwave_nrt_wave_path
from data_path import path_globwave

def read_nc_wind(id,time_wd):
    filelist = findFilename(id, time_wd,provider ='ndbc_nrt_wind')
    if filelist!=[]:
        time_ind,filelist = GetTimeInd (filelist,time_wd)

        if all([ti==() for ti in time_ind]):
            return None,None,None
        else:
            # import pdb; pdb.set_trace()
            time,wspd,wdir = Getallwind(filelist,time_ind)
            return time,wspd,wdir
    else:
        return None,None,None        

def read_nc_pos(id,time_wd,provider):
    filelist = findFilename( id , time_wd,provider = provider)

    if filelist !=[]:
        
        time_ind,filelist = GetTimeInd (filelist,time_wd)

        if all([ti==() for ti in time_ind]):
            
            return None,None
        else:
        
            lon,lat =Getlonlat(filelist,time_ind,provider = provider)
            lon = lon[0]; lat = lat[0]

            lon = lon+(lon<0)*360;

            return lon,lat
    else:
        return None,None

def read_buoy_nc_2(id,time_wd,provider,phi = np.arange(0,365,10),con = 'to'):
    filelist = findFilename(id, time_wd,provider =provider)
    # import pdb; pdb.set_trace()
    
    if filelist!=[]:
        time_ind,filelist = GetTimeInd (filelist,time_wd)
        if all([ti==() for ti in time_ind]):
            
            return None,None,None,None,None,None
        else:
            lon,lat = Getlonlat(filelist,time_ind,provider =provider)
            f,sf,time,c1,c2,c3,c4 = GetAllData(filelist,time_ind,provider =provider)
            spfphi = Get2dSpec(sf,c1,c2,c3,c4,phi,provider = provider)
            if con=='to':
                spfphi = spfrom2to(spfphi,phi)

            return lon,lat,time,f,sf,spfphi
    else:
    
        return None,None,None,None,None,None
    


def read_buoy_nc(id,time_wd,provider,phi = np.arange(0,365,10),con = 'to'):
    filelist = findFilename(id, time_wd,provider =provider)
    # import pdb; pdb.set_trace()
    if filelist!=[]:
        time_ind,filelist = GetTimeInd(filelist,time_wd)
        logging.debug('time_ind: %s',time_ind)
        if all([ti==() for ti in time_ind]):
            # raise Exception ("ERROR : No data found !!!  " )
            return None,None,None,None,None,None,None,None
        else:
            lon,lat = Getlonlat(filelist,time_ind,provider =provider)
            f,sf,time,c1,c2,c3,c4 = GetAllData(filelist,time_ind,provider =provider)

    else:
        # raise Exception ("ERROR : No file found !!!  " )
        return None,None,None,None,None,None,None,None

    spfphi = Get2dSpec(sf,c1,c2,c3,c4,phi,provider = provider)
    sp2d,k =  spfphi2kphi(spfphi,f)

    if con=='to':
        sp2d = spfrom2to(sp2d,phi)

    return lon,lat,time,f,k,sf,sp2d,c1    

def findFilename( id, time_wd, provider ='cdip_globwave'):   
    logging.debug('search the files')
    filelist = []  
    if time_wd['type'] == 't0dt':
        time_wd2 = {'type':'t1t2',
                    'startDate': time_wd['time0']-time_wd['dt_max'],
                    'stopDate': time_wd['time0']+time_wd['dt_max']}
    else:
        time_wd2 = time_wd
    startDate,stopDate = time_wd2['startDate'],time_wd2['stopDate']

    archivePath = path_globwave[provider]
#     if provider == '':
#         archivePath = ndbc_globwave_nrt_wave_path
#     if provider in ['ndbc_globwave','ndbc_globwave_nrt_spectra','ndbc_globwave_archive_spectra',''] :
#         archivePath = ndbc_globwave_Path
#         filepattern = 'WMO'+id+'*_spectrum.nc'

    if provider == 'cdip_globwave' :
#         archivePath = cdip_globwave_Path
        if len(id)==5:      filepattern = 'WMO'+id+'*.nc'
        if len(id)==3:      filepattern = id+'_*.nc'

    elif provider == 'ndbc_nrt' :
#         archivePath = ndbc_nrt_Path
        filelist = [os.path.join( archivePath, id+'w9999.nc' )]
        filelist = [f for f in filelist if os.path.exists(f)]

    elif provider == 'ndbc_nrt_wind' :
#         archivePath = ndbc_nrt_wind_Path
        filelist = [os.path.join( archivePath, id+'h9999.nc' )] 
        filelist = [f for f in filelist if os.path.exists(f)]     

    elif provider == 'ndbc_arc' :               
#         archivePath = ndbc_archive_Path
        filelist = [os.path.join(archivePath,str(s),id+'w'+str(s)+'.nc') for s in np.arange(startDate.year,stopDate.year+1)]
        filelist = [f for f in filelist if os.path.exists(f)]
    else:
        filepattern = 'WMO'+id+'*_spectrum.nc'
    logging.info('archive path: %s',archivePath)
    logging.info('pattern of search: %s',filepattern)
#     if provider == 'cdip_globwave' or provider == 'ndbc_globwave' :
    if 'globwave' in  provider:
#         curDate = datetime(startDate.year,startDate.month,1)        
        for dd in rrule.rrule(rrule.MONTHLY,dtstart=startDate,until=stopDate):
            month_str  = str(dd.month).zfill(2)
            monthDir = os.path.join(archivePath, str(dd.year),month_str  )
            logging.debug('monthDir: %s',monthDir)
#         while curDate < stopDate:
#             monthDir = os.path.join(archivePath, str(curDate.year), "%02d" % curDate.month )
            if os.path.exists(monthDir):
                # import pdb; pdb.set_trace() 
                tmp = os.path.join(monthDir,filepattern)
                logging.debug('tested glob: %s',tmp)
                filelist += glob.glob(tmp)

#             if curDate.month == 12:
#                 curDate = curDate.replace(year=curDate.year+1,month=1)
#             else:
#                 curDate = curDate.replace(month=curDate.month+1)

    filelist.sort()

    if time_wd['type'] == 't0dt' and len(filelist)>1 :
        file_ind,time = [],[]
        for i,f in enumerate(filelist):
            data = netcdf.Dataset( f, 'r' )
            t = list(netcdf.num2date( data.variables['time'][:], data.variables['time'].units ))
            time = time+t
            file_ind = file_ind+ [f]*len(t)
            data.close()
        time = np.array(time)
        time0,dt_max= time_wd['time0'],time_wd['dt_max']
        dt = min(abs(time-time0))
        if dt<dt_max :
            filelist = [file_ind[np.argmin(abs(time-time0))]    ]       
        else:
            filelist = []
    logging.info('Nber of files found : %s',len(filelist))
    logging.debug('found %s',filelist)
    return filelist


def GetTimeInd (filelist,time_wd):
    logging.debug('time_wd: %s',time_wd)
    if filelist==[]:
        print 'No file exists!'
        return [()]

    time_ind ,filelist2= [],[]

    for buoyFile in filelist:

        data = netcdf.Dataset( buoyFile, 'r' )
        time = netcdf.num2date( data.variables['time'][:], data.variables['time'].units )
        data.close()

        # check the time in the time window or not
        if time_wd['type'] == 't1t2':
            # GET the spectrum data in [startDate,stopDate]
            startDate,stopDate = time_wd['startDate'],time_wd['stopDate'] 
            list_time_difference = [((t-startDate).total_seconds(),(stopDate-t).total_seconds()) for t in time]
            logging.debug('time differences: %s',list_time_difference)
            dt1,dt2 = np.array(zip(*list_time_difference))
            index = tuple(np.where((dt1>0) & (dt2>0))[0])
        if time_wd['type'] == 't0dt':         
            # GET the spectrum data for the given time0, within the time difference less than dt_max(unit:hours)
            time0,dt_max= time_wd['time0'],time_wd['dt_max']
            dt = min(abs(time-time0))
            if dt<dt_max :
                # import pdb; pdb.set_trace() 
                index = tuple([np.argmin(abs(time-time0))])
            else:
                index = ()
        
        if index != ():  
            time_ind.append(index)
            filelist2.append(buoyFile)

    return time_ind,filelist2

def Getlonlat(filelist,time_ind,provider = 'cdip_globwave'):
    lon,lat = [],[]
    for buoyFile,ind in  zip(filelist,time_ind):
        data = netcdf.Dataset( buoyFile, 'r' )
        if provider == 'ndbc_nrt' or provider == 'ndbc_arc':
            lat = lat + [np.squeeze(data.variables['latitude'][:])]*len(ind)
            lon = lon + [np.squeeze(data.variables['longitude'][:])]*len(ind)

        if provider == 'ndbc_globwave' or provider == 'cdip_globwave':
            lat = lat + [np.squeeze(data.variables['lat'][:])]*len(ind)
            lon = lat + [np.squeeze(data.variables['lon'][:])]*len(ind)

    data.close()

    return np.array(lon), np.array(lat)

def Getwind(filepath, index):

    data = netcdf.Dataset( filepath, 'r' )
    time = netcdf.num2date( data.variables['time'][:], data.variables['time'].units )
   
    time = time[index]

    wspd = np.squeeze(data.variables['wind_spd'][index,:]).reshape((len(index),-1)) 
    wdir = np.squeeze(data.variables['wind_dir'][index,:]).reshape((len(index),-1))    

    data.close()

    return time,wspd,wdir

def Getallwind(filelist,time_ind):
    # import pdb; pdb.set_trace() 
    if all([ti==() for ti in time_ind]):
        print 'No data during the period'
        return None,None,None

    time,wspd,wdir = Getwind(filelist[0],np.array(time_ind[0])) 

    for buoyFile,ind in zip(filelist[1::],time_ind[1::]):
        # print buoyFile
        itime,iwspd,iwdir = Getwind(buoyFile,np.array(ind))

        time = np.array(list(time)+list(itime))
        wspd = np.concatenate((wspd,iwspd), axis=0)
        wdir = np.concatenate((wdir,iwdir), axis=0)

    
    return time,wspd,wdir


def Getdata(filepath, index,provider = 'cdip_globwave'):

    data = netcdf.Dataset( filepath, 'r' )
    time = netcdf.num2date( data.variables['time'][:], data.variables['time'].units )
   
    time = time[index]

    if provider == 'ndbc_nrt' or provider =='ndbc_arc' :
        # import pdb; pdb.set_trace()  
        f = np.squeeze(data.variables['frequency'][:])
        sf = data.variables['spectral_wave_density'][index,:].reshape((len(index),-1))
        c1 = np.squeeze(data.variables['mean_wave_dir'][index,:]).reshape((len(index),-1))         # alpha 1
        c2 = np.squeeze(data.variables['principal_wave_dir'][index,:]).reshape((len(index),-1))    # alpha 2
        c3 = np.squeeze(data.variables['wave_spectrum_r1'][index,:]).reshape((len(index),-1))    # r1
        c4 = np.squeeze(data.variables['wave_spectrum_r2'][index,:]).reshape((len(index),-1))   # r2
 
    if 'cdip_globwave' in provider :
        # import pdb; pdb.set_trace()  
        f = np.squeeze(data.variables['central_frequency'][index[0],:])
        sf = data.variables['sea_surface_variance_spectral_density'][index,:]
    if provider in ['ndbc_globwave','ndbc_globwave_nrt_spectra'] : 
        # import pdb; pdb.set_trace()  
        f = np.squeeze(data.variables['wave_directional_spectrum_central_frequency'][:])
        sf = data.variables['spectral_wave_density'][index,:]

    if 'globwave' in provider:
        c1 = data.variables['theta1'][index,:]     # theta1  
        c2 = data.variables['theta2'][index,:]     # theta2 
        c3 = data.variables['stheta1'][index,:]    # stheta1 
        c4 = data.variables['stheta2'][index,:]    # stheta2 
        

    data.close()

    return f,sf,time,c1,c2,c3,c4

def GetAllData(filelist,time_ind,provider = 'cdip_globwave'):
    """
    collect the data for each time step found
    """
    if all([ti==() for ti in time_ind]):
        print 'No data during the period'
        return None,None,None,None,None,None,None

    f,sf,time,c1,c2,c3,c4 = Getdata(filelist[0],np.array(time_ind[0]),provider = provider)
    for buoyFile,ind in zip(filelist[1::],time_ind[1::]):
        # print buoyFile
        _,isf,itime,ic1,ic2,ic3,ic4 = Getdata(buoyFile,np.array(ind),provider = provider)

        time = np.array(list(time)+list(itime))
        sf = np.concatenate((sf,isf), axis=0)
        c1 = np.concatenate((c1,ic1), axis=0)
        c2 = np.concatenate((c2,ic2), axis=0)
        c3 = np.concatenate((c3,ic3), axis=0)
        c4 = np.concatenate((c4,ic4), axis=0)
    
    return f,sf,time,c1,c2,c3,c4


def Get2dSpec(sf,c1,c2,c3,c4,phi,provider = 'cdip_globwave'):
    spfphi = []
    
    for vsf,vc1,vc2,vc3,vc4 in zip(sf,c1,c2,c3,c4):
        # import pdb; pdb.set_trace() 
        # convert  to a1,b1,a2,b2
#         if provider == 'cdip_globwave' or provider == 'ndbc_globwave':
        if 'globwave' in provider:
            a1,a2,b1,b2 = th2ab(vc1,vc2,vc3,vc4)
        if provider     == 'ndbc_nrt' or provider =='ndbc_arc':
            a1,a2,b1,b2 = ar2ab(vc1,vc2,vc3,vc4)
        
        v,_ = buoy_spectrum2d(vsf,a1,a2,b1,b2, dirs = phi)
        spfphi.append(v)

    return spfphi

if __name__ == '__main__':
    from datetime import timedelta 
    phi = np.arange(0,365,10)
# # test of cdip globwave format
    id = '165'
    time_wd = {'type':'t0dt','time0': datetime(2014,5,1,0,2),'dt_max':timedelta(hours = 0.5)}
    # time_wd = {'type':'t1t2','startDate': datetime(2014,5,28,0,2),'stopDate':datetime(2014,6,5,0,52)}
#     lon,lat,time,f,k,sf,sp_list = read_buoy_nc(id,time_wd,phi = phi,con = 'to', provider = 'cdip_globwave')
    lon,lat,time,f,k,sf,sp2d,c1 = read_buoy_nc(id,time_wd,phi = phi,con = 'to', provider = 'cdip_globwave')

# test of ndbc globwave formate
    id = '51100'
    time_wd = {'type':'t1t2','startDate': datetime(2014,5,2,0,2),'stopDate':datetime(2014,5,8,0,52)}

    lon,lat,time,f,k,sf,sp2d,c1 = read_buoy_nc(id,time_wd,phi = phi,con = 'to', provider = 'ndbc_globwave')

# # test of ndbc nrt 
    id = '32012'
    time_wd = {'type':'t1t2','startDate': datetime(2014,5,28,0,2),'stopDate':datetime(2014,6,5,0,52)}
    lon,lat,time,f,k,sf,sp2d,c1 = read_buoy_nc(id,time_wd,phi = phi,con = 'to', provider = 'ndbc_nrt')
    pdb.set_trace()

