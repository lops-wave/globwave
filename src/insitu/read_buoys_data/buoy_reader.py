from cdip_txt_reader import read_cdip_txt,read_cdip_pos,read_cdip_txt_2
from buoy_nc_reader import read_buoy_nc,read_buoy_nc_2
from buoy_nc_reader import read_nc_pos
from buoy_nc_reader import read_nc_wind

import numpy as np
provider_nc = ['cdip_globwave','ndbc_globwave','ndbc_arc' ,'ndbc_nrt']

def read_buoy_2(id,time_wd,provider,phi = np.arange(0,365,10),con = 'to'):
    if provider in provider_nc:
        lon,lat,time,f,sf,spfphi = read_buoy_nc_2(id,time_wd,provider,phi = phi,con = con)
    elif provider == 'cdip_txt':
        lon,lat,time,f,sf,spfphi = read_cdip_txt_2(id,time_wd,phi = phi,con = con)
    else:
        raise Exception ("ERROR : provider error!!!  " )
    return    lon,lat,time,f,sf,spfphi

def read_buoy(id,time_wd,provider,phi = np.arange(0,365,10),con = 'to'):
    if provider in provider_nc:
        lon,lat,time,f,k,sf,sp2d,_ = read_buoy_nc(id,time_wd,phi = phi,con = con, provider = provider)
    elif provider == 'cdip_txt':
        lon,lat,time,f,k,sf,sp2d = read_cdip_txt(id,time_wd,phi = phi,con = con)
    else:
        raise Exception ("ERROR : provider error!!!  " )

    return lon,lat,time,f,k,sf,sp2d    
def read_buoy_pos(id,time_wd,provider):
    if provider in provider_nc:
        lon,lat = read_nc_pos(id,time_wd,provider)

    elif provider == 'cdip_txt':
        lon,lat,_,_ = read_cdip_pos(id,time_wd)
    else:
        raise Exception ("ERROR : provider error!!!  " )

    return lon,lat
def read_wind(id,time_wd):    
    time,wspd,wdir = read_nc_wind(id,time_wd)
    return time,wspd,wdir
if __name__ == '__main__':

    from datetime import datetime,timedelta 
    phi = np.arange(0,365,10)
    # 1.  test cdip txt
    # provider = 'cdip_txt'
    # id = '168'
    # time_wd = {'type':'t0dt','time0': datetime(2014,7,1,0,20),'dt_max':timedelta(hours = 0.5)}
    # # time_wd = {'type':'t1t2','startDate': datetime(2014,6,28,0,2),'stopDate':datetime(2014,7,5,0,52)}

    # lon,lat,time,f,k,sf,sp_list = read_buoy(id,time_wd,provider,phi = phi,con = 'to')

    # 2.  test cdip globwave
    # id = '165'
    # time_wd = {'type':'t0dt','time0': datetime(2014,5,1,0,2),'dt_max':timedelta(hours = 0.5)}
    # # time_wd = {'type':'t1t2','startDate': datetime(2014,5,28,0,2),'stopDate':datetime(2014,6,5,0,52)}
    # lon,lat,time,f,k,sf,sp_list = read_buoy(id,time_wd,'cdip_globwave',phi = phi,con = 'to')

    # 3. test of ndbc globwave formate
    # id = '51100'
    # time_wd = {'type':'t1t2','startDate': datetime(2014,5,2,0,2),'stopDate':datetime(2014,5,8,0,52)}

    # lon,lat,time,f,k,sf,sp_list = read_buoy(id,time_wd,'ndbc_globwave',phi = phi,con = 'to')

    # # 4. test of ndbc nrt 
    # id = '32012'
    # time_wd = {'type':'t1t2','startDate': datetime(2014,5,28,0,2),'stopDate':datetime(2014,6,5,0,52)}
    # lon,lat,time,f,k,sf,sp_list = read_buoy(id,time_wd,'ndbc_nrt',phi = phi,con = 'to')

    # 5. test of ndbc 
    # id = '51000'
    # time_wd = {'type':'t1t2','startDate': datetime(2009,12,25,14,0),'stopDate':datetime(2009,12,25,15,30)}    
    # lon,lat,time,f,k,sf,sp_list = read_buoy(id,time_wd,'ndbc_arc',phi = phi,con = 'to')
    

    # test for the wind
    id = '51004'
    time_wd = {'type':'t0dt','time0': datetime(2014,8,31,0),'dt_max':timedelta(hours = 0.5)}
    time,wspd,wdir = read_nc_wind(id,time_wd)
