"""
context: since there is a processing using latest data and another one using monthly coriolis data we need to put in common some of the functions
(incoherence between monthly processing and daily processing on the variable name for coriolis buoys in globwave)
author: antoine grouazel
purpose: from input data return a  new field cerbere object (in particular regarding the name)
creation: 11april 2018
"""
import numpy
import math
import os
import logging
from ceraux.landmask import Landmask
from bathymetry import Bathymetry #previously comming from cerbere the module has been added to globwave repo
from ceraux.landmask_resources import LAND_MASK_PATH
import copy
import datetime
from globwave.src.insitu.variables_conventions import VarConventions
from globwave.src.insitu.globwave_shared_infos import DICO
from cerform import cfconvention
from cerbere.mapper.ncfile import NCFile,WRITE_NEW
from globwave.src.insitu.timeserieconcat import ConcatenateFiles, findContigousPlatformFiles, findWidderPlatformFile
from cerbere.datamodel.variable import Variable
from cerbere.datamodel.field import Field, QCLevel, QCDetail
UNITS = {#src:std #this list the possible src units corresponding to a given std unit
#         'degree clockwise relative to true North':'degree',
    'kg/m3':'kg.m-3',
  'm2 second':'m2 s-1',
  'meters/second':'m s-1',
  'meter/second':'m s-1',
  ' meter/second':'m s-1',
  'm/s':'m s-1',
  'degrees_true':'angular_degree',
#   'degree_Celsius':'celsius',
  'degree_celsius':'degree Celsius',
  'degree_Celsius':'degree Celsius',
  'celsius':'degree Celsius',
  'degC':'celsius',
  'hPa':'Pa',
  'hectopascal':'Pa',
  'hectoPascal':'Pa',
  'meters':'m',
  'meter':'m',
  'seconds':'s',
  'second':'s',
  '(meter * meter)/Hz':'m2 Hz-1',
  'ml/l':'ml l-1',
  '1e-3':'g l-1',
  'degree':'degree clockwise relative to true North',
  'angular_degree':'degree clockwise relative to true North',
  '%':'percent',
  'ntu':'ntu',
  'N.T.U Nephelo Turb. Unit':'N.T.U Nephelo Turb. Unit',
  'N.T.U Nephelo Turb Unit':'N.T.U Nephelo Turb. Unit',
  'pH unit':'pH unit',
  'cm/second':'cm s-1',
  'psu':'psu',
  'PSU':'psu',
  'milligram/m3':'mg m-3',
  'mg/m3':'mg m-3',
  'mg/l':'mg l-1',
  'degrees':'angular_degree',
  'deg':'angular_degree',
  'm-1':'m-1',
  'none':'none',
  'S/m':'S/m',
  'mS/cm':'mS/cm',
  'W m-2':'W m-2',
  'millimeter/day':'millimeter/day',
  'hectopascal/hour':'hectopascal/hour',
  'micromole photon/(m2.s)':'micromole photon/(m2.s)',
  'FFU':'FFU',
  'decibar':'decibar',}

    

def CreateFieldcomon(v,dims,qc_levels,qc_details,inputvalues,inputVarDescription,input_units):
    '''
    instanciate cerbere class Field with geophysical variables of the provider netCDF
    what to do on values:
    squeeze the one with depth=1 ?
    take only the interval time for the ones having time dimensions
    
    note : present version is coming from coriolis_read_latest.py 
    a different version of this method exist in oceansitesbuoymooredbuoys.py
    '''
    logging.debug('get variable %s informations ',v)
#         vname = self.get_cersatname(v)
#     field_container = collections.OrderedDict()
#     var_dimension_names = copy.deepcopy(feature.get_field(v).dimensions)
    fieldobj = None
    if v in DICO:
        conv_info = VarConventions(DICO[v][0],return_accro=True)
        if conv_info is not None:
            vname,longname,description,sensor,stdunits,acronym = conv_info
            logging.debug('translate %s ->%s-> %s',v,DICO[v],vname)
            stdname = cfconvention.get_standardname(vname)
            #logging.debug("original name:",v,"globawave name", vname,'stdanard name: ',stdname)
                # attributs !!!
            #descr = feature.get_field(v).variable.description
            varobj = Variable(shortname=acronym,
                                    description= longname,
                                    authority = cfconvention.get_convention(),
                                    standardname = stdname )
#                 ovars[v] = varobj
#             var_nb_dimension = ifd.variables[v].ndim
            var_nb_dimension = len(dims)
            logging.debug('%s has %s dimensions',v,var_nb_dimension)
#             sl = {'time':slice(intstart,intend,1),'z':slice(0,0,1)}
            
#             values = feature.get_values(v)
            #values = copy.deepcopy(feature.get_values(v,slices=sl))
            values = inputvalues
            logging.debug('values of %s : %s => field %s',v,values.shape,dims.values())
#                         values = numpy.ma.squeeze(values)
            #src_unit = feature.get_field(v).units
            values = ConversionUnits(input_units,v,values,stdunits)
            logging.debug('shape of values for variable %s is %s ',v,values.shape)
            values = numpy.ma.masked_array(values)
            fv = values.fill_value
            if values.count() != 0 and (values.mask==False).any():
                fieldobj = Field(varobj,
                                                 copy.copy(dims),
                                                 values = values,
                                                 units = stdunits,
                                                 fillvalue=fv,
                                                 qc_levels=qc_levels,
                                                 qc_details=qc_details,
                                                 attributes={'description':description,
                                                             "description_provider":inputVarDescription
                                                             }
                                                 )
#                 field_container[v] = fieldobj
            else:
                logging.warning('variable %s has not been added to the new netcdf because there is no useable values',v)
                logging.debug('type of values :%s   values.count:%s',type(values),values.count())
        else:
            logging.warning('%s is not defined in VarConventions',DICO[v])
    else:
        logging.warning('%s is not defined in DICO',v)
    return fieldobj
conversion = {'PRRT':('PRECIPIT_VOLUME',numpy.multiply,24),#millimeter/hour -> mm/day
              }

def ConversionUnits(src_unit,variable_name,values,std_unit):
    '''
    convert values in function of units and get globwave units convention 
    '''
    flag_warning = False
    if std_unit != src_unit:
        if src_unit in UNITS:
            if UNITS[src_unit] != std_unit:
                logging.debug('converted source unit: %s',UNITS[src_unit])
                flag_warning = True
        else:
            flag_warning = True
            
        # Convert to Pascal
#             src_unit = ifd.variables[variable_name].units
        if src_unit in ['hPa','hectopascal','hectoPascal'] :
            values = values * 100.
        elif src_unit in ['decibar']:
            values = values * 1000.
        else:
            if flag_warning:#warning only if the conversion is not handle
                #{it is only debug information since the units provided in oceansitesbuoymooredbuoy are not certified to be standard}
                logging.warning('units are different std:%s != src:%s',std_unit,src_unit)
        # Convert to m s-1
        if src_unit in ['cm/second'] :
            logging.debug("cm/second -> conversion m s-1")
            values = values / 100.
    # if ifd.variables[v].ndim
#         if 'units' in ifd.variables[v].ncattrs():
#             logging.debug('variable %s units %s',v,ifd.variables[v].units)     
#             units = self.get_units(ifd.variables[v].units)
#         else:
#             units = None
    if variable_name =='PRRT':
        cst = conversion[variable_name][1]
        operator = conversion[variable_name][0]
        values = operator(values,cst)
    return values

def WriteGWOceansiteFile(ofname,listOfFilesCreated,ts,globalmetadata):
    '''
    function to write the timeserie in a final nertcdf file 
    (note that the test on existing file won't affect the process
     if the data previously processed has been removed)
     '''
    existingfile = findWidderPlatformFile(ofname)
    if existingfile == None:
        if os.path.exists(ofname):
            logging.info('removing the existing version of %s',ofname)
            os.remove( ofname )
        tsncf = NCFile(url=ofname, mode=WRITE_NEW, ncformat='NETCDF4')
        listOfFilesCreated.append(ofname)
        ts.save( tsncf, attrs=globalmetadata )
        tsncf.close()
        logging.info('file written %s',ofname)
    else:
        logging.info('file %s not created because %s already exist',ofname,existingfile)
    return listOfFilesCreated




def DefineGlobalMetaData(file_name,alloriginalglobalattr,starttimeserie,endtimeserie,lat,lon,processor_version):
    """
    :args:
        file_name (str):
        alloriginalglobalattr (dict)
    """
    
    # Global metadata
    logging.debug("global metadata copy")
    # ---------------
    globalmetadata = {}
#         for attr in ifd.ncattrs():
#         src_meta = feature.get_metadata()
    src_meta = alloriginalglobalattr
#         globalmetadata = get_metadata
    for aa,attr in enumerate(src_meta):
        if attr not in ['time_coverage_end','time_coverage_start']:
            globalmetadata[attr] = src_meta[attr]
#         globalmetadata['institution'] = ifd.getncattr('institution')
    globalmetadata['institution_abbreviation'] = ''
    globalmetadata['naming_authority'] = "WMO"
    globalmetadata['summary'] = "Coriolis - MyOcean distribution data from moored buoys"
    globalmetadata['title'] = "Meteorological and Oceanographic data obtained from the MyOcean project, collected for GlobWave project"
    globalmetadata['cdm_feature_type'] = "station"
    globalmetadata['scientific_project'] = "GlobWave"
    globalmetadata['acknowledgement'] = "We are grateful to MyOcean for providing these data to GlobWave project"
    globalmetadata['standard_name_vocabulary'] = "CF-1.6"
    globalmetadata['restrictions'] = "Restricted to Ifremer and GlobWave usage"
    globalmetadata['processing_software'] = "cerbere 1.0 (CERSAT)"
    globalmetadata['processing_level'] = "0"
    globalmetadata['processor_version'] = processor_version
    globalmetadata['history'] = "1.0 : Processing to GlobWave netCDF format"
    globalmetadata['publisher_name'] = "Ifremer/Cersat"
    globalmetadata['publisher_url'] = "http://cersat.ifremer.fr"
    globalmetadata['publisher_email'] = "antoine.grouazel@ifremer.fr"
    globalmetadata['creator_name'] = "Antoine Grouazel"
    globalmetadata['creator_url'] = "http://cersat.ifremer.fr"
    globalmetadata['creator_email'] = "antoine.grouazel@ifremer.fr"
#         globalmetadata['geospatial_lat_min'] = ifd.getncattr('geospatial_lat_min') 
#         globalmetadata['geospatial_lat_max'] = ifd.getncattr('geospatial_lat_max') 
#         globalmetadata['geospatial_lon_min'] = ifd.getncattr('geospatial_lon_min')
#         globalmetadata['geospatial_lon_max'] = ifd.getncattr('geospatial_lon_max')
#         globalmetadata['geospatial_vertical_min'] = ifd.getncattr('geospatial_vertical_min')
#         globalmetadata['geospatial_vertical_max'] = ifd.getncattr('geospatial_vertical_max') 
#         globalmetadata['time_coverage_start'] = ifd.getncattr('time_coverage_start')
#         globalmetadata['time_coverage_end'] = ifd.getncattr('time_coverage_end')
#         endtimeserie = feature.get_end_time()
#         starttimeserie = feature.get_start_time()
    if isinstance(endtimeserie,str):
        globalmetadata['time_coverage_end'] = datetime.datetime.strptime(endtimeserie,'Y%m%dT%H%M%S.%fZ')
    else:
        globalmetadata['time_coverage_end'] = starttimeserie
    if isinstance(starttimeserie,str):
        globalmetadata['time_coverage_start'] = datetime.datetime.strptime(starttimeserie,'Y%m%dT%H%M%S.%fZ')
    else:
        globalmetadata['time_coverage_start'] = starttimeserie
#         if 'Z' in ifd.getncattr('time_coverage_end'):
#             coverage_end = datetime.datetime.strptime( ifd.getncattr('time_coverage_end'),'Y%m%dT%H%M%SZ')
#         else:
#             coverage_end = datetime.datetime.strptime( ifd.getncattr('time_coverage_end'),'Y%m%dT%H%M%S')
#         globalmetadata['time_coverage_end'] = coverage_end
#         globalmetadata['data_source'] = os.path.basename(f)
    file_name = file_name[0:len(file_name)-8]+'*'
    globalmetadata['source'] = os.path.basename(file_name)
    if 'wmo_platform_code' in globalmetadata:
        if src_meta['wmo_platform_code'] != "no_value" or src_meta['wmo_platform_code']==' ':
            globalmetadata['wmo_id'] = src_meta['wmo_platform_code']
        else :
            globalmetadata['wmo_id'] = ''
        del globalmetadata['wmo_platform_code'] #useless since we have wmo_id
    else:
        globalmetadata['wmo_id'] = ''
    globalmetadata['id'] = src_meta['site_code'] 
    globalmetadata['platform_code'] = src_meta['platform_code']
    globalmetadata['site_elevation'] = ''
    if 'platform_name' in src_meta:
        globalmetadata['station_name'] = src_meta['platform_name']
    else:
        globalmetadata['station_name'] = ''
        
#         lon = feature.get_lon()
#         lat = feature.get_lat()
    if not 'sea_floor_depth_below_sea_level' in globalmetadata:
#             if self._bathymetry == None:
            #self._bathymetry = Bathymetry(filename='/home/cercache/users/agrouaze/git/cerbere/cerbere/geo/resources/GridOne.nc')
#                 self._bathymetry = Bathymetry(filename='/home/cersat5/application_data/globwawe/static_file/bathy/GridOne.nc')
        
#             bt = Bathymetry(filename='/home/cersat5/application_data/globwawe/static_file/bathy/GridOne.nc')
        bt = Bathymetry(filename='/home/losafe/users/agrouaze/git/ceraux/ceraux/resources/GridOne.nc')
        floorDepth = -bt.get_depth(lon,lat)
    else:
        floorDepth = src_meta['sea_floor_depth_below_sea_level']
        globalmetadata['sea_floor_depth_below_sea_level'] = math.fabs(floorDepth)
    landmask_instance = Landmask(os.path.join(LAND_MASK_PATH,'landmask_025.nc'))
    dist = int(landmask_instance.getDistanceToShore(lat,lon,filename=os.path.join(LAND_MASK_PATH,'NAVO-lsmask-world8-var.dist5.5.nc')))
#         dist = int(LandMask.getDistanceToShore(lat,lon,filename='/home/cercache/users/agrouaze/git/cerbere/cerbere/geo/resources/NAVO-lsmask-world8-var.dist5.5.nc'))
    globalmetadata['distance_to_shore']=str(dist)+'m'
    globalmetadata['nominal_latitude'] = lat
    globalmetadata['nominal_longitude'] = lon
    return globalmetadata