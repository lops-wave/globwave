from buoy_reader import read_buoy


if __name__ == '__main__':
	import numpy as np
	from datetime import datetime,timedelta 
	phi = np.arange(0,365,10)
	# # 1.  test cdip txt
	# provider = 'cdip_txt'
	# id = '168'
	# time_wd = {'type':'t0dt','time0': datetime(2014,7,1,0,20),'dt_max':timedelta(hours = 0.5)}
	# # time_wd = {'type':'t1t2','startDate': datetime(2014,6,28,0,2),'stopDate':datetime(2014,7,5,0,52)}

	# lon,lat,time,f,k,sf,sp_list = read_buoy(id,time_wd,provider,phi = phi,con = 'to')

	# # 2.  test cdip globwave
	# id = '165'
	# time_wd = {'type':'t0dt','time0': datetime(2014,5,1,0,2),'dt_max':timedelta(hours = 0.5)}
	# # time_wd = {'type':'t1t2','startDate': datetime(2014,5,28,0,2),'stopDate':datetime(2014,6,5,0,52)}
	# lon,lat,time,f,k,sf,sp_list = read_buoy(id,time_wd,'cdip_globwave',phi = phi,con = 'to')

	# # 3. test of ndbc globwave formate
	# id = '51100'
	# time_wd = {'type':'t1t2','startDate': datetime(2014,5,2,0,2),'stopDate':datetime(2014,5,8,0,52)}

	# lon,lat,time,f,k,sf,sp_list = read_buoy(id,time_wd,'ndbc_globwave',phi = phi,con = 'to')

	# 4. test of ndbc nrt 
	id = '32012'
	time_wd = {'type':'t1t2','startDate': datetime(2014,8,4,18,30),'stopDate':datetime(2014,8,4,19,30)}
	lon,lat,time,f,k,sf,sp_list = read_buoy(id,time_wd,'ndbc_nrt',phi = phi,con = 'to')

	for sp,t in zip(sp_list,time):
			spec_plot1(sp,k,phi,
				title ='Stratus buoy spectrum',wsys =wavesys_buoy,
				sub_title = t.strftime('%d-%b-%Y %H:%M:%S'),
				file_out= t.strftime('%d-%b-%Y-%H')+'.png', cut_off=150.)

	# # 5. test of ndbc 
	# id = '51000'
	# time_wd = {'type':'t1t2','startDate': datetime(2012,12,25,14,0),'stopDate':datetime(2012,12,25,15,30)}	
	# lon,lat,time,f,k,sf,sp_list = read_buoy(id,time_wd,'ndbc_arc',phi = phi,con = 'to')

