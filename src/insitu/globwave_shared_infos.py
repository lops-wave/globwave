"""
Antoine Grouazel
purpose: store conf informations share between different binaries
"""
import os
import logging
from globwave.src.insitu.variables_conventions import VarConventions
# print "version april 2018"
GLOBWAVE_ROOT = '/home/cercache/project/globwave/data/globwave/'
PROVIDER_ROOT = '/home/cercache/project/globwave/data/provider/'
active_networks = {
                   "MEDS":os.path.join(GLOBWAVE_ROOT,'meds')
                   ,"CORIOLIS":os.path.join(GLOBWAVE_ROOT,'coriolis')
                   ,"NDBC":os.path.join(GLOBWAVE_ROOT,'ndbc/','nrt')
                   #,"NDBC_ARCHIVE":'/home/datawork-cersat-public/cache/project/globwave/data/globwave/ndbc/archive/'
                   ,"NDBC_ARCHIVE":os.path.join(GLOBWAVE_ROOT,'ndbc/archive/')
                   ,'NDBC_TXT':os.path.join(GLOBWAVE_ROOT,'ndbc/spectra_txt/')
                   ,"PMEL":os.path.join(GLOBWAVE_ROOT,'pmel')
                   ,"CEREMA":os.path.join(GLOBWAVE_ROOT,'cetmef')
                   ,"CDIP":os.path.join(GLOBWAVE_ROOT,'cdip')
                   ,"METEOFRANCE":os.path.join(PROVIDER_ROOT,'GTS_via_meteofrance')
                   }

DICO = {
        'WSPD':('wind_speed','anemometer'),
        'WSPN':('northward_wind','anemometer'),
        'SIGMA_THETA':('SIGMA_THETA','ocean_temperature_sensor'),
    'wind_dir':('wind_direction','anemometer'),
    'air_temperature':('air_temperature','ocean_temperature_sensor'),
    'dewpt_temperature':('dew_point_temperature','air_temperature_sensor'),
    'hourly_max_gust':('hourly_max_gust','anemometer'),
    'direction_of_hourly_max_gust':('direction_of_hourly_max_gust','anemometer'),
    'continuous_wind_speed':('continuous_wind_speed','anemometer'),
    'continuous_wind_direction':('continuous_wind_direction','anemometer'),
    'gust':('wind_speed_of_gust','anemometer'),
    'air_pressure_at_sea_level':('air_pressure_at_sea_level','barometer'),
    'ATPT':('ATPT','barometer'),
    'air_pressure':('air_pressure','barometer'),
    'sea_surface_temperature':('sea_surface_temperature','ocean_temperature_sensor'),
    'wave_height':('significant_wave_height','wave_sensor'),
    'dominant_wpd':('dominant_wave_period','wave_sensor'),
    'dominant_dir':('dominant_wave_direction','wave_sensor'),
    'average_wpd':('average_wave_period','wave_sensor'),
    'mean_wave_dir':('alpha1','wave_sensor'),
    'spectral_wave_density':('spectral_wave_density','wave_sensor'),
    'relative_humidity':('relative_humidity','air_humidity_sensor'),
    'PRRD':('daily_precipitation_rate','air_humidity_sensor'),
    'principal_wave_dir':('alpha2','wave_sensor'),
    'wave_spectrum_r1':('r1','wave_sensor'),
    'wave_spectrum_r2':('r2','wave_sensor'),
#     'CPHL':('chl_a','chlorometer'),
    'SLEV':('SLEV','tide_gage'),
    'WSPE':('EASTWS','anemometer'),
    'PSAL':('PSAL','ocean_salinity_sensor'),
    'CNDC':('sea_water_conductivity','ocean_salinity_sensor'),
    'TEMP':('sea_water_temperature','ocean_temperature_sensor'),
    'ATMP':('air_pressure','barometer'),
    'DOXY':('oxygen_saturation','dissolved_oxygen_sensor'),
    'DOX1':('dissolved_oxygen','dissolved_oxygen_sensor'),
    'OSAT':('fractional_saturation_of_oxygen_in_sea_water','dissolved_oxygen_sensor'),
    'AIRT':('air_temperature','air_temperature_sensor'),
    'RELH':('relative_humidity','air_humidity_sensor'),
    'WDIR':('wind_direction','anemometer'),
    'VAVH_hs':('significant_wave_height','wave_sensor'),
    'VTDH_h1de':('VAVH','wave_sensor'),
    'VAVH':('VAVH','wave_sensor'),#deliberatly point to VGHS generic HS to avoid conflict with VTDH_h1d3 that since Marta document should also refer to VAVH...
    'VAVT':('VAVT','wave_sensor'),
    'VTZA_tz':('VTZA','wave_sensor'), #Sea Surface Wave Zero Upcrossing Period (wave_period_tz)
    'VTZA_tm02':('VSMC','wave_sensor'),
    'VTZA_tm10':('VSMB','wave_sensor'),
    'WV_SIG_PER':('VAVH','wave_sensor'),
    'VTDH_h1d3':('VTDH_h1d3','wave_sensor'),  #Average Wave Height (highest one third) (wave_height_h1d3) #deliberatly commented to eliminate such variable because always provided with VAVH that are supposed to be the same by definition
    'VDIR_th0':('VMDR','wave_sensor'),
    'VMDR':('VMDR','wave_sensor'),
    'VZMD':('VZMD','wave_sensor'),
    'SWHT':('SWHT','wave_sensor'),
    'VHZA':('VHZA','wave_sensor'),
    'VTM10':('VTM10','wave_sensor'),
#     'VDIR_th0':('alpha1','wave_sensor'),   #Mean Wave Direction
    'VTZA_th1d3':('VAVT','wave_sensor'), #Aver. Zero Crossing Wave Period (highest one third)  (wave_period_th1d3)
    'VTZA_t1d3':('VAVH','wave_sensor'),
    'WV_MAX_HGT':('VZMX','wave_sensor'),
    'VAVH_max':('VZMX','wave_sensor'),
    'PHPH':('sea_water_ph','phmeter'),  #sea_water_ph_reported_in_total_scale
    'VDIR':('VDIR', 'wave_sensor'),#since Marta it should not be kept anymore
    'VTZA_tp':('VTPK','wave_sensor'),  # Wave Period (at spectral maximum)
    'HCSP':('current_speed','current_meters'), #sea_water_speed
    'HCDT':('current_direction','current_meters'),  #direction_of_sea_water_velocity
    'EWCT':('eastward_current_speed','current_meters'), # eastward_sea_water_velocity
    'NSCT':('northward_current_speed','current_meters'), # northward_sea_water_velocity    
    'CUSP':('current_speed','current_meters'), #sea_water_speed
    'SCDT':('current_direction','current_meters'), # SEA SURF CURRENT DIR. REL T. N direction_of_sea_water_velocity
    'ATMS':('ATMS','barometer'), #air_pressure_at_sea_level
    'DRYT':('air_temperature','air_temperature_sensor'), #DRY BULB TEMPERATURE  air_temperature
    'DEWT':('dew_point_temperature','air_temperature_sensor'),
    'GSPD':('wind_speed_of_gust','anemometer'), #wind_speed_of_gust
    'WDIR':('wind_direction','anemometer'), #wind_to_direction
    'WTODIR':('wind_direction','anemometer'),#relative to north
    'VTZA':('VTZA','wave_sensor'), # AVER ZERO CROSSING WAVE PERIOD  (sea_surface_swell_wave_zero_upcrossing_period)
    'VTDH':('significant_swell_wave_height','wave_sensor'), #SIGNIFICANT WAVE HEIGHT (sea_surface_swell_wave_significant_height)
    'VGHS':('VGHS','wave_sensor'),
    'VGTA':('VGTA','wave_sensor'),
    'PRES':('sea_water_pressure','manometer'), #  sea_water_pressure
    'CPH1':('chl_a','chlorometer'), # CHLOROPHYLL-A TOTAL (mass_concentration_of_chlorophyll_in_sea_water)
    'VTPK':('VTPK','wave_sensor'), # WAVE_SPECTRUM_PEAK_PERIOD (peak_period)
    'VMAT':('third_highest_maximum_wave_period','wave_sensor'),
    'VTZM':('period_of_highest_wave','wave_sensor'),
    'VZMX':('VZMX','wave_sensor'), # MAXI_ZERO_CROSSING_WAVE_HEIGHT (maximum_waveheight)
    'VSMC':('VSMC','wave_sensor'),#SPECT. MOMENT(0,2) WAVE PERIOD
    'VTM02':('VSMC','wave_sensor'),#correspondance
    'VH110':('VH110','wave_sensor'),
    'VT110':('VT110','wave_sensor'),
    'VEMH':('VZMX','wave_sensor'),#correspondance
    'SWPR':('SWPR','wave_sensor'),
    'VTMX':('VTMX','wave_sensor'),
    'VCMX':('VCMX','wave_sensor'),#MAX CREST TROUGH WAVE HEIGHT
    'VPSP':('VPSP','wave_sensor'),#DIR. SPREADING AT WAVE PEAK formerly dominant_wave_spreading
    'VPED':('VPED','wave_sensor'),#WAVE SPECTRUM PEAK ENERGY DIR.
    'VEPK':('VEPK','wave_sensor'),#WAVE SPECTRUM PEAK ENERGY
    'VHM0':('VHM0','wave_sensor'),#SPECTRAL SIGNIF. WAVE HEIGHT
    'ATEM':('air_temperature','air_temperature_sensor'), # airtemperature
    'TUR2':('TUR2','turbidimeters'), # light_attenuation
    'TURB':('TUR2','turbidimeters'),
    'VAVH_hmax':('VZMX','wave_sensor'),
    'FLU3':('fluorescence','turbidimeters'),
    'FLU2':('fluorescence','turbidimeters'),
    'FLUO':('fluorescence','turbidimeters'),
    'TUR4':('TUR4','turbidimeters'),
    'LGH4':('light_surface_irradiance','turbidimeters'),
    'SDFA':('solar_irradiance','turbidimeters'),
    'STH1M':('sea_surface_wave_directional_spread_from_first_directional_moment','wave_sensor'),
    'TH1M':('sea_surface_wave_mean_direction_from_first_directional_moment','wave_sensor'),
    'EF':('sea_surface_wave_variance_spectral_density','wave_sensor'),
    'PRRT':('PRECIPIT_VOLUME','meteo_sensors'),
    'RDIN':('SDFA','meteo_sensors'),
    'DENS':('DENS','ocean_temperature_sensor'),
    'SINC':('surface_downwelling_shortwave_flux_in_air','meteo_sensors'),
    'LINC':('long_wave_incoming_radiation','meteo_sensors'),
    'HOURLY_RAIN':('PRECIPIT_VOLUME','meteo_sensors'),
    'TUR6':('TUR4','turbidimeters'),
    'CHLT':('lkdjsmfls',''),
    'SWDR':('SWDR','wave_sensor'),
    'GDIR':('GDIR','anemometer'),
    'GSPD':('GSPD','anemometer'),
    'CPHL':('CPHL','chlorometer'),
    'VCSP':('VCSP','current_meter'),
    'PRHT':('PRECIPIT_VOLUME','meteo_sensor'),
    'SVEL':('SVEL','sonar'),
    'DOX2':('DOX2','dissolved_oxygen_sensor'),
    
  }

UNWANTEDVAR = ['DC_REFERENCE','TIME','POSITIONING_SYSTEM', 'LATITUDE','LONGITUDE','DEPTH','CPHL_QC','SLEV_QC','ATMP_QC','TEMP_QC','PSAL_QC','DOXY_QC','DOX1_QC','RELH_QC','AIRT_QC',
            'WSPD_QC', 'WDIR_QC', 'frequency','water_level','visibility','rhq','gamma2','gamma3','phih','c11m','VAVH_hs_QC','VAVH_QC','VTZA_tz_QC',
            'VTDH_h1d3_QC','SWHT_QC','VDIR_th0_QC','VTZA_th1d3_QC','PHPH_QC','VDIR_QC','VTZA_tp_QC','HCSP_QC','HCDT_QC','EWCT_QC','NSCT_QC',
            'GPS_LATITUDE','GPS_LONGITUDE','TEMP_DM','PSAL_DM','CUSP_DM','SCDT_QC','SCDT_DM','ATMS_QC','ATMS_DM','DRYT_QC','DRYT_DM',
            'CUSP_QC','WSPD_DM','GSPD_QC','GSPD_DM','WDIR_DM','VTZA_QC','VTZA_DM','VTDH_QC','VTDH_DM','VDIR_DM','PRES_QC','PRES_DM','DOX1_DM',
            'CPH1_QC','CPH1_DM','SLEV_DM','DOXY_DM','VTPK_QC','VZMX_QC','ATEM_QC','TUR2_QC','DEPH','DEPH_QC','DEPH_DM','DATA_MODE_CORA','DIRECTION','DATA_MODE']

#def DetermineInstrumentGroups(feature):
def DetermineInstrumentGroups(fieldnames):
    """
    :args:
        fieldnames (list): list of string of the fieldnames in the input file
    """
    # determine instrument groups
    logging.debug("determine instrument groups ")
    groups = {}
    for v in fieldnames:
        #if not v in UNWANTED and not v in FLAGS_GEN:
        logging.debug('variables found %s',v)
        if v not in UNWANTEDVAR and v[-3:]!='_QC' and  v[-3:] != '_DM' and v[0:4]!='GPS_':
            res = VarConventions(DICO[v][0],return_accro=True)
            if res is not None:
                GLOBEWAVE_input_var_name,longname,description,sensor,units,acronym = res
#             if v in DICO and not v in FLAGS_GEN:
                #logging.debug("wanted", v)
#                 sensor = self.get_instrument(v)
                if groups.has_key(sensor):
                    groups[sensor].append(v)
                else:
                    groups[sensor] = [v]
            else:
                logging.warning('variable %s is not referenced in dictionary ',DICO[v][0])
    return groups
