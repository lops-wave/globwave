import logging
import os
treshold_to_create_new_file = 86400
time_units = 'days since 1970-01-01 00:00:00Z'
def GetFinalNameOfTheBuoy(globalmetadata):
    """
    return the identifier of a given buoy
    Args:
        globalmetadata (dic):
    Returns:
        nameofthebuoy (str):
    """
    nameofthebuoy = ''
    if globalmetadata['wmo_id'] != '' and globalmetadata['wmo_id'] != ' ':
        nameofthebuoy='WMO'+globalmetadata['wmo_id']
    else:
        if 'platform_code' in globalmetadata.keys():
            if globalmetadata['platform_code'] != '' and globalmetadata['platform_code'] != ' ':
                nameofthebuoy=globalmetadata['platform_code']
        if nameofthebuoy == '':
            if 'institution_edmo_code' in globalmetadata.keys():
                if globalmetadata['institution_edmo_code'] != '':
                    nameofthebuoy='EDMO'+globalmetadata['institution_edmo_code']
        if nameofthebuoy == '':
            if 'id' in globalmetadata.keys():
                if globalmetadata['id'] != '':
                    nameofthebuoy = globalmetadata['id']
        if nameofthebuoy == '':
            if 'station' in globalmetadata.keys():
                nameofthebuoy = globalmetadata['station']
    #standardize the number of underscore in filenames
    nameofthebuoy = nameofthebuoy.replace('_','')
    return nameofthebuoy

def Define_output_filename(instr,lon,lat,intstart,intend,globalmetadata,spectral,outputdir):
    '''
    define final name of the netCDF file 
    Args:
        instr (str): sensor 
        lon (float):
        lat (float):
        intstart (datetime obj): 
        intend (datetime obj):
        globalmetadata (dic):
        spectral (bool):
        outputdir (str): path to store the file
        
    '''
    # ------------------
    logging.debug('create final time serie for this timeinterval %s-%s in this instrument %s',intstart,intend,instr)
    if lon < 0:
        strlon = "%.02fW" % abs(lon)
    else:
        strlon = "%.02fE" % lon
    if lat < 0:
        strlat = "%.02fS" % abs(lat)
    else:
        strlat = "%.02fN" % lat
    outdir = os.path.join(outputdir, instr, intstart.strftime('%Y/%m'))
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    if spectral:
        suffix = '_spectrum'
    else:
        suffix = ''
    nameofthebuoy = GetFinalNameOfTheBuoy(globalmetadata)
    ofname =  outdir+'/'+nameofthebuoy + '_%s_%s_Lat_%s_Lon_%s%s.nc' % \
                 (\
                  intstart.strftime('%Y%m%dT%H%M'),\
                  intend.strftime('%Y%m%dT%H%M'),\
                  strlat,
                  strlon,
                  suffix)
    logging.debug(ofname)
    return ofname