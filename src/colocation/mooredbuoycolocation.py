# encoding: utf-8
"""
globwave.colocation.mooredbuoycolocation
=========================================

Routines for colocating satellite data with moored buoys

:copyright: Copyright 2013 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""

import datetime
import os
import logging
import importlib

import cerbere.mapper

from cerbere.mapper.ncfile import NCFile

from cerbere.datamodel.trajectory import Trajectory
from cerbere.datamodel.pointtimeseries import PointTimeSeries

import colocation
import network


def get_buoy_times(fname, classreader=NCFile):
    """Read the measurement times of a moored buoy file"""
    ncf = classreader(url=fname)
    series = PointTimeSeries()
    series.load(ncf)
    res = series.get_datetimes()
    ncf.close()
    return res


def fill_buoy_times(matchupset, classreader=NCFile):
    '''
    return the merged lat, lon, times of all input files
    '''
    for matchup in matchupset:
        for matched_record in matchup:
            # get the list of  files associated to this buoy
            buoy_files = matched_record.get_storage()
            for fname in buoy_files:
                # add buoy times only if not yet added
                if not matched_record.get_time() \
                    or not fname in matched_record.get_time():
                    matched_record.set_time(
                            {fname: get_buoy_times(fname, classreader)}
                            )
    return


def process(source_file, source_name, match_name,
            network_root, output_root='.',
            time_limit=60., distance_limit=100.,
            source_classreader=NCFile, match_classreader=NCFile,
            source_datamodel=Trajectory):
    """
    time_limit:
        in minutes

    distance_limit:
        in km
    """
    # load satellite file
    logging.info("Processing %s", source_file)
    ncf = source_classreader(source_file)
    source = source_datamodel()
    source.load(ncf)
    source_lats = source.get_lat().ravel()
    source_lons = source.get_lon().ravel()
    source_times = source.get_datetimes().ravel()
    sourcetree = colocation.DataTree(source_lats, source_lons, source_times,
                                     source_name, source_file)

    # load buoys
    buoynet = network.Network(network_root)
    #print source_times
    buoytree = buoynet.get_buoy_tree(
                start=source_times.min()\
                        - datetime.timedelta(seconds=time_limit * 60.),\
                end=source_times.max()\
                         + datetime.timedelta(seconds=time_limit * 60.)
                )

    # colocation
    matchupset = colocation.MatchupSet(sourcetree, buoytree)
    matchupset.process_spatial_neighbours(radius=distance_limit / 100. * 1.5)

    # load buoy times for found matchups
    fill_buoy_times(matchupset, classreader=match_classreader)

    # reduce matchups to required criteria
    matchupset.temporal_reduction(time_limit * 60.)
    matchupset.spatial_reduction_to_closest(distance_limit * 1000.)
    #print matchupset.matchups

    # reduce matchups to keep only one matchup per buoy
    matchupset.reduction_to_single_source_matching()

    # save result
    output_dir = os.path.join(output_root,
                              source.get_datetimes().min().strftime('%Y/%j'))
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    outfname = os.path.basename(source_file).replace(
                        '.nc',
                        '_%s_%s_matchups.nc' % (source_name, match_name)
                        )
    outputfile = os.path.join(output_dir, outfname)
    if os.path.exists(outputfile):
        os.remove(outputfile)
    matchupset.save(outfile=outputfile,
                    src1=source_name,
                    src2=match_name,
                    vicinity=5,
                    index_only=False,
                    classreader1=source_classreader)
    return


if __name__ == "__main__":
    """
    python mooredbuoycolocation.py jason2 '/home3/mauka/tmp/cersat/project/globwave/data/l2p/altimeter/gdr/jason2/2010/324/*.nc' /home/cercache/project/globwave/data/globwave/nodc/wave/ . 60. 100.
    """
    import glob
    import gc
    from optparse import OptionParser

    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(levelname)-5s %(message)s',
                        datefmt='%d/%m/%Y %I:%M:%S')

    parser = OptionParser()

    # Set need argument      
    parser.add_option("-s","--source_name",
                      action="store", type="string",
                      dest="source_name", metavar="FILE",
                      help="name associated with the source 1 data (satellite,model,...). Example : 'cryosat2' [REQUIRED]")
    # Set need argument      
    parser.add_option("-n","--network_name",
                      action="store", type="string",
                      dest="network_name", metavar="FILE",
                      help="name associated with the source 2 data (matched buoy network). Example : 'ndbc' [REQUIRED]")
    parser.add_option("-i","--input",
                      action="store", type="string",
                      dest="source", metavar="FILE",
                      help="full path pattern of the source input files (satellite) to process [REQUIRED]")
    parser.add_option("-b","--network_root",
                      action="store", type="string",
                      dest="network_root", metavar="DIR",
                      help="full root path to the buoy network data repository [REQUIRED]")
    parser.add_option("-o","--output_root",
                      action="store", type="string",
                      dest="output_root", metavar="DIR",
                      help="full path to the output repository for matchups [REQUIRED]")
    parser.add_option("-t","--timedelta",
                      action="store", type="string",
                      dest="time_limit", metavar="int",
                      help="maximum time difference (in minutes) for matchups (temporal colocation criteria) [REQUIRED]")
    parser.add_option("-d","--distance",
                      action="store", type="string",
                      dest="distance_limit", metavar="int",
                      help="maximum distance (in km) for matchups (spatial colocation criteria) [REQUIRED]")
    parser.add_option("-r","--source_classreader",
                      action="store", type="string",
                      dest="source_classreader", metavar="string",
                      help="class to read the source 1 data files (default: mapper.ncfile.NCFile) [OPTIONAL]")
    parser.add_option("-w","--buoy_classreader",
                      action="store", type="string",
                      dest="match_classreader", metavar="string",
                      help="class to read the source 2 (buoy) data files (default: NCFile) [OPTIONAL]")
    parser.add_option("-m","--source_datamodel",
                      action="store", type="string",
                      dest="source_datamodel", metavar="string",
                      help="data model class corresponding to source data(default: Trajectory) [OPTIONAL]")
    (options, args) = parser.parse_args()

    source_name = options.source_name
    match_name = options.network_name
    source_files = glob.glob(options.source.strip())
    network_root = options.network_root
    output_root = options.output_root
    time_limit = float(options.time_limit)
    distance_limit = float(options.distance_limit)
    if not options.source_classreader:
        source_classreader = NCFile
    else:
        source_classreader = getattr(
            importlib.import_module(
                    '.'.join(options.source_classreader.split('.')[:-1])
                    ),
            options.source_classreader.split('.')[-1]
            )
    if not options.source_datamodel:
        source_datamodel = Trajectory
    else:
        source_datamodel = getattr(
            importlib.import_module(
                    '.'.join(options.source_datamodel.split('.')[:-1])
                    ),
            options.source_datamodel.split('.')[-1]
            )
    if not options.match_classreader:
        match_classreader = NCFile
    else:
        match_classreader = options.match_classreader
        match_classreader = getattr(
            importlib.import_module(
                    '.'.join(options.match_classreader.split('.')[:-1])
                    ),
            options.match_classreader.split('.')[-1]
            )

    for f in source_files:
        process(source_file=f,
                source_name=source_name,
                match_name=match_name,
                network_root=network_root,
                output_root=output_root,
                time_limit=60.,
                distance_limit=100,
                source_classreader=source_classreader,
                match_classreader=match_classreader,
                source_datamodel=source_datamodel
                )
        gc.collect()
