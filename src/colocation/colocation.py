# encoding: utf-8
"""
globwave.colocation.colocation
==============================

Classes for colocation

:copyright: Copyright 2013 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""

import datetime
import logging
import copy as cp
import os
from collections import OrderedDict

import netCDF4
from numpy import *
from scipy.spatial import KDTree


from cerbere.geo.point import Point
from cerbere.mapper.abstractmapper import WRITE_NEW
from cerbere.mapper.ncfile import NCFile
from cerbere.datamodel.field import Field
from cerbere.datamodel.variable import Variable


class DataTree():
    """Class implementing the KDTree representation of a feature"""
    def __init__(self, lats, lons, times, sources, storages):
        '''
        each record in a data tree is defined by lat,lon, times and storages
        times and storages elements can be either :
          1. single values (a time value for times or a filename for storages)
          2. a list of filenames for storages and a dictionary of time values
          for times (where the keys are the filenames)
        '''
        self.lats = lats
        self.lons = lons
        self.times = times
        self.sources = sources
        self.storages = storages
        self.kdtree = KDTree(zip(lats, lons))
        return

    def get_source(self, element):
        if type(self.sources) is list:
            return self.sources[element]
        else:
            return self.sources


class Record(object):
    """Implements a member of a matchup tuple

    A record refers to a measurement in a source data feature.
    """
    def __init__(self, tree_indice, data_tree=None,
                 storage_offset=None, storage=None,
                 distance=None, timedelta=None):
        self.tree_indice = tree_indice
        self.data_tree = data_tree
        self.time = None
        self.distance = distance
        self.timedelta = timedelta
        self.storage_offset = storage_offset
        self.storage = storage

    def get_storage(self):
        if self.storage:
            return self.storage
        if type(self.data_tree.storages) is list:
            return self.data_tree.storages[self.tree_indice]
        return  self.data_tree.storages

    def get_time(self):
        if self.time:
            return self.time
        return self.data_tree.times[self.tree_indice]

    def set_time(self, time):
        if isinstance(time, datetime.datetime):
            self.time = time
        if type(time) is dict:
            for k in time.keys():
                self.data_tree.times[self.tree_indice][k] = time[k]
        else:
            raise Exception("Invalid time")

    def get_source(self):
        return self.data_tree.get_source(self.tree_indice)


class Matchup():
    def __init__(self, src, matches):
        self.src = src
        self.matches = matches

    def get_record(self):
        return self.src

    def __iter__(self):
        for r in self.matches:
            yield r


class MatchupSet():
    """Class implementing a set of matchups"""
    def __init__(self, datatree1, datatree2):
        self.data_tree1 = datatree1
        self.data_tree2 = datatree2
        self.matchups = None
        self.distance_limit = None
        self.time_limit = None
        return

    def __iter__(self):
        for matchup in self.matchups:
            yield matchup

    def process_spatial_neighbours(self, radius=100000.):
        """
        Retrieve for each location of src1 the closest neighbors in src2, in the limit of the search radius. Src1 and src2 are KD-trees storing the spatial locations of each respective measurements.
        @param src1: KDTree representation of the source1 list of locations
        @param src2: KDTree representation of the source2 list of locations
        """
        self.distance_limit = radius
        logging.info("Start spatial cross colocation")
        crossed_tree = self.data_tree1.kdtree.query_ball_tree( self.data_tree2.kdtree, r=radius, eps=0.5)
        self.matchups = []
        for  n,closests in enumerate(crossed_tree):
            if len(closests) != 0:
                self.matchups.append( Matchup(Record(tree_indice=n,data_tree=self.data_tree1, storage_offset=n),\
                                              [Record(tree_indice=i, data_tree=self.data_tree2) for i in closests] ))
        return


    def reduction_to_single_source_matching( self ):
        """
        ensure that two sources have only one single matchup. When several matchups exist, keep the machtup with the smallest spatial distance.
        """
        # initialize the dictionary of the crossed sources
        crossed_sources = {}
        reduced_matchups = []
        for matchup in self:
            for matched_record in matchup:
                key = '%s_%s' % ( matchup.get_record().get_source(), matched_record.get_source() )
                if not crossed_sources.has_key( key ) or matched_record.distance < crossed_sources[key].matches.distance:
                    matchup.matches = matched_record
                    crossed_sources[key] = matchup
        self.matchups = crossed_sources.values()
        return
        

    def spatial_reduction_to_closest( self, max_dist ):
        """
        Keep the closest neighbour, discard matchups where the closest distance is larger than distMax
        """
        self.distance_limit = max_dist
        reduced_matchups = []
        distances = []
        for i,matchup in enumerate(self):
            if isinstance(matchup.matches[0],Record):
                indices = [rec.tree_indice for rec in matchup]
            else:
                indices = matchup.matches
            curdist = Point.get_distance_latlon( array(self.data_tree2.lats)[indices], array(self.data_tree2.lons)[indices],\
                                                           self.data_tree1.lats[matchup.get_record().tree_indice], self.data_tree1.lons[matchup.get_record().tree_indice] )
            ind =argmin(curdist)
            if ind >= 0 and curdist[ind] <= max_dist:
                match = matchup.matches[ind]
                match.distance = curdist[ind]
                reduced_matchups.append( Matchup(matchup.get_record(), [match]) )
        self.matchups = reduced_matchups
        self.distances = distances
        return   


    def __get_closest_in_time( cls, reftime, timeseries, time_limit):
        """
        return the indice and time difference of the elements within a time window centered on a reference time. If no elements
        are found within this time limit to the reference time, no element is returned (None).

        Return
        ------
        indice of the closest element found (-1 if no element found)
        """
        return argmin(abs(timeseries - reftime))
    __get_closest_in_time=classmethod(__get_closest_in_time)

    def __get_within_time_window( cls, reftime, timeseries, time_limit ):
        """
        return the indice and time difference of the elements within a time window centered on a reference time. If no elements
        are found within this time limit to the reference time, no element is returned (None).

        Return
        ------
        elements found in time window (None if no element found). Elements are defined as (indice i
        """
        return where( abs(timeseries - reftime) <= datetime.timedelta(seconds=time_limit) )
    __get_within_time_window=classmethod(__get_within_time_window)
    
    def temporal_reduction( self, time_limit=300 ):
        """
        Keep the closest neighbor in spatial distance (time proximity is not considered), discard matchups where the closest distance is larger than distMax
        Consider only the neighbors that are within time_limit seconds
        """
        # loop on all found matchups
        # --------------------------
        self.time_limit = time_limit
        reduced_matchups = []
        for matchup in self:
            # closests_r2 contains references to the matching records from source2. This references link to the source2 data tree.
            # They can be either :
            # 1. an indice to the lat,lon,times,storages list attributes of the data tree
            # 2. a data reference (indice in data tree, file, indice in file, time) (ex: moored buoys, with a time series attached to the same lat/lon/buoy identifier)
            new_closests_matches = []
            # STEP 1
            # ------
            # case where the data references for source 2 are not yet complete (ex: moored buoys, with a time series attached to the same lat/lon/buoy identifier), we reduce
            # in order to get only one measurement selected
            if type( matchup.matches[0].get_time() ) is dict:
                for rec in matchup.matches:    
                    time_diff = None
                    measurement_ref = None
                    for ref2 in  rec.get_storage():
                        #print matchup.get_record().get_time()
                        #print "REC : ", ref2, rec.__dict__
                        ind = MatchupSet.__get_closest_in_time( matchup.get_record().get_time(),  rec.get_time()[ref2], time_limit )
                        #print ind
                        if ind:
                            res_time_diff =  abs(rec.get_time()[ref2][ind] - matchup.get_record().get_time()).total_seconds()
                            if not time_diff or res_time_diff < time_diff:
                                time_diff = res_time_diff
                                measurement_ref = cp.copy(rec)
                                measurement_ref.storage_offset = ind
                                measurement_ref.storage = ref2
                                measurement_ref.timedelta = res_time_diff
                                measurement_ref.time = rec.get_time()[ref2][ind]
                                
                    if measurement_ref:
                        new_closests_matches.append( measurement_ref )
            else:
                indices = MatchupSet.__get_within_time_window( self.data_tree1.times[r1], self.data_tree2.times[ref2], time_limit )
                new_closests_matches = [(closests_r2[i], self.data_tree2.storages[closests_r2[i]], closests_r2[i], self.data_tree2.times[ref2][i]) for i in indices]
            if new_closests_matches != []:
                matchup.matches = new_closests_matches
                reduced_matchups.append( matchup )
        self.matchups = reduced_matchups
        return




    def save(self,
             outfile,
             src1='sat',
             src2='buoy',
             classreader1=NCFile,
             classreader2=NCFile,
             vicinity=5,
             index_only=True):
        """Save the matchups.

        output (string):
            name of matchups file
        src1 (string)
            identifier to use for source 1
        src2 (string)
            identifier to use for source 2
        classreader1 (:class:'mapper')
            class of the mapper to read the data from `src1`. Only if
            index_only is False.
        classreader2 (:class:'mapper')
            class of the mapper to read the data from `src2`. Only if
            index_only is False.
        vicinity : int
            halfsize of the box of neighbouring values to extract around the
            center pixel of the matchup. Only if index_only is False.
        index_only (bool)
            True if only the references to the source data are to be saved. If
            False, data are also copied from the sources into the matchup file.
        """
        if len(self.matchups) == 0:
            return
        nbrecords = len(self.matchups)
        # create output file
        ncf = NCFile(url=outfile, mode=WRITE_NEW)
        ncf.create_dim('matchup', nbrecords)
        ncf.create_dim('string128', 128)
        ncf.create_dim('string16', 16)
        if not index_only:
            ncf.create_dim('vicinity', vicinity * 2 + 1)
        # create index part
        # -----------------
        v = Variable(shortname='%s_filename' % src1,
                     description="name of file for %s source" % src1)
        f1 = Field(variable=v,
                   dimensions=OrderedDict([('matchup', nbrecords),
                                           ('string128', 128)]),
                   datatype=dtype('S1'),
                   )
        f1.set_values(ma.masked_all((nbrecords, 128,), dtype=f1.datatype))
        ncf.create_field(f1)

        v = Variable(shortname='%s_filename' % src2,
                     description="name of file for %s source" % src2)
        f2 = Field(variable=v, datatype=dtype('S1'),
                   dimensions=OrderedDict([('matchup', nbrecords),
                                           ('string128', 128)])
                   )
        f2.set_values(ma.masked_all((nbrecords, 128,), dtype=f2.datatype))
        ncf.create_field(f2)

        v = Variable(shortname='%s_offset' % src1,
                     description="offset of measurement in %s source file" % src1)
        o1 = Field(variable=v,
                   dimensions=OrderedDict([('matchup', nbrecords)]),
                   datatype=dtype('i4'),
                   )
        o1.set_values(ma.masked_all((nbrecords,),
                                    dtype=o1.datatype))
        ncf.create_field(o1)

        v = Variable(shortname='%s_offset' % src2,
                     description="offset of measurement in %s source file" % src2)
        o2 = Field(variable=v,
                   dimensions=OrderedDict([('matchup', nbrecords)]),
                   datatype=dtype('i4'),
                   )
        o2.set_values(ma.masked_all((nbrecords,), dtype=o2.datatype))
        ncf.create_field(o2)

        v = Variable(shortname='%s_identifier' % src2,
                     description="identifier of %s source" % src2)
        i2 = Field(variable=v,
                   datatype=dtype('S1'),
                   dimensions=OrderedDict([('matchup', nbrecords),
                                           ('string16', 16)])
                   )
        i2.set_values(ma.masked_all((nbrecords, 16), dtype=i2.datatype))
        ncf.create_field(i2)

        v = Variable(shortname='matchup_timedelta',
                     description="time difference between %s time and %s time"\
                                    % (src1, src2))
        mt = Field(variable=v,
                   dimensions=OrderedDict([('matchup', nbrecords)]),
                   datatype=dtype('f4'),
                   units='s')
        mt.set_values(ma.masked_all((nbrecords,), dtype=mt.datatype))
        ncf.create_field(mt)

        v = Variable(shortname='matchup_distance',
                     description="distance between %s and %s measurements"\
                                    % (src1, src2))
        md = Field(variable=v,
                   dimensions=OrderedDict([('matchup', nbrecords)]),
                   datatype=dtype('f4'),
                   units='m')
        md.set_values(ma.masked_all((nbrecords,), dtype=md.datatype))
        ncf.create_field(md
                         )
        ncf.write_global_attributes(
                        {'title': "%s / %s matchups" % (src1, src2),
                        'time_limit': self.time_limit,
                        'distance_limit': self.distance_limit}
                        )
        for i, m in enumerate(self.matchups):
            f1._values[i, :] = netCDF4.stringtoarr(
                    os.path.basename(m.get_record().get_storage()), 128)
            f2._values[i, :] = netCDF4.stringtoarr(
                    os.path.basename(m.matches.get_storage()), 128)
            o1._values[i] = m.get_record().storage_offset
            o2._values[i] = m.matches.storage_offset
            i2._values[i, :] = netCDF4.stringtoarr(m.matches.get_source(), 16)
            mt._values[i] = m.matches.timedelta
            md._values[i] = m.matches.distance
        ncf.write_field(f1)
        ncf.write_field(f2)
        ncf.write_field(o1)
        ncf.write_field(o2)
        ncf.write_field(i2)
        ncf.write_field(mt)
        ncf.write_field(md)
        # create data part
        # ----------------
        fieldlist1 = OrderedDict()
        fieldlist2 = OrderedDict()
        vic_fieldlist1 = OrderedDict()
        vic_fieldlist2 = OrderedDict()
        # read field definition for source 1
        for m in self:
            reader1 = classreader1(url=m.get_record().get_storage())
            for fn in ['lat', 'lon', 'time']:
                if not fn in fieldlist1:
                    fieldlist1[fn] = reader1.read_field(
                                reader1.get_geolocation_field(fn))
                    fieldlist1[fn].dimensions\
                            = OrderedDict([('matchup', nbrecords)])
                    fieldlist1[fn].variable.shortname = "%s_%s" % (src1, fn)
                    fieldlist1[fn].set_values(
                                ma.masked_all((nbrecords,),
                                dtype=fieldlist1[fn].datatype)
                                )
                if not fn in vic_fieldlist1:
                    vic_fieldlist1[fn] = reader1.read_field(
                                reader1.get_geolocation_field(fn))
                    vic_fieldlist1[fn].dimensions\
                            = OrderedDict([('matchup', nbrecords),
                                           ('vicinity', vicinity * 2 + 1)])
                    vic_fieldlist1[fn].variable.shortname\
                                     = "%s_%s_vicinity" % (src1, fn)
                    vic_fieldlist1[fn].set_values(
                                ma.masked_all((nbrecords, vicinity * 2 + 1,),
                                              dtype=fieldlist1[fn].datatype)
                                )
            # vicinity for source 1
            for fieldname in reader1.get_fieldnames():
                outfieldname = "%s_%s" % (src1, fieldname)
                if not fieldname in fieldlist1:
                    fieldlist1[fieldname] = reader1.read_field(fieldname)
                    fieldlist1[fieldname].dimensions\
                            = OrderedDict([('matchup', nbrecords)])
                    fieldlist1[fieldname].variable.shortname = outfieldname
                    fieldlist1[fieldname].set_values(
                            ma.masked_all((nbrecords,),
                            dtype=fieldlist1[fieldname].datatype)
                            )
                    vic_field = reader1.read_field(fieldname)
                    vic_field.variable.shortname = outfieldname + '_vicinity'
                    vic_field.dimensions\
                            = OrderedDict([('matchup', nbrecords),
                                           ('vicinity', vicinity * 2 + 1)])
                    vic_fieldlist1[fieldname] = vic_field
                    vic_fieldlist1[fieldname].set_values(
                            ma.masked_all((nbrecords, vicinity * 2 + 1,),
                                           dtype=vic_field.datatype)
                            )
            reader1.close()
        # read field definition for source 2
        for m in self:
            reader2 = classreader2(url=m.matches.get_storage())
            for fn in ['lat', 'lon', 'time']:
                if not fn in fieldlist2:
                    fieldlist2[fn] = reader2.read_field(
                                            reader2.get_geolocation_field(fn))
                    fieldlist2[fn].dimensions\
                            = OrderedDict([('matchup', nbrecords)])
                    fieldlist2[fn].variable.shortname = "%s_%s" % (src2, fn)
                    fieldlist2[fn].set_values(
                            ma.masked_all((nbrecords,),
                                          dtype=fieldlist2[fn].datatype)
                            )
            for fieldname in reader2.get_fieldnames():
                outfieldname = "%s_%s" % (src2, fieldname)
                if not fieldname in fieldlist2:
                    fieldlist2[fieldname] = reader2.read_field(fieldname)
                    fieldlist2[fieldname].variable.shortname = outfieldname
                    fieldlist2[fieldname].dimensions\
                            = OrderedDict([('matchup', nbrecords)])
                    fieldlist2[fieldname].set_values(
                            ma.masked_all((nbrecords,),
                                          dtype=fieldlist2[fieldname].datatype)
                            )
            reader2.close()
        # get_matching_dimname
        for fieldname, field in fieldlist1.items():
            print "CREATE ", fieldname
            ncf.create_field(field)
        for fieldname, field in fieldlist2.items():
            #print field.variable.shortname
            ncf.create_field(field)
        for fieldname, field in vic_fieldlist1.items():
            ncf.create_field(field)
        # fill the values
        for i, m in enumerate(self):
            reader1 = classreader1(url=m.get_record().get_storage())
            for fieldname in fieldlist1.keys():
                field = fieldlist1[fieldname]
                if reader1.get_dimensions(fieldname) == ():
                    field._values[i] = reader1.read_values(fieldname)
                    vic_fieldlist1[fieldname]._values[i, :]\
                            = reader1.read_values(fieldname)
                else:
                    #print fieldname, m.get_record().storage_offset
                    i0 = max(0, m.get_record().storage_offset - vicinity)
                    i1 = min(reader1.get_dimsize('time') - 1,
                             m.get_record().storage_offset + vicinity) + 1
                    vic_i0 = vicinity - (m.get_record().storage_offset - i0)
                    vic_i1 = vicinity + i1 - m.get_record().storage_offset
                    #print i0,i1,vic_i0,vic_i1
                    storage_offset = m.get_record().storage_offset
                    field._values[i] = reader1.read_values(
                            fieldname,
                            slices=[slice(storage_offset,
                                          storage_offset + 1,
                                          1)]
                            )
                    vic_fieldlist1[fieldname]._values[i, vic_i0:vic_i1]\
                        = reader1.read_values(fieldname,
                                              slices=[slice(i0, i1, 1)])
            reader1.close()
        for i, m in enumerate(self):
            reader2 = classreader2(url=m.matches.get_storage())
            for fieldname in fieldlist2.keys():
                field = fieldlist2[fieldname]
                if fieldname in reader2.get_fieldnames()\
                        or fieldname in ['lat', 'lon', 'time']:
                    if reader2.get_dimensions(fieldname) != ('time',):
                        field._values[i] = reader2.read_values(fieldname)
                    else:
                        field._values[i] = reader2.read_values(
                            fieldname,
                            slices=[slice(m.matches.storage_offset,
                                          m.matches.storage_offset + 1,
                                          1)]
                            )
            reader2.close()
        for fieldname, field in fieldlist1.items():
            ncf.write_field(field)
        for fieldname, field in vic_fieldlist1.items():
            ncf.write_field(field)
        for fieldname, field in fieldlist2.items():
            ncf.write_field(field)
        ncf.close()
        return

if __name__ == "__main__":

    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)-5s %(message)s', datefmt='%d/%m/%Y %I:%M:%S')
