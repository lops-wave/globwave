import glob
import os
import datetime
import numpy
import collections

#from pykdtree.kdtree import KDTree 
from scipy.spatial import KDTree

import colocation



class Network():

	def __init__(self, inputdir):
		self._root = inputdir
		pass

	def get_buoy_info( self, bfile=None ):
		direction = {'W':-1.,'E':1,'N':1.,'S':-1.}
		id, sstart, send, latstr, lat, lonstr, lon = os.path.basename(bfile).strip('.nc').split('_')
		lat = direction[lat[-1]] * float(lat[:-1])
		lon = direction[lon[-1]] * float(lon[:-1])
		return id, lat, lon
	
	def get_buoy_tree(self, start, end, buoy_filter='WMO*[A-Z]'):
		'''
		'''
		curDate = start
		curDate = curDate.replace(day=1,hour=0,minute=0,second=0)
		buoy_ref = collections.OrderedDict()
		lats = []
		lons = []
		times = []
		sources = []
		references = []
		direction = {'W':-1.,'E':1,'N':1.,'S':-1.}
		while curDate < end:
			buoyfiles = glob.glob( os.path.join(self._root,curDate.strftime('%Y/%m/')+'%s.nc' % buoy_filter) )
			for f in buoyfiles:
				id, lat, lon = self.get_buoy_info( f )
				bkey = '%s_%s_%s' % (id, lat, lon)		  
				if not buoy_ref.has_key(bkey):
					buoy_ref[bkey] = [f]
					lats.append(lat)
					lons.append(lon)
					times.append( {} )
					sources.append( id )
				else:
					buoy_ref[bkey].append( f )
			if curDate.month == 12:
				curDate = curDate.replace(year=curDate.year+1,month=1)
			else:
				curDate = curDate.replace(month=curDate.month+1)
		for bkey in buoy_ref.keys():
			references.append( buoy_ref[bkey] )
		try:
			datatree = colocation.DataTree( lats, lons, times, sources, references )
		except:
			print start, end, os.path.join(self._root,curDate.strftime('%Y/%m/')+'%s.nc' % buoy_filter)
			print lats, lons, times, sources, references
			raise
		return datatree

